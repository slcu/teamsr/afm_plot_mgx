from __future__ import absolute_import, print_function, unicode_literals, division
from pyqt_fit import CurveFitting
from numpy import mean, median, ones, zeros, newaxis, array
from pyqt_fit import bootstrap
from itertools import chain, cycle
try:
    from itertools import izip
except ImportError:
    izip = zip


def cfm_fct((x1, y1, fdev1, fdev2, sdev2), x):
    result = y1 + fdev1*x
    sel = x > x1
    result[sel] += fdev2*(x[sel]-x1) + sdev2*(x[sel]-x1)**2
    return result

cfm_fct.description = "Quadratic fit of a force indentation curve"


def cfm_dfct((x1, y1, fdev1, fdev2, sdev2), x):
    sel = x > x1
    result = zeros((5, x.shape[0]), dtype=x.dtype)
    result[0, sel] = 2 * sdev2 * x1 - 2 * sdev2 * x[sel] - fdev2  # d/dx1
    result[1] = 1  # d/dy1
    result[2] = x  # d/dfdev1
    result[3, sel] = x[sel] - x1  # d/dfdev2
    result[4, sel] = (x[sel] - x1) ** 2  # d/dfdev3
    return result


sel_axis = array([0, 3, 4], dtype=int)[:, newaxis]


def cfm_dfct2((x1, y1, fdev1, fdev2, sdev2), x):
    sel = x <= x1
    result = zeros((5,) + x.shape, dtype=x.dtype)
    result[0] = 2 * sdev2 * x1 - 2 * sdev2 * x - fdev2  # d/dx1
    result[1] = 1  # d/dy1
    result[2] = x  # d/dfdev1
    result[3] = x - x1  # d/dfdev2
    result[4] = (x - x1) ** 2  # d/dfdev3
    result[sel_axis, sel] = 0
    return result


def cfm_fct_linear((x1, y1, fdev1, fdev2), x):
    result = y1 + fdev1*x
    sel = x > x1
    result[sel] += fdev2 * (x[sel] - x1)
    return result


def izip_first(fst, snd):
    """
    Zip fst and snd up to the end of fst, adding None is snd is too small.
    Example:
        >>> list(izip_first((1,2,3),(1,2)))
        [(1, 1), (2, 2), (3, None)]
        >>> list(izip_first((1,2,3),(1,2,3,4)))
        [(1, 1), (2, 2), (3, 3)]
        >>> list(izip_first((1,2),()))
        [(1, None), (2, None)]
        >>> list(izip_first((),(1,2,3)))
        []

    """
    return izip(fst, chain(snd, cycle((None,))))


def choose(default_values, set_values):
    """
    Returns a tuple of same length as default_values, setting eigher what is in
    set_values or default_values is set_values is None or too short.
    Example:
        >>> choose((1,2,3,4), (None,12,13))
        (1, 12, 13, 4)
        >>> choose((1,2,3), (None, 12, 13, 14, 15))
        (1, 12, 13)
        >>> choose((1,), ())
        (1,)
    """
    return tuple(x1 if x2 is None else x2 for x1, x2 in izip_first(default_values, set_values))


def cfm_fit(z, force, sensor_stiffness, p0=None, **kwrds):
    """
    z and force must be ndarrays of same size

    The 4th field (infodict) also has the field 'corrected_z', which is the
    'z', corrected for the stiffness of the sensor

    contact_point_stiffness is the threshold used for the initial guess for the contact point
    """
    raise_error = kwrds.pop('raise_error', False)
    try:
        if p0 is None:
            p0 = (None,)*5
        elif len(p0) < 5:
            p0 = tuple(p0) + (None,)*(5-len(p0))
        elif len(p0) > 5:
            p0 = tuple(p0)[:5]
        z = z - force/sensor_stiffness
        m = len(z)//2
        zm = z[m]
        zM = z[-1]
        fm = force[m]
        fM = force[-1]
        slope = (fM-fm)/(zM-zm)
        p0 = choose((zm, (zm+z[0])/2, 0., slope, 0), p0)
        fit = CurveFitting(function=cfm_fct, xdata=z, ydata=force, Dfun=cfm_dfct2, col_deriv=1,
                p0=p0, **kwrds)
        fit.fit()
        fit.infodict['corrected_z'] = z
        return fit
    except Exception, ex:
        if raise_error:
            raise
        return ('Error', ex)


def cfm_fit_bootstrap(z, force, sensor_stiffness, p0=None, maxfev=None, CI=(95,), **kwrds):
    """
    z and force must be ndarrays of same size

    The 4th field (infodict) also has the field 'corrected_z', which is the
    'z', corrected for the stiffness of the sensor
    """
    raise_error = kwrds.pop('raise_error', False)
    try:
        if p0 is None:
            p0 = (None,)*5
        z = z - force/sensor_stiffness
        m = len(z)//2
        zm = z[m]
        zM = z[-1]
        fm = force[m]
        fM = force[-1]
        slope = (fM-fm)/(zM-zm)
        p0 = choose((zm, (zm+z[0])/2, 0., slope, 0), p0)
        kwrds.setdefault('fit', cfm_fit)
        kwrds.setdefault('shuffle_method', bootstrap.bootstrap_residuals)
        fit_args = kwrds.setdefault('fit_args', {})
        fit_args.setdefault('Dfun', cfm_dfct2)
        fit_args.setdefault('col_deriv', 1)
        if maxfev is not None:
            fit_args.setdefault('maxfev', maxfev)
        res = bootstrap.bootstrap(fct=cfm_fct, xdata=z, ydata=force, CI=CI, p0=p0, **kwrds)
        res[-1]['corrected_z'] = z
        return res
    except Exception, ex:
        if raise_error:
            raise
        return ('Error', ex)

#def cfm_fit2(z, force, sensor_stiffness, linear_fit, **kwrds):
    #"""
    #z and force must be ndarrays of same size

    #The 4th field (infodict) also has the field 'corrected_z', which is the
    #'z', corrected for the stiffness of the sensor
    #"""
    #p0 = list(linear_fit)+[0.0]
    #return cfm_fit(z, force, sensor_stiffness, p0=p0, **kwrds)


def cfm_fit_linear(z, force, sensor_stiffness, p0=None, **kwrds):
    """
    z and force must be ndarrays of same size

    The 4th field (infodict) also has the field 'corrected_z', which is the
    'z', corrected for the stiffness of the sensor
    """
    raise_error = kwrds.pop('raise_error', False)
    try:
        if p0 is None:
            p0 = (None,)*4
        z = z - force/sensor_stiffness
        m = len(z)//2
        zm = z[m]
        zM = z[-1]
        fm = force[m]
        fM = force[-1]
        slope = (fM-fm)/(zM-zm)
        p0 = choose((zm, (zm+z[0])/2, 0., slope), p0)
        fit = CurveFitting(function=cfm_fct_linear, xdata=z, ydata=force,
                           p0=p0, **kwrds)
        fit.infodict['corrected_z'] = z
        return fit
    except Exception, ex:
        if raise_error:
            raise
        return ('Error', ex)
