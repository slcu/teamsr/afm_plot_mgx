import pandas as pd
import numpy as np
import cfm_export
import glob
import os
import os.path
import cfm
import pyqt_fit
import cfm_export
from PyQt4 import QtGui
#app = QtGui.QApplication([])

def plot_afm_data(input, output, center=False):
	data = pd.read_csv(input)
	z = data['Contact Point [m]']
	x = data['X Position'][~z.isnull()]
	y = data['Y Position'][~z.isnull()]
	values = data["Young's Modulus [Pa]"][~z.isnull()]
	z = z[~z.isnull()]
	if center:
		x -= (x.min() + x.max()) / 2
		y -= (y.min() + y.max()) / 2
		z -= (z.min() + z.max()) / 2
	trs = cfm_export.make_triangles(np.c_[x,y])
	cfm_export.OBJWriteTriangles(output, trs, np.c_[x,y,z]*1e6, values, "Young's Modulus (Pa)", value_range=[values.min(), values.max()])

def fct(include = None, exclude = None):
	input = str(QtGui.QFileDialog.getOpenFileName())
	if not input:
		return
	if input.endswith('.csv'):
		output = input[:-4] + '.obj'
	else:
		output = input + '.obj'
	plot_afm_data(input, output, True)
    #os.chdir(fn)

# to run it 
# ipython qtconsole --colors=linux --pylab=qt --pydb --editor=gvim
# import os
# os.chdir(the place)
# import afm
# afm.fct()
