// Parameter reader class
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <cctype>
#include <algorithm>
#include <iterator>
#include "parms.hpp"
#include <qdir.h>

#ifdef __unix__
#  include <fcntl.h>
#  include <sys/file.h>
#  include <sys/stat.h>
#endif

QTextStream Parms::err(stderr);

using std::ostream_iterator;
using std::copy;

Parms::Parms(int vl )
  : ParmFileName()
{
  verboseLevel( vl );
  init();
}

Parms::Parms(QString parmFile, int vl )
  : ParmFileName( parmFile )
{
  verboseLevel( vl );
  init();
}

void Parms::init()
{
  loaded=false;
  if(ParmFileName.isEmpty())
  {
    loaded = true;
    return;
  }
#ifdef __unix__
  // First, obtain the lock
  QByteArray ba = ParmFileName.toLocal8Bit();
  int fd = open(ba.data(), O_RDONLY);
  flock(fd, LOCK_SH);
#endif
  QFile fIn(ParmFileName);
  if(!fIn.open(QIODevice::ReadOnly))
  {
    if( VerboseLevel > 0 )
      err << "Parms::Parms:Error opening " << ParmFileName << endl;
    return;
  }
  QTextStream ss(&fIn);
  unsigned int line = 0;
  int pos;
  QString buff;
  while(!ss.atEnd() and ss.status() == QTextStream::Ok) {
    line++;
    // read in line
    buff = ss.readLine();
    // find C++ style comments
    pos = buff.indexOf(";");
    // and remove to end of line
    if(pos != -1)
      buff = buff.mid(0, pos);
    // remove leading and trailing whitespace
    buff = buff.trimmed();
    // skip line if blank
    if(buff.length() == 0)
      continue;
    // Look for section
    if(buff[0] == '[' && buff.right(1)[0] == ']') {
      Section = buff.mid(1, buff.length() - 2);
      if(( Section.length() == 0) && ( VerboseLevel > 0 ) )
        err << "Parms::Parms:Error on line " << line << ", []" << endl;
      continue;
    }
    // split key and value
    pos = buff.indexOf("=");
    // error if no : delimiter
    if(pos == -1) {
      if( VerboseLevel > 0 )
        err << "Parms::Parms:Error on line " << line << ", missing :" << endl;
      continue;
    }
    // get key and value and remove leading/trailing blanks
    QString key = buff.mid(0, pos).trimmed();
    QString value = buff.mid(pos + 1).trimmed();
    // error if no key
    if(key.length() == 0) {
      if( VerboseLevel > 0 )
        err << "Parms::Parms:Error on line " << line << ", missing key" << endl;
      continue;
    }
    // error if no value
    if(value.length() == 0) {
      if( VerboseLevel > 2 )
        err << "Parms::Parms:Warning on line "<< line << ", missing value" << endl;
    }

    // Now we have key and value, add to map
    Parameters[Section][key] << value;
  }
#ifdef __unix__
  // At last, release the lock
  flock(fd, LOCK_UN);
#endif
  loaded = true;
}

bool Parms::write(QString parmFile) const
{
  QFile file(parmFile);
  if(!file.open(QIODevice::WriteOnly))
  {
    if(VerboseLevel > 0)
      err << "Parms::write:Error, cannot open file '" << parmFile << "' for writing" << endl;
    return false;
  }
  QTextStream ts(&file);
  QStringList section_list = Parameters.keys();
  section_list.sort();
  for(QString section: section_list)
  {
    ts << QString("[%1]").arg(section) << endl;
    const section_t& section_content = Parameters.value(section);
    QStringList key_list = section_content.keys();
    key_list.sort();
    for(QString key: key_list)
    {
      const QStringList& values = section_content.value(key);
      for(QString val: values)
      {
        ts << QString("%1= %2").arg(key).arg(val) << endl;
      }
    }
    ts << endl;
  }
  file.close();
  return true;
}

Parms::~Parms()
{
}

/*
   void removeWhitespace(string &s)
   {
   size_t pos = s.find_first_not_of(" \t\r\n");
   if(pos == string::npos)
   s = "";
   else {
   s = s.substr(pos, string::npos);
   pos = s.find_last_not_of(" \t\r\n");
   if(pos != string::npos)
   s = s.substr(0, pos + 1);
   }
   }
   */

bool Parms::operator()( QString section, QString key, bool& value ) const
{
  return operator()<bool>( section, key, value );
}

bool Parms::operator()( QString section, QString key, int& value ) const
{
  return operator()<int>( section, key, value );
}

bool Parms::operator()( QString section, QString key, float& value ) const
{
  return operator()<float>( section, key, value );
}

bool Parms::operator()( QString section, QString key, double& value ) const
{
  return operator()<double>( section, key, value );
}

bool Parms::operator()( QString section, QString key, std::string& value ) const
{
  return operator()<std::string>( section, key, value );
}

bool Parms::operator()( QString section, QString key, QString & value ) const
{
  return operator()<QString>( section, key, value );
}

bool Parms::set(QString section, QString key, const bool& value)
{
  return set<bool>(section, key, value);
}
bool Parms::set(QString section, QString key, const int& value)
{
  return set<int>(section, key, value);
}
bool Parms::set(QString section, QString key, const float& value)
{
  return set<float>(section, key, value);
}
bool Parms::set(QString section, QString key, const double& value)
{
  return set<double>(section, key, value);
}
bool Parms::set(QString section, QString key, const std::string& value)
{
  return set<std::string>(section, key, value);
}
bool Parms::set(QString section, QString key, QString value)
{
  return set<QString>(section, key, value);
}

bool Parms::setAll( QString section, QString key, const std::vector<bool>& value )
{
  return setAll<std::vector<bool> >( section, key, value );
}
bool Parms::setAll( QString section, QString key, const std::vector<int>& value )
{
  return setAll<std::vector<int> >( section, key, value );
}
bool Parms::setAll( QString section, QString key, const std::vector<float>& value )
{
  return setAll<std::vector<float> >( section, key, value );
}
bool Parms::setAll( QString section, QString key, const std::vector<double>& value )
{
  return setAll<std::vector<double> >( section, key, value );
}
bool Parms::setAll( QString section, QString key, const std::vector<QString>& value )
{
  return setAll<std::vector<QString> >( section, key, value );
}
bool Parms::setAll( QString section, QString key, const QStringList& value )
{
  return setAll<QStringList>( section, key, value );
}

bool Parms::all( QString section, QString key, std::vector<bool>& value ) const
{
  return all<std::vector<bool> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<int>& value ) const
{
  return all<std::vector<int> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<float>& value ) const
{
  return all<std::vector<float> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<double>& value ) const
{
  return all<std::vector<double> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<QString>& value ) const
{
  return all<std::vector<QString> >( section, key, value );
}
bool Parms::all( QString section, QString key, QStringList& value ) const
{
  return all<QStringList>( section, key, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<bool> >& value ) const
{
  return all<bool>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<int> >& value ) const
{
  return all<int>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<float> >& value ) const
{
  return all<float>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<double> >& value ) const
{
  return all<double>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<QString> >& value ) const
{
  return all<QString>( section, value );
}

bool Parms::all( QString section, QHash<QString, QStringList>& value ) const
{
  return all<QString>( section, value );
}

bool Parms::readValue( QString raw_value, bool& variable ) const
{
  QString value = raw_value.toLower();
  if( value == "true" )
  {
    variable = true;
    return true;
  }
  else if( value == "false" )
  {
    variable = false;
    return true;
  }
  return false;
}

bool Parms::readValue( QString value, QString& variable ) const
{
  variable = value;
  return true;
}

bool Parms::readValue( QString value, std::string& variable ) const
{
  variable = value.toStdString();
  return true;
}

bool Parms::writeValue(QString& raw, const bool& variable)
{
  raw = (variable ? "true" : "false");
  return true;
}

bool Parms::writeValue( QString& value, QString variable )
{
  value = variable;
  return true;
}

bool Parms::writeValue( QString& value, const std::string& variable )
{
  value = QString::fromStdString(variable);
  return true;
}

bool Parms::extractValues( QString section, QString key, QStringList& values, bool checkExist ) const
{
  parameters_t::const_iterator found_section = Parameters.find(section);
  if(found_section != Parameters.end())
  {
    section_t::const_iterator found = found_section->find(key);
    if(found != found_section->end())
    {
      values = *found;

      if( VerboseLevel > 3 )
      {
        err << "Parms::extractValues:Debug strings for key [" << section << "]" << key << ": -"
          << values.join("-") << endl;
      }
      return true;
    }
  }

  if( checkExist && ( VerboseLevel > 0 ) )
    err << "Parms::operator():Error key not found [" << section << "]"
      << key << endl;
  return false;
}

bool Parms::setValues( QString section, QString key, const QStringList& values )
{
  Parameters[section][key] = values;
  return true;
}

bool Parms::addValue( QString section, QString key, QString value )
{
  Parameters[section][key] << value;
  return true;
}

QTextStream& operator>>(QTextStream& ss, bool b)
{
  QString val;
  ss >> val;
  val.toLower();
  b = (val == "true");
  return ss;
}

QTextStream& operator<<(QTextStream& ss, bool b)
{
  ss << (b ? "true" : "false");
  return ss;
}

