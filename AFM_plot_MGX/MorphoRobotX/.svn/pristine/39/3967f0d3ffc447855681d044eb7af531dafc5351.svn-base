/* 
 * File:   CFMController.h
 * Author: huflejt
 *
 * Created on March 15, 2012, 2:51 PM
 */

#ifndef CFMCONTROLLER_H
#define	CFMCONTROLLER_H

#ifdef	__cplusplus
extern "C" {
#endif
    #include <stdio.h>
    #include "CFMMachine.h"
    #include "GridScan.h"
    #include "CandidateList.h"
    #include "iniparser.h"
    #include "DataLog.h"   
    #include <time.h>
    #include <math.h>

   // #include "CFMServer.h"

    typedef struct
    {
        _CFMMachine     CFMMachine;
        _Experiment     currentExperiment;
       // _CFMServer*     itsServer;
    } _CFMController;

    int CFMController_Constructor(_CFMController* this);
    int CFMController_Initialize(_CFMController* this);
    int CFMController_Finalize(_CFMController* this);
    int CFMController_PerformPointScan(_CFMController* this, _PointScan* pointScan, struct timespec start);
    int CFMController_PerformGridScan(_CFMController* this, _GridScan* gridScan);
    int CFMController_SaveReport(_CFMController* this);
    int CFMController_PrepareExperimentFromINI(_CFMController* this);
    int CFMController_PerformExperiment(_CFMController* this);
    int CFMController_PerformSensorProfiling(_CFMController* this);
    int CFMController_PerformSensorProfiling2(_CFMController* this);
    int CFMController_PerformSensorProfiling3(_CFMController* this);
    int CFMController_PerformSurfaceCrawling(_CFMController* this, _PointScan* pointScan, struct timespec start);
    int CFMController_PerformSurfaceCrawling2(_CFMController* this);
    int CFMController_PerformSurfaceCrawling3(_CFMController* this);
    int CFMController_PerformAlainsPoking(_CFMController* this);
    int CFMController_PerformWaterProfiling(_CFMController* this);
     
    int CFMController_PerformWaterSongRecording(_CFMController* this);
    
    int CFMController_PerformEmbryoScanning(_CFMController* this);
    int CFMController_PerformEmbryoScanningWithPulling(_CFMController* this);
    int CFMController_PerformGridScanningPrecise(_CFMController* this);

    int CFMController_PerformEmbryoPointScan(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID);
    int CFMController_PerformEmbryoPointScanFast(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID);
    int CFMController_PerformEmbryoPointScanFast2(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel);
    int CFMController_PerformEmbryoPointScanFast3(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel);
    int CFMController_PerformEmbryoPointScanFast4(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel);
    int CFMController_PerformEmbryoPointScanFast5(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel);
    int CFMController_PerformEmbryoPointScanFast5Precise(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel,  int withinPreciseZone);
    int CFMController_PerformEmbryoPointScanFast5PreciseScanMode(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel,  int withinPreciseZone);
    int CFMController_PerformEmbryoPointScanFast5PreciseScanModeOneCrystalDepth(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int *surfaceLevel, int withinPreciseZone);
    
    int CFMController_PerformEmbryoPointScanFast6(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int* surfaceLevel);
    int CFMController_BY2Scan(_CFMController* this);
    int CFMController_BY2Scan2(_CFMController* this);
    int CFMController_BY2Scan3(_CFMController* this);
    int CFMController_BY2Scan4(_CFMController* this);
    int CFMController_ScanStep(_CFMController* this, int axis, int stepSize);
    int CFMController_PerformScanStepTests(_CFMController* this);
    int CFMController_SmoothGoto(_CFMController *this, int axis, int targetPosition, int maxSpeed);
    int CFMController_CrystalProfiling(_CFMController *this);
    
    int CFMController_FastGoto(_CFMController* this, int axis, int targetPosition);
    int CFMController_PerformComediBufferTest1(_CFMController* this);
   

    ///// functions by ALR:
    int CFMController_PerformGlassCrawling(_CFMController* this); // modified by ALR on 2012 07, for detection of furrows on cell counting slide   
    int CFMController_PerformStiffnessProfiling(_CFMController* this);// added by ALR on 2013 01 03. New profiling function.Test speed of steps with/without updating pos0/1 and crystal state.
    // Regular grid with "crawling mode", perform wiggles only on flat parts:
    int CFMController_PerformGridScanningRegular_DetectFlatAxis0(_CFMController* this);// added by ALR on 2012 11 21
    int CFMController_PerformGridScanningRegular_DetectFlat2Axis(_CFMController* this);// added by ALR on 2012 12 12
    int CFMController_PerformGridScanningRegular_DetectFlat2Axis_4Neighbors(_CFMController* this);// added by ALR on 2012 12 14
    int CFMController_PerformGridScanningRegular_DetectFlat2Axis_8Neighbors(_CFMController* this);// added by ALR on 2012 12 18

    /////// modules by ALR:
    //  high level modules:  
    int CFMController_PerformEmbryoPointScanFast5PreciseScanModeOneCrystalDepth_ALR(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int *surfaceLevel, int previousSurfaceLevel, int heightDiffOnFlat); 
    int CFMController_DoWigglesScanningMode(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int stepPrecision); 
    int CFMController_FindSurfaceWithClosedLoop_WithCandidates(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int *surfaceLevel, int stepPrecision, int maxRepeatReadPosition); 
    int CFMController_FindSurfaceWithClosedLoop_NoCandidates(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int *surfaceLevel, int stepPrecision); 
    int CFMController_MoveUpBeforeMoveSideway(_CFMController* this, int previousSurfaceLevel, int surfaceLevel, int measurementID, int holdMs, int position, int moveUpBeforeSideway, int stepPrecision, int maxRepeatReadPosition, int randomStep, int gridStepSize); 
    // high level, written with modules:
    int CFMController_FindSurfaceWithClosedLoop_NoCandidates_Modules(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int *surfaceLevel, int stepPrecision, int maxRepeatReadPosition);
    int CFMController_DoWigglesScanningMode_Modules(_CFMController* this, _DataLog* datalog, struct timespec start, int measurementID, int stepPrecision, int maxRepeatReadPosition, int scanSpeed, int scanStep); 
    //  low level modules:
    int CFMController_Module_ReadPositionAndForce(_CFMController* this, int *pcrystalState2, int *ppos0, int *ppos1, int *pposition, double *pvoltageIN, double *pstiffness, int updatePos0Pos1, int updateCrystalState);
    int CFMController_Module_ReportPointInDatalog(_CFMController* this,  _DataLog* datalog, struct timespec start, int measurementID, int crystalState2, int pos0, int pos1, int position, double voltageIN, double stiffness, int flag, int repetition);
    int CFMController_Module_GoToTargetPosition(_CFMController* this, int *pPos, int alongAxis, int target, int holdMs, int stepPrecision, int maxRepeatReadPosition);
    int CFMController_Module_MakeStepClosedLoop(_CFMController* this, int *pPos, int alongAxis, int stepDirection, int stepSize, int holdMs, int stepPrecision, int maxRepeatReadPosition);
 


    

#ifdef	__cplusplus
}
#endif

#endif	/* CFMCONTROLLER_H */

