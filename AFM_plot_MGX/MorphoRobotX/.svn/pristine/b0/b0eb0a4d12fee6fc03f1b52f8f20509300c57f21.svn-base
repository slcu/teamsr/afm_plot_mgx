/* 
 * File:   ADCard.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 6:38 PM
 */

#ifndef ADCARD_HPP
#define	ADCARD_HPP

#include <comedilib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>

typedef struct
{
    char *filename;
    double value;
    int subdevice;
    int channel;
    int aref;
    int range;
    int physical;
    int verbose;
    int n_chan;
    int n_scan;
    double freq;
} parsed_options; //required by comedi library

typedef enum
{
    FORCE_ACQUISITION_MODE_NONE,
    FORCE_ACQUISITION_MODE_BURST,
    FORCE_ACQUISITION_MODE_CONTINUOUS        
} FORCE_ACQUISITION_MODE; // ugly name !

static char * const default_filename = "/dev/comedi0";

#define     N_CHANS 16
#define     BUFSZ 100000

class ADCard {
    
public:
    
    ADCard();
    ADCard(const ADCard& orig);
    virtual ~ADCard();
    bool Init(FORCE_ACQUISITION_MODE mode, int integrationTimeMs);
    bool GetVoltage(double& voltage); //TODO: add a way to get more channels at once, might become useful at some point, this would require to change the Init() as well
    
protected:
    
    comedi_t *dev;
    comedi_cmd  command;
    comedi_cmd *cmd;
    parsed_options options;
    comedi_polynomial_t polynomial_converter;
    unsigned int chanlist[N_CHANS];
    comedi_range * range_info[N_CHANS];
    lsampl_t maxdata[N_CHANS];
    FORCE_ACQUISITION_MODE forceAcquisitionMode;
    char buf[BUFSZ];
    
    bool PrepareCmdLib(comedi_t *dev, int subdevice, int n_scan, int n_chan, unsigned scan_period_nanosec, comedi_cmd *cmd, FORCE_ACQUISITION_MODE mode);
    void init_parsed_options(parsed_options *options);
    char* cmd_src(int src, char *buf);
    void dump_cmd(FILE *out,comedi_cmd *cmd);

};

#endif	/* ADCARD_HPP */

