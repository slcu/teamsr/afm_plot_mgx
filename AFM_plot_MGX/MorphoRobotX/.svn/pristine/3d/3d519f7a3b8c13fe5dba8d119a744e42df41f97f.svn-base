%%%% ANALYSIS OF WIGGLES, COMES AFTER CRAWLING MODE
%if (wiggles_performed==0) 
%	disp('no wiggles performed');
%end

close all
	
%%%%% PARAMETERS:
minF=6; %% lower force threshold for linear fit 	
maxF=8;%% upper force threshold for linear fit 	
contactF=0.2; %% contact force threshold
%%%%%


show_curves_points=find(FLAT_AND_SLOPE_POINTS);
if length(show_curves_points)>15
show_curves_points=show_curves_points(1:15);
end
%show_curves_points=[281,291,227,237];


fmax_wiggle_grid= [];
indentmax_wiggle_grid= [];
posM_wiggle_grid= [];
force_contact_wiggle_grid= [];
linear_stiffness_wiggle_down= [];
linear_stiffness_wiggle_up= [];
linear_stiffness_wiggle_down_grid=[];
linear_stiffness_wiggle_up_grid=[];
 

for gridID=[0:max(measurementID)];
	ind_wiggle_down=[]; ind_wiggle_up=[]; ind_ID=[];
	ind_ID=(find(measurementID==gridID));% indices corresponding to this point on grid
       	last_repet=max(repetition(ind_ID)); %%% last wiggle repetition (should be the good one)
       	repetition_grid(gridID+1)=last_repet;
       	if  (isempty(find(show_curves_points==gridID))==0)
                           	cmap=colormap(jet(last_repet+1));
                         		%figure('name',['measurement point nr ' num2str(gridID)]); hold on;
                         		figure('name',['measurement point nr ' num2str(gridID) ' slope: ' num2str(FLAT_AND_SLOPE_POINTS(gridID)) ' (1=flat, -1= large slope)']); hold on;
                         		xlabel('indentation depth um')
                           	ylabel('force uN')
                         	end
                         	                         			
       	for repet=[1:last_repet+1];
       		%%% extract wiggle down
       		ind_wiggle_down=ind_ID(find( (flag(ind_ID)==2) & (repetition(ind_ID)==(repet-1)) )); 
       		ind_wiggle_down(1)=[]; % get rid of first point (it is ind_below_surf from crawling mode)
       		%%% extract wiggle up
       		ind_wiggle_up=[ ind_ID(find((flag(ind_ID)==4) & (repetition(ind_ID)==(repet-1)))); ind_ID(find((flag(ind_ID)==5) & (repetition(ind_ID)==(repet-1))))]; 
   		%%% extract contact point during wiggle up based on force 
      		ind_contact_wiggle(repet)=ind_wiggle_up(find(force(ind_wiggle_up)<contactF,1)); %%% contact from wiggle up curve
      		%ind_contact_wiggle(repet)=ind_wiggle_down(find(force(ind_wiggle_down)>contactF,1)) %%% contact from wiggle down curve
       		if isempty(ind_contact_wiggle(repet))
           			ind_contact_wiggle(repet)=ind_wiggle_up(end);
      		end
             		ind_lowest_point=ind_ID(find((flag(ind_ID)==4) & (repetition(ind_ID)==(repet-1)))); % point in between wiggle down and wiggle up
		fmax_wiggle_grid(gridID+1,repet)=force(ind_lowest_point);
		indentmax_wiggle_grid(gridID+1,repet)=posM(ind_contact_wiggle(repet))-posM(ind_lowest_point);
		posM_wiggle_grid(gridID+1,repet)=posM(ind_contact_wiggle(repet));
		force_contact_wiggle_grid(gridID+1,repet)=force(ind_contact_wiggle(repet));
		%%% extract stiffness from linear fit
		%% truncate data lower than Fmin, higher than Fmax
		ind_linear_down=ind_wiggle_down(find((force(ind_wiggle_down)>minF) & (force(ind_wiggle_down)<maxF)));
                           	ind_linear_up=ind_wiggle_up(find((force(ind_wiggle_up)>minF) & (force(ind_wiggle_up)<maxF)));
                           	%% take away points which have a problem in the crystal_state
                           	prob_crysal_state_down=find(abs(step_crystal_state_posM(ind_linear_down))>0.5*409.6);
                           	ind_linear_down(prob_crysal_state_down)=[];
                                                     prob_crysal_state_up=find(abs(step_crystal_state_posM(ind_linear_up))>0.5*409.6);
                           	ind_linear_up(prob_crysal_state_up)=[];
                           	n_linear_down(gridID+1,repet)=size(ind_linear_down,1);
                           	n_linear_up(gridID+1,repet)=size(ind_linear_up,1);
                           	%% do linear fit
                           	if length(ind_linear_down)>2
                          			p_down=polyfit(posM(ind_linear_down),force(ind_linear_down),1); 
                          			linear_stiffness_wiggle_down(gridID+1,repet)=-p_down(1);
                          		else
                          			p_down=0;
                          			linear_stiffness_wiggle_down(gridID+1,repet)=0;
                          		end
                         	 	if length(ind_linear_up)>2
                          			p_up=polyfit(posM(ind_linear_up),force(ind_linear_up),1); 
                          			linear_stiffness_wiggle_up(gridID+1,repet)=-p_up(1);
                         	 	else
                         	 		p_up=0;
                          			linear_stiffness_wiggle_up(gridID+1,repet)=0;
                       	   	end
                        	 	%%%% draw linear fit
                          		f_eval_down=polyval(p_down,posM(ind_linear_down));
                          		f_eval_up=polyval(p_up,posM(ind_linear_up));                      
                         		if (isempty(find(show_curves_points==gridID))==0)            
                              	                          %ind_contact_draw=ind_contact_wiggle(repet); %%%% if use different contact point for each wiggle repetition
                         			ind_contact_draw=ind_contact_wiggle(1); %%%% if use same contact point for all wiggle repetitions
                         			plot(-(posM([ind_wiggle_down;ind_wiggle_up])-posM(ind_contact_draw)),force([ind_wiggle_down;ind_wiggle_up]),'s-','color',cmap(repet,:)); 
                         			hold on
                         			plot(-(posM(ind_linear_down)-posM(ind_contact_draw)),f_eval_down,'color','r'); 
                         			hold on
                         			plot(-(posM(ind_linear_up)-posM(ind_contact_draw)),f_eval_up,'color','g'); 
                         			hold on
                         	                           	%plot(-(posM([ind_wiggle_down;ind_wiggle_up])-posM(ind_contact_draw)),crystal_state_posM([ind_wiggle_down;ind_wiggle_up])/409.6,'*-','color',cmap(repet,:)); 
                         	                           	hold on
                         	                           	%plot(-(posM([ind_wiggle_down;ind_wiggle_up])-posM(ind_contact_draw)),step_crystal_state_posM([ind_wiggle_down;ind_wiggle_up])/409.6,'-.','color',cmap(repet,:)); 
                         	                           	hold on
                         	                           	plot(-(posM([contact_crawl_id(gridID+1):1:contact_crawl_id(gridID+1)+2])-posM(ind_contact_draw)),force([contact_crawl_id(gridID+1):1:contact_crawl_id(gridID+1)+2]),'o','color','r'); 
                         	                           	axis([-1,1,-2,12])
                         		end
                         end
 end


%% take median values of results extracted from wiggles
fmax_wiggle_grid=median(fmax_wiggle_grid,2)';
indentmax_wiggle_grid=median(indentmax_wiggle_grid,2)';
force_contact_wiggle_grid=median(force_contact_wiggle_grid,2)';
posM_wiggle_grid=min(posM_wiggle_grid'); %posM_wiggle_grid=median(posM_wiggle_grid,2)';

%%% for stiffness: take median values 
%linear_stiffness_wiggle_down_grid=median(linear_stiffness_wiggle_down,2)';
%linear_stiffness_wiggle_up_grid=median(linear_stiffness_wiggle_up,2)';

%% for stiffness: take values from wiggle with maximal number of fitted points
[temp,ind_max_down]=max(n_linear_down');
[temp,ind_max_up]=max(n_linear_up');
for i=[1:length(pos0_grid)]
	linear_stiffness_wiggle_down_grid(1,i)=linear_stiffness_wiggle_down(i,ind_max_down(i));
	linear_stiffness_wiggle_up_grid(1,i)=linear_stiffness_wiggle_up(i,ind_max_up(i));
end





%%%% take result from 1st wiggle only
%fmax_wiggle_grid=fmax_wiggle_grid(:,1)';
%indentmax_wiggle_grid=indentmax_wiggle_grid(:,1)';
%posM_wiggle_grid=posM_wiggle_grid(:,1)';
%force_contact_wiggle_grid=force_contact_wiggle_grid(:,1)';
%linear_stiffness_wiggle_down_grid=linear_stiffness_wiggle_down_grid(:,1)';
%linear_stiffness_wiggle_up_grid=linear_stiffness_wiggle_up_grid(:,1)';




%%% draw results for each point
gridpoints=[0:length(pos0_grid)-1]; 
figure('name',['compare contact wiggle  ' file_name]);  plot(gridpoints',[posM_grid'-min(posM), posM_wiggle_grid'-min(posM),linear_stiffness_wiggle_up_grid',linear_stiffness_wiggle_down_grid']); 
xlabel('measure ID')
legend('contact crawling','contact wiggle up (force based)','stiffness wiggle up','stiffness wiggle down')








