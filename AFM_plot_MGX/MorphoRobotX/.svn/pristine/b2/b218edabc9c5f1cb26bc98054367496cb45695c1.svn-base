/* 
 * File:   Force.h
 * Author: huflejt
 *
 * Created on March 14, 2012, 2:15 PM
 */

#ifndef FORCE_H
#define	FORCE_H

#ifdef	__cplusplus
extern "C" {
#endif
    
   
    //#include "NIDAQmxBase.h"
    #include <comedilib.h>
    #include "ForceSensor.h"
    #include <errno.h>
    #include <pthread.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <stdint.h>
    

    typedef struct
    {
        char *filename;
        double value;
        int subdevice;
        int channel;
        int aref;
        int range;
        int physical;
        int verbose;
        int n_chan;
        int n_scan;
        double freq;
    } parsed_options;
    
    typedef enum 
    {
        APPROACH_FLAG_FINE = 0,//A:0, AL:0
        APPROACH_FLAG_SCAN_UP = 5,//A:1, AL:1
        APPROACH_FLAG_SCAN_DOWN = 2, //A:2, AL:2
        APPROACH_FLAG_COARSE = 3,//A:3, AL:3
        APPROACH_FLAG_FMAX = 4,//A:? , AL:4
        APPROACH_FLAG_FMAX2 = 5,//A:?, AL:5
        APPROACH_FLAG_HOLDING = 4,//A:4
        APPROACH_FLAG_BAD_WIGGLE = 5//A:5
    } APPROACH_FLAG;
    
    typedef enum
    {
        FORCE_ACQUISITION_MODE_NONE,
        FORCE_ACQUISITION_MODE_BURST,
        FORCE_ACQUISITION_MODE_CONTINUOUS        
    } FORCE_ACQUISITION_MODE;
    
    //#define     bufferSize (uInt32)20000
    #define     N_CHANS 16
    #define     BUFSZ 100000
    #define     QSIZE 100000
        
    //typedef struct waveformThreadData;

    typedef struct
    {
       //TaskHandle  taskHandleIN;
       //TaskHandle  taskHandleOUT;
       //char        chanIN[32];
       //char        chanOUT[32];
       //char        errBuff[2048];
       
       char buf[BUFSZ];
       
       char queue[QSIZE];
       int qtail;
       int qhead;
       char qempty;
       char qfull;
       
       //FILE *waveformFile;
       
       
       
       char *waveformPipePath;
       char *waveformFilePath;
       char *waveformDecimatedMinPath;
       char *waveformDecimatedMaxPath;
       
       
       int waveformPipeRead;
       int waveformPipeWrite;
       int waveformFile;
       FILE * waveformDecimatedMinFile;
       FILE * waveformDecimatedMaxFile;
       
       lsampl_t decimation_min;
       lsampl_t decimation_max;
       int decimation_elems;
       
       
       unsigned int chanlist[N_CHANS];
       comedi_range * range_info[N_CHANS];
       lsampl_t maxdata[N_CHANS];
       int bytes_per_sample;
       
       pthread_t waveformThread;
       pthread_attr_t attr;
       pthread_t waveformSaverThread;
              
       pthread_mutex_t buf_mutex;
      // waveformThreadData threadData; //struct of data to be passed to the thread by pointer
      
       int waveformThreadRunning;
       int waveformSaverThreadRunning;
       
       //double     data[bufferSize];
       double     minV;
       double     maxV;
      _ForceSensor forceSensor;
       double     contactFMax;
       double     contactFMaxVoltage;   
       double     contactFMin;
       double     contactFMinVoltage;
       double     measureFMax;
       double     measureFMaxVoltage;
       double     coarseFMax1;
       double     coarseFMax1Voltage;
       double     emergencyFMax;
       double     emergencyFMaxVoltage;
       double     negativeEmergencyFMax;
       double     negativeEmergencyFMaxVoltage;
       double     avgForce; //offset
       double     stiffness;
       double     previousStiffness;
       double     previousForce;
       double     previousForceVoltage;
       int        previousPosition;
       int        pokingVersor;
       int        sensorGainReverse;
       comedi_t *dev;
       comedi_cmd  command;
       comedi_cmd *cmd;
       parsed_options options;
       comedi_polynomial_t polynomial_converter;
       FORCE_ACQUISITION_MODE forceAcquisitionMode;
       
    } _ForceMeasurement;
    
  //  typedef struct {
//        int  thread_id;
//        _ForceMeasurement *forceMeasurement;
//    } waveformThreadData;

    
    int ForceMeasurement_UpdateStiffness(_ForceMeasurement* self, int position, double forceVoltage, double* stiffness, double* stiffnessDelta);
    int ForceMeasurement_ResetStiffness(_ForceMeasurement* self, int position, double forceVoltage);
    int ForceMeasurement_Constructor(_ForceMeasurement* self);
    int ForceMeasurement_Initialize(_ForceMeasurement* self, FORCE_ACQUISITION_MODE mode);
    int ForceMeasurement_SetPokingVersor(_ForceMeasurement* self, int pokingVersor);
    int ForceMeasurement_Finalize(_ForceMeasurement* self);
    int ForceMeasurement_WriteVToDAC0(_ForceMeasurement* self, double voltage);
    int ForceMeasurement_SensorPowerUp(_ForceMeasurement* self);
    int ForceMeasurement_SensorPowerDown(_ForceMeasurement* self);
    int ForceMeasurement_ReadForceVoltage(_ForceMeasurement* self, double* voltage);
    int ForceMeasurement_RecordVoltageToCSV(_ForceMeasurement* self, char path[256], long maxSamples);
    int ForceMeasurement_ReadForceVoltageAndVibration(_ForceMeasurement* self, double* voltage, double* vibration1, double* vibration2);
    int ForceMeasurement_ReadAverageForceVoltage(_ForceMeasurement* self, double* avgVoltage, int msTime);
    int ForceMeasurement_AppendSignalToCSV(_ForceMeasurement* self, char path[256]);
    extern void dump_cmd(FILE *file,comedi_cmd *cmd);
    extern void init_parsed_options(parsed_options *options);
    int ForceMeasurement_PrepareCmdLib(_ForceMeasurement* self, comedi_t *dev, int subdevice, int n_scan, int n_chan, unsigned scan_period_nanosec, comedi_cmd *cmd, FORCE_ACQUISITION_MODE mode);
    int ForceMeasurement_LaunchWaveformThread(_ForceMeasurement *self, char waveformPipePath[256], char waveformFilePath[256], char waveformDecimatedMinPath[256], char waveformDecimatedMaxPath[256]);
    int ForceMeasurement_QuitWaveformThread(_ForceMeasurement *self);
    void *ForceMeasurement_WaveformThread(void *t);
    void *ForceMeasurement_WaveformSaverThread(void *t);
    void ForceMeasurement_AppendWaveformBuffer(_ForceMeasurement* self, int numbytes);

    void ForceMeasurement_QueueInit(_ForceMeasurement* self);
    int ForceMeasurement_QueueAddBytes(_ForceMeasurement* self, char* buf, int numbytes);
    
    int ForceMeasurement_GetForce(_ForceMeasurement *self, int nsamples, float* voltage);

#ifdef	__cplusplus
}
#endif

#endif	/* FORCE_H */

