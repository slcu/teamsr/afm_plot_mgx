# -*- coding: utf-8 -*-
"""
:Author: Pierre Barbier de Reuille <pierre.barbierdereuille@gmail.com>

This module loads data from a CSV file generated by the CFM robot control code.

The main entry point is loadData and can be used like this:

    >>> import cfm_reader
    >>> data = cfm_reader.loadData(filename)

After which, data is a record array with all the data from the file. The fields in the record array are currently:
    - `x`: float
        X position of the robot (in μm)
    - `y`: float
        Y position of the robot (in μm)
    - `z`: float
        Z position of the robot (in μm)
    - `voltage`: float
        Voltage applied on the crystal (in V)
    - `force`: float
        Force exerted on the sensor (in N)
    - `time`: int
        Time stamp of the measure (in μs)
    - `flag`: int
        Flag indicating the phase, for now known flags are 2 (down), 3 (coarse) and 5 (up)
    - `point_id`: int
        If of the measure
    - `ref_voltage`: float
        Reference voltage used to offset the force (in V)
    - `repeat`: int
        Number of attempt for measuring the current point

It is usually convenient to split the data to be grouped by point id. For this do:
    >>> data = cfm_reader.splitPoints(data)

After which, data is a dictionnary that associate, for each point_id a PointData structure. The PointData structure contains:
    - `down`: list of rec.array
        List of all the down movements made during the measure
    - `up`: list of rec.array
        List of all the up movements made during the measure
    - `coarse`: list of rec.array
        List of all the coarse movements made during the measure
    - `data`: rec.array
        Part of the array containing the point_id. It must be contiguous is the main array!

"""

# Have '/' always produce a floating point, even if arguments are integer, and '//' be the integer division
from __future__ import division
from csv import reader as csv_reader
from numpy import array, dtype, rec, nonzero
import numpy as np
from itertools import izip
from collections import namedtuple
from UserDict import UserDict as UserD

class UserDict(UserD):
  def __init__(self, *args, **kwords):
    UserD.__init__(self, *args, **kwords)
  def __iter__(self):
    return iter(self.keys())

cfm_columns = [('x', float), ('y', float), ('z', float), ('voltage', float), ('force', float), ('time', int), ('flag', int), ('point_id', int), ('u2', float), ('ref_voltage', float), ('repeat', int)]
cfm_dtype = dtype(cfm_columns)

def checkHeader(header):
    """
    Check if the list 'header' is a valid header (i.e. it's not all numbers)
    """
    try:
        [ float(_val) for _val in header ]
        return False
    except ValueError:
        return True

def loadData(filename):
    """
    Load the CSV file generated by the CFM script and returns a record array with the data loaded.

    Note that any extra column will be dropped.
    """
    nb_fields = len(cfm_columns)
    with file(filename, "r") as f:
        r = csv_reader(f)
        header = tuple(r.next())[:nb_fields]
        if not checkHeader(header):
            header = tuple(c[0] for c in cfm_columns)
            f.seek(0)
            r = csv_reader(f)
        if len(header) < nb_fields:
            raise ValueError('Error, the file in argument should be a CSV file with at least %s fields.' % nb_fields)
        data = rec.array([tuple(_line)[:nb_fields] for _line in r], dtype=cfm_dtype)
        return data

PointData = namedtuple("PointData", "data down up coarse")

FLAG_DOWN = 2
FLAG_COARSE = 3
FLAG_UP = 5

def createPointData(data):
    """
    Fields:
        `down` - list of down wiggle movements
        `up` - list of up wiggle movements
        `coarse` - list of coarse approach
        `data` - full dataset
    """
    changes = list(nonzero(data.flag[:-1] != data.flag[1:])[0]+1)
    changes.insert(0,0)

    downStartIdx = nonzero(data.flag[changes] == FLAG_DOWN)[0]
    upStartIdx = nonzero(data.flag[changes] == FLAG_UP)[0]
    coarseStartIdx = nonzero(data.flag[changes] == FLAG_COARSE)[0]
    changes.append(None)

    down = []
    up = []
    coarse = []

    for idx in downStartIdx:
        down.append(data[changes[idx]:changes[idx+1]])
    for idx in upStartIdx:
        up.append(data[changes[idx]:changes[idx+1]])
    for idx in coarseStartIdx:
        coarse.append(data[changes[idx]:changes[idx+1]])

    return PointData(data, down, up, coarse)

def down_phase_z_decreases(data):
    """
    Take up to 5 random points in data and return True if most of them decrease during the last down phase.
    """
    l = len(data)
    if l < 5:
        sample = range(l)
    else:
        sample = np.random.randint(l, size=5)
    invert = [ data[s].down[-1][-1] < data[s].down[-1][0] for s in sample ]
    return np.sum(invert) > (l//2)

def splitPoints(data, field='point_id'):
    """
    Take a record array and split it by experimental point. The second argument
    is the name of the field to use to identify points.

    The result is an array of views in the array. The point_id must be
    contiguous in the array, or a RuntimeError exception is raised.

    :Note: The z field of the data will be inverted in place if required

    :Returns: dict of (int: PointData)
        Return a dictionnary with a point data for each measurement point. The
        keys are the measurement id of the point.
    """
# First, find unique points
    uids = data[field]
    changes = list(1+nonzero(uids[1:] != uids[:-1])[0])
    changes.insert(0,0)
    changes.append(None)
    result = UserDict()
    for f,s in izip(changes[:-1], changes[1:]):
        u = uids[f]
        if u in result:
            raise RuntimeError("Error, the array given as argument doesn't group the point_id")
        result[u] = createPointData(data[f:s])
    invert_z = down_phase_z_decreases(result)
    if invert_z:
        data.z *= -1
    result.invert_z = invert_z
    return result

def loadParms(filename):
    parms = []
    f = open(filename, 'rb')
    cR = csv.reader(f, delimiter=',')
    key = cR.next()
    for row in cR:
        item = {}
        for i in range(0,len(key)):
            item[key[i]] = row[i]
        parms.append(item)
    f.close()
    return parms

# Fetch parameters of a specific cell 
def findProp(parms,date,cellID,prop):
    for row in parms:
        if int(row['Date [yymmdd]']) == date:
            if int(row['Cell ID']) == cellID:
                return row[prop]
    return

def read_result(filename):
    """
    Read result data file CSV file. It is assumed that all the values are floats.
    """
    with open(filename, 'rb') as f:
        r = csv_reader(f)
        return array(list(r), dtype=float)

