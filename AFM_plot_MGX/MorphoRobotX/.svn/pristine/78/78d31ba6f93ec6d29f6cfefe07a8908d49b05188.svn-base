#include "Factory.hpp"
#include <iostream>
#include <QTextStream>
#include <stdio.h>

Factory::Factory()
{
}

Factory& Factory::instance()
{
  static std::unique_ptr<Factory> _instance = std::unique_ptr<Factory>(nullptr);
  if(!_instance)
    _instance.reset(new Factory());
  return *_instance;
}

bool Factory::register_process(QString name, std::shared_ptr<ProcessMaker> proc)
{
  if(processes.contains(name))
  {
    QTextStream err(stderr);
    err << "Error, there is already a process named '" << name << "'";
    return false;
  }
  processes[name] = proc;
  return true;
}

bool Factory::unregister_process(QString name, std::shared_ptr<ProcessMaker> proc)
{
  auto found = processes.find(name);
  if(found != processes.end())
  {
    if(found.value() == proc)
    {
      processes.erase(found);
      return true;
    }
    else
    {
      QTextStream err(stderr);
      err << "Error, the process named '" << name << "' doesn't correspond to this process maker" << endl;
    }
  }
  else
  {
    QTextStream err(stderr);
    err << "Error, there are no process named '" << name << "'";
  }
  return false;
}

std::shared_ptr<Process> Factory::process(QString name)
{
  auto found = processes.find(name);
  if(found != processes.end())
    return found.value()->create();
  return std::shared_ptr<Process>(nullptr);
}

