OPTION(USE_INI_FORMAT "Choose if parameters follow standard INI files or not." TRUE)

CONFIGURE_FILE(config.hpp.cmake config.hpp)

SET(SOURCE_FILES
    Axis.cpp
    DataLogger.cpp
    Experiment.cpp
    Protocol.cpp
    FileManager.cpp
    HardwareManager.cpp
    MeasurementState.cpp
    Parms.cpp
    Robot.cpp
    RobotSubunit.cpp
    Sensor.cpp    
    StateManager.cpp
    Timer.cpp
    Comm.cpp
    Version.cpp
    ForceSensor.cpp
    )

SET(HEADER_FILES
    Axis.hpp
    DataLogger.hpp
    Experiment.hpp
    Protocol.hpp
    FileManager.hpp
    HardwareManager.hpp
    MeasurementState.hpp
    Parms.hpp
    Robot.hpp
    RobotSubunit.hpp
    Sensor.hpp
    StateManager.hpp
    Timer.hpp
    Comm.hpp
    Version.hpp
    Factory.hpp
    ForceSensor.hpp
    ${CMAKE_CURRENT_BINARY_DIR}/config.hpp
    )

#TODO: should I add -fPIC to these flags when compilig shared library, or CMake is #clever enough ?
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

INCLUDE_DIRECTORIES(SYSTEM ${QT_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})
INCLUDE(${QT_USE_FILE})
ADD_DEFINITIONS(${QT_DEFINITIONS})

ADD_LIBRARY(morphorobotx SHARED ${SOURCE_FILES})
TARGET_LINK_LIBRARIES(morphorobotx rt QtCore)
SET_TARGET_PROPERTIES(morphorobotx PROPERTIES PUBLIC_HEADER "${HEADER_FILES}")
INSTALL(TARGETS morphorobotx DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/include/morphorobotx)

INSTALL(FILES MorphoRobotXConfig.cmake DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/cmake/MorphoRobotX)

SET(PROGRAM_SOURCE morphorobotx.cpp)
ADD_EXECUTABLE(MorphoRobotX ${PROGRAM_SOURCE})
TARGET_LINK_LIBRARIES(MorphoRobotX morphorobotx ${QT_LIBRARIES})
INSTALL(TARGETS MorphoRobotX DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

SET(DUMMY_SOURCE
    DummyAxis.cpp
    DummySensor.cpp
    )

SET(DUMMY_HEADERS
    DummyAxis.hpp
    DummySensor.hpp
    )

ADD_LIBRARY(dummy SHARED ${DUMMY_SOURCE})
SET_TARGET_PROPERTIES(dummy PROPERTIES PUBLIC_HEADER "${DUMMY_HEADERS}")
TARGET_LINK_LIBRARIES(dummy morphorobotx ${QT_LIBRARIES})
INSTALL(TARGETS dummy LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX/include)

SET(COMEDI_SOURCE
    ComediForceSensor.cpp
    ComediSensor.cpp
    ComediHardwareController.cpp
    )

SET(COMEDI_HEADER
    ComediForceSensor.hpp
    ComediSensor.hpp
    ComediHardwareController.hpp
    )

ADD_LIBRARY(comediparts SHARED ${COMEDI_SOURCE})
SET_TARGET_PROPERTIES(comediparts PROPERTIES PUBLIC_HEADER "${COMEDI_HEADER}")
TARGET_LINK_LIBRARIES(comediparts morphorobotx comedi ${QT_LIBRARIES})
INSTALL(TARGETS comediparts LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX/include)

SET(SMARACT_SOURCE
    SmaractAxis.cpp
    SmaractCommands.cpp
    SmaractHardwareController.cpp
    MCSController.cpp
    )

SET(SMARACT_HEADER
    SmaractAxis.hpp
    SmaractCommands.hpp
    SmaractHardwareController.hpp
    MCSController.hpp
    )

ADD_LIBRARY(smaract SHARED ${SMARACT_SOURCE})
SET_TARGET_PROPERTIES(smaract PROPERTIES PUBLIC_HEADER "${SMARACT_HEADER}")
TARGET_LINK_LIBRARIES(smaract morphorobotx mcscontrol ftd2xx ftchipid ${QT_LIBRARIES})
INSTALL(TARGETS smaract LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX/include)

SET(ZABER_SOURCE
    ZaberAxis.cpp
    ZaberHardwareController.cpp)

SET(ZABER_HEADER
    ZaberAxis.hpp
    ZaberHardwareController.hpp)

ADD_LIBRARY(zaber SHARED ${ZABER_SOURCE})
SET_TARGET_PROPERTIES(zaber PROPERTIES PUBLIC_HEADER "${ZABER_HEADER}")
TARGET_LINK_LIBRARIES(zaber morphorobotx ${QT_LIBRARIES})
INSTALL(TARGETS zaber LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/share/MorphoRobotX/include)

find_package(Doxygen)
if(DOXYGEN_FOUND)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
add_custom_target(doc
${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
COMMENT "Generating documentation with Doxygen" VERBATIM
)
endif(DOXYGEN_FOUND)
