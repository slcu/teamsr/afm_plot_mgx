#!/usr/bin/env python
from __future__ import division
from numpy import zeros, rec, dtype, log10, floor
from collections import namedtuple

cell_dtype = dtype([('x', float), ('y', float), ('z', float), ('label', int), ('type', dtype('a1'))])
edge_dtype = dtype([('src', int), ('tgt', int)])

def readCellFile(filename):
    with file(filename, "r") as f:
        line = f.readline().strip()
        try:
            nb_nodes = int(line)
        except ValueError:
            raise ValueError("Error, the first line of the file must be a simple integer")
        print nb_nodes
        nodes = rec.array(zeros((nb_nodes,), dtype=cell_dtype))
        edges = set() # rec.array(dtype=edge_dtype)
        cells = {}
        cnt = 0
        for line in f:
            fields = line.split()
            if cnt < nb_nodes:
                if len(fields) != 6:
                    raise ValueError("Error on line %d of the file: a nodes needs 6 fields: id x y z label type" % (cnt+2,))
                nid = int(fields[0])
                x = float(fields[1])
                y = float(fields[2])
                z = float(fields[3])
                lab = int(fields[4])
                typ = fields[5][0]
                nodes[nid] = (x,y,z,lab,typ)
            else:
                if len(fields) < 2:
                    raise ValueError("Error on line %d of the file: the neighborhood of a node needs at least 2 fields: the node id and the number of neighbors" % (cnt+2,))
                nid = int(fields[0])
                nb_neighbors = int(fields[1])
                if len(fields) != nb_neighbors+2:
                    raise ValueError("Error on line %d of the file: there should be %d neighbors, but %d are present" % (nb_neighbors, len(fields)-2))
                ns = [ int(n) for n in fields[2:] ]
                if nodes[nid].type == 'c':
                    cells[nid] = ns
            cnt += 1
# Now, construct edges and cells
        for c in cells:
            ns = cells[c]
            j = ns[-1]
            for i in ns:
                if i < j:
                    edges.add((i,j))
                else:
                    edges.add((j,i))
                j = i
        edges = rec.array(list(edges), dtype=edge_dtype)
        edge_map = dict((tuple(e),i) for i,e in enumerate(edges))
        cell_tuple = namedtuple("CellTuple", "pos neg")
        for c in cells:
            ns = cells[c]
            cell_pos = set()
            cell_neg = set()
            j = ns[-1]
            for i in ns:
                if j < i:
                    cell_pos.add(edge_map[(j,i)])
                else:
                    cell_neg.add(edge_map[(i,j)])
                j = i
            cells[c] = cell_tuple(cell_pos, cell_neg)

        return nodes, edges, cells

def writeINPCells(f, cells, place):
    for c in cells:
        f.write("*Elset, elset=_Surf-%d_SPOS, %s\n" % (c, place))
        cspos = list(cells[c].pos)
        csneg = list(cells[c].neg)
        for i in xrange(len(cspos)//16):
            f.write(", ".join([str(e+1) for e in cspos[16*i:16*(i+1)]]))
            f.write("\n")
        f.write("*Elset, elset=_Surf-%d_SNEG, %s\n" % (c, place))
        for i in xrange(len(csneg)//16):
            f.write(", ".join([str(e+1) for e in csneg[16*i:16*(i+1)]]))
            f.write("\n")
        f.write("""*Surface, type=ELEMENT, name=Surf-%(id)d
_Surf-%(id)d_SPOS, SPOS
_Surf-%(id)d_SNEG, SNEG
""" % { 'id': c })

def writeINP(filename, nodes, edges, cells):
    with file(filename, "w") as f:
        f.write("""*Heading
** Job name: Model Model name: Model-1
** Generate by: cell2inp.py script
*Preprint, echo=No, model=NO, history=No, contact=No
**
** PARTS
**
*Part, name=Part-1
*Node
""")
        node_map = {}
        node_count = 0
        for i,n in enumerate(nodes):
            if n.type == 'j':
                node_count += 1
                node_map[i] = node_count
                f.write("% 7d,%13g,%13g\n" % (node_count, n.x, n.y))
        # Add one element for the axis symmetry
        top = nodes.y.argmin()
        bottom = nodes.y.argmax()
        left = nodes.x.argmin()
        xleft = nodes[left].x
        if left != bottom:
            yref = nodes[bottom].y
            ref = bottom
        else:
            yref = nodes[top].y
            ref = top
        node_count += 1
        f.write("% 7d,%13g,%13g\n" % (node_count, xleft, yref))
        f.write("*Element, type=SAX1\n")
        nb_dig = int(floor(log10(len(edges)))+1)
        format_edge = "%%%dd,%%%dd,%%%dd\n" % (nb_dig, nb_dig+1, nb_dig+1)
        for i,e in enumerate(edges):
            f.write(format_edge % (i+1, node_map[e.src], node_map[e.tgt]))
# Horizontal edge
        f.write(format_edge % (len(edges)+1, node_map[ref], node_count))
# Vertical edge
        f.write(format_edge % (len(edges)+2, node_count, node_map[left]))
# Now add the axis of symmetry
        f.write("*Nset, nset=SymmetryAxis, internal, generate\n")
        f.write(format_edge % (1, node_count, 1))
        f.write("*Elset, elset=SymmetryAxis, internal, generate\n")
        f.write(format_edge % (1, len(edges)+2, 1))
        writeINPCells(f, cells, "internal")
        f.write("""
** Section: Section-1
*Shell Section, elset=SymmetryAxis, material=Material-1
1., 5
*End Part
**
** ASSEMBLY
**
*Assembly, name=Assembly
**
*Instance, name=Part-1-1, part=Part-1
*FlUID BEHAVIOR, NAME=FLUID
*FLUID DENSITY
1
*FLUID BULK MODULUS
 2200
*End Instance
**""")
        for i,c in enumerate(cells):
            f.write("""
*Node
%7d,%13g,%13g,%13g""" % (i+1, 0, nodes[c].y, 0))
        f.write("\n")
        writeINPCells(f, cells, "instance=Part-1-1")
        for i,c in enumerate(cells):
            f.write("""
*FLUID CAVITY, NAME=CAVITY%(pos)d, REF NODE=%(pos)d, BEHAVIOR=Part-1-1.FLUID, SURFACE=Surf-%(id)d, CHECK NORMALS=YES
""" % {'id': c, 'pos': i+1})
        f.write("""
*End Assembly
** 
** MATERIALS
** 
*Material, name=Material-1
*Elastic, type=ENGINEERING CONSTANTS
1.,  2.,  2.,  0.,  0.,  0., 0.5, 0.5
 0.5,
""")

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 3:
        print >> sys.stderr, "Usage: %s INPUT_FILE OUTPUT_FILE" % (sys.argv[0],)
        sys.exit(1)
    res = readCellFile(sys.argv[1])
    writeINP(sys.argv[2], *res)

