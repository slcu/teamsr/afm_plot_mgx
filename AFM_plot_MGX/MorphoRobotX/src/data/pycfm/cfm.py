"""
:Author: Pierre Barbier de Reuille <pierre.barbierdereuille@gmail.com>

This modules is the main entry point for loading/processing the data.

The simplest use is:

    >>> import cfm
    >>> result, data = cfm.processFile(filename, sensor_stiffness=200)

This assumes the z coordinates increase when pushing on the object.

The data object is the one returns by cfm_reader.splitPoint.

The result object is a dictionnary returning for each point id a tuple with the
result of the data fitting (see pyqt-fit documentation).

The result is altered in two ways:
    1 - In case of error, the result is the tuple ('Error', error description)
    2 - The last element of the tuple is a dictionnary to which the element
    'corrected_z' is added, which contains the z coordinate, corrected for the
    stiffness of the sensor.

To use the module without (re-)loading the data, you can reprocess using:

    >>> result = cfm.processData(data, sensor_stiffness=200)

This module can also extract plots for single points with:

    >>> cfm.plot_single_point(data, result, pid) # if the plot function hasn't been changed
    >>> cfm.plot_linear_single_point(data, result, pid) # if a linear plot function has been used

:Note: The interface should change so the result also contains the function used for the fitting.

At last, you can plot the raw data for a measurement with:

    >>> cfm.plot_measure(data, pid, field='z')
    >>> cfm.plot_measure(data, pid, field='force')
"""

from __future__ import absolute_import, print_function, division
from cfm_curvefit import cfm_fit, cfm_fit_linear, cfm_fct, cfm_fct_linear
import cfm_reader
import numpy as np
from numpy import inf, nan
import pyqt_fit
from pyqt_fit import plot_fit
from pkg_resources import parse_version
from matplotlib import pylab as plt

required_pyqt = "1.1.1"

try:
    version_tuple = parse_version(pyqt_fit.__version__)
    required_tuple = parse_version(required_pyqt)
    assert (version_tuple >= required_tuple), \
        "Error, update pyqt_fit to version %s" % (required_pyqt,)
except AttributeError:
    assert False, "Error, update pyqt_fit to version %s" % (required_pyqt,)


def fitting(pid, data, rank, total, info=False, fit=cfm_fit, **kwrds):
    if info:
        print("Processing PID %d (%d/%d)" % (pid, rank, total))
    try:
        return fit(data[pid].down[-1].z, data[pid].down[-1].force, maxfev=10000, **kwrds)
    except IndexError:
        return ('Error', 'Incomplete data')


def two_step_fitting(pid, data, rank, total, info=False, **kwrds):
    if info:
        print("Processing PID %d (%d/%d)" % (pid, rank, total))
    zs = data[pid].down[-1].z  # Remove first point as it is bogus
    forces = data[pid].down[-1].force
    try:
        res1 = cfm_fit_linear(zs, forces, maxfev=10000, **kwrds)
        if isinstance(res1[0], np.ndarray) and np.all(res1[1] != inf):
            p0 = tuple(res1[0])
            res2 = cfm_fit(zs, forces, p0=p0 + (0.,), maxfev=10000, **kwrds)
            return res2
        else:
            if isinstance(res1[0], np.ndarray):
                res1 = (np.concatenate((res1[0], [0])),) + res1[1:]
        return res1
    except IndexError:
        return ('Error', 'Incomplete data')


def loadFile(filename, info=False):
    if info:
        print("Reading file")
    data = cfm_reader.loadData(filename)
    if info:
        print("Splitting dataset")
    return cfm_reader.splitPoints(data)


def pprocessData(cluster, data, series=None, fitting=fitting, info=False, **kwrds):
    """
    Process the data, using the `fitting` method.
    If `info` is true, informations on the process will be printed on the standard output
    """
    if series is None:
        # Make sure we only try to analyse fully acquired points
        series = [k for k in data if data[k].down and data[k].up and data[k].coarse]
    if info:
        print("Processing ...")
    cluster.push(dict(data_=data, series_=series, info_=info, kwrds_=kwrds, fitting_=fitting))

    def compute(i):
        import __main__
        series = __main__.series_
        fitting = __main__.fitting_
        data = __main__.data_
        info = __main__.info_
        kwrds = __main__.kwrds_
        s = series[i]
        return s, fitting(s, data, i, len(series), info=info, **kwrds)

    return cluster.map(compute, range(len(series)))
    #return dict((s, fitting(s, data, i, len(series), info=info, **kwrds))
                #for i, s in enumerate(series))


def processData(data, series=None, fitting=fitting, info=False, **kwrds):
    """
    Process the data, using the `fitting` method.
    If `info` is true, informations on the process will be printed on the standard output
    """
    if series is None:
        # Make sure we only try to analyse fully acquired points
        series = [k for k in data if data[k].down and data[k].up and data[k].coarse]
    if info:
        print("Processing ...")
    return dict((s, fitting(s, data, i, len(series), info=info, **kwrds))
                for i, s in enumerate(series))


def processFile(filename, series=None, fitting=fitting, info=True, **kwrds):
    """
    Process a file. This means: load the data in the file, and process the data.
    """
    data = loadFile(filename, info=info)
    return processData(data, info=info, **kwrds), data


def extractPlots(result, data, selection=None):
    """
    Returns the graph of the contact points
    :Parameters:
        - `result`: dict of (int: output of fitting)
            Result of the fitting, as provided by the processFile method
        - `data`: dict of (int: PointData)
            Original data, split per point acquired
        - `selection`: None or [int] or Callable(data, result) -> [int]
            Select only part of the data considered as valid. The list can be
            any kind of collection of integers. The point extracted will be the
            intersection between the selection list and the list of points
            whose fitting didn't end up as an error.

    :Returns:
        - `idx`: ndarray (N,) of int
            List of indices used
        - `x`: ndarray (N,) of float
            X position of the points of corresponding index
        - `y`: ndarray (N,) of float
            Y position of the points of corresponding index
        - `param`: ndarray (N,M) of float
            Parameters fitted for the points. M depend on the kind of fitting.
    """
    idx = [i for i in sorted(result.keys()) if not isinstance(result[i], tuple)]
    if selection is not None:
        if callable(selection):
            selection = selection(data, result)
        idx = np.array(list(set(idx) & set(selection)), dtype=int)
        idx.sort()
    else:
        idx = np.array(idx, dtype=int)
    x = np.array([data[i].down[-1].x[0] for i in idx])
    y = np.array([data[i].down[-1].y[0] for i in idx])
    params = [result[i].popt for i in idx]
    max_p = np.max([len(p) for p in params])
    params = np.array([list(p) + [nan] * (max_p - len(p)) for p in params], dtype=float)
    return idx, x, y, params


def plot_fit_result(res, forces, fct=cfm_fct, fct_desc=None, **kwrds):
    rz = res.infodict['corrected_z']
    fit = plot_fit.fit_evaluation(res, rz, forces, xname="Z", yname="Force",
                                  param_names=('x_1', 'y_1', 'a_1', 'a_2', 'b_2'),
                                  fct_desc=fct_desc,
                                  eval_points=np.arange(np.min(rz), np.max(rz), np.ptp(rz) / 1024))
    plot_fit.plot1d(fit, **kwrds)


def plot_single_point(data, result, pid, fct=cfm_fct, **kwrds):
    r = result[pid]
    fd = r"$y_1 + a_1 x + [ a_2 (x-x_1) + b_2 (x-x_1)^2]_{x > x_1}$ for point %d" % (pid,)
    return plot_fit_result(r, data[pid].down[-1].force, fct, fct_desc=fd, **kwrds)

def write_fit_result(filename, res, forces, fct=cfm_fct, fct_desc=None):
    rz = res.infodict['corrected_z']
    fit = plot_fit.fit_evaluation(res, rz, forces, xname="Z", yname="Force",
                                  param_names=('x_1', 'y_1', 'a_1', 'a_2', 'b_2'),
                                  fct_desc=fct_desc,
                                  eval_points=np.arange(np.min(rz), np.max(rz), np.ptp(rz) / 1024))
    plot_fit.write1d(filename, fit, 'Standard residuals', '')


def write_single_point(filename, data, result, pid, fct=cfm_fct, **kwrds):
    r = result[pid]
    fd = r"$y_1 + a_1 x + [ a_2 (x-x_1) + b_2 (x-x_1)^2]_{x > x_1}$ for point {:d}".format(pid)
    return write_fit_result(filename, r, data[pid].down[-1].force, fct,
                            fct_desc=fd, **kwrds)


def plot_linear_single_point(data, result, pid, fct=cfm_fct_linear):
    r = result[pid]
    rz = r.infodict['corrected_z']
    rf = data[pid].down[-1].force
    fd = r"$y_1 + a_1 x + [ a_2 (x-x_1) ]_{x > x_1}$ for point %d" % (pid,)
    fit = plot_fit.fit_evaluation(r, rz, rf, xname="Z", yname="Force",
                                  param_names=('x_1', 'y_1', 'a_1', 'a_2'),
                                  eval_points=np.arange(np.min(rz), np.max(rz), np.ptp(rz) / 1024),
                                  fct_desc=fd)
    plot_fit.plot_fit(fit)


def plot_part(data, part, field, color, mul):
    d = getattr(data, part)
    pl = []
    for p in d:
        pl += plt.plot(p.time, mul * p[field], color, linewidth=2)
    if pl:
        pl[0].set_label(part)
    return pl


def plot_measure(data, pid, field='z', invert=False):
    d = data[pid]
    mul = 1
    if invert:
        mul = -1
    plt.plot(d.data.time, mul * d.data[field], 'k')
    plot_part(d, 'down', field, 'g', mul)
    plot_part(d, 'up', field, 'b', mul)
    plot_part(d, 'coarse', field, 'r', mul)
    plt.legend(loc=0)
