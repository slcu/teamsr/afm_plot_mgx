# -*- coding: utf-8 -*-
"""
:Author: Pierre Barbier de Reuille <pierre.barbierdereuille@gmail.com>

This modules export the processed data in non-trivial ways, either as OBJ files to give to
MorphoGraphX, or an matplotlib plots.

Typical usage for a constant interpolation OBJ file with the appearant stiffness at 0.5 μm
(for quadratic interpolation):

    >>> vr = cfm_export.Voronoi_diagram_analysis(result, data, field=cfm_export.extractStiffness(0.5), subdivide_edges = True)
    >>> cfm_export.OBJwrite_constant(filename, vr, u'Stiffness at 0.5 μm (N/m)'.encode('utf-8'), vr.field_values)

Or to display it locally:
    >>> vp = cfm_export.drawVoronoi(vr, coloring=vr.field_values)
    >>> colorbar(vp)
"""

from __future__ import absolute_import, print_function, division
from scipy.spatial import Delaunay
from numpy import (c_, asarray, concatenate, percentile, array, inf, cross, argsort, unique,
                   lexsort, sum, sqrt, copy, isnan, min, max, newaxis, mean, arctan2, nan,
                   zeros, roll, nonzero, ones, all, nanmin, nanmax, median, compress, add,
                   arange)
from numpy import ma
try:
    from itertools import izip
except ImportError:
    izip = zip
from numpy.linalg import norm
from collections import namedtuple
import cfm
from matplotlib import collections
from matplotlib import pyplot as plt
from matplotlib.backends import pylab_setup
#import matplotlib.colors

new_figure_manager, draw_if_interactive, _show = pylab_setup()[-3:]


def sq_length(x, axis=-1):
    return sum(x * x, axis=axis)


def mirror_points(pts, s):
    xmin, ymin = pts.min(0)
    xmax, ymax = pts.max(0)
    p1 = c_[2 * (xmin - s) - pts[:, 0], pts[:, 1]]
    p2 = c_[2 * (xmax + s) - pts[:, 0], pts[:, 1]]
    p3 = c_[pts[:, 0], 2 * (ymin - s) - pts[:, 1]]
    p4 = c_[pts[:, 0], 2 * (ymax + s) - pts[:, 1]]
    return concatenate((pts, p1, p2, p3, p4))


def unique_rows(ar):
    sorted_ar = ar[lexsort(ar.T)]
    diff = concatenate((sum(sorted_ar[1:] != sorted_ar[:-1], 1, dtype=bool), [True]))
    return sorted_ar[diff]


def make_triangles(pts):
    """
    compute the delaunay triangulation of the points, removing the elongated side triangles.
    :Parameters:
        - `pts`: (N,2) ndarray
            List of points
    :Returns:
        (M,3) ndarray of indices, defining the triangles
    """
    pts = asarray(pts)
    # First create delaunay to find distances
    d = Delaunay(pts)
    # Compute distances
    edges = concatenate((c_[d.vertices[:, 1], d.vertices[:, 0]],
                         c_[d.vertices[:, 2], d.vertices[:, 1]],
                         c_[d.vertices[:, 0], d.vertices[:, 2]]))
    edges.sort()
    edges = unique_rows(edges)
    diffs = pts[edges[:, 1]] - pts[edges[:, 0]]
    dists = sqrt(sum(diffs ** 2, 1))
    s = percentile(dists, 25) / 2
    # Find bbox
    full_pts = mirror_points(pts, s)
    d = Delaunay(full_pts)
    full_trs = d.vertices
# Keep only triangles with points in pts
    max_idx = pts.shape[0]
    sel = ((full_trs[:, 0] < max_idx) &
           (full_trs[:, 1] < max_idx) &
           (full_trs[:, 2] < max_idx))
    trs = full_trs[sel]
# Re-order the triangles
    us = pts[trs[:, 1]] - pts[trs[:, 0]]
    vs = pts[trs[:, 2]] - pts[trs[:, 0]]
    cr = cross(us, vs)
    sel_inv = cr < 0
    trs[sel_inv, 1:] = trs[sel_inv, 2:0:-1]
    #print("max index = %d - # points = %d" % (trs.max(), pts.shape[0]))
    return trs


def OBJWriteTriangles(filename, trs, pts, value=None, value_name=None, pt_labels=None,
                      label_heat=None, heat_desc=None, value_range=None):
    with file(filename, 'w') as f:
        f.write("# OBJ file created from CFM data\n")
        f.write("\n".join("v {0:.6g} {1:.6g} {2:.6g}".format(*pts[i])
                          for i in xrange(pts.shape[0])))
        f.write("\n")
        f.write("\n".join("f {0:d} {1:d} {2:d}".format(face[0] + 1, face[1] + 1, face[2] + 1)
                          for face in trs))
        f.write("\n")
        if value_name is not None:
            f.write("vv_desc {}\n".format(value_name))
        if value is not None:
            f.write("\n".join("vv {:g}".format(v) for v in value))
            f.write("\n")
        if pt_labels is not None:
            f.write("\n".join("label {:d}".format(v) for v in pt_labels))
            f.write("\n")
        if label_heat is not None:
            f.write("\n".join("heat {0:d} {1:g}".format(l, v) for l, v in label_heat.iteritems()))
            f.write("\n")
        if heat_desc is not None:
            f.write("heat_desc {}\n".format(heat_desc))
        if value_range is not None:
            f.write("vv_range {0} {1}".format(*value_range))


def extractStiffness(depth):
    def f(params):
        return params[:, 3] + 2 * params[:, 4] * depth
    return f


def OBJwrite_linear(filename, result, data, field=0, field_name="Z position (µm)", selection=None,
                    max_range=None, center=True):
    """
    Field is the place of the estimated parameters
    """
    idx, x, y, params = cfm.extractPlots(result, data, selection)  # extract x, y, and parameters
    if selection is None:
        selection = [i for i, j in enumerate(idx) if all(result[j].pcov != inf)]
    sel = selection  # shortcut ...
    if not sel:
        raise ValueError("Didn't find any valid values in the results")
    xs = x[sel]
    ys = y[sel]
    if data.invert_z:
        zs = -params[sel, 0]
    else:
        zs = params[sel, 0]
    if center:
        xs -= (xs.min() + xs.max()) / 2
        ys -= (ys.min() + ys.max()) / 2
        zs -= (zs.min() + zs.max()) / 2
    trs = make_triangles(c_[xs, ys])  # find triangles
    print("%d vertices, and %d triangles" % (len(sel), trs.shape[0]))
    print("Bounds = (%g,%g,%g) - (%g,%g,%g)" % (xs.min(), ys.min(), zs.min(),
                                                xs.max(), ys.max(), zs.max()))
    if field == 0:
        field_values = zs
    elif isinstance(field, int):
        field_values = params[sel, field]
    else:
        field_values = field(params[sel])
    if max_range is not None:
        field_values = copy(field_values)
        field_values[field_values < max_range[0]] = max_range[0]
        field_values[field_values > max_range[1]] = max_range[1]
    print("max range = %g - %g" % (min(field_values), max(field_values)))
    return OBJWriteTriangles(filename, trs, c_[xs, ys, zs], field_values, field_name,
                             value_range=max_range)


def cc_dotp(a, b):
    """
    Dot-product of the vectors in a and b, result with the same number of dimensions than a and b
    """
    return sum(a * b, 1)[:, newaxis]


def cc_cross2(a, b, c):
    """
    Double cross-product for computing the circum center.
    This is the 2D equivalent of a^(b^c) in 3D = (ac)b - (ab)c
    """
    return cc_dotp(a, c) * b - cc_dotp(a, b) * c


def cc_ncross2(a, b):
    """
    2D equivalent for ||a^b||^2 = (a^2*b^2-(a*b)^2) -- for circum center calculation
    """
    return cc_dotp(a, a) * cc_dotp(b, b) - cc_dotp(a, b) ** 2


def circumCenter(pts, trs):
    """
    Find the circum center of the triangles, using the 2D equivalent of the following 3D formula:
        For a triangle (A,B,C)
        a = A-C
        b = B-C
        o = \frac{\left(\|a\|^2 b - \|b\|^2 a\right) \times (a \times b)}{2 \|a \times b\|^2} + C
    """
    a = pts[trs[:, 0]] - pts[trs[:, 2]]
    b = pts[trs[:, 1]] - pts[trs[:, 2]]
    up = cc_cross2(cc_dotp(a, a) * b - cc_dotp(b, b) * a, a, b)
    down = 2 * cc_ncross2(a, b)
    o = up / down + pts[trs[:, 2]]
    return o


def make_edge(i, j):
    if i < j:
        return i, j
    return j, i


def normsq(v):
    return sum(v ** 2, axis=-1)


def norm(v):
    return sqrt(sum(v ** 2, axis=-1))


def make_region(c, pts, poly, border, used_edges, pts_neighbors, trs, border_edges, trs_centers):
    """
    Define the polygon of the region for `c`. If the point is internal we use
    the Voronoi region. Otherwise, we adapt it to use part of the edges of the
    triangulation.
    :Parameters:
         - `c`: int
            Index of the point used for the voronoi region
         - `pts`: (N,2)-ndarray of float
            List of all the points, including the circum centers of the triangles
         - `poly`: (M,)-ndarray of int
            List of circum-centers defining the Voronoi region of the point `c`
         - `border`: (N,)-ndarray of bool
            Array of boolean indicating which points are at the border of the grid
         - `used_edges`: dict of ((int,int): int)
            Dictionnary of edges already used in the tesselation
         - `pts_neighbors: dict of (int: list of int)
            Dictionnary giving the list of neighbors for all the points
         - `trs`: (T,3)-ndarray of int
            List of triangles
         - `edges`: dict of (int,int): int
            Dictionnary associating the border edges in the triangulation, to
            the triangle they are part of

    :returns: the ordered list of points defining the region for the point `c`
    """
    poly = unique(asarray(poly, dtype=int))

    poly_pts = pts[poly]
# Find barycenter and center the polygon
    b = mean(poly_pts, axis=0)
    dp = poly_pts - b
# Now find the angles
    angles = arctan2(dp[:, 1], dp[:, 0])
    reorder = argsort(angles)
    reg_pts = poly_pts[reorder]
    reg = poly[reorder]

# If the point is on the border, add the two side edges
    if border[c]:
        nb_pts = pts.shape[0]

# We need to check if the circum-center is outside the edges. If not, add the half-edge point
        pre = []
        post = []
        pre_pos = zeros((0, 2), dtype=float)
        post_pos = zeros((0, 2), dtype=float)

        cpos = pts[c]  # Position of the center of the cell

        border_neighbors = [n for n in pts_neighbors[c] if make_edge(c, n) in border_edges]
        assert len(border_neighbors) == 2  # There must be exactly to neighbors here

        n1 = border_neighbors[0]  # Id of the first neighbor
        e1 = make_edge(c, n1)
        tr1 = border_edges[e1]  # Triangle containing the edge
        x1 = trs[tr1, (trs[tr1] != c) & (trs[tr1] != n1)]  # Position of the third point of the triangle
        o1 = trs_centers[tr1]  # Id of the circum-center of the triangle
        po1 = pts[o1]  # Position of the circum-center
        dn1 = pts[n1] - cpos  # Reference direction for the neighbor
        is_in1 = (cross(po1 - cpos, dn1) * cross(pts[x1] - cpos, dn1)) / normsq(dn1) > -1e-12
        if is_in1:  # Check the circum-center is in the polygon, if not add it
            ne1 = used_edges.setdefault(e1, nb_pts + len(used_edges))
            pre = [ne1]
            pre_pos = ((pts[c] + pts[n1]) / 2)[newaxis]

        n2 = border_neighbors[1]  # Id of the second neighbor
        e2 = make_edge(c, n2)
        tr2 = border_edges[e2]  # Triangle containing the edge
        x2 = trs[tr2, (trs[tr2] != c) & (trs[tr2] != n2)]
        o2 = trs_centers[tr2]
        po2 = pts[o2]
        dn2 = pts[n2] - cpos
        is_in2 = (cross(po2 - cpos, dn2) * cross(pts[x2] - cpos, dn2)) / normsq(dn2) > -1e-12
        if is_in2:
            ne2 = used_edges.setdefault(e2, nb_pts + len(used_edges))
            post = [ne2]
            post_pos = ((pts[c] + pts[n1]) / 2)[newaxis]

# Check if o1 is before o2
        if reg.shape[0] == 1:
            reg = concatenate(([c], post, reg, pre), axis=0)
            reg_pts = concatenate((cpos[newaxis], post_pos, reg_pts, pre_pos), axis=0)
            cr = cross(reg_pts[1] - reg_pts[0], reg_pts[2] - reg_pts[0])
            if cr > 0:
                reg[1:] = reg[:0:-1]
        elif reg.shape[0] == 2:
            cr = cross(reg_pts[0] - cpos, reg_pts[1] - cpos)
            if cr < 0:
                reg = reg[::-1]
            if reg[0] == o1:
                pre, post = post, pre
            reg = concatenate(([c], post, reg, pre), axis=0)
        else:
            pos_o1 = nonzero(reg == o1)[0][0]
            pos_o2 = nonzero(reg == o2)[0][0]

            if pos_o1 > pos_o2:
                if pos_o1 - pos_o2 == 1:
                    reg = roll(reg, -pos_o1)
                    pre, post = post, pre
                else:
                    assert pos_o1 == len(reg) - 1 and pos_o2 == 0
            else:
                if pos_o2 - pos_o1 == 1:
                    reg = roll(reg, -pos_o2)
                else:
                    assert pos_o2 == len(reg) - 1 and pos_o1 == 0
                    pre, post = post, pre
            reg = concatenate(([c], post, reg, pre), axis=0)

        assert reg[0] == c
    return reg

VoronoiResult = namedtuple('VoronoiResult', 'delaunay regions pts barycentric ref_pts neighbors ref_trs')


def filterPoints(o):
    """
    Remove points too close from each other and return a mapping from old to new positions

    :returns:
        - The compressed list of points
        - The original index of the point
        - The new index of the points
    """
# Check the distance of successive points
    o = asarray(o)
    if len(o) > 1:
        distsq = normsq(o[newaxis] - o[:, newaxis])
        distsq /= distsq.max()  # Normalize w.r.t. the size of the whole grid
        idx_x, idx_y = nonzero(distsq < 1e-12)
        non_diag = idx_x < idx_y
        idx_x = idx_x[non_diag]
        idx_y = idx_y[non_diag]
        # At that point, the indices at the same positions of idx_x and idx_y are too close
        ref_pts = arange(len(o), dtype=int)
        IX = idx_y.argsort()
        idx_x = idx_x[IX]
        idx_y = idx_y[IX]  # Make sure we deal first with low index points
        for i, y in enumerate(idx_y):
            ref_pts[y] = ref_pts[idx_x[i]]  # Should deal with multiple associations
        to_keep = [r == i for i, r in enumerate(ref_pts)]
        o = compress(to_keep, o, 0)
        maps = add.accumulate(to_keep)[ref_pts] - 1
        ref_pts = compress(to_keep, ref_pts)
    else:
        ref_pts = array([0])
        maps = array([0])

    return o, ref_pts, maps


def Voronoi(pts):
    """
    Compute a modified Voronoi diagram, in which the border regions re-use the
    triangulation instead of begin infinite.

    :Parameters:
        - `pts`: (N,2) ndarray of float
            Array of points used for the regions

    :Returns:
        - `trs`: (M,3) ndarray of int
            Delaunay triangulation, useful for interpolation
        - `regions`: dict(int: list of int)
            For each point, the list of points defining the region. If the
            point itself is on the region, it will be the first element.
        - `pts`: (N',2) ndarray of float
            List of all the points used in the voronoir regions. The N first
            points are the original ones.
    """
    pts = asarray(pts, dtype=float)
    trs = make_triangles(pts)
    nb_pts = pts.shape[0]
    nb_trs = trs.shape[0]

    # Find circumcenters of triangles, see Wikipedia article for formula
    o = circumCenter(pts, trs)

    # Merge centers too close from each other
    o, ref_trs, trs_centers = filterPoints(o)
    all_pts = concatenate((pts, o), axis=0)

    # Need to shift the points to after the existing ones
    trs_centers += nb_pts

    nb_centers = len(o)

    # Now find the barycentric coordinates of the points
    barycentric = zeros((nb_pts + nb_centers, 3), dtype=float)
    ref_pts = zeros((nb_pts + nb_centers, 3), dtype=int)

    barycentric[:nb_pts, 0] = 1
    ref_pts[:nb_pts, 0] = range(nb_pts)

    # Compute cos(angle) of triangles
    ref_pts[nb_pts:, :] = trs[ref_trs]
    lg = c_[sq_length(pts[trs[ref_trs, 1]] - pts[trs[ref_trs, 2]]),
            sq_length(pts[trs[ref_trs, 0]] - pts[trs[ref_trs, 2]]),
            sq_length(pts[trs[ref_trs, 0]] - pts[trs[ref_trs, 1]])]
    for i in xrange(3):
        j = (i + 1) % 3
        k = (j + 1) % 3
        barycentric[nb_pts:, i] = (lg[:, j] + lg[:, k] - lg[:, i]) / 2 * \
            sqrt(lg[:, i] / (lg[:, j] * lg[:, k]))
    tot_tri = sum(barycentric[nb_pts:], axis=1)[:, newaxis]
    barycentric[nb_pts:] /= tot_tri
    #barycentric[nb_pts:,:] = 1./3

    # Find the points on the border of the grid of the triangles = pts with # of neighbors > # of triangles
    pts_neighbors = {}
    pts_triangles = zeros((pts.shape[0],), dtype=int)
    for i in xrange(trs.shape[0]):
        for j in xrange(3):
            pts_neighbors.setdefault(trs[i, j], set()).update(trs[i])
            pts_triangles[trs[i, j]] += 1
    pts_nbneighbors = array([len(pts_neighbors[i]) - 1 for i in xrange(pts.shape[0])])
    border = pts_nbneighbors != pts_triangles

    # Find all the edges
    full_edges = concatenate((trs[:, :2], trs[:, 1:], trs[:, [2, 0]]), axis=0)
    edges = set(tuple(l) for l in full_edges)
    border_edges = {make_edge(*e): (i % nb_trs) for i, e in enumerate(full_edges)
                    if not ((e[0], e[1]) in edges and (e[1], e[0]) in edges)}

    # assert all_pts.shape[0] == nb_pts + nb_trs  # sanity check
    # Now, find the triangles centers forming the Voronoi regions
    regions_pts = {}
    for i in xrange(nb_trs):
        for j in xrange(3):
            regions_pts.setdefault(trs[i, j], []).append(trs_centers[i])
    # Sort the polygons to make them convex
    regions = {}
    used_edges = {}
    for c in regions_pts:
        regions[c] = make_region(c, all_pts, regions_pts[c], border, used_edges, pts_neighbors,
                                 trs, border_edges, trs_centers)
    if used_edges:
        edge_count = range(all_pts.shape[0], all_pts.shape[0] + len(used_edges))
        new_pts = concatenate((all_pts, zeros((len(edge_count), 2))), axis=0)
        ref_pts = concatenate((ref_pts, zeros((len(edge_count), 3), dtype=int)), axis=0)
        for e in used_edges:
            ue = used_edges[e]
            new_pts[ue] = (pts[e[0]] + pts[e[1]]) / 2
            ref_pts[ue] = [e[0], e[1], 0]
        c = 0.5 * ones((len(used_edges),), dtype=float)
        barycentric = concatenate((barycentric, c_[c, c, zeros(c.shape, dtype=float)]), axis=0)
    else:
        new_pts = all_pts
    assert barycentric.shape == ref_pts.shape
    return VoronoiResult(delaunay=trs, regions=regions, pts=new_pts, barycentric=barycentric,
                         ref_pts=ref_pts, neighbors=pts_neighbors, ref_trs=ref_trs)


def drawVoronoi(vr, subset=None, cmap=None, coloring=None, default_value=nan):
    """
    :Parameters:
        - `vr`: VoronoiDiagram
            Result of the Voronoi_diagram or Voronoi_diagram_analysis functions
        - `subset`: None or container of int
            List of regions to render
        - `cmap`: callable(ndarray (N,) of float) -> ndarray (N,4) of float
            Colormap to use
        - `coloring`: dict of (int: float) or ndarray (N,) of float
            The value for each region of given index.
        - `default_value`: float
            Value to use for colors not specified by coloring
    """
    if subset is None:
        subset = array(vr.regions.keys(), dtype=int)
        subset.sort()
    if coloring is None:
        colors = ma.array([float(i) / len(vr.regions) for i in subset])
    elif hasattr(coloring, 'get'):
        colors = ma.masked_invalid(array([coloring.get(i, default_value) for i in subset],
                                         dtype=float), copy=False)
    else:  # It must be an array
        colors = asarray(coloring, dtype=float)[subset]
    polys = [array([vr.pts[i, :2] for i in vr.regions[s]]) for s in subset]
    ax = plt.gca()
    cp = collections.PolyCollection(polys, array=colors, cmap=cmap, transform=ax.transData)
    cp.set_clim(vmin=colors.min(), vmax=colors.max())
    #draw_if_interactive()
    pts = concatenate(polys, axis=0)
    corners = (pts.min(axis=0), pts.max(axis=0))
    ax.update_datalim(corners)
    ax.autoscale_view()
    ax.add_collection(cp)
    plt.axis('equal')
    return cp

VoronoiDiagram = namedtuple('VoronoiDiagram', 'delaunay regions pts barycentric ref_pts neighbors '
                            'pt_labels triangulation')


def Voronoi_diagram(pts, center=True, subdivide_edges=False):
    """
    Compute the voronoi diagram from the points, using the field values.
    :Parameters:
        - `pts`: ndarray (N,3)
            3D coordinates of the points. In the z column, NaN will be replaced with linear
            interpolation of the neighbors
        - `center`: boolean
            If true, the positions will be reset to be centered on the origin
        - `subdivide_edges`: boolean
            If true, the edge cells will be subdivided by adding another point
            on top of the one existing, to form a triangle fan, appropriate for
            MorphoGraphX.
    :Returns:
        - `delaunay`: ndarray (M,3)
            Delaunay triangulation
        - `regions`: dict (int: ndarray of int)
            For each point, the set of points forming the border of the region
        - `pts`: ndarray (N1,3)
            List of points, augmented by the ones needed for the regions. Any
            NaN in pts[:,2] has been replaced with interpolated values, if
            possible.
            N1 > N and pts[:N,:2] didn't change.
        - `barycentric`: ndarray (N1,3) of float
            Barycentric coordinates for all the points
        - `ref_pts`: ndarray (N1,3) of int
            Index of the reference points used for the barycentric coordinates
        - `neighbors`: dict (int: set of int)
            For each initial point, the set of neighbors in the delaunay triangulation
        - `pt_labels`: ndarray (N1,) of int
            Label of the points, -1 if the point is not defining a region (i.e. boundary point)
        - `triangulation`: ndarray (M1,3) of int
            Full triangulation defining the drawing of the voronoi regions.
    """
    if center:
        pts -= (nanmin(pts, axis=0) + nanmax(pts, axis=0)) / 2
    bounds = (nanmin(pts, axis=0), nanmax(pts, axis=0))
    print("Bounds = %s - %s" % bounds)
    vr = Voronoi(pts[:, :2])

    zinvsel = isnan(pts[:, 2])
    zsel = set((~zinvsel).nonzero()[0])
    for i in zinvsel.nonzero()[0]:
        ns = list(vr.neighbors[i] & zsel)
        pts[i, 2] = mean(pts[ns, 2])

    init_pts = pts

    pts_z = sum(pts[vr.ref_pts, 2] * vr.barycentric, axis=1)
    pts = c_[vr.pts, pts_z]

# Compute the full triangulation
    nb_trs = 0
    add_pts = 0
    for c in vr.regions:
        r = vr.regions[c]
        n = len(r)
        if c in r:
            if subdivide_edges:
                add_pts += 1
            else:  # If the point is part of the region, then we have 2 less triangles
                n -= 2
        nb_trs += n

    nb_pts = pts.shape[0]
    if add_pts > 0:
        pts = concatenate([pts, zeros((nb_pts, 3))], axis=0)
    full_trs = zeros((nb_trs, 3), dtype=int)
    cur_tri = 0

    for c in vr.regions:
        r = vr.regions[c]
        n = len(r)
        if c in r:
            if c != r[0]:
                cidx = nonzero(c == r)[0][0]
                r = roll(r, -cidx)
            if subdivide_edges:
                r[0] = nb_pts
                pts[nb_pts] = pts[c]
                nb_pts += 1
                tris = c_[c * ones(n, dtype=int), r, roll(r, -1)]
            else:
                n -= 2
                tris = c_[c * ones(n, dtype=int), r[1:-1], r[2:]]
        else:
            tris = c_[c * ones(n, dtype=int), r, roll(r, -1)]
        assert tris.shape[0] == n
        full_trs[cur_tri:cur_tri + n] = tris
        cur_tri += n

    pt_labels = -ones(pts.shape[0], dtype=int)
    pt_labels[:init_pts.shape[0]] = range(init_pts.shape[0])

    return VoronoiDiagram(delaunay=vr.delaunay, regions=vr.regions, pts=pts,
                          barycentric=vr.barycentric, ref_pts=vr.ref_pts, neighbors=vr.neighbors,
                          pt_labels=pt_labels, triangulation=full_trs)

VoronoiDiagramResult = namedtuple('VoronoiDiagram', 'delaunay regions pts barycentric ref_pts '
                                  'neighbors pt_labels triangulation field_values')

def Voronoi_diagram_analysis(result, data, field=0, center=True, selection=None,
                             subdivide_edges=False, value_selection=None):
    idx, x, y, params = cfm.extractPlots(result, data, selection)  # extract x, y, and parameters
    zsel = set([i for i, j in enumerate(idx) if all(result[j].pcov != inf)])
    print("length of zsel = %d" % len(zsel))
    if value_selection is None:
        value_selection = zsel
    elif callable(value_selection):
        value_selection = set(value_selection(result, data))
        value_selection = set([i for i, j in enumerate(idx) if i in value_selection]) & zsel
    else:
        value_selection = set(value_selection)
        value_selection = set([i for i, j in enumerate(idx) if i in value_selection]) & zsel
    zselection = asarray(list(zsel), dtype=int)
    zselection.sort()
    value_selection = asarray(list(value_selection), dtype=int)
    value_selection.sort()
    sel = value_selection  # shortcut ...
    if not sel.size:
        raise ValueError("Didn't find any valid values in the results")
    xs = x
    ys = y
    zs = nan * ones(xs.shape, dtype=float)
    if data.invert_z:
        zs[zselection] = -params[zselection, 0]
    else:
        zs[zselection] = params[zselection, 0]

    vr = Voronoi_diagram(c_[xs, ys, zs], center, subdivide_edges)

    if field == 0:
        field_values = zs[sel]
    elif isinstance(field, int):
        field_values = params[sel, field]
    else:
        field_values = field(params[sel])
    print("max range = %g - %g" % (min(field_values), max(field_values)))

    return VoronoiDiagramResult(delaunay=vr.delaunay, regions=vr.regions, pts=vr.pts,
                                barycentric=vr.barycentric, ref_pts=vr.ref_pts,
                                neighbors=vr.neighbors, pt_labels=vr.pt_labels,
                                triangulation=vr.triangulation,
                                field_values=dict(izip(value_selection, field_values)))


def OBJwrite_constant(filename, vr, field_name, field_values):
    """
    See Voronoi_diagram_analysis for arguments past the filename, plus:
        - `filename`: string
            Name of the file to write to
        - `vr`: VoronoiDiagramResult
            Description of the Voronoi diagram, as returned by Voronoi_diagram or
            Voronoi_diagram_analysis
        - `field_name`: string
            Name of the field to be extracted (for user presentation)
        - `field_values`: dict (int: float)
            Dictionnary giving the value for a subset of the labels
    """
    OBJWriteTriangles(filename, vr.triangulation, vr.pts, pt_labels=vr.pt_labels,
                      label_heat=field_values, heat_desc=field_name)
    return vr


def select_part(data, result):
    idx, contact = zip(*[(i, result[i].popts[0]) for i in result if not isinstance(result, tuple)])
    idx = array(idx, dtype=int)
    contact = array(contact, dtype=float)
    iq = percentile(contact, 0.75) - percentile(contact, 0.25)
    mc = median(contact)
    val = (contact > mc - iq) & (contact < mc + iq)
    #print("Nb selected values = %d" % sum(val))
    return idx[val]
