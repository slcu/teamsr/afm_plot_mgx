# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, unicode_literals, division
import cfm_export
from mayavi import mlab
import mayavi.tools.pipeline
from numpy import zeros, nan, argmax, percentile, median, mean, index_exp
from tvtk.api import tvtk
import encodings

def select_part(idx, x, y, params):
    iq = percentile(params[:,0], 0.75) - percentile(params[:,0], 0.25)
    val = argmax(params[:,0] < median(params[:,0]) - iq)
    #print("iq = %s, val = %s" % (iq, val))
    return index_exp[:val]

def Voronoi(*args, **kwords):
    """
    See Voronoi_diagram for arguments past the filename, plus:
        - `field_name`: string
            Name of the field to be extracted (for user presentation)
    """
    field_name = kwords.pop("field_name", u"Z position (µm)")
    if isinstance(field_name, unicode):
        field_name = encodings.codecs.latin_1_encode(field_name)[0]
    data_range = kwords.pop("data_range", (None,None))
    vr = cfm_export.Voronoi_diagram(*args, **kwords)
    #heat = {s: v for s,v in izip(vr.selection, vr.field_values)}
    scalar = zeros(vr.triangulation.shape, dtype=float)
    # Find the label of the triangles
    labs = vr.pt_labels[vr.triangulation].max(axis=1)
    vals = nan*zeros(labs.max()+1)
    vals[vr.selection] = vr.field_values
    values = vals[labs]
    mesh = tvtk.PolyData(points=vr.pts, polys=vr.triangulation)
    mesh.cell_data.scalars = values
    mesh.cell_data.scalars.name = field_name
    normals = mlab.pipeline.poly_data_normals(mesh)
    normals.filter.feature_angle = 80
    #mm = normals.children[0]
    #mm.scalar_lut_manager.data_range = data_range
    surf = mlab.pipeline.surface(normals, colormap='jet', representation='surface', vmin = data_range[0], vmax = data_range[1])
    surf.scene.background = (0,0,0)
    return vr, mesh, normals, surf, values


