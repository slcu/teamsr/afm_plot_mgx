close all

%%%%% MAKE GRID OUT OF CRAWLING MODE SURFACE DETECTION

%%% offset first pos0 and pos1 to (0,0) for visualisation 
pos0_grid=pos0_grid-pos0(1,1);
pos1_grid=pos1_grid-pos1(1,1);
%%%%%%

%%%%%% truncate points corresponding to water poking
water_poking=find(posM_grid>median(posM_grid)+20,1);
if isempty(water_poking)==0
    posM_grid(water_poking)=posM_grid(water_poking(end)+1);
end
%%%%%%

%%%%%% find step size in both direction (rounded to 0.1 um precision)
stepsize_pos0_grid= round(abs(pos0_grid(2:end)-pos0_grid(1:end-1))*10)/10; % rounded to 0.1 um precision
stepsize_pos0_grid=stepsize_pos0_grid(find(stepsize_pos0_grid));
stepsize_pos0_grid=median(stepsize_pos0_grid)

stepsize_pos1_grid= round(abs(pos1_grid(2:end)-pos1_grid(1:end-1))*10)/10; % rounded to 0.1 um precision
stepsize_pos1_grid=stepsize_pos1_grid(find(stepsize_pos1_grid));
stepsize_pos1_grid=median(stepsize_pos1_grid)
%%%%%%

%%%%%% make grid 
start0=round(10*min(pos0_grid))/10; %% corner points are rounded to 0.1 um precision
start1=round(10*min(pos1_grid))/10; %% corner points are rounded to 0.1 um precision
end0=round(10*max(pos0_grid))/10; %% corner points are rounded to 0.1 um precision
end1=round(10*max(pos1_grid))/10; %% corner points are rounded to 0.1 um precision

[POS0_I,POS1_I] = meshgrid([start0:stepsize_pos0_grid:end0],[start1:stepsize_pos1_grid:end1]); %%% to have the grid corners aligned with x,y data points
%[POS0_I,POS1_I] = meshgrid([start0-stepsize_pos0_grid*3/2:stepsize_pos0_grid:end0+stepsize_pos0_grid*3/2],[start1-stepsize_pos1_grid*3/2:stepsize_pos1_grid:end1+stepsize_pos1_grid*3/2]);  %%% to have the grid corners in between  x,y data points
%%%%%%

%%%%%% interpolate data on grid
POSM_I = griddata(pos0_grid,pos1_grid,posM_grid-min(posM),POS0_I,POS1_I);
FORCEMAX_I=griddata(pos0_grid,pos1_grid,force_contact_crawl,POS0_I,POS1_I);
STIFF_I=griddata(pos0_grid,pos1_grid,stiffness_crawl,POS0_I,POS1_I);
%%%%%%

%%%%%%%%% compute slope and curvature on grid surface
[SLOPE_pos0,SLOPE_pos1] = gradient(POSM_I, stepsize_pos0_grid, stepsize_pos1_grid);
SLOPE_pos0=45*SLOPE_pos0;% convert the gradient in degrees (1 => 45 degrees)
SLOPE_pos1=45*SLOPE_pos1;% convert the gradient in degrees (1 => 45 degrees)
CURV = del2(POSM_I,1,4); % curvature approximated by discrete Laplace operator

%%%%%%%%% select points on the flat parts / parts on a large slope
%FLAT_POINTS=[abs(SLOPE_pos0)<5 & abs(SLOPE_pos1)<5];
FLAT_POINTS=[abs(SLOPE_pos0)<5 & abs(SLOPE_pos1)<10 & CURV<0.05];
SLOPE_POINTS=[abs(SLOPE_pos0)>45 ];% || abs(SLOPE_pos1)>20];
FLAT_AND_SLOPE_POINTS=double(FLAT_POINTS)-double(SLOPE_POINTS);


%%%%%%%%% FIGURES %%%%%%%%%%%%%%%%

figure('name',[ 'check xyz data on grid ' file_name])	
mesh(POS0_I,POS1_I,POSM_I); 
hold on
plot3(pos0_grid, pos1_grid, posM_grid-min(posM),'d-')


figure('name',[ 'height map from crawling ' file_name])	
surf(POS0_I,POS1_I,POSM_I); % or use mesh
%mesh(POS0_I,POS1_I,POSM_I); 
%hold on
%plot3(pos0_grid, pos1_grid, posM_grid,'d-')
view(180,80) %%% for inverted setup
%view(270,90)%%% for confocal setup
%axis square
 
%figure('name',[ 'max force during crawling ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,FORCEMAX_I); % or use mesh
%view(180,80) %%% for inverted setup
%%view(270,90)%%% for confocal setup
%%axis square
%colorbar
  %
%figure('name',[ 'stiffness during crawling mode ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,STIFF_I); % or use mesh
%view(180,80) %%% for inverted setup
%%view(270,90)%%% for confocal setup
%%axis square
%colorbar

%%%%%%%%% SLOPE AND CURVATURE  %%%%%%%%%%%%%

%figure('name',[ 'slope (x direction) during crawling mode ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,SLOPE_pos0 ); % or use mesh
%view(180,80) %%% for inverted setup
%caxis([-20,20])
%%view(270,90)%%% for confocal setup
%%axis square
%colorbar
%
%figure('name',[ 'slope (y direction) during crawling mode ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,SLOPE_pos1 ); % or use mesh
%view(180,80) %%% for inverted setup
%caxis([-20,20])
%%view(270,90)%%% for confocal setup
%%axis square
%colorbar
%
%figure('name',[ 'curvature during crawling mode ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,CURV ); % or use mesh
%view(180,80) %%% for inverted setup
%%caxis([-20,20])
%%view(270,90)%%% for confocal setup
%%axis square
%colorbar

figure('name',[ 'points with slope < 5 deg during crawling mode ' file_name])	
surf(POS0_I,POS1_I,POSM_I,FLAT_AND_SLOPE_POINTS); % or use mesh





