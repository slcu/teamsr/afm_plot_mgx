%%%%  list of data (in DataLog.c  flush)
 %axis0Nm/1000.0,
 %axis1Nm/1000.0,
 %axis2Nm/1000.0,
 %voltage,
 %force,
 %msTime,
 %flag,
 %measurementID,
 %stiffness,
 %assumedVoltageOffset,
 %repetition   
 %crystal voltage


%%%% list of flags (in ForceMeasurements.h)
   %APPROACH_FLAG_FINE,
   %APPROACH_FLAG_SCAN_UP,
   %APPROACH_FLAG_SCAN_DOWN,
   %APPROACH_FLAG_COARSE,
   %APPROACH_FLAG_FMAX,
   %APPROACH_FLAG_FMAX2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

close all
clear all

%%%%%%%%%%%HERE CHOOSE FILE TO OPEN  %%%%%%%%%%%%%%%5
%file_name='EmbryoScan_onion_scan_sensor2584_2012_10_04_20h51m.csv'; %%% single wiggles
%file_name='EmbryoScan_onion_scan_triple_wiggles_2012_10_05_19h22m.csv';%%% triple wiggles, step size 60 nm
%file_name='EmbryoScan_onion_triple_wiggle_100ms_integration_2012_10_05_21h06m.csv';% longer force integration frame
%file_name='EmbryoScan_glass_grid1_2012_10_08_12h38m.csv'
%file_name='EmbryoScan_onion_grid_accordion_2012_10_08_15h48m.csv'
file_name='EmbryoScan_onion_grid_20nm_triple_wiggles_sensor_2798_2012_10_10_19h37mTruncated.csv';%%% triple wiggles, step size 20 nm

%%%% 

file_name(end-3:end)=[];



%%%%%%% PARAMETERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
profiling=0;% 1 if profiling tip, 0 otherwise
wiggles_performed=1; % 1 if wiggles, 0 otherwise
windows_version=0;

if windows_version==0
    home_dir='/home/annelise/Desktop/2012_10_data_onion/'; % AL station at work
    %home_dir='/home/kierzkowski/Desktop/2012_10_data_onion/'; % AL station at home
    save_dir_figures='/home/annelise/Desktop/2012_10_data_onion/'; % directory for saving figures
    load([home_dir file_name '.mat'], 'data');
elseif windows_version ==1
    home_dir='E:\CFM\CFM_CONFOCAL\2012_07_19\2012_07_19_mat_files\'
    save_dir_figures='C:\Users\admin\Desktop\figs_results_CFM\'; % directory for saving figures
    data=load([home_dir file_name '_MATLAB.mat'], '-ascii');
end
%%%%%%%%%%%%%%%%%%%%
 

%%%% data content                 
pos0 = data(:,1); % position in axis0 in microns
pos1 = data(:,2); % position in axis1 microns
posM = data(:,3); % position along measurement axis in microns
voltage=data(:,4); % in volts   
force=data(:,5);  % in uN 
time=data(:,6);  % in ms   
flag=data(:,7) ;   
measurementID=data(:,8);    
stiffness=data(:,9); % in N/m                
voltageoffset=data(:,10); % in Volts   
repetition=data(:,11); 
if size(data,2)==12          
	crystal_state_posM=data(:,12); 
else 
	crystal_state_posM=0*pos0; % if there is no 12th column in data (data previous to 2012/10), then set crystal state to 0
end	


if size(data,2)==14         
	crystal_state_pos1=data(:,12); 
	crystal_state_pos0=data(:,13); 
	crystal_state_posM=data(:,14); 
else 
	crystal_state_pos0=0*pos0; % if there is no 14th column in data (data previous to 2012/10/08), then set crystal state to 0
	crystal_state_pos1=0*pos0; % if there is no 14th column in data (data previous to 2012/10/08), then set crystal state to 0
end

             
step_posM=[0;posM(2:end)-posM(1:end-1)]; % in microns
dt=[0;time(2:end)-time(1:end-1)]; % in ms
speed_posM=1000*[0;step_posM(2:end)./dt(2:end)]; % in microns/s
speed_posM(find(step_posM==0))=0; %%% at begining of file the robot doesn't move, step size = 0 
stiffness(find(step_posM==0))=0; %%% at begining of file the robot doesn't move, step size = 0, stiffness = inf

step_crystal_state_posM=[0;crystal_state_posM(2:end)-crystal_state_posM(1:end-1)]; % in arbitraty voltage units [0,4096]


                         
%%%% ANALYSIS OF TIP PROFILING
if profiling==1                                                                          
	f1=figure('name',['water profiling for ' file_name]); plot(time/1000, [step_posM, force, stiffness],'o-'); 
	xlabel('time in seconds')
	legend('steps in z','force','stiffness')
	ylim([-25,5])
		            
	f2=figure('name',['stiffness histogram for ' file_name]);
	hist(stiffness,[-2:0.1:2])
	
	f3=figure('name',['force histogram for ' file_name]);
	hist(force,[-20:1:20])
	
	disp(['*results for '  file_name '*'] )
	disp('Stiffness: ')
	disp(['max value stiffness = ' num2str(max(stiffness)) ' N/m'])
	disp(['99.9 percentile stiffness = ' num2str(prctile(stiffness,99.9)) ' N/m'])
	disp(['min value stiffness = ' num2str(min(stiffness))  ' N/m'])
	disp(['00.1 percentile stiffness = ' num2str(prctile(stiffness,00.1)) ' N/m'])
	disp('Force: ')
	disp(['max value force = ' num2str(max(force)) ' uN'])
	disp(['99.9 percentile force = ' num2str(prctile(force,99.9)) ' uN'])
                          disp(['min value force = ' num2str(min(force))  ' uN'])
	disp(['00.1 percentile force = ' num2str(prctile(force,00.1)) ' uN'])
	disp('Speed : ')
	disp(['steps size  = ' num2str(median(abs(step_posM))) ' um'])
	disp(['steps speed  = ' num2str(median(abs(speed_posM*1000))) ' um/s'])
	disp(['steps duration  =  ' num2str(median(dt)) ' ms'])
	
	print(f1, [save_dir_figures  file_name '_results.jpg'])
	print(f2, [save_dir_figures  file_name '_stiffness.jpg'])
	print(f3, [save_dir_figures  file_name '_force.jpg'])
end



%%%%% ANALYSIS FOR CRAWLING MODE
%% flag==3: coarse approach
%% flag==2: below surface

%%%% ANALYSIS OF CRAWLING WITH/WITHOUT WIGGLES
if profiling==0       
	if wiggles_performed==0
	                           figure('name',['raw data 1 for ' file_name]); 
	                           h=plot(time/1000, [stiffness,flag,crystal_state_posM/409.6, force, step_posM, speed_posM,posM-median(posM)], '+-'); 
	                           set(h(1),'color','c'); set(h(2),'color','m'); set(h(3),'color',[0.5,0.5,0]); set(h(4),'color','r'); set(h(5),'color','k'); set(h(6),'color','g');  set(h(7),'color','b');
		xlabel('time in seconds')
		legend('stiffness','flag','crystal (0 to 10)','force','steps in z','speed steps','z pos')
		ylim([-10,10])
	elseif wiggles_performed==1
	                           figure('name',['raw data 1 for ' file_name]); 
	                           h=plot(time/1000, [stiffness,flag,repetition,crystal_state_posM/409.6, force, step_posM, speed_posM,posM-median(posM)], '+-'); 
	                           set(h(1),'color','c'); set(h(2),'color','m'); set(h(3),'color',[0.8,0.8,0]); set(h(4),'color',[0.5,0.5,0]); set(h(5),'color','r'); set(h(6),'color','k'); set(h(7),'color','g');  set(h(8),'color','b');
		xlabel('time in seconds')
		legend('stiffness','flag',' wiggle repetition','crystal (0 to 10)','force','steps in z','speed steps','z pos')
		ylim([-10,10])			
	end

	for gridID=[0:max(measurementID)];
		ind_ID=(find(measurementID==gridID));% indices corresponding to this point on grid
   		 if isempty(ind_ID)
      		 	 continue
    		end 
    		%%%% CONTACT POINT IN CRAWLING MODE
		ind_below_surf=(ind_ID(find(flag(ind_ID)==2,1))-1); % point detected as below surface (above stiffness threshold) in crawling mode. Warning: below_surf is duplicated (to put the flag on it)
    		ind_contact=(ind_below_surf-1); % the contact point is the point just before bellow_surf. 
		if (isempty(ind_below_surf)) % if surface not found, take last Z position of this measurement
			ind_contact=ind_ID(end);
      		                           ind_below_surf=ind_contact;
   		end
   		contact_crawl_id(gridID+1)=ind_contact;
   		pos0_grid(gridID+1)=pos0(ind_contact);
		pos1_grid(gridID+1)=pos1(ind_contact);	
		posM_grid(gridID+1)=posM(ind_contact);
		force_contact_crawl(gridID+1)=force(ind_contact);
		force_below_surf_crawl(gridID+1)=force(ind_below_surf);
		stiffness_crawl(gridID+1)=stiffness(ind_below_surf);
		time_grid(gridID+1)=time(ind_below_surf);	
		crystal_state_pos0_grid(gridID+1)=crystal_state_pos0(ind_contact);
		crystal_state_pos1_grid(gridID+1)=crystal_state_pos1(ind_contact);
		crystal_state_posM_grid(gridID+1)=crystal_state_posM(ind_contact);	
   	end
 end

gridpoints=[0:length(posM_grid)-1]';  
figure('name',['max force crawling ' file_name]);  plot(gridpoints,[posM_grid'-min(posM), force_below_surf_crawl',force_contact_crawl',stiffness_crawl']); 
xlabel('measure ID')
legend('z pos contact','max force crawling','force at contact','max stiffness at crawling')

figure('name',[ 'surface from crawling mode'  file_name])		
plot3(pos0_grid, pos1_grid, posM_grid-min(posM),'d-')
xlabel('pos0')
ylabel('pos1')


figure('name',[ 'distribution steps size ' file_name]); hist(step_posM,[-0.4:0.01:2.1])


%%%% check crystal state in 3 axis %%%%%%%%%%%%%%%%%%%%%%%%%%
if size(data,2)>12
	figure('name',['crystal state at contact point ' file_name]);  plot(gridpoints,[posM_grid'-min(posM),pos1_grid'-pos1_grid(1),crystal_state_pos0_grid'/406.9,crystal_state_pos1_grid'/406.9,crystal_state_posM_grid'/406.9]); 
	xlabel('measure ID')
	legend('z pos contact','secondary axis','crystal state pos0','crystal state pos1','crystal state Z axis')
	
	figure('name',[ 'surface from crawling mode, crystal state in pos0 '  file_name])
	plot3(pos0_grid, pos1_grid, posM_grid-min(posM),'d-')
	hold on		
	scatter3(pos0_grid, pos1_grid, posM_grid-min(posM),10,crystal_state_pos0_grid,'s')
	xlabel('pos0')
	ylabel('pos1')
	
	figure('name',[ 'surface from crawling mode, crystal state in pos1 '  file_name])
	plot3(pos0_grid, pos1_grid, posM_grid-min(posM),'d-')
	hold on		
	scatter3(pos0_grid, pos1_grid, posM_grid-min(posM),10,crystal_state_pos1_grid,'s')
	xlabel('pos0')
	ylabel('pos1')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55




  
  
begin_true_measure=find(step_posM>0,1); %%% only take points after sensor warm up
stiffness(find(stiffness>10000))=0; %% take away wrong values from stiffness

disp(['*' file_name '*'] )
disp('Time performance: ')
disp(['grid size = ' num2str(length(pos0_grid)) 'points'  ', duration =  ' num2str(time(end)/60000) ' min' , ' , time between 2 grid points= '  num2str(mean((time_grid(2:end)-time_grid(1:end-1))/1000)) ' s'  ])
disp(['steps size  = ' num2str(median(abs(step_posM))) ' um'])
disp(['steps speed  = ' num2str(median(abs(speed_posM))) ' um/s'])
disp(['steps duration  =  ' num2str(median(dt)) ' ms'])

disp('Force at maximal indentation (crawling):  ')
disp(['max force crawling  = ' num2str(max(force_below_surf_crawl)) ' uN'])
disp(['median force crawling  = ' num2str(median(force_below_surf_crawl)) ' uN'])

disp('Stiffness on the fly:  ')
disp(['max value stiffness = ' num2str(max(stiffness(begin_true_measure:end))) ' N/m'])
disp(['95 percentile stiffness = ' num2str(prctile(stiffness(begin_true_measure:end),95)) ' N/m'])
disp(['min value stiffness = ' num2str(min(stiffness(begin_true_measure:end)))  ' N/m'])
disp(['5 percentile stiffness = ' num2str(prctile(stiffness(begin_true_measure:end),5)) ' N/m'])
  
  
  
  
  

  
  
  
  
  
  
  
  
  
  
  
  
  
