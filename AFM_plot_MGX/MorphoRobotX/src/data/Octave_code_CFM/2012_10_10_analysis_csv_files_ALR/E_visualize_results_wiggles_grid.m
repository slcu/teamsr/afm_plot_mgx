close all

%%%%% MAKE GRID OUT OF WIGGLES AND CRAWLING MODE

%%% offset first pos0 and pos1 to (0,0) for visualisation 
pos0_grid=pos0_grid-pos0(1,1);
pos1_grid=pos1_grid-pos1(1,1);
%%%%%%

%%%%%% truncate points corresponding to water poking
water_poking=find(posM_grid>median(posM_grid)+20,1);
if isempty(water_poking)==0
    posM_grid(water_poking)=posM_grid(water_poking(end)+1);
end
%%%%%%

%%%%%% find step size in both direction (rounded to 0.1 um precision)
stepsize_pos0_grid= round(abs(pos0_grid(2:end)-pos0_grid(1:end-1))*10)/10; % rounded to 0.1 um precision
stepsize_pos0_grid=stepsize_pos0_grid(find(stepsize_pos0_grid));
stepsize_pos0_grid=median(stepsize_pos0_grid)

stepsize_pos1_grid= round(abs(pos1_grid(2:end)-pos1_grid(1:end-1))*10)/10; % rounded to 0.1 um precision
stepsize_pos1_grid=stepsize_pos1_grid(find(stepsize_pos1_grid));
stepsize_pos1_grid=median(stepsize_pos1_grid)
%%%%%%

%%%%%% make grid 
start0=round(10*min(pos0_grid))/10; %% corner points are rounded to 0.1 um precision
start1=round(10*min(pos1_grid))/10; %% corner points are rounded to 0.1 um precision
end0=round(10*max(pos0_grid))/10; %% corner points are rounded to 0.1 um precision
end1=round(10*max(pos1_grid))/10; %% corner points are rounded to 0.1 um precision

[POS0_I,POS1_I] = meshgrid([start0:stepsize_pos0_grid:end0],[start1:stepsize_pos1_grid:end1]); %%% to have the grid corners aligned with x,y data points
%[POS0_I,POS1_I] = meshgrid([start0-stepsize_pos0_grid*3/2:stepsize_pos0_grid:end0+stepsize_pos0_grid*3/2],[start1-stepsize_pos1_grid*3/2:stepsize_pos1_grid:end1+stepsize_pos1_grid*3/2]);  %%% to have the grid corners in between  x,y data points
%%%%%%

%%%%%% interpolate data on grid
POSM_I = griddata(pos0_grid,pos1_grid,posM_grid-min(posM),POS0_I,POS1_I); %% contact point detected by crawling mode
POSM_WIGGLE_I= griddata(pos0_grid,pos1_grid,posM_wiggle_grid,POS0_I,POS1_I); %% contact point detected during wiggle
FORCEMAX_WIGGLE_I=griddata(pos0_grid,pos1_grid,fmax_wiggle_grid,POS0_I,POS1_I);
STIFF_UP_I=griddata(pos0_grid,pos1_grid,linear_stiffness_wiggle_up_grid,POS0_I,POS1_I);
STIFF_DOWN_I=griddata(pos0_grid,pos1_grid,linear_stiffness_wiggle_down_grid,POS0_I,POS1_I);
%%%%%%
  
figure('name',[ 'height map from crawling ' file_name])	
surf(POS0_I,POS1_I,POSM_I); % or use mesh
view(180,80) %%% for inverted setup
%view(270,90)%%% for confocal setup
%axis square



figure('name',[ 'height map from wiggle ' file_name])	
surf(POS0_I,POS1_I,POSM_WIGGLE_I); % or use mesh
view(180,80) %%% for inverted setup
%view(270,90)%%% for confocal setup
%axis square
 
figure('name',[ 'max force during wiggle' file_name])	
surf(POS0_I,POS1_I,POSM_I,FORCEMAX_WIGGLE_I); % or use mesh
view(180,80) %%% for inverted setup
%view(270,90)%%% for confocal setup
%axis square
colorbar
  
figure('name',[ 'linear fit stiffness during wiggle up ' file_name])	
surf(POS0_I,POS1_I,POSM_I,STIFF_UP_I); % or use mesh
view(180,80) %%% for inverted setup
%view(270,90)%%% for confocal setup
%axis square
%caxis([12,30])
colorbar

figure('name',[ 'linear fit stiffness during wiggle down ' file_name])	
surf(POS0_I,POS1_I,POSM_I,STIFF_DOWN_I); % or use mesh
view(180,80) %%% for inverted setup
%view(270,90)%%% for confocal setup
%axis square
%caxis([12,30])
colorbar




%%% draw the same without colorbar, to enable rotation in octave 

%figure('name',[ 'linear fit stiffness during wiggle up ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,STIFF_UP_I); % or use mesh
%view(180,80) %%% for inverted setup
%%view(270,90)%%% for confocal setup
%%axis square
%caxis([12,30])
%
%
%figure('name',[ 'linear fit stiffness during wiggle down ' file_name])	
%surf(POS0_I,POS1_I,POSM_I,STIFF_DOWN_I); % or use mesh
%view(180,80) %%% for inverted setup
%%view(270,90)%%% for confocal setup
%%axis square
%caxis([12,30])





