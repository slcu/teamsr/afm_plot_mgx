clear all; 

%%% HERE GIVE THE PATH TO FOLDER CONTAINING CSV FILES %%%%%%%%%%%
home_dir='/home/annelise/Desktop/2012_10_data_onion/'; % AL station at work
%home_dir='/home/kierzkowski/Desktop/2012_10_data_onion/'; % AL station at home
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% HERE GIVE THE PATH TO FOLDER CONTAINING MAT FILES %%%%%%%%%%%
save_dir='/home/annelise/Desktop/2012_10_data_onion/';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% HERE GIVE  .csv FILE NAME %%%%%%%%%%%%%%%%%%%%%%%%%%
%file_name='EmbryoScan_onion_scan_sensor2584_2012_10_04_20h51m.csv'; %%% 
%file_name='EmbryoScan_onion_scan_triple_wiggles_2012_10_05_19h22m.csv';%%% 
%file_name='EmbryoScan_onion_triple_wiggle_100ms_integration_2012_10_05_21h06m.csv';% 
%file_name='EmbryoScan_glass_grid1_2012_10_08_12h38m.csv'
%file_name='EmbryoScan_onion_grid_accordion_2012_10_08_15h48m.csv'
file_name='EmbryoScan_onion_grid_20nm_triple_wiggles_sensor_2798_2012_10_10_19h37mTruncated.csv'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

path = [home_dir file_name ]
disp('reading csv...');
data = mh_csvread(path); 
%data =  csvread(path);
file_name(end-3:end)=[];
save([save_dir file_name '.mat'], 'data')
