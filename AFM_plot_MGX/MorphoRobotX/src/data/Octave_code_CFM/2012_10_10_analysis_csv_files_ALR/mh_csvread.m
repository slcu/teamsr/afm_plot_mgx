function data = mh_csvread(path)
disp(path);
fid = fopen(path,'r');
disp(fid);

linecount = 0;
ncolumns = 0;

while(1)
	s = fgetl(fid);	
	if (s>0) 
		linecount = linecount  + 1;
	else
		break;
	end
	if (mod(linecount,1000)==0)
		%disp(linecount);
		fflush(stdout);
	end
end
	
frewind (fid);
s = fgetl(fid);
%m = str2mat(strsplit(s,','));
m = strsplit(s,',');
ncolumns = length(m)


data = zeros(linecount, ncolumns);	

frewind (fid);
for i=1:linecount
	s = fgetl(fid);
	ss = strsplit(s,',');
	for j=1:length(ss)
		data(i,j) =  str2num(ss{j});
	end
	
	if (mod(i,1000)==0)
	%disp(i);
	fflush(stdout);
	
	end
end

fclose(fid);
