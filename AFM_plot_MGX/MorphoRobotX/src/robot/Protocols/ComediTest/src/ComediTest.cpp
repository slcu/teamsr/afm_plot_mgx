#include "ComediTest.hpp"
#include "Experiment.hpp"
#include "StateManager.hpp"
#include "Comm.hpp"
#include "Robot.hpp"
#include <unistd.h>

REGISTER_PROTOCOL_TYPE(ComediTest);

ComediTest::ComediTest(QString name, Experiment& e)
	: Protocol(name, e)
{
}

bool ComediTest::run()
{
	for(int i = 0 ; i < 10 ; ++i)
	{
		_sensor.update();
		double force = experiment().stateManager().state()[_forceField];
		Comm::user << "Current force reading = " << force << endl;
		sleep(1);
		experiment().stateManager().writeAndRenew();
	}
	return true;
}

/**
 * Function that will read the parameters from the given object and will create/get its subprotocols
 */
bool ComediTest::init(const Parms& parms)
{
	if(!parms(name(), "Force", _forceName)) return false;
	SensorPtr sensor = experiment().robot().sensor(_forceName);
	if(!sensor)
	{
		Comm::err << "Error, no sensor called '" << _forceName << "' has been defined in the robot" << endl;
		return false;
	}
	_sensor = sensor;
	Comm::out << "Sensor name = " << _sensor.sensor()->name() << endl;
	Comm::out << "Sensor axis = " << _sensor.axis() << endl;
	Comm::out << "Sensor direction = " << _sensor.direction() << endl;
	_forceField = experiment().stateManager().fieldIndex(_forceName);
	if(_forceField < 0)
	{
		Comm::err << "Error, no field '" << _forceName << "' has been registered by the robot" << endl;
		return false;
	}
	return true;
}

/**
 * Function that will write extra parameters to the object
 */
bool ComediTest::writeParameters(Parms& parms) const
{
	return true;
}
