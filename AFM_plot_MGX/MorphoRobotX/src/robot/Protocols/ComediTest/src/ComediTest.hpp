#ifndef COMEDITEST_HPP
#define COMEDITEST_HPP

#include "Protocol.hpp"
#include "Sensor.hpp"
#include "ForceSensor.hpp"

class ComediTest :  public Protocol
{
public:
	ComediTest(QString name, Experiment& e);
	
	bool run() override;    

    /**
     * Function that will read the parameters from the given object and will create/get its subprotocols
     */
    bool init(const Parms& parms) override;

    /**
     * Function that will write extra parameters to the object
     */
    bool writeParameters(Parms& parms) const override;
    
protected:
	QString _forceName;
	int _forceField;
	ForceSensor _sensor;
};

typedef std::shared_ptr<ComediTest> ComediTestPtr;

#endif //COMEDITEST_HPP
