#include "MinimumProtocol.hpp"
#include <Comm.hpp>
#include <Experiment.hpp>
#include <StateManager.hpp>

REGISTER_PROTOCOL_TYPE(MinimumProtocol);

constexpr Version MinimumProtocol::version;

MinimumProtocol::MinimumProtocol(QString name, Experiment& e)
    : Protocol(name, e)
{ }

bool MinimumProtocol::init(const Parms& parms)
{
    Comm::user << "Initialization of MinimumProtocol" << endl;
    Version dataVersion;
    if(parms(name(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("MinimumProtocol [%1]").arg(name())))
            return false;
    return true;
}

bool MinimumProtocol::writeParameters(Parms& parms) const
{
    Comm::user << "Writing parameters of MinimumProtocol" << endl;
    parms.set(name(), "Type", "MinimumProtocol");
    parms.set(name(), "Version", version);
    return true;
}

bool MinimumProtocol::run()
{
    Comm::user << "Running protocol" << endl;
    return true;
}

