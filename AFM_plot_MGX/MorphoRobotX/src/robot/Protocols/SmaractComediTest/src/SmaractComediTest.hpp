#ifndef SMARACTCOMEDITEST_HPP
#define SMARACTCOMEDITEST_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Axis.hpp>
#include <SmaractAxis.hpp>
#include <ForceSensor.hpp>
#include <StateManager.hpp>

class SmaractComediTest : public Protocol
{
public:
    SmaractComediTest(QString name, Experiment& e);
    ~SmaractComediTest();

    bool run() override;

    bool runSmaract();
    bool runAxis();

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

    bool badCommunication() const;

    static constexpr Version version = Version{1,0};
protected:
    QString _axisName;
    SmaractAxisPtr _smaractAxis;
    AxisPtr _axis;
    bool _useSmaract;
        
    ForceSensor _forceSensor;
	QString _forceName;    
    
    StateManager::Field _position; //needed to access position in the StateManager
	StateManager::Field _flag; //needed to access flag in the StateManager
	StateManager::Field _force; //needed to access flag in the StateManager
	
};


#endif // SMARACTCOMEDITEST_HPP
