#ifndef ZABERCOMEDITEST_HPP
#define ZABERCOMEDITEST_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Axis.hpp>
#include <ZaberAxis.hpp>
#include "Sensor.hpp"
#include "ForceSensor.hpp"
#include "StateManager.hpp"

class ZaberComediTest : public Protocol
{
public:
    ZaberComediTest(QString name, Experiment& e);
    ~ZaberComediTest();

    bool run() override;

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

    bool badCommunication() const;

    static constexpr Version version = Version{1,0};
protected:
    QString _axisName;    
    AxisPtr _axis;
    bool _needHome;
    unsigned int _homeId;
    bool _canCheckHome;
    StateManager::Field _force;
    StateManager::Field _position;
	StateManager::Field _flag;
    double _duration;
	ForceSensor _sensor;
	QString _forceName;
};


#endif // ZABERCOMEDITEST_HPP
