#include "ZaberComediTest.hpp"
#include <Experiment.hpp>
#include <Axis.hpp>
#include <Robot.hpp>
#include <StateManager.hpp>
#include <MeasurementState.hpp>
#include <Comm.hpp>
#include <unistd.h>

REGISTER_PROTOCOL_TYPE(ZaberComediTest);

constexpr Version ZaberComediTest::version;

ZaberComediTest::ZaberComediTest(QString name, Experiment& e)
	: Protocol(name, e)
{ }

ZaberComediTest::~ZaberComediTest()
{
	Comm::out << "#!# ~ZaberComediTest" << endl;
}

bool ZaberComediTest::init(const Parms& parms)
{
	Version dataVersion;
	if(parms(name(), "Version", dataVersion, version))
		if(not version.canReadData(dataVersion, QString("ZaberComediTest [%1]").arg(name())))
			return false;
	if(!parms(name(), "Axis", _axisName)) return false;
	_axis = experiment().robot().axis(_axisName);
	if(!_axis)
	{
		Comm::err << "Error, no axis named " << _axisName << endl;
		return false;
	}
	unsigned int needHomeId = _axis->optionId("NeedHome", Axis::ReadOnlyOption | Axis::NumberOption);
	if(needHomeId)
	{
		_axis->get(needHomeId, _needHome);
		_canCheckHome = true;
		_homeId = _axis->optionId("Home", Axis::ReadWriteOption | Axis::NumberOption);
		if(not _homeId)
		{
			_canCheckHome = false;
			_homeId = _axis->optionId("Home", Axis::WriteOnlyOption | Axis::NumberOption);
		}
	}
	else
		_needHome = false;

	if(!parms(name(), "Force", _forceName)) return false;
	SensorPtr sensor = experiment().robot().sensor(_forceName);
	if(!sensor)
	{
		Comm::err << "Error, no sensor called '" << _forceName << "' has been defined in the robot" << endl;
		return false;
	}
	_sensor = sensor;
	_force = _state.field(_forceName);
	_position = _state.field(_axisName);
	if(not _force)
	{
		Comm::err << "Error, no field '" << _forceName << "' has been registered by the robot" << endl;
		return false;
	}
	_flag = _state.field(_state.registerFieldExc("Flag", "", true));

	if(!parms(name(), "Duration", _duration)) return false;	
		
	return true;
}

bool ZaberComediTest::writeParameters(Parms& parms) const
{
	bool isOK = true;
	isOK &= parms.set(name(), "Version", version);
	isOK &= parms.set(name(), "Axis", _axisName);
	isOK &= parms.set(name(), "Force", _forceName);
	isOK &= parms.set(name(), "Duration", _duration);
	return isOK;
}

bool ZaberComediTest::badCommunication() const
{
	Comm::err << "Error, cannot communicate with the hardware.";
	return false;
}


bool ZaberComediTest::run()
{
	if(_needHome)
	{
		if(_canCheckHome)
		{
			bool knowsHome = false;
			if(!_axis->get(_homeId, knowsHome)) return badCommunication();

			Comm::out << "Knows home? " << knowsHome << endl;
			for(int i = 0 ; not knowsHome and (i < 4) ; ++i)
			{
				Comm::user << "Press enter to go home" << endl;
				char c;
				Comm::in >> c;
				_axis->set(_homeId, true);
				if(not _axis->get(_homeId, knowsHome)) return badCommunication();
				Comm::out << "Knows home? " << knowsHome << endl;
			}
		}
		else
			if(not _axis->set(_homeId, true)) return badCommunication();
	}
	
	if(!_axis->gotoSync(0.075)) return badCommunication();		
	
	Comm::user << "Press enter to calibrate neutral sensor position" << endl;
				char c;
				Comm::in >> c;
	_sensor.update(); //where is _sensor ?
	
	double fzero = _force(); 
	
	_flag() = 0;
	_state.renew();

	//for(size_t i = 0 ; i < steps.size() ; ++i)
	double time = 0.0;
	double smoothForce = 0.0;
	while(time < _duration)
	{
		_sensor.update();
		double force = _force(); 
		
		Comm::user << "Current force reading = " << force << endl;		
		if(!_axis->updatePosition()) return badCommunication();		//do I have to do it, or the state is updated during gotoSync ?
		double position = _position();
		Comm::user << "Current position reading = " << position << endl;
		Comm::user << "Relative force = " << force-fzero << endl;		
		smoothForce = 0.5*smoothForce + 0.5*(force-fzero);
		if (smoothForce >0)
			_flag() = 1;
		else
			_flag() = -1;
		if(!_axis->gotoSync(position + 100*smoothForce)) return badCommunication();		
		_state.renew();
		//usleep(100000);
	}

	return true;
}

