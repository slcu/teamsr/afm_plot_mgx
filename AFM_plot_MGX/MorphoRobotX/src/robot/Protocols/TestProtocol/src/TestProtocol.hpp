#ifndef MINIMUMPROTOCOL_HPP
#define MINIMUMPROTOCOL_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Parms.hpp>
#include <Axis.hpp>
#include <Sensor.hpp>


class TestProtocol : public Protocol
{
public:
    TestProtocol(QString name, Experiment& e);

    bool run() override;

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

    constexpr static Version version = Version{1,0};
protected:
    AxisPtr mainAxis, sensorAxis;
    SensorPtr sensor;
    unsigned int num_option_id;
    unsigned int str_option_id;
    unsigned int wly_option_id;
};

#endif // MINIMUMPROTOCOL_HPP

