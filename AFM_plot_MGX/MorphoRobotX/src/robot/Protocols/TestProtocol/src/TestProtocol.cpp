#include "TestProtocol.hpp"
#include <Comm.hpp>
#include <Experiment.hpp>
#include <StateManager.hpp>

REGISTER_PROTOCOL_TYPE(TestProtocol);

constexpr Version TestProtocol::version;

TestProtocol::TestProtocol(QString name, Experiment& e)
    : Protocol(name, e)
{ }

bool TestProtocol::init(const Parms& parms)
{
    Comm::user << "Initialization of TestProtocol" << endl;

    // First, test the version of the protocol
    Version dataVersion;
    if(parms(name(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("TestProtocol [%1]").arg(name())))
            return false;

    // Get the name of the sensor to use
    QString n;
    if(!parms(name(), "Sensor", n)) return false;
    Robot& robot = experiment().robot();
    sensor = robot.sensor(n);
    if(!sensor)
    {
        Comm::err << "Error, no sensor called '" << n << "'" << endl;
        return false;
    }

    // Get the sensor axis from its options
    QString sensorAxis;
    unsigned int saId = sensor->optionId("Axis", Sensor::ReadOnlyOption | Sensor::StringOption);
    if(!sensor->get(saId, sensorAxis))
    {
        Comm::err << "Cannot retrieve the axis of the sensor" << endl;
        return false;
    }
    Comm::out << "Axis of the sensor = '" << sensorAxis << "'" << endl;

    // Get the main axis
    if(!parms(name(), "MainAxis", n)) return false;
    mainAxis = robot.axis(n);
    if(!mainAxis)
    {
        Comm::err << "Error, no axis called '" << n << "'" << endl;
        return false;
    }

    // Get the options from the main axis
    num_option_id = mainAxis->optionId("DummyNumber", Axis::ReadWriteOption | Axis::NumberOption);
    if(not num_option_id) return false;
    str_option_id = mainAxis->optionId("DummyString", Axis::ReadWriteOption | Axis::StringOption);
    if(not str_option_id) return false;
    wly_option_id = mainAxis->optionId("WriteOnlyString", Axis::WriteOnlyOption | Axis::StringOption);
    if(not wly_option_id) return false;
    return true;
}

bool TestProtocol::writeParameters(Parms& parms) const
{
    Comm::user << "Writing parameters of TestProtocol" << endl;
    parms.set(name(), "Type", "TestProtocol");
    parms.set(name(), "Version", version);
    parms.set(name(), "Sensor", sensor->name());
    parms.set(name(), "MainAxis", mainAxis->name());
    return true;
}

bool TestProtocol::run()
{
    Comm::user << "Running protocol" << endl;

    // Get the list of settable options from the main axis
    auto options = mainAxis->defaults();
    Comm::user << "\nList of options with their default values:" << endl;
    for(unsigned int id: options.options())
    {
      auto opt = QString("  %1 (%2) %3%4").arg(mainAxis->optionName(id)).arg(id);
      if(options.isSet(id))
      {
          if(options.numeric(id))
              opt = opt.arg("= ").arg(options.value(id));
          else
              opt = opt.arg("= ").arg(options.string(id));
      }
      else
      {
          opt = opt.arg("<UNSET>").arg("");
      }
      Comm::user << opt << endl;
    }
    Comm::user << endl;
    // Change one of these values
    options.value(num_option_id) = 24;

    // Get the value of the options *before*
    double n;
    QString s;
    mainAxis->get(num_option_id, n);
    mainAxis->get(str_option_id, s);
    Comm::user << "Numeric option = " << n << endl
               << "String option = '" << s << "'" << endl;

    // Set all the options as wanted
    mainAxis->setAll(options);
    for(int i = 0 ; i < 10 ; ++i)
    {
        // Set all the options as we want
        mainAxis->gotoSync(i);
        mainAxis->updatePosition();
        sensor->update();
        state().renew();
    }
    int flag_id;
    // Show how to add fields / protocols from a running protocol
    {
        // First, pause the experiment. When the pause object is destroyed, the experiment will be re-configured and be
        // able to resume
        auto pause = experiment().pause();
        // Here we could do anything, we'll simply add a flag field
        flag_id = state().registerFieldExc("NewField", "");
    }
    // Create a field form the field id
    auto flag = state().field(flag_id);
    // and use it
    flag() = 1;

    // Set the option that was unset previously
    options.string(wly_option_id) = "New";
    mainAxis->setAll(options);

    // Make sure we cannot do that here
    if(state().registerField("TestField", "") != -3) // -3 is the error indicating the error is due to the experiment running
    {
        Comm::err << "*#$*#$*#$ Error, this should not work" << endl;
        return false;
    }

    // And another loop
    for(int i = 11 ; i < 20 ; ++i)
    {
        mainAxis->gotoSync(i);
        mainAxis->updatePosition();
        sensor->update();
        state().renew();
    }
    // Get the value of the options *after*
    mainAxis->get(1, n);
    mainAxis->get(2, s);
    Comm::user << "Numeric option = " << n << endl
               << "String option = '" << s << "'" << endl;
    return true;
}

