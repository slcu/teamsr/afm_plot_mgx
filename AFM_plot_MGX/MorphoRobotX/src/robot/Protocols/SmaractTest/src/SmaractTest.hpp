#ifndef SMARACTTEST_HPP
#define SMARACTTEST_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Axis.hpp>
#include <SmaractAxis.hpp>
#include <StateManager.hpp>

class SmaractTest : public Protocol
{
public:
    SmaractTest(QString name, Experiment& e);
    ~SmaractTest();

    bool run() override;

    bool runSmaract();
    bool runAxis();

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

    bool badCommunication() const;

    static constexpr Version version = Version{1,0};
protected:
    QString _axisName;
    SmaractAxisPtr _smaractAxis;
    AxisPtr _axis;
    bool _useSmaract, _needHome; 
    std::vector<double> steps, waits;
    
    StateManager::Field _position; //needed to access position in the StateManager
	StateManager::Field _flag; //needed to access flag in the StateManager
};


#endif // SMARACTTEST_HPP
