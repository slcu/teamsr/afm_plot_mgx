#include "SmaractTest.hpp"
#include <Experiment.hpp>
#include <Axis.hpp>
#include <Robot.hpp>
#include <StateManager.hpp>
#include <MeasurementState.hpp>
#include <Comm.hpp>
#include <unistd.h>
#include <SmaractAxis.hpp> //to be able to use SmaractAxisSpecific stuff, like options

REGISTER_PROTOCOL_TYPE(SmaractTest);

constexpr Version SmaractTest::version;

SmaractTest::SmaractTest(QString name, Experiment& e)
    : Protocol(name, e)
{ }

SmaractTest::~SmaractTest()
{
    Comm::out << "#!# ~SmaractTest" << endl;
}

bool SmaractTest::init(const Parms& parms)
{
    Version dataVersion;
    if(parms(name(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("SmaractTest [%1]").arg(name())))
            return false;
    if(!parms(name(), "UseSmaract", _useSmaract)) return false;

    /**
     * In the protocol.ini you define the _axisName to be used. This
     * name must conicide with one of the logical axis names
     * defined in robot.ini
     * */
    if(!parms(name(), "Axis", _axisName)) return false;
    _axis = experiment().robot().axis(_axisName);
    if(!_axis)
    {
        Comm::err << "Error, no axis named " << _axisName << endl;
        return false;
    }
    //once you have the axis name, you can ask the state about the position field
    _position = _state.field(_axisName);
    if(not _position) //to check if OK
    {
        Comm::err << "Error, no field '" << _axisName << "' has been registered by the robot" << endl;
        return false;
    }

    //to register a protocol flag field to the state
    _flag = _state.field(_state.registerFieldExc("Flag", "", true));

    if(_useSmaract)
    {
        _smaractAxis = std::dynamic_pointer_cast<SmaractAxis>(_axis);
        if(!_smaractAxis)
        {
            Comm::err << "Error, the axis extracted is not a Smaract axis" << endl;
            return false;
        }
    }
    else
    {

    }
    //if(!parms.all(name(), "Step", steps)) return false;
    //if(!parms.all(name(), "Wait", waits)) return false;
    /*if(steps.size() != waits.size())
    {
        Comm::err << "Error, there must be as many Wait as Step lines" << endl;
        return false;
    }*/
    return true;
}

bool SmaractTest::writeParameters(Parms& parms) const
{
    bool isOK = true;
    isOK &= parms.set(name(), "Version", version);
    isOK &= parms.set(name(), "Axis", _axisName);
    return isOK;
}

bool SmaractTest::badCommunication() const
{
    Comm::err << "Error, cannot communicate with the hardware.";
    return false;
}

bool SmaractTest::run()
{
    if(_useSmaract)
        return runSmaract();
    return runSmaract();
}

bool SmaractTest::runAxis()
{

    for(size_t i = 0 ; i < steps.size() ; ++i)
    {
        if(!_axis->gotoSync(steps[i])) return badCommunication();
        usleep(int(waits[i]*1e6));
    }

    return true;
}

bool SmaractTest::runSmaract()
{
	//DEMO: setting options
	//getting default options
	auto options = _smaractAxis->defaults(); //the axis returns the default option set, can be used as a starting point
	//update default options (without sending them to hardware yet):
	options.value(SmaractAxis::CLOSED_LOOP_MAX_FREQUENCY) = 18500; //in Hz
	options.value(SmaractAxis::CLOSED_LOOP_SPEED) = 0; //in meters per second, max 0.1m/s, zero means no limit
	
	options.value(SmaractAxis::GOTO_TIMEOUT) = 2.0; //in seconds
	options.value(SmaractAxis::CLOSED_LOOP_HOLD_TIME) = 200e-3; //in seconds
	options.value(SmaractAxis::GOTO_PRECISION_MARGIN) = 30e-9; //in meters
	
    //Send options to the hardware. They remain valid until you overwrite with new values or the machine resets.    
	if (not _smaractAxis->setAll(options)) return false; //can fail due to bad communication or to illegal values
       
    //getting an individual option directly from the hardware:
    double val;
    if (not _smaractAxis->get(SmaractAxis::CLOSED_LOOP_SPEED, val)) return false; //hardware can be queried, communication may fail     
    Comm::out << "just got closed loop speed from the hardware: " << val << " m/s" << endl;
    
    
    
    
    //creating another option set for slow axis motion - can be used later by calling setAll(...)
    auto optionsSlow = _smaractAxis->defaults(); //the axis returns the default option set, can be used as a starting point
    //you can also create optionsSlow from existing option set:
    //auto optionsSlow = options;
    optionsSlow.value(SmaractAxis::CLOSED_LOOP_MAX_FREQUENCY) = 1000; //in Hz
	optionsSlow.value(SmaractAxis::CLOSED_LOOP_SPEED) = 10e-6; //in meters per second, max 0.1m/s, zero means no limit	
	optionsSlow.value(SmaractAxis::GOTO_TIMEOUT) = 2; //in seconds
	optionsSlow.value(SmaractAxis::CLOSED_LOOP_HOLD_TIME) = 200e-3; //in seconds
	optionsSlow.value(SmaractAxis::GOTO_PRECISION_MARGIN) = 30e-9; //in meters

    //DEMO: setting parameters for gotoSync method
    //Set individual options directly to hardware. They remain valid until you overwrite with new values or the machine resets.
    _smaractAxis->set(SmaractAxis::GOTO_TIMEOUT, 2); //in seconds
    _smaractAxis->set(SmaractAxis::CLOSED_LOOP_HOLD_TIME, 200e-3); //in seconds
    _smaractAxis->set(SmaractAxis::GOTO_PRECISION_MARGIN, 30e-9); //in meters

    //DEMO: update flag
    _flag() = 1; //updating protocol flag value in the state, 1 going forward in absolute steps
    for (int i=0; i<100; i++)
    {
        if(!_smaractAxis->gotoSync(i*2e-6)) return badCommunication(); //position in meters
        if(!_smaractAxis->updatePosition()) return badCommunication(); //gets current axis position from the hardware and sets it to the state
        _state.renew(); //writes the state to the output file and invalidates fields
    }

    //DEMO: setting frequency and speed
    //let's go slowly
    //you can use the optionsSlow and set all options at once:
	if (not _smaractAxis->setAll(optionsSlow)) return false; //can fail due to bad communication or to illegal values
    
    //you can set or get individual options too:
    //_smaractAxis->set(SmaractAxis::CLOSED_LOOP_MAX_FREQUENCY, 1000); //in Hz
    //_smaractAxis->set(SmaractAxis::CLOSED_LOOP_SPEED, 10e-6); //in meters per second
    _smaractAxis->get(SmaractAxis::CLOSED_LOOP_SPEED, val);
    Comm::out << "just got closed loop speed from the hardware: " << val << " m/s" << endl;

    //DEMO: grid
    _flag() = 2; //updating protocol flag value in the state,  2 going forward in absolute steps
    for (int i=100; i>=0; i--)
    {
        if(!_smaractAxis->gotoSync(i*2e-6)) return badCommunication(); //position in meters
        if(!_smaractAxis->updatePosition()) return badCommunication(); //gets current axis position from the hardware and sets it to the state
        _state.renew(); //writes the state to the output file and invalidates fields
    }

    //let's go fast again
    //setting fast motion options again:
    if (not _smaractAxis->setAll(options)) return false; //can fail due to bad communication or to illegal values
    //or set options individually:
    //_smaractAxis->set(SmaractAxis::CLOSED_LOOP_MAX_FREQUENCY, 18500); //in Hz, max 18500
    //_smaractAxis->set(SmaractAxis::CLOSED_LOOP_SPEED, 0); //in meters per second, zero is no limit

    //DEMO: relative steps
    _flag() = 3; //updating protocol flag value in the state,  3 going forward in relative steps

    for (int i=0; i<100; i++)
    {
        if(!_smaractAxis->updatePosition()) return badCommunication(); //gets current axis position from the hardware and sets it to the state
        double currentPos = _position(); //this is how you get the last known position from the state
        Comm::out << "currentPos is " << currentPos << endl;
        if(!_smaractAxis->gotoSync(currentPos + 2.0e-6)) return badCommunication(); //relative motion from the last known position
        if(!_smaractAxis->updatePosition()) return badCommunication(); //gets current axis position from the hardware and sets it to the state
        _state.renew(); //writes the state to the output file and invalidates fields
    }


    //DEMO: stop command
    _flag() = 4; //updating protocol flag value in the state,  4 demonstrates stop command
	//set slow motion options again with the optionsSlow set:
	if (not _smaractAxis->setAll(optionsSlow)) return false; //can fail due to bad communication or to illegal values
	//or set individual options:
    _smaractAxis->set(SmaractAxis::CLOSED_LOOP_MAX_FREQUENCY, 1000); //in Hz
    _smaractAxis->set(SmaractAxis::CLOSED_LOOP_SPEED, 10e-6); //in meters per second
    _smaractAxis->get(SmaractAxis::CLOSED_LOOP_SPEED, val);
    Comm::out << "just got closed loop speed from the hardware: " << val << " m/s" << endl;

    //setting holding time to a large value
    _smaractAxis->set(SmaractAxis::CLOSED_LOOP_HOLD_TIME, 10); //in seconds

    //status variable, used below to query the status
    SmaractAxisStatus status;

    for (int i=0; i<10; i++)
    {
		//get position
        if(!_smaractAxis->updatePosition()) return badCommunication(); //gets current axis position from the hardware and sets it to the state
        double currentPos = _position(); //this is how you get the last known position from the state
        Comm::out << "currentPos is " << currentPos << endl;
        
        //goto:
        if(!_smaractAxis->gotoSync(currentPos + 20.0e-6)) return badCommunication(); //relative motion from the last known position
        
        //get status to see what is going on:
        if(!_smaractAxis->getStatus(status)) return badCommunication(); //gets axis status
        Comm::out << "axis status is " << status.name() << ": " << status.description() << endl;
        
        //stop the axis:
        if(!_smaractAxis->stop()) return badCommunication(); //stopping axis motion
        
        //get status again to see what happened after the stop command:
        if(!_smaractAxis->getStatus(status)) return badCommunication(); //gets axis status, it should have been stopped by now (SA_STOPPED)
        Comm::out << "axis status is " << status.name() << ": " << status.description() << endl; //axis has beed stopped, no closed-loop holding taking place
        
        //get latest position
        if(!_smaractAxis->updatePosition()) return badCommunication(); //gets current axis position from the hardware and sets it to the state
        _state.renew(); //writes the state to the output file and invalidates fields        
        //After _state.renew() the non-stable fields in the state are invalidated, 
        //so if you need _position(), call updatePosition() first. 
        //Otherwise, _position() would return NaN, because it is a non-stable field.
        //Stable fields retain their values in the state, for example _flag() would return the last set valid value.
    }

    return true;
}

