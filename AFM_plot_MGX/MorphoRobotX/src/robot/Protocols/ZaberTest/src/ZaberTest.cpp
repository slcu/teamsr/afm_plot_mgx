#include "ZaberTest.hpp"
#include <Experiment.hpp>
#include <Axis.hpp>
#include <Robot.hpp>
#include <StateManager.hpp>
#include <MeasurementState.hpp>
#include <Comm.hpp>
#include <unistd.h>

REGISTER_PROTOCOL_TYPE(ZaberTest);

constexpr Version ZaberTest::version;

ZaberTest::ZaberTest(QString name, Experiment& e)
	: Protocol(name, e)
{ }

ZaberTest::~ZaberTest()
{
	Comm::out << "#!# ~ZaberTest" << endl;
}

bool ZaberTest::init(const Parms& parms)
{
	Version dataVersion;
	if(parms(name(), "Version", dataVersion, version))
		if(not version.canReadData(dataVersion, QString("ZaberTest [%1]").arg(name())))
			return false;
	if(!parms(name(), "UseZaber", _useZaber)) return false;
	if(!parms(name(), "Axis", _axisName)) return false;
	_axis = experiment().robot().axis(_axisName);
	if(!_axis)
	{
		Comm::err << "Error, no axis named " << _axisName << endl;
		return false;
	}
	if(_useZaber)
	{
		_zaberAxis = std::dynamic_pointer_cast<ZaberAxis>(_axis);
		if(!_zaberAxis)
		{
			Comm::err << "Error, the axis extracted is not a Zaber axis" << endl;
			return false;
		}
	}
	else
	{
		unsigned int needHomeId = _axis->optionId("NeedHome", Axis::ReadOnlyOption | Axis::NumberOption);
		if(needHomeId)
		{
			_axis->get(needHomeId, _needHome);
			_canCheckHome = true;
			_homeId = _axis->optionId("Home", Axis::ReadWriteOption | Axis::NumberOption);
			if(not _homeId)
			{
				_canCheckHome = false;
				_homeId = _axis->optionId("Home", Axis::WriteOnlyOption | Axis::NumberOption);
			}
		}
		else
			_needHome = false;
		//if(not _homeId)
		//{
		//	Comm::err << "Error, the axis has no 'Home' option" << endl;
		//	return false;
		//}
	}
	if(!parms.all(name(), "Step", steps)) return false;
	if(!parms.all(name(), "Wait", waits)) return false;
	if(steps.size() != waits.size())
	{
		Comm::err << "Error, there must be as many Wait as Step lines" << endl;
		return false;
	}
	return true;
}

bool ZaberTest::writeParameters(Parms& parms) const
{
	bool isOK = true;
	isOK &= parms.set(name(), "Version", version);
	isOK &= parms.set(name(), "Axis", _axisName);
	return isOK;
}

bool ZaberTest::badCommunication() const
{
	Comm::err << "Error, cannot communicate with the hardware.";
	return false;
}

bool ZaberTest::run()
{
	if(_useZaber)
		return runZaber();
	return runAxis();
}

bool ZaberTest::runAxis()
{
	if(_needHome)
	{
		if(_canCheckHome)
		{
			bool knowsHome = false;
			if(!_axis->get(_homeId, knowsHome)) return badCommunication();

			Comm::out << "Knows home? " << knowsHome << endl;
			for(int i = 0 ; not knowsHome and (i < 4) ; ++i)
			{
				Comm::user << "Press enter to go home" << endl;
				char c;
				Comm::in >> c;
				_axis->set(_homeId, true);
				if(not _axis->get(_homeId, knowsHome)) return badCommunication();
				Comm::out << "Knows home? " << knowsHome << endl;
			}
		}
		else
			if(not _axis->set(_homeId, true)) return badCommunication();
	}

	for(size_t i = 0 ; i < steps.size() ; ++i)
	{
		if(!_axis->gotoSync(steps[i])) return badCommunication();
		usleep(int(waits[i]*1e6));
	}

	return true;
}

bool ZaberTest::runZaber()
{
	bool knowsHome = false;

	if(!_zaberAxis->knowsHome(knowsHome)) return badCommunication();
	Comm::out << "Knows home? " << knowsHome << endl;
	for(int i = 0 ; not knowsHome and (i < 4) ; ++i)
	{
		Comm::user << "Press enter to go home" << endl;
		char c;
		Comm::in >> c;
		_zaberAxis->goHome();
		if(not _zaberAxis->knowsHome(knowsHome)) return badCommunication();
		Comm::out << "Knows home? " << knowsHome << endl;
		//if(not _zaberAxis->knowsHome(knowsHome)) return badCommunication();
		//Comm::out << "Knows home? " << knowsHome << endl;
	}

	for(size_t i = 0 ; i < steps.size() ; ++i)
	{
		if(!_zaberAxis->gotoSync(steps[i])) return badCommunication();
		usleep(int(waits[i]*1e6));
	}

	return true;
}

