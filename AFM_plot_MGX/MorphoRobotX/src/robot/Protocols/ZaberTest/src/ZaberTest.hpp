#ifndef ZABERTEST_HPP
#define ZABERTEST_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Axis.hpp>
#include <ZaberAxis.hpp>

class ZaberTest : public Protocol
{
public:
    ZaberTest(QString name, Experiment& e);
    ~ZaberTest();

    bool run() override;

    bool runZaber();
    bool runAxis();

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

    bool badCommunication() const;

    static constexpr Version version = Version{1,0};
protected:
    QString _axisName;
    ZaberAxisPtr _zaberAxis;
    AxisPtr _axis;
    bool _useZaber, _needHome;
    unsigned int _homeId;
    bool _canCheckHome;
    std::vector<double> steps, waits;
};


#endif // ZABERTEST_HPP
