#ifndef COMPOUND_HPP
#define COMPOUND_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Parms.hpp>
#include <Axis.hpp>
#include <Sensor.hpp>
#include "SubProtocol.hpp"


class Compound : public Protocol
{
public:
    Compound(QString name, Experiment& e);

    bool run() override;

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

protected:
    Version version;
    AxisPtr mainAxis, sensorAxis;
    SensorPtr sensor;
    SubProtocolPtr subProt;
};

#endif // COMPOUND_HPP

