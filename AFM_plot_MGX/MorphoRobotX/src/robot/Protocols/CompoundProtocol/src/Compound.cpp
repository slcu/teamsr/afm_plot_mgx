#include "Compound.hpp"
#include <Comm.hpp>
#include <Experiment.hpp>
#include <StateManager.hpp>

REGISTER_PROTOCOL_TYPE(Compound);

Compound::Compound(QString name, Experiment& e)
    : Protocol(name, e)
    , version(0,1)
{ }

bool Compound::init(const Parms& parms)
{
    Comm::user << "Initialization of Compound" << endl;
    QString n;
    if(!parms(name(), "Sensor", n)) return false;
    Robot& robot = experiment().robot();
    sensor = robot.sensor(n);
    if(!sensor)
    {
        Comm::err << "Error, no sensor called '" << n << "'" << endl;
        return false;
    }
    if(!parms(name(), "MainAxis", n)) return false;
    mainAxis = robot.axis(n);
    if(!mainAxis)
    {
        Comm::err << "Error, no axis called '" << n << "'" << endl;
        return false;
    }
    QString protName;
    if(!parms(name(), "SubProtocol", protName)) return false;
    ProtocolPtr p;
    bool is_init;
    std::tie(p, is_init) = experiment().protocol(protName);
    if(!is_init)
    {
      Comm::err << "Error, could not create or initialize protocol " << protName << endl;
      return false;
    }
    subProt = std::dynamic_pointer_cast<SubProtocol>(p);
    if(!subProt)
    {
      Comm::err << "Error, protocol created is not of type SubProtocol" << endl;
      return false;
    }
    return subProt->init(parms);
}

bool Compound::writeParameters(Parms& parms) const
{
    Comm::user << "Writing parameters of Compound" << endl;
    parms.set(name(), "Type", "Compound");
    parms.set(name(), "Version", version);
    parms.set(name(), "MainAxis", mainAxis->name());
    parms.set(name(), "SubProtocol", subProt->name());
    return true;
}

bool Compound::run()
{
    Comm::user << "Running protocol" << endl;
    for(int i = 0 ; i < 10 ; ++i)
    {
        mainAxis->gotoSync(i);
        mainAxis->updatePosition();
        sensor->update();
        _state.renew();
        subProt->checkStarting();
    }
    return true;
}

