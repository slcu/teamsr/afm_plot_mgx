#ifndef SUBPROTOCOL_HPP
#define SUBPROTOCOL_HPP

#include <Protocol.hpp>
#include <Version.hpp>
#include <Parms.hpp>
#include <Axis.hpp>
#include <Sensor.hpp>


class SubProtocol : public Protocol
{
public:
    SubProtocol(QString name, Experiment& e);

    bool run() override;

    bool init(const Parms& parms) override;

    bool writeParameters(Parms& parms) const override;

    void checkStarting();
protected:
    static Version version;
    QString message;
};

typedef std::shared_ptr<SubProtocol> SubProtocolPtr;


#endif // SUBMINPROTOCOL_HPP
