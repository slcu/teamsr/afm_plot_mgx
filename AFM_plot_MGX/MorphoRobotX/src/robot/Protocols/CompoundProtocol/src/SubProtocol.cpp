#include "SubProtocol.hpp"
#include <Comm.hpp>

REGISTER_PROTOCOL_TYPE(SubProtocol);

Version SubProtocol::version = Version(1,0);

SubProtocol::SubProtocol(QString name, Experiment& e)
  : Protocol(name, e)
{ }

bool SubProtocol::run()
{
  Comm::user << "Running SubProtocol" << endl;
}

bool SubProtocol::init(const Parms& parms)
{
  Version dataVersion;
  if(parms(name(), "Version", dataVersion, version))
  {
    if(not version.canReadData(dataVersion))
    {
      Comm::err << "Error, bad version" << endl;
      return false;
    }
  }
  if(!parms(name(), "Message", message)) return false;
  return true;
}

bool SubProtocol::writeParameters(Parms& parms) const
{
  bool isOK = true;
  isOK &= parms.set(name(), "Version", version);
  isOK &= parms.set(name(), "Message", message);
  return isOK;
}

void SubProtocol::checkStarting()
{
  Comm::out << "The message is **" << message << "**" << endl;
}
