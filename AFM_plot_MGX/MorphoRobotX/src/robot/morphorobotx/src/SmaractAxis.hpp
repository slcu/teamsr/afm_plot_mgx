/* 
 * File:   SmaractAxis.hpp
 * Author: cfmadmin
 *
 * Created on August 20, 2013, 6:13 PM
 */

#ifndef SMARACTAXIS_HPP
#define	SMARACTAXIS_HPP

#include "Version.hpp"
#include "Axis.hpp"
#include <vector>
#include <MCSControl.h>

class SmaractHardwareController;
class SmaractAxisStatus;
typedef std::shared_ptr<SmaractHardwareController> SmaractHardwareControllerPtr;


class SmaractAxis : public Axis 
{
public:
    /**
    * \enum Options supported by SmaractAxis, for use with set and get methods.
    */
   enum SmaractAxisOption
   {    
       GOTO_TIMEOUT = 1, /**< Timeout for gotoSync method, in seconds*/
       GOTO_PRECISION_MARGIN = 2, /**< Precision margin for gotoSync method, in seconds*/
       CLOSED_LOOP_MAX_FREQUENCY = 3, /**< Maximum driving frequency during closed-loop goto commands in Hz, max. 18500. Write only (hardware doesn't support reads).*/
       CLOSED_LOOP_SPEED = 4, /**< Speed for closed-loop commands*/
       CLOSED_LOOP_HOLD_TIME = 5 /**< Holding time for closed-loop commands, in seconds*/
   };
    
    SmaractAxis(QString partName, QString name, Robot& robot);
    SmaractAxis(const Parms& parms, QString axisName, SmaractHardwareController* hardwareController);
    
    virtual bool readParameters(const Parms& parms) override;
    virtual bool writeParameters(Parms& parms) const override;
    
    bool get(unsigned int option, double& value) override;
    bool set(unsigned int option, double value) override;
    bool initialize(bool force = false) override;
    
    bool gotoSync(double pos) override;
    bool updatePosition() override;
    
    bool valid() const override;
    
    bool setClosedLoopMaxFrequency(unsigned int frequency);
    bool setClosedLoopSpeed(double speed);
    bool getClosedLoopSpeed(double& speed);
    bool getStatus(SmaractAxisStatus& status);
    bool stop();
    
    static constexpr Version version = Version{1,0};
    
protected:
    
    //it seems that protected members are not seen by doxygen. options ?
    
    int _posField; /**< a key to the state field where the position in meters is stored*/
    SmaractHardwareControllerPtr _controller; /**< SmaractHardwareController to which the SmaractAxis belongs*/
    QString _controllerName;  /**< Logical name of the SmaractHardwareController*/
    bool _valid = false; 
    unsigned int _smaractAxisID; /**< identifier of the axis as used with the low-level commands. E.g. 3-axis smaract controller has axes 0,1,2*/
    unsigned int _holdMs; /**<Closed loop holding time in miliseconds - hardware-native units, requires conversion when used with public members */
    unsigned int _timeoutMs; /**<gotoSync() timeout in miliseconds - hardware-native units, requires conversion when used with public members */
    unsigned int _precisionMargin; /**<gotoSync() margin of precision in nanometers - hardware-native units, requires conversion when used with public members */
};

typedef std:: shared_ptr<SmaractAxis> SmaractAxisPtr;

class SmaractAxisStatus {
public:
    SmaractAxisStatus();    
    virtual ~SmaractAxisStatus();
    unsigned int status();
    /**
     * 
     * @param axisStatus which you want to assign
     */
    void setStatus(unsigned int axisStatus)
    {
        _status = axisStatus;
    }
    QString description();
    QString name();
    static QMap<unsigned int, QString> StatusMapDescription();
    static QMap<unsigned int, QString> StatusMapName();
protected:
    unsigned int _status; //unfortunately the smaract library does not offer a type for this

};

#endif	/* SMARACTAXIS_HPP */

