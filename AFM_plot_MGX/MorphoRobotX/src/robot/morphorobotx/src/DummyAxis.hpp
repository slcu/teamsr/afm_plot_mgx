#ifndef DUMMYAXIS_HPP
#define DUMMYAXIS_HPP

#include "Axis.hpp"
#include "Version.hpp"

class DummyAxis : public Axis
{
public:
    DummyAxis(QString partName, QString name, Robot& robot);

    bool initialize(bool force = false) override;

    bool readParameters(const Parms& parms) override;
    bool writeParameters(Parms& parms) const override;
    bool gotoSync(double positionMicrons) override;
    bool updatePosition() override;

    bool valid() const override;

    static constexpr Version version = Version(1,0);

    bool get(unsigned int option, double& value) override;
    bool get(unsigned int option, QString& value) override;
    bool set(unsigned int option, double value) override;
    bool set(unsigned int option, QString value) override;

protected:
    double position = 0.0;
    int _fieldNum = -1;

    double _number;
    QString _string;
};

typedef std::shared_ptr<DummyAxis> DummyAxisPtr;

#endif // DUMMYAXIS_HPP

