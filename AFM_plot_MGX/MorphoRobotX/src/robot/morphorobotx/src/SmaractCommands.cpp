#include "SmaractCommands.hpp"
#include "Comm.hpp"

namespace
{
QHash<SA_STATUS, QString> createStatusMap()
{
    QHash<SA_STATUS, QString> StatusMap;
    StatusMap[SA_OK] = "SA_OK: The function call was successful.";
    StatusMap[SA_INITIALIZATION_ERROR] = "SA_INITIALIZATION_ERROR: An error occurred while initializing the DLL. All systems should be disconnected and reset before the next attempt is made.";
    StatusMap[SA_NOT_INITIALIZED_ERROR] = "SA_NOT_INITIALIZED_ERROR: A function call has been made while the DLL not being initialized. You must call SA_InitSystems before communicating with the hardware.";
    StatusMap[SA_NO_SYSTEMS_FOUND_ERROR] = "SA_NO_SYSTEMS_FOUND_ERROR: May occur at initialization if no Modular Control Systems have been detected on the PC system. Check the connection of the USB cable and make sure the drivers are installed properly. Note: After power up / USB connection it may take several seconds for the system to be detectable.";
    StatusMap[SA_TOO_MANY_SYSTEMS_ERROR] = "SA_TOO_MANY_SYSTEMS_ERROR: The number of allowed systems connected to the PC is limited to 32. If you have more connected, disconnect some.";
    StatusMap[SA_INVALID_SYSTEM_INDEX_ERROR] = "SA_INVALID_SYSTEM_INDEX_ERROR: An invalid system index has been passed to a function. The system index parameter of various functions is zero based. If N is the number of acquired systems, then the valid range for the system index is 0..(N-1).";
    StatusMap[SA_INVALID_CHANNEL_INDEX_ERROR] = "SA_INVALID_CHANNEL_INDEX_ERROR: An invalid channel index has been passed to a function. The channel index parameter of various functions is zero based. If N is the number of channels available on a system, then the valid range for the channel index is 0..(N-1).";
    StatusMap[SA_TRANSMIT_ERROR] = "SA_TRANSMIT_ERROR: An error occurred while sending data to the hardware. The system should be reset.";
    StatusMap[SA_WRITE_ERROR] = "SA_WRITE_ERROR: An error occurred while sending data to the hardware. The system should be reset.";
    StatusMap[SA_INVALID_PARAMETER_ERROR] = "SA_INVALID_PARAMETER_ERROR: An invalid parameter has been passed to a function. Check the function documentation for valid ranges.";
    StatusMap[SA_READ_ERROR] = "SA_READ_ERROR: An error occurred while receiving data from the hardware. The system should be reset.";
    StatusMap[SA_INTERNAL_ERROR] = "SA_INTERNAL_ERROR: An internal communication error occurred. The system should be reset.";
    StatusMap[SA_WRONG_MODE_ERROR] = "SA_WRONG_MODE_ERROR: The called function does not match the communication mode that was selected at initialization (see SA_InitSystems). In synchronous communication mode only functions of sections I and IIa may be called. In asynchronous communication mode only functions of sections I and IIb may be called.";
    StatusMap[SA_PROTOCOL_ERROR] = "SA_PROTOCOL_ERROR: An internal protocol error occurred. The system should be reset.";
    StatusMap[SA_TIMEOUT_ERROR] = "SA_TIMEOUT_ERROR: The hardware did not respond. Make sure that all cables are connected properly and reset the system.";
    StatusMap[SA_NOTIFICATION_ALREADY_SET_ERROR] = "SA_NOTIFICATION_ALREADY_SET_ERROR: Since all channels of a system share the same receive buffer, the receive notification object can only be set once per system. Multiple calls of SA_SetReceiveNotification_A lead to this status code.";
    StatusMap[SA_ID_LIST_TOO_SMALL_ERROR] = "SA_ID_LIST_TOO_SMALL_ERROR: When calling SA_GetAvailableSystems you must pass a pointer to an array that is large enough to hold the system IDs of all connected systems. If the number of detected systems is larger than the array, this error will be generated.";
    StatusMap[SA_SYSTEM_ALREADY_ADDED_ERROR] = "SA_SYSTEM_ALREADY_ADDED_ERROR: In order to acquire specific systems you must call SA_AddSystemToInitSystemsList before calling SA_InitSystems. A system ID may only be added once to the list of systems to be acquired. Multiple calls with the same ID lead to this error.";
    StatusMap[SA_WRONG_CHANNEL_TYPE_ERROR] = "SA_WRONG_CHANNEL_TYPE_ERROR: Most functions of section II are only callable for certain channel types. For example, calling SA_StepMove_S for a channel that is an end effector channel will lead to this error. The detailed function description notes the types of channels that the function may be called for.";
    StatusMap[SA_NO_SENSOR_PRESENT_ERROR] = "SA_NO_SENSOR_PRESENT_ERROR: This error occurs if a function was called that requires sensor feedback, but the addressed positioner has none attached.";
    StatusMap[SA_AMPLITUDE_TOO_LOW_ERROR] = "SA_AMPLITUDE_TOO_LOW_ERROR: The amplitude parameter that was given is too low.";
    StatusMap[SA_AMPLITUDE_TOO_HIGH_ERROR] = "SA_AMPLITUDE_TOO_HIGH_ERROR: The amplitude parameter that was given is too high.";
    StatusMap[SA_FREQUENCY_TOO_LOW_ERROR] = "SA_FREQUENCY_TOO_LOW_ERROR: The frequency parameter that was given is too low.";
    StatusMap[SA_FREQUENCY_TOO_HIGH_ERROR] = "SA_FREQUENCY_TOO_HIGH_ERROR: The frequency parameter that was given is too high.";
    StatusMap[SA_SCAN_TARGET_TOO_HIGH_ERROR] = "SA_SCAN_TARGET_TOO_HIGH_ERROR: The target position for a scanning movement that was given is too high.";
    StatusMap[SA_SCAN_SPEED_TOO_LOW_ERROR] = "SA_SCAN_SPEED_TOO_LOW_ERROR: The scan speed parameter that was given for a scan movement command is too low.";
    StatusMap[SA_SCAN_SPEED_TOO_HIGH_ERROR] = "SA_SCAN_SPEED_TOO_HIGH_ERROR: The scan speed parameter that was given for a scan movement command is too high.";
    StatusMap[SA_SENSOR_DISABLED_ERROR] = "SA_SENSOR_DISABLED_ERROR: This error occurs if an addressed positioner has a sensor attached, but it is disabled. See SA_SetSensorEnabled_S.";
    //StatusM[t(SA_COMMAND_OVERRIDEN_ERROR] = "t(SA_COMMAND_OVERRIDEN_ERROR: This error is only generated in the asynchronous communication mode. When the software commands a movement which is then interrupted by the Hand Control Module, an error of this type is generated.";
    StatusMap[SA_END_STOP_REACHED_ERROR] = "SA_END_STOP_REACHED_ERROR: This error is generated by a positioner channel in asynchronous mode if the target position of a closed-loop command could not be reached because a mechanical end stop was detected. After this error the positioner will have the SA_STOPPED_STATUS status code.";
    StatusMap[SA_WRONG_SENSOR_TYPE_ERROR] = "SA_WRONG_SENSOR_TYPE_ERROR: This error occurs if a closed-loop command does not match the sensor type that is currently configured for the addressed channel. For example, calling SA_GetPosition_S while the targeted channel is configured as rotary will lead to this error.";
    StatusMap[SA_COULD_NOT_FIND_REF_ERROR] = "SA_COULD_NOT_FIND_REF_ERROR: This error is generated in asynchronous mode if the search for a reference mark was aborted. If an end stop is detected during the search, the search direction is reversed. If a second end stop is detected, the search is aborted.";
    StatusMap[SA_WRONG_END_EFFECTOR_TYPE_ERROR] = "SA_WRONG_END_EFFECTOR_TYPE_ERROR: This error occurs if a command does not match the end effector type that is currently configured for the addressed channel. For example, calling SA_GetForce_S while the targeted channel is configured for a gripper will lead to this error.";
    StatusMap[SA_OTHER_ERROR] = "SA_OTHER_ERROR: An error that can't be otherwise categorized.";
    return StatusMap;
}
}

namespace smaract
{
const QHash<SA_STATUS, QString> Result::StatusMap{createStatusMap()};

QString Result::getDescription() const
{
    return StatusMap.value(_status);
}
}
