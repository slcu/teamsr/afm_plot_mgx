#include "DummyAxis.hpp"
#include "Comm.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include "Robot.hpp"
#include "StateManager.hpp"
#include "MeasurementState.hpp"

REGISTER_ROBOT_SUBUNIT_TYPE(DummyAxis);

constexpr Version DummyAxis::version;

bool DummyAxis::initialize(bool force)
{
    return true;
}

DummyAxis::DummyAxis(QString partName, QString name, Robot& robot)
    : Axis(partName, name, robot)
{
    registerOption("DummyNumber", 1, ReadWriteOption | NumberOption);
    registerOption("DummyString", 2, ReadWriteOption | StringOption);
    registerOption("WriteOnlyString", 3, WriteOnlyOption | StringOption);
}

bool DummyAxis::readParameters(const Parms& parms)
{
    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("DummyAxis [%1]").arg(partName())))
            return false;
    _fieldNum = robot().stateManager().registerFieldExc(name(), "um", true);
    setDefault(1, 12, true);
    setDefault(2, "Blah", true);
    return true;
}

bool DummyAxis::writeParameters(Parms& parms) const
{
    if(!parms.set(partName(), "Version", version)) return false;
    return true;
}

bool DummyAxis::gotoSync(double positionMicrons)
{
    position = positionMicrons;
    //Comm::user << "Gone to position " << positionMicrons << endl;
    LOG(QString("Gone to position %1").arg(positionMicrons));
    return true;
}

bool DummyAxis::updatePosition()
{
    robot().stateManager().state()[_fieldNum] = position;
    return true;
}

bool DummyAxis::valid() const
{
    return true;
}

bool DummyAxis::get(unsigned int option, double& value)
{
    if(option == 1)
    {
        value = _number;
        return true;
    }
    return false;
}

bool DummyAxis::get(unsigned int option, QString& value)
{
    if(option == 2)
    {
        value = _string;
        return true;
    }
    else if(option == 3)
        return true;
    return false;
}

bool DummyAxis::set(unsigned int option, double value)
{
    LOG(QString("Setting option %1 to %2").arg(option).arg(value));
    if(option == 1)
    {
        _number = value;
        return true;
    }
    return false;
}

bool DummyAxis::set(unsigned int option, QString value)
{
    LOG(QString("Setting option %1 to '%2'").arg(option).arg(value));
    if(option == 2)
    {
        _string = value;
        return true;
    }
    else if(option == 3)
        return true;
    return false;
}

