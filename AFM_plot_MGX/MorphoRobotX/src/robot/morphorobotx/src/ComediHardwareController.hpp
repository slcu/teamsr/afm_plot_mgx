/*
 * File:   ComediCardHardwareController.hpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 3:04 PM
 */

#ifndef COMEDIHARDWARECONTROLLER_HPP
#define COMEDIHARDWARECONTROLLER_HPP
#include "RobotSubunit.hpp"
#include <QHash>
#include <valarray>
#include "Version.hpp"
#include <ctime>
#include "Timer.hpp"

class Parms;
class ComediSensor;
class ComediHardwareControllerPrivate;

class ComediHardwareController : public RobotSubunit
{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN();

    ComediHardwareController(QString partName, QString name, Robot& robot);

    bool readParameters(const Parms& parms) override;
    bool writeParameters(Parms& parms) const override;

    bool initialize(bool force = false) override;
    bool voltage(int ADChannel, double& voltage);
    bool registerSensor(ComediSensor * sensor);

    bool valid() const override { return _valid; }
    
    double voltage(int id);

    static constexpr Version version = Version(1,0);
    
    double integrationTime() const;
protected:
    bool getConsistentSamplingFrequency(double * samplingFrequency);
    bool getConsistentIntegrationTime(double * integrationTimeMs);
    
    //bool initMultichannel(const QString devicePath, FORCE_ACQUISITION_MODE mode, int integrationTimeMs, unsigned int samplingFrequency, int* chanlist, int nchan);
    bool updateVoltages();  ///TODO: add a way to get more channels at once, might become useful at some point, this would require to change the init() as well

    QString _devPath = QString("/dev/comedi0");
    QString _calibrationFile = QString("cal.dat");
    double _samplingFrequency = 1e3;
    
    int _channels[16]; ///structure holding channels to be sampled by comedi, pointer to it needed during init
    int _channelMap[16]; //maps channel number to the index of placeholder in _channels, this one is being built during
    std::valarray<double> _voltages;
    std::vector<ComediSensor*> _sensors;
    bool _valid = false;
    bool _initialized = false;
    int _subdevice = 0;
    Timer acquisitionTime;
    double _voltageValidityTime = 0.0;
    
    std::unique_ptr<ComediHardwareControllerPrivate> p;
};

typedef std::shared_ptr<ComediHardwareController> ComediHardwareControllerPtr;

#endif /* COMEDICARDHARDWARECONTROLLER_HPP */

