#ifndef DUMMYSENSOR_HPP
#define DUMMYSENSOR_HPP

#include "Sensor.hpp"
#include "Version.hpp"

class DummySensor : public Sensor
{
public:
    DummySensor(QString partName, QString name, Robot& robot);

    bool initialize(bool force = true) override;

    bool readParameters(const Parms& parms) override;
    bool writeParameters(Parms& parms) const override;

    bool update() override;

    bool valid() const override;

    bool get(unsigned int id, QString& value);

    static constexpr Version version = Version(1,0);

protected:
    int _fieldNum, _rawFieldNum;
    QString _axisName;
    bool _valid;
};

typedef std::shared_ptr<DummySensor> DummySensorPtr;

#endif // DUMMYSENSOR_HPP

