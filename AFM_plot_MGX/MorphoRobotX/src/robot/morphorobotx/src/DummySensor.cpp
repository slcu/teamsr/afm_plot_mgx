#include "DummySensor.hpp"
#include "Parms.hpp"
#include "StateManager.hpp"
#include "MeasurementState.hpp"
#include "Robot.hpp"
#include "Comm.hpp"
#include "HardwareManager.hpp"

REGISTER_ROBOT_SUBUNIT_TYPE(DummySensor);

constexpr Version DummySensor::version;

DummySensor::DummySensor(QString partName, QString name, Robot& robot)
    : Sensor(partName, name, robot)
    , _valid(false)
{
    if(!registerOption("Axis", 1, ReadOnlyOption | StringOption)) return;
    _valid = true;
}

bool DummySensor::initialize(bool force)
{
    return true;
}

bool DummySensor::readParameters(const Parms& parms)
{
    _valid = false;
    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("DummyAxis [%1]").arg(partName())))
            return false;
    _fieldNum = robot().stateManager().registerFieldExc(name(), "N", true);
    _rawFieldNum = robot().stateManager().registerFieldExc(name() + "_Voltage", "V", false);
    if(!parms(partName(), "Axis", _axisName)) return false;
    _valid = true;
    return true;
}

bool DummySensor::writeParameters(Parms& parms) const
{
    if(!parms.set(partName(), "Version", version)) return false;
    if(!parms.set(partName(), "Axis", _axisName)) return false;
    return true;
}

bool DummySensor::get(unsigned int id, QString& value)
{
    switch(id)
    {
      case 1:
        value = _axisName;
        return true;
      default:
        break;
    }
    return false;
}

bool DummySensor::update()
{
    static double val = 0.0;
    val += 0.1;
    robot().stateManager().state()[_rawFieldNum] = val;
    robot().stateManager().state()[_fieldNum] = 12*val;
    return true;
}

bool DummySensor::valid() const
{
    return _valid;
}
