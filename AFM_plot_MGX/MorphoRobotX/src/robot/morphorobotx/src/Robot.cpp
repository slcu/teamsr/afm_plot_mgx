/*
 * File:   Robot.cpp
 * Author: cfmadmin
 *
 * Created on February 7, 2013, 9:42 PM
 */

#include "Robot.hpp"
#include "Experiment.hpp"
#include "Axis.hpp"
#include "Sensor.hpp"
#include "Comm.hpp"

constexpr Version Robot::version;

Robot::Robot(Experiment& experiment)
    : _experiment(experiment)
    , _stateManager(experiment.stateManager())
{
}

Robot::~Robot()
{
}

bool Robot::readParameters(const Parms& parms)
{
    //snippet how to get all keys from a section
    //isOK &= parms.all( QString section, QHash<QString, Container>& values);
    QStringList robotParts;

    Version dataVersion;

    if(parms("Robot", "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, "robot description"))
            return false;

    if(!parms.all("Robot", "Part", robotParts))
    {
        Comm::out << "Could not parse RobotRecipe section, something wrong about [Robot]->Part" << endl;
        return false;
    }
    //rintf("%1\n", robotSubunitHash.keys().at(i)
    for (int i=0; i<robotParts.size(); i++)
    {
        Comm::out << QString("robotAxes.at(%1)=%2").arg(i).arg(robotParts[i]) << endl;
        QString def = robotParts[i];
        QStringList fields = def.split(" ", QString::SkipEmptyParts);
        if(fields.size() != 2)
        {
            Comm::err << "Error, a robot part must be on the form:\n"
                           "Part = Name PartName\n"
                           "Where Name is the logical name and PartName the name of the section describing the part.\n"
                           "Incorrect robot description '" << robotParts[i] << "'" << endl;
            return false;
        }
        QString n = fields[0].trimmed();
        QString pn = fields[1].trimmed();
        Comm::out << "Trying to addPart(pn, n) pn: " << pn << " n: " << n << endl;
        RobotSubunitPtr subunit = addPart(pn, n);
        if(not (subunit and subunit->valid()))
        {
            Comm::err << "Error, Robot subunit null or invalid." << endl;
            if (subunit)
            {
                Comm::err << "Error, Robot subunit invalid. partName " <<  subunit->partName() << endl;                
            }
            else
            {
                Comm::err << "Error, Robot subunit null." << endl;
            }
            return false;
        }
    }

    Comm::out << "Finish adding everything." << endl;

    return true;
}

bool Robot::writeParameters(Parms& parms) const
{
    bool isOK = true;
    isOK &= parms.set("Robot", "Version", version);
    parms.erase("Robot", "Part");
    for(RobotSubunitPtr part: _parts)
    {
        isOK &= parms.add("Robot", "Part", QString("%1 %2").arg(part->name()).arg(part->partName()));
        if(!part->writeParameters(parms)) return false;
        QString type = hardwareManager().instanceType(part->partName());
        if(type.isEmpty())
        {
            Comm::err << "Error, cannot find the type of the part '" << part->partName() << "'" << endl;
            return false;
        }
        if(!parms.set(part->partName(), "Type", type)) return false;
    }
    return isOK;
}

bool Robot::hasPart(QString name) const
{
    for(RobotSubunitPtr part: _parts)
        if(part->name() == name)
            return true;
    return false;
}

RobotSubunitPtr Robot::addPart(QString partName, QString name)
{
    Comm::out << "inside Robot:addPart("<< partName << ", " << name <<")" << endl;
    const Parms& parms = experiment().robotParms();
    QString type;
    if(!parms(partName, "Type", type)) 
    {
        Comm::err << "Error, " << partName << " has no Type."<< endl;
        return false;
    }
    RobotSubunitPtr p = hardwareManager()(type, partName, name, *this);
    if(!p)
    {
        Comm::err << "Error, no robot subunit called " << name << " and of type " << type << endl;
        return false;
    }
    if(part(name) == p)
    {
        Comm::out << "Hint: part '" << name << "' already exists, returning pointer to it" << endl;
        return p;
    }
        
    if(addPart(p))
    {
        p->readParameters(parms);
        return p;
    }
    return RobotSubunitPtr(); // TODO: why is p gone ?
}

bool Robot::addPart(RobotSubunitPtr part)
{
    Comm::out << "inside Robot:addPart(RobotSubunitPtr)" << endl;
    if (part!=0)
    {
        for(RobotSubunitPtr other: _parts)
            if(other->name() == part->name())
            {
                Comm::err << "There are two parts with the name '" << part->name() << "'\n"
                            << "  Part 1: " << other->partName() << "\n"
                            << "  Part 2: " << other->partName() << endl;
                return false;
            }
        _parts.push_back(part);
        Comm::out << "inside Robot:addPart(RobotSubunitPtr) - OK, added part " << part->partName() << endl;
        return true;
    }
    else
    {
        Comm::err << "Error, tried to add a null part" << endl;
    }
    return false;
}

bool Robot::initialize()
{
    // browse through internal structures (controllers, axes, sensors and initialize)
    Comm::out << "Inside Robot::initialize()" << endl;
    for (size_t i=0; i<_parts.size(); i++)
    {
        if(!_parts[i]->initialize())
        {
            Comm::out << QString("ERROR: failed to initialize part %1").arg(_parts[i]->name()) << endl;
            return false;
        }
    }
    return true;
}

RobotSubunitPtr Robot::part(QString name)
{
    for(RobotSubunitPtr su: _parts)
    {
        if(su->name() == name)
            return su;
    }
    return RobotSubunitPtr();
}

///returns sensor pointer by name or null if not found
SensorPtr Robot::sensor(QString sensorName)
{
    return part<Sensor>(sensorName);
}

///returns axis pointer by name or null if not found
AxisPtr Robot::axis(QString axisName)
{
    return part<Axis>(axisName);
}

AxisPtr Robot::indentationAxis()
{
/*
 *    SensorPtr s = indentationSensor();
 *    if(s)
 *    {
 *        return axis(((ForceSensor*)s)->axisName());
 *    }
 *    Comm::out << "ERROR: indentationSensor does not exist" << endl;
 *    if (_sensors.size() > 0)
 *    {
 *        for (int i=0; i<_sensors.size(); i++)
 *        {
 *           Comm::out << QString("sensor: %1").arg(_sensors.at(i)->name()) << endl;
 *        }
 *    }
 *    else
 *    {
 *        Comm::out << "no available sensors" << endl;
 *    }
 *
 */
    return 0;
}

SensorPtr Robot::indentationSensor()
{
    return sensor("IndentationForceSensor");
}

bool Robot::remove(RobotSubunitPtr part)
{
    if(not part) return false;
    for(auto it = _parts.begin() ; it != _parts.end() ; ++it)
    {
        if(*it == part)
        {
            _parts.erase(it);
            return true;
        }
    }
    return false;
}
