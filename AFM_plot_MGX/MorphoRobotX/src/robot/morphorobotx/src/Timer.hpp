/*
 * File:   Timer.hpp
 * Author: huflejt
 *
 * Created on November 19, 2012, 6:16 PM
 */

#ifndef TIMER_HPP
#define TIMER_HPP

#include <time.h>
#include <stdint.h>
#include <stdio.h>

class Timer {
public:
    Timer();
    virtual ~Timer();
    void reset()
    {
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    unsigned long timeElapsedMs()
    {
        clock_gettime(CLOCK_MONOTONIC, &end);
        return timespecDiffMs(&end, &start);
    }
    
    double timeElapsed()
    {
        clock_gettime(CLOCK_MONOTONIC, &end);
        return timespecDiff(&end, &start);
    }

    timespec timeNow()
    {
        struct timespec now;
        clock_gettime(CLOCK_MONOTONIC, &now);
        return now;
    }
private:
    struct timespec end;
    struct timespec start;
    uint64_t timespecDiffMs(struct timespec *timeEnd_p, struct timespec *timeStart_p)
    {
        return (timeEnd_p->tv_sec - timeStart_p->tv_sec)*1000 +
                (timeEnd_p->tv_nsec - timeStart_p->tv_nsec)/1000000;
    }
    double timespecDiff(struct timespec *timeEnd_p, struct timespec *timeStart_p)
    {
        return double(timeEnd_p->tv_sec - timeStart_p->tv_sec) +
               1e-9*double(timeEnd_p->tv_nsec - timeStart_p->tv_nsec);
    }
};

#endif /* TIMER_HPP */

