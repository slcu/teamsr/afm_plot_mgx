/*
 * File:   ZaberAxis.hpp
 * Author: cfmadmin
 *
 * Created on March 5, 2013, 11:55 AM
 */

#ifndef ZABERAXIS_HPP
#define ZABERAXIS_HPP

#include "Version.hpp"
#include "Axis.hpp"
#include <vector>

class ZaberHardwareController;
typedef std::shared_ptr<ZaberHardwareController> ZaberHardwareControllerPtr;

class ZaberAxis : public Axis {
public:
    ZaberAxis(QString partName, QString name, Robot& robot);
    ZaberAxis(const Parms& parms, QString axisName, ZaberHardwareController* hardwareController);

    virtual bool readParameters(const Parms& parms) override;
    virtual bool writeParameters(Parms& parms) const override;

    bool get(unsigned int option, double& value) override;
    bool set(unsigned int option, double value) override;
    bool initialize(bool force = false) override;

    bool goHome();
    bool knowsHome(bool& result);
    bool setAcceleration(double val);
    bool setGotoPrecisionMargin(double val);
    bool setGotoTimeout(double val);
    bool disablePotentiometer();
    bool enablePotentiometer();

    double microstepSize() const { return _microstepSize; }

    bool gotoSync(double pos) override;
    bool updatePosition() override;

    bool valid() const override;

    static constexpr Version version = Version{1,0};

protected:
    int _rawPosField, _realPosField;
    ZaberHardwareControllerPtr _controller;
    QString _controllerName;
    double _microstepSize;
    bool _valid = false;
};

typedef std::shared_ptr<ZaberAxis> ZaberAxisPtr;

#endif /* ZABERAXIS_HPP */

