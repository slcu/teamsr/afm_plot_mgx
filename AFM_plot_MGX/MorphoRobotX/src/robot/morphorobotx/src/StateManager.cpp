/*
 * File:   StateManager.cpp
 * Author: cfmadmin
 *
 * Created on December 11, 2012, 6:02 PM
 */

#include "StateManager.hpp"
#include "Experiment.hpp"

QString FieldError::text() const throw()
{
    switch(error)
    {
      case DUPLICATE_FIELD:
        return QString("Trying to create field '%1' when it already exists.").arg(name);
      case INVALID_NAME:
        return QString("The name '%1' is invalid for a field name.").arg(name);
      case EXPERIMENT_RUNNING:
        return QString("Trying to create field '%1' while the experiment is running").arg(name);
      case UNKNOWN:
        return QString("Unknown error on field '%1'.").arg(name);
        break;
    }
    return "Badly constructed FieldError.";
}

StateManager::StateManager(): _logger(*this){

}

/*StateManager::StateManager(const StateManager& orig) {
}*/

StateManager::~StateManager() {
}

int StateManager::fieldIndex(QString name)
{
    int key = -1;
    key = state().fieldIndex(name);
    return key;
}

bool StateManager::renew()
{
    if(!_logger.writeCurrentState()) return false;
    _state.renew();
    return true;
}

bool StateManager::writeParameters(Parms& parms) const
{
    bool isOK = true;
    isOK &= _state.writeParameters(parms);
    return isOK;
}

int StateManager::registerField(QString name, QString unit, bool stable)
{
    if(_logger.valid()) return -3;
    return state().registerField(name, unit, stable);
}

int StateManager::registerFieldExc(QString name, QString unit, bool stable)
{
    //TODO: if the header of the log has been already written, it should be forbidden to register further fields
    int retval = registerField(name, unit, stable);
    if (retval < 0)
    {
        FieldError::Error err;
        switch(retval)
        {
        case -1:
            err = FieldError::DUPLICATE_FIELD;
            break;
        case -2:
            err = FieldError::INVALID_NAME;
            break;
        case -3:
            err = FieldError::EXPERIMENT_RUNNING;
            break;
        default:
            err = FieldError::UNKNOWN;
        }
        throw FieldError{err, name};
    }
    return retval;
}

StateManager::Field StateManager::field(QString name)
{
    return Field(fieldIndex(name), this);
}

StateManager::Field StateManager::field(int id)
{
    return Field(id, this);
}

bool StateManager::configure(QString filename)
{
    return _logger.configure(filename);
}

bool StateManager::suspend()
{
    return _logger.reset();
}
