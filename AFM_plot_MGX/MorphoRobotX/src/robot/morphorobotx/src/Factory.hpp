#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <QHash>
#include <QString>
#include <memory>
#include <typeinfo>
#include <stdlib.h>
#include "Comm.hpp"
#include <QStringList>

/**
 * \class Factory Factory.hpp "Factory.hpp"
 *
 * Generic factory.
 *
 * It is supposed to be created with:
 *
 * MAKE_FACTORY(FactoryClass, createObject);
 *
 * where FactoryName will be the name of the factory class and createObject must be a free function that creates a
 * shared pointer to an object.
 */
template <char const * const objectName, typename T, typename... Args>
class Factory
{
public:
    typedef T ObjectType;
    typedef std::shared_ptr<ObjectType> ObjectPtr;

    static char const * const object_name;

    /**
     * Internal class used to maintain the list of object makers
     */
    struct Maker
    {
        typedef ObjectType* (*MakerFct)(Args...);

        MakerFct make;

        Maker(MakerFct mk)
          : make(mk)
        { }

        ObjectPtr create(Args&&... args) const
        {
            ObjectPtr ptr;
            ptr.reset(make(std::forward<Args>(args)...));
            return ptr;
        }
    };

    /**
     * The maker should not disappear!
     */
    typedef std::shared_ptr<Maker> MakerPtr;

    template <typename T1>
    static T* makeObject(Args... args)
    {
        return new T1(std::forward<Args>(args)...);
    }

protected:
    /**
     * Creation of the factory is a private matter
     */
    Factory() { }

    /**
     * Dictionnary of all the makers by their name
     */
    QHash<QString, MakerPtr> makers;

    Factory(const Factory& copy) = delete;

    static std::unique_ptr<Factory> _instance;

public:

    /**
     * Access the singleton instance of the factory
     */
    static Factory& instance()
    {
        if(!_instance) _instance.reset(new Factory());
        return *_instance;
    }

    virtual ~Factory()
    {
    }

    /**
     * Register a new object type
     * \param name name of the object type
     * \param proc Object maker of the specific type
     * \returns False if the object maker type is already in use
     */
    bool add(QString name, MakerPtr proc)
    {
        if(makers.contains(name))
        {
            Comm::err << "Error, there is already a " << objectName << " named '" << name << "'";
            return false;
        }
        makers[name] = proc;
        return true;
    }

    /**
     * Un-register a object type
     * \param name name of the object type
     * \param proc Instance of the object maker to remove
     * \return False if the name doesn't exist or if \c proc doesn't correspond to this name
     */
    virtual bool remove(QString name, MakerPtr proc)
    {
        auto found = makers.find(name);
        if(found != makers.end())
        {
            if(found.value() == proc)
            {
                makers.erase(found);
                return true;
            }
            else
            {
                Comm::err << "Error, the " << objectName << " named '"
                            << name << "' doesn't correspond to this "
                            << objectName << " maker" << endl;
            }
        }
        else
        {
            Comm::err << "Error, there are no " << objectName << " named '" << name << "'";
        }
        return false;
    }

    /**
     * Get an object, creating it if needed
     * \param type Type of object
     * \returns A shared pointer to the object
     */
    ObjectPtr operator()(QString type, Args&&... args)
    {
        auto found = makers.find(type);
        if(found != makers.end())
        {
            return found.value()->create(std::forward<Args>(args)...);
        }
        Comm::err << "Error, no " << objectName << " of type " << type << endl;
        return nullptr;
    }

    /**
     * Return the name of the class corresponding to the given type
     */
    QString typeClass(QString type) const
    {
        return typeid(type).name();
    }
};

template <char const * const objectName, typename T, typename... Args>
char const * const Factory<objectName,T,Args...>::object_name = objectName;

template <char const * const objectName, typename T, typename... Args>
std::unique_ptr<Factory<objectName,T,Args...>> Factory<objectName,T,Args...>::_instance{nullptr};


/**
 * InstanceFactory Factory.hpp "Factory.hpp"
 *
 * Factory that also manage instances created. The creation function now has to take a QString as first argument, which
 * will be set to the name of the object.
 *
 * It is supposed to be created with:
 *
 * MAKE_INSTANCE_FACTORY(FactoryClass, createObject);
 */
template <char const * const objectName, typename T, typename... Args>
class InstanceFactory : public Factory<objectName, T, QString, Args...>
{
    typedef Factory<objectName, T, QString, Args...> BaseClass;
public:
    typedef T ObjectType;
    typedef std::shared_ptr<ObjectType> ObjectPtr;

    typedef typename BaseClass::Maker Maker;
    typedef typename BaseClass::MakerPtr MakerPtr;

protected:
    struct TypedObject
    {
        ObjectPtr object;
        QString type;
    };

    /**
     * Dictionnary of all the instances, by their name.
     */
    QHash<QString, TypedObject> instances;

    InstanceFactory()
      : BaseClass()
    { }
    InstanceFactory(const InstanceFactory& copy) = delete;

public:
    /**
     * Access the singleton instance of the factory
     */
    static InstanceFactory& instance()
    {
        if(!BaseClass::_instance) BaseClass::_instance.reset(new InstanceFactory());
        return static_cast<InstanceFactory&>(*BaseClass::_instance);
    }

    QString instanceType(QString name) const
    {
        auto found = instances.find(name);
        if(found == instances.end()) return QString();
        return found.value().type;
    }

    /**
     * Get an object from the factory.
     *
     * Each factory can only have unique object with a given name. And if the types don't correspond, a null pointer is
     * returned.
     *
     * \param type Type of the object to return
     * \param name Name of theobject
     * \param args Construction arguments
     * \param wrong_type If provided, the boolean will be set to true in case there was a type error
     */
    ObjectPtr operator()(QString type, QString name, Args ...args, bool* wrong_type = 0)
    {
        if(wrong_type) *wrong_type = false;
        auto found = instances.find(name);
        if (found != instances.end())
        {
            QString objectType = found.value().type;
            if(type != objectType)
            {
                if(wrong_type) *wrong_type = true;
                Comm::err << QString("Error, %1 '%2' exists but is of class '%3' when a %1 of class '%4' was requested")
                    .arg(objectName)
                    .arg(name)
                    .arg(objectType)
                    .arg(type) << endl;
                return nullptr; // The protocol exist, but is of the wrong type
            }
            return found.value().object;
        }
        else
        {
            ObjectPtr obj = BaseClass::operator()(type, std::move(name), std::forward<Args>(args)...);
            if(obj)
                instances.insert(name, TypedObject{obj, type});
            return obj;
        }
    }

    /**
     * Forget about a given instance's name
     *
     * \note This won't delete the object unless nothing else is holding onto it
     */
    bool forget(QString name)
    {
        auto found = instances.find(name);
        if(found != instances.end())
        {
            instances.erase(found);
            return true;
        }
        return false;
    }

    /**
     * Remove a type, and erase all its instances
     */
    bool remove(QString name, MakerPtr proc) override
    {
        if(BaseClass::remove(name, proc))
        {
            QStringList to_delete;
            for(auto it = instances.begin() ; it != instances.end() ; ++it)
            {
                if(it.value().type == name)
                    to_delete << it.key();
            }
            for(QString name : to_delete)
                instances.remove(name);
            return true;
        }
        return false;
    }

};

/**
 * That's the base class
 */
template <typename Factory>
class FactoryRegistration
{
public:
    typedef typename Factory::Maker Maker;
    typedef typename Maker::MakerFct MakerFct;

private:
    typedef std::shared_ptr<Maker> MakerPtr;

    QString name;
    MakerPtr maker;

public:
    FactoryRegistration(QString n, MakerFct mk)
      : name(n)
      , maker(std::make_shared<Maker>(mk))
    {
        Comm::out << "Registration of " << Factory::object_name << " " << n << endl;
        if(!Factory::instance().add(name, maker))
            ::abort();
    }

    ~FactoryRegistration()
    {
        Comm::out << "Removing registration of " << Factory::object_name << " " << name << endl;
        if(!Factory::instance().remove(name, maker))
            ::abort();
    }
};


template <char const* const objectName, typename T, typename... Args>
Factory<objectName,T,Args...> makeFactory(T* (*)(Args...));

template <char const* const objectName, typename T, typename... Args>
InstanceFactory<objectName,T,Args...> makeInstanceFactory(T* (*)(QString, Args...));

/**
 * \define MAKE_FACTORY(FactoryClass, objectName, createObject)
 *
 * This definition creates a new factory called \c FactoryClass.
 *
 * \param FactoryClass Name of the class created
 * \param objectName Constant variable containing the name of the objects stored in the factory
 * \param createObject This is a model of function used to create objects. The function needs not exist.
 *
 * Example:
 *
 * \code
 * class Protocol { ... };
 * typedef std::shared_ptr<Protocol> ProtocolPtr;
 * extern const char[] protocol_name;
 * extern ProtocolPtr makeProtocol(QString name, Experiment& experiment);
 * MAKE_FACTORY(ProtocolFactory, protocol_name, makeProtocol);
 * \endcode
 *
 * \relates Factory
 */
#define MAKE_FACTORY(FactoryClass, objectName, createObject) \
  typedef decltype(makeFactory<objectName>(createObject)) FactoryClass

/**
 * \define MAKE_INSTANCE_FACTORY(FactoryClass, objectName, createObject)
 *
 * This definition creates a new instance factory called \c FactoryClass.
 *
 * \param FactoryClass Name of the class created
 * \param objectName Constant variable containing the name of the objects stored in the factory
 * \param createObject This is a model of function used to create objects. The function needs not exist.
 *
 * Example:
 *
 * \code
 * class Protocol { ... };
 * typedef std::shared_ptr<Protocol> ProtocolPtr;
 * extern const char[] protocol_name;
 * extern ProtocolPtr makeProtocol(QString name, Experiment& experiment);
 * MAKE_INSTANCE_FACTORY(ProtocolFactory, protocol_name, makeProtocol);
 * \endcode
 *
 * \relates Factory
 */
#define MAKE_INSTANCE_FACTORY(FactoryClass, objectName, createObject) \
  typedef decltype(makeInstanceFactory<objectName>(createObject)) FactoryClass

/**
 * \define REGISTER_FACTORY_OBJECT(FactoryClass, name, makeFct)
 *
 * Register the maker \c makeFct as \c name to the factory class \c FactoryClass
 */
#define REGISTER_FACTORY_OBJECT(FactoryClass, name, makeFct) \
  static FactoryRegistration<FactoryClass> makeFct##FactoryClass##_registration(name, makeFct)

/**
 * \define REGISTER_FACTORY_OBJECT_TYPE(FactoryClass, ObjectClass)
 *
 * Register the class \c ObjectClass as a creator for the factory \c FactoryClass.
 */
#define REGISTER_FACTORY_OBJECT_TYPE(FactoryClass, ObjectClass) \
  static FactoryRegistration<FactoryClass> ObjectClass##FactoryClass##_registration(#ObjectClass, &FactoryClass::makeObject<ObjectClass>)

#endif // FACTORY_HPP
