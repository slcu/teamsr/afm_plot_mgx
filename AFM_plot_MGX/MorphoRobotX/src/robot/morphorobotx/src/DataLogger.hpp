/*
 * File:   DataLogger.hpp
 * Author: huflejt
 *
 * Created on November 7, 2012, 11:27 AM
 */

#ifndef DATALOGGER_HPP
#define DATALOGGER_HPP

#include <QString>
#include <QFile>
#include <QTextStream>
//#include "MeasurementState.hpp"
//#include "StateManager.hpp"
#include <sstream>
#include <iostream>
#include "Timer.hpp"


class StateManager;

/**
 * @class DataLogger DataLogger.hpp "DataLogger.hpp"
 *
 */
class DataLogger {
public:
    explicit DataLogger(StateManager& stateManager);
    /*DataLogger(const DataLogger& orig);*/
    virtual ~DataLogger();

    bool writeCurrentState();

    bool valid() const { return _isHeaderPrepared; } //isEmpty means invalid

    /**
     * Reset the logger
     */
    bool reset();

    /**
     * Configure the data logger
     *
     * \param filename File to write the data to
     */
    bool configure(QString filename);

protected:
    bool prepareFileHeader();//call that from writeCurrentState() if not yet called

    QString _filename;
    //MeasurementState* state;
    StateManager& _stateManager;
    QFile _file;
    QTextStream _stream;
    bool _isHeaderPrepared;
    int _fieldIDTime;
    Timer _timer;

};

#endif /* DATALOGGER_HPP */

