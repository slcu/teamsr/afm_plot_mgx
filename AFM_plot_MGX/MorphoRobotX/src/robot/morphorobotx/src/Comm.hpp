#ifndef COMM_HPP
#define COMM_HPP

#include <QTextStream>
#include <QString>

#ifndef COMM_CPP
#  ifdef stdout
#    undef stdout
#    undef stderr
#  endif
#  define stdout  ERROR_YOU_SHOULD_NOT_USE_STDOUT
#  define stderr  ERROR_YOU_SHOULD_NOT_USE_STDERR
#endif

/**
 * \namespace Comm
 *
 * This namespace contains fluxes and functions to use for various input/output with the user.
 *
 */
namespace Comm
{
/**
 * Stream for terminal user output.
 */
extern QTextStream user;
/**
 * Flux for output (may fall on a file)
 */
extern QTextStream out;
/**
 * Flux for error output
 */
extern QTextStream err;

/**
 * Redirect the error stream to a file
 */
void setError(QString filename, bool append);

/**
 * Redirect the standard error to the terminal
 */
void setStdError();

/**
 * Redirect the output to a file
 */
void setOutput(QString filename, bool append);

/**
 * Redirect the output to the terminal
 */
void setStdOutput();

/**
 * Pause the application, waiting for the user to press enter
 *
 * The text "Press Enter to continue..." will always be added on a separate line from the text.
 */
void pause(QString text = QString());

/**
 * Ask the user for a string
 */
QString ask(QString text);

#define LOG(x) ::Comm::_log(__FILE__, __LINE__, __PRETTY_FUNCTION__, x)

void _log(const char filename[], size_t line_number, const char function[], QString text);

void _init_log();

struct QStringException : public std::exception
{
    QStringException()
        : std::exception()
        , _w(0)
    { }
    QStringException(const QStringException& copy)
      : std::exception(copy)
      , _w(0)
    { }
    ~QStringException() throw() { delete [] _w; }

    virtual QString text() const throw() = 0;

    const char* what() const throw() override;

private:
    mutable char* _w;
};

}

#endif // COMM_HPP

