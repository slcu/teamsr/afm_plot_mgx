#include "Experiment.hpp"
#include <QCoreApplication>
#include "Comm.hpp"
#include <sysexits.h>
#include <QLibrary>
#include <QDir>
#include <QFile>
#include "Protocol.hpp"
#include "HardwareManager.hpp"
#include <getopt.h>
#include <signal.h>

QDir prefix()
{
    QDir dir(QCoreApplication::applicationDirPath());
    dir.cdUp();
    return dir;
}

int loadExtensions()
{
    // First, find the application path
    QDir dllDir = prefix();

    if(dllDir.cd("share") and
       dllDir.cd("MorphoRobotX"))
    {
        Comm::out << "dllDir = " << dllDir.absolutePath() << endl;
        // Now, load all libraries
        QStringList files = dllDir.entryList(QStringList() << "*.so",
                                             QDir::Files | QDir::Readable);
        for(QString filename : files)
        {
            QLibrary dll(dllDir.filePath(filename));
            Comm::out << "Trying to load " << filename << endl;
            if(dll.isLoaded())
                Comm::out << "Dll already loaded" << endl;
            else if(not dll.load())
            {
                Comm::err << "Error loading library '" << filename << "':\n"
                            << dll.errorString() << endl;
            }
            else
                Comm::out << "Success!!" << endl;
        }
    }
    return 0;
}

Experiment * experiment = 0;

int launchExperiment()
{
    try
    {
        experiment = new Experiment();

        if(not experiment->initialize())
        {
            Comm::err << "Error initializing the experiment" << endl;
            return EX_USAGE;
        }
    }
    catch(const std::exception& ex)
    {
        Comm::err << "Error during initialization of the experiment:\n"
                    << ex.what() << endl;
        return EX_USAGE;
    }

    try
    {
        if(not experiment->configure())
        {
            Comm::err << "Failed while writing experiment parameter files" << endl;
            return EX_SOFTWARE;
        }
    }
    catch(const std::exception& ex)
    {
        Comm::err << "Error while writing experiment parameter files:\n"
                    << ex.what() << endl;
        return EX_SOFTWARE;
    }

    try
    {
        if(not experiment->run())
        {
            Comm::err << "Error running the experiment" << endl;
            return EX_SOFTWARE;
        }
    }
    catch(const std::exception& ex)
    {
        Comm::err << "Error while running the experiment:\n"
                    << ex.what() << endl;
        return EX_SOFTWARE;
    }

    return 0;
}

int printPrefix()
{
    QDir dir = prefix();
    if(dir.exists())
        Comm::user << prefix().canonicalPath();
    else
    {
        Comm::err << "Error, no value prefix found" << endl;
        return EX_SOFTWARE;
    }
    return 0;
}

void catch_break(int sig)
{
    Comm::user << "***** BREAK INTERCEPTER ****" << endl;
    Comm::user << "*****    Signal : " << strsignal(sig) << endl;
    if(experiment) delete experiment;
    experiment = 0;
    exit(1);
}

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationName("MorphoRobotX");
    QCoreApplication::setApplicationVersion("1.0");
    QCoreApplication::setOrganizationName("MorphoRobotX");
    QCoreApplication::setOrganizationName("morphorobotx.org");

    QStringList args = QCoreApplication::arguments();
    args.pop_front();
    for(QString opt: args)
    {
        if(opt == "--prefix")
            return printPrefix();
        else
        {
            Comm::err << "Error, unknown option '" << opt << "'" << endl;
            return EX_USAGE;
        }
    }

    Comm::err << "Trying to intercept signals" << endl;

    int no_sigs[] = { SIGHUP,
                      SIGINT,
                      SIGQUIT,
                      SIGILL,
                      SIGABRT,
                      SIGFPE,
                      //SIGSEGV,
                      SIGPIPE,
                      SIGTERM
                    };
    for(size_t i = 0 ; i < sizeof(no_sigs) / sizeof(int) ; ++i)
        if(signal(no_sigs[i], catch_break) == SIG_ERR)
            Comm::err << "#!#! Error, cannot catch " << strsignal(no_sigs[i]) << endl;

    Comm::_init_log();

    int err = loadExtensions();
    if(err)
        return err;

    Comm::out << "Extensions loaded" << endl;

    return launchExperiment();
}
