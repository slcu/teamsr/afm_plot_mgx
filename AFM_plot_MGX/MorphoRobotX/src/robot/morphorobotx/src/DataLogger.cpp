/*
 * File:   DataLogger.cpp
 * Author: huflejt
 *
 * Created on November 7, 2012, 11:27 AM
 */

#include "DataLogger.hpp"
#include "StateManager.hpp"
#include "Comm.hpp"

DataLogger::DataLogger(StateManager& stateManager): _stateManager(stateManager)
{
    //this->state = state;
    //this->filename = filename;
    //this->file.setFileName(filename);
    _isHeaderPrepared = false;
    _fieldIDTime   = _stateManager.registerFieldExc(QString("LoggerTime"), QString("s"), false);
    _timer.reset();
}

/*DataLogger::DataLogger(const DataLogger& orig) {
}*/

DataLogger::~DataLogger() {
}

bool DataLogger::reset()
{
    _isHeaderPrepared = false;
    _filename = QString();
    if(_file.isOpen()) _file.close();
    return true;
}

bool DataLogger::configure(QString filename)
{
    _filename = filename;
    _file.setFileName(filename);
    prepareFileHeader();
    Comm::out << QString("setting filename to %1").arg(filename) << endl;
    return true;
}

bool DataLogger::prepareFileHeader()
{
    QStringList headers = _stateManager.state().getHeaders();

    if(!_file.open(QIODevice::WriteOnly))
    {
        Comm::out << QString("DataLogger::PrepareFileHeader() failed, cannot open file %1 for writing ").arg(_filename) << endl;
        return false;
    }
    _stream.setDevice(&_file);
    _stream << headers.join(",") << endl;
    _isHeaderPrepared = true;
    return true;
}

bool DataLogger::writeCurrentState()
{
    if(!this->valid())
    {
        LOG("Using invalid data logger");
        return false;
    }

    _stateManager.state()[_fieldIDTime] = _timer.timeElapsed(); //TODO: consider absolute time

    QStringList currentValues;

    if(!_stateManager.state().dumpCurrentValues(currentValues))
    {
        LOG("stateManager.state.getCurrentValues(currentValues) failed");
        return false;
    }

    _stream << currentValues.join(",") << endl;

    return true;
}
