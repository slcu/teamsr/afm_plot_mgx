/* 
 * File:   MCSControl.hpp
 * Author: cfmadmin
 *
 * Created on August 22, 2013, 7:57 PM
 */

#ifndef MCSCONTROLLLER_HPP
#define	MCSCONTROLLLER_HPP

#include "RobotSubunit.hpp"
#include "Version.hpp"
#include "SmaractHardwareController.hpp"
#include <MCSControl.h>


class MCSController : public RobotSubunit
{
public:
    MCSController(QString partName, QString name, Robot& robot);
    
    virtual bool readParameters(const Parms& parms) override;
    virtual bool writeParameters(Parms& parms) const override;
    
    bool get(unsigned int option, double& value) override;
    bool set(unsigned int option, double value) override;
    bool initialize(bool force = false) override;
    bool valid() const override;
    
    static constexpr Version version = Version{1,0};
    
    bool registerSmaractController(SmaractHardwareController* controller);
    bool numberOfInitializedSystems(unsigned int & initializedSystems);
    bool getSystemID(unsigned int systemIndex, unsigned int * systemID);
    
protected:
    
    bool clearInitSystemsList();
    bool makeSystemsList();
    bool initSystems();
            
    bool _valid;
    std::vector<SmaractHardwareController*> _controllers;
    bool _initialized;

};

#endif	/* MCSCONTROLLLER_HPP */

