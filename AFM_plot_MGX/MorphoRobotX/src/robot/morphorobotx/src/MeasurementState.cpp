/*
 * File:   MeasurementState.cpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 3:54 PM
 */

#include "MeasurementState.hpp"
#include <vector>
#include <limits>
#include "Timer.hpp"
#include "Parms.hpp"


constexpr double MeasurementState::double_nan;

const char* UnknownKeyException::what() const throw()
{
    return "The key used to access the state dowsn't exists.";
}

MeasurementState::MeasurementState() {
    //init();
}

//MeasurementState::MeasurementState(const MeasurementState& orig) {
//}

MeasurementState::~MeasurementState() {
}

int MeasurementState::registerField(QString name, QString unit, bool s)
{
    bool nameAlreadyExists = false;
    
    if(name.isEmpty()) return -2;
    if(not name[0].isLetter()) return -2;
    for(QChar c: name)
        if(!(c.isLetterOrNumber() or c == QChar('_')))
            return -2;
    

    for (size_t i=0; i<names.size(); i++)
    {
        if (names.at(i).compare(name) == 0)
        {
            nameAlreadyExists = true;
            break;
        }
    }

    if (nameAlreadyExists)
    {
        //TODO: what to do here? update fields or report error ?
        return -1;
    }
    else
    {
        names.push_back(name);
        units.push_back(unit);
        stability.push_back(s);
        values.push_back(double_nan);
        return names.size()-1; //returns the index of registered field
    }
}

QStringList MeasurementState::getHeaders() const
{
    QStringList list;

    for (size_t i=0; i<names.size(); i++)
    {
        list.push_back(names.at(i));
        //list << names.at(i) + units.at(i);
    }

    return list;
}

void MeasurementState::renew()
{
    for (size_t i=0; i<names.size(); i++)
    {
        if(not stable(i)) invalidate(i);
    }
}

static QString valueToString(double d)
{
    if(isnan(d)) return QString();
    return QString::number(d, 'g', 15);
}

bool MeasurementState::dumpCurrentValues(QStringList& stringList)
{
    bool retval = true;

    if (retval) /// if all fields valid
    {
        stringList.clear();
        for (size_t i=0; i<names.size(); i++)
        {
            stringList.push_back(valueToString(values.at(i)));
            if(not stable(i)) invalidate(i);
        }
    }
    return retval;
}

const double& MeasurementState::operator[](int key) const
{
    if(key >= 0 and key < (int)values.size())
       return values[key];
    throw UnknownKeyException();
}

double& MeasurementState::operator[](int key)
{
    if(key >= 0 and key < (int)values.size())
       return values[key];
    throw UnknownKeyException();
}

int MeasurementState::fieldIndex(QString name)
{
    int key = -1;
    for (size_t i=0; i<names.size(); i++)
    {
        if (names.at(i).compare(name) == 0)
        {
            key = i; //name found at i
            break; // don't look for further occurences
        }
    }
    return key;
}

bool MeasurementState::writeParameters(Parms& parms) const
{
    bool isOK = true;
    parms.erase("Data");
    for(size_t i = 0 ; i < values.size() ; ++i)
    {
        isOK &= parms.add("Data", "Column", names[i]);
        isOK &= parms.add("Data", "Unit", units[i]);
        isOK &= parms.add("Data", "Stable", stability[i]);
    }
    if(isOK)
    {
        if(!parms.setComment("Data", "Description of the content of the data.csv file")) return false;
        if(!parms.setComment("Data", "Column", "Name of the columns")) return false;
        if(!parms.setComment("Data", "Stable", "Boolean indicating if the data is stable or not.\n"
                             "If a data is not stable, it is reset to invalid (i.e. empty position)\n"
                             "on each new line. Stable values are not reset between lines."))
            return false;
        if(!parms.setComment("Data", "Unit", "Unit for the column value")) return false;
    }

    return isOK;
}
