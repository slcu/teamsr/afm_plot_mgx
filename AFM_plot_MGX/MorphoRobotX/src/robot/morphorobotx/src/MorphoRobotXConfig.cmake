EXECUTE_PROCESS(COMMAND MorphoRobotX --prefix OUTPUT_VARIABLE MorphoRobotX_PREFIX)

SET(MorphoRobotX_INCLUDE_DIR ${MorphoRobotX_PREFIX}/include/morphorobotx CACHE PATH "Include folder for MorphoRobotX")
SET(MorphoRobotX_LIB_DIR ${MorphoRobotX_PREFIX}/lib CACHE PATH "Library folder for MorphoRobotX")
SET(MorphoRobotX_EXTENSION_DIR ${MorphoRobotX_PREFIX}/share/MorphoRobotX CACHE PATH "Path to the extensions")

SET(MorphoRobotX_LIBRARIES morphorobotx)

SET(CMAKE_INSTALL_RPATH ${MorphoRobotX_EXTENSION_DIR})
#SET(MorphoRobotX_INSTALL_TO_DEFAULT TRUE CACHE BOOL "Install extension to default folder")
#IF(MorphoRobotX_INSTALL_TO_DEFAULT)
  #SET(CMAKE_INSTALL_PREFIX ${MorphoRobotX_PREFIX} CACHE PATH "Install prefix" FORCE)
#ENDIF()
LINK_DIRECTORIES(${MorphoRobotX_LIB_DIR})
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

