/*
 * File:   SmaractCommands.hpp
 * Author: cfmadmin
 *
 * Created on February 20, 2013, 9:38 AM
 */

#ifndef SMARACTCOMMANDS_HPP
#define SMARACTCOMMANDS_HPP

#include "MCSControl.h"
#include "QString"
#include "QHash"
#include <functional>
#include "Timer.hpp"
#include "Comm.hpp"

namespace smaract
{
class Result {
public:
    constexpr Result() { }
    constexpr Result(SA_STATUS status)
      : _status(status)
    { }

    constexpr operator bool()
    {
        return _status == SA_OK;
    }

    QString getDescription() const;
    constexpr SA_STATUS status() { return _status; }

    static const QHash<SA_STATUS, QString> StatusMap;
protected:
    SA_STATUS _status = SA_OK;
};

/**
 * Sends a command to MCSControl library.
 * 
 * @param cmd - a function you want to be called
 * @param repeat - number of repeats, 0 means infinite, limited by timeout only
 * @param timeout - timeout in seconds, 0 means infinite, limited by repeats only
 * @return Result - an object of class Result describing the last result returned by a smaract function.
 */
template <typename Fct>
Result send(Fct cmd, unsigned int repeat, double timeout = 0)
{    
    Timer timer;
    timer.reset();    
    
    double time = -1;    
    unsigned int rep = 0;    
    auto result = Result{cmd()};
    if (not result) Comm::out << "\tsmaract::send(...) - result is: " << result.getDescription() << endl;
    //Comm::out << "boolean value of the result is: " << (bool)result << endl;    
    if (timeout > 0) time = timer.timeElapsed();   
    while((not result) and (time < timeout) and (rep <= repeat))
    {        
        switch (result.status())
        {
            case SA_WRITE_ERROR:                
                break;                
            case SA_READ_ERROR:                                
                break;                
            default:
                break;
//            return result;
        };
        Comm::out << "\tsmaract::send(...) -  retrying sending..." << endl;
        result = Result{cmd()};
        Comm::out << "\tsmaract::send(...) -  retried sending - result is: " << result.getDescription() << endl;
        if (repeat == 0) --rep;
        rep++;
        if (timeout > 0) time = timer.timeElapsed();
    }
    return result;
}
/**
 * Calls cmd and retries a number of times if needed.
 * 
 * @param cmd - the function to be called
 * @param repeat - how many additional retries should be attempted in case of error, zero means infinite
 * @return Result - an object of class Result describing the last result returned by a smaract function.
 */
template <typename Fct>
Result sendRepeat(Fct cmd, unsigned int repeat)
{
    return send(cmd, repeat);
}

/**
 * Calls cmd and in case of errors it retries as many times it can within the timeout period.
 * @param cmd - the function to be called
 * @param timeout - timeout in seconds, zero means infinite
 * @return Result - an object of class Result describing the last result returned by a smaract function.
 */
template <typename Fct>
Result sendTimeout(Fct cmd, double timeout)
{
    return send(cmd, 0, timeout);
}


}
#endif /* SMARACTCOMMANDS_HPP */

