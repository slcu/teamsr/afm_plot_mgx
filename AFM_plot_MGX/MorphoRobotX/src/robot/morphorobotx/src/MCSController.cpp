/* 
 * File:   MCSControl.cpp
 * Author: cfmadmin
 * 
 * Created on August 22, 2013, 7:57 PM
 */

#include "MCSController.hpp"
#include "SmaractHardwareController.hpp"
#include "Comm.hpp"
#include "Robot.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include "SmaractCommands.hpp"

REGISTER_ROBOT_SUBUNIT_TYPE(MCSController);

constexpr Version MCSController::version;

MCSController::MCSController(QString partName, QString name, Robot& robot)
    : RobotSubunit(partName, name, robot)
    , _valid(false)
    , _initialized(false)
{
    
}

bool MCSController::readParameters(const Parms& parms)
{
    Comm::out << "Inside MCSController::readParameters()" << endl;
    _valid = false;
    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
    {
        if(not version.canReadData(dataVersion, QString("MCSController [%1]").arg(partName())))
        {
            Comm::out << "Inside MCSController::readParameters() - cannot read data due to version" << endl;
            return false;
        }
    }
    else
    {
        Comm::out << "Inside MCSController::readParameters() - Version not specified" << endl;
    }
    
    //TODO: read more params
    
    Comm::out << "Inside MCSController::readParameters() - everything OK" << endl;
    _valid = true;
    return true;    
}

bool MCSController::writeParameters(Parms& parms) const
{
    Comm::out << "Inside MCSControlelr::writeParameters()" << endl;
    bool isOK = true;
    isOK &= parms.set(partName(), "Version", version);
    //TODO: more params here
    return isOK;
}

bool MCSController::set(unsigned int option, double value)
{
    //TODO: implement
    return false;
}

bool MCSController::get(unsigned int option, double& value)
{
    //TODO: implement
    return false;
}
/**
 * Prepares for initialization and initializes all registered SmaractHardwareControllers. Creation of lists and initialization is handled inside.
 * 
 * @param force - ignored
 * @return True for success, false for failure.
 */
bool MCSController::initialize(bool force)
{
    Comm::out << "Inside MCSController::initialize()" << endl;
    //Comm::err << "MCSControl::initialize() failed - not implemented yet" << endl;
    if(_initialized)
    {
        Comm::out << "Inside MCSController::initialize() - already initialized, returning" << endl;
        //TODO: rather than saying OK, check if all units (controllers) are initialized and happy
        return true;
    }
    else
    {
        Comm::out << "Inside MCSController::initialize() - not yet initialized, trying to initialize..." << endl;
    }
    //1. make a list of units to initialize
    if (not clearInitSystemsList()) 
    {
        Comm::err << "Inside MCSController::initialize() - clearInitsystemsList() failed" << endl;
        return false;
    }
    if (not makeSystemsList())
    {
        Comm::err << "Inside MCSController::initialize() - makeSystemsList() failed" << endl;
        return false;
    }    
    //2. initialize
    if (not initSystems())
    {
        Comm::err << "Inside MCSController::initialize() - initSystems() failed" << endl;
        return false;
    }
    _initialized = true;
    //3. check how many systems are initialized and if it matches the requested number (_controllers.size())
    unsigned int initializedSystems = 0;
    if (not numberOfInitializedSystems(initializedSystems))
    {
        Comm::err << "Inside MCSController::initialize() - numberOfInitializedSystems() failed" << endl;
        return false;
    }
    if (initializedSystems != _controllers.size())
    {
        Comm::out << "ERROR: MCSController::initialize() failed: number of initialized systems different from requested" << endl;
        Comm::out << QString(" - number of initialized systems: %1, number of requested systems: %2").arg(initializedSystems, _controllers.size()) << endl;
        return false;
    }
    //4. now, each sub-component needs to check itself inside of its initialize() method, if its parent returned true     
    _initialized = true;    
    //TODO: what about _valid ?
    return _initialized;
}

bool MCSController::valid() const
{
    return _valid;
}
/**
 * Registers a SmaractHardwareController for initialization. Do it before calling makeSystemsList().
 * @param controller - Pointer to the controller to be registered.
 * @return True for success, false for failure.
 */
bool MCSController::registerSmaractController(SmaractHardwareController* controller)
{   
    if (not controller)
    {
        Comm::err << "Error, MCSControl::registerSmaractController(), trying to register a null pointer" << endl;
        return false;
    }
    _controllers.push_back(controller); //push back or insert into a set ?
    Comm::out << "MCSController::registerSmaractController() _controllers.size() = " << _controllers.size() << endl;
    //TODO: what else besides adding to a list ?
    return true; 
}

/**
 * Clears the list of systems to be initialized. The list is being erased in the hardware.
 * @return True for success, false for failure.
 */
bool MCSController::clearInitSystemsList()
{
    Comm::out << "inside MCSController::clearInitSystemsList()" << endl;
    auto result = smaract::sendRepeat(SA_ClearInitSystemsList, 3);
    if (not result)
    {                
        Comm::err << "MCSController::clearInitSystemsList() - SA_ClearInitSystemsList() returned " << result.getDescription() << endl;
        return false;
    }
    Comm::out << "MCSController::clearInitSystemsList() - SA_ClearInitSystemsList() returned SA_OK" << endl;
    return true;
}

/**
 * Makes a list of systems to be initialized. The list is being automatically prepared from the registered smaract controllers and is being transmitted to the hardware.
 * @return True for success, false for failure.
 */
bool MCSController::makeSystemsList()
{    
    for (size_t i=0; i<_controllers.size(); i++)
    {   //Comm::out << QString("adding _hardwareIDs[%1] (%2) to init list").arg(i, _hardwareIDs[i]) << endl;
        //Comm::out.flush();
        Comm::out << "MCSController::makeSystemsList() - trying to add " << _controllers[i]->hardwareID() << "to systems list..." << endl;
        auto result = smaract::sendRepeat(std::bind(SA_AddSystemToInitSystemsList, _controllers[i]->hardwareID()), 1);
        if (not result)
        {
            Comm::err << QString("ERROR: MCSController::makeSystemsList() failed: unable to add SmarAct system %1 to init list").arg(_controllers[i]->hardwareID()) << endl;
            Comm::err << "Reason: " << result.getDescription() << endl;                    
            return false;
        }
        else
        {
            Comm::out << "MCSControlelr::makeSystemsList() - SA_AddSystemToInitSystemsList returned SA_OK for system: " << _controllers[i]->hardwareID() << endl;
        }
    }
    return true;
}

/**
 * Initializes all registered smaract controllers from the systems list at once. Call clearInitSystemslist() and makeSystemsList() first.
 * Usually this operation takes about 10 seconds.
 * @return True for success, false for failure.
 */
bool MCSController::initSystems()
{
    Comm::out << "MCSController::initSystems() - please wait, this usually takes a few seconds..." << endl;
    auto result = smaract::sendRepeat(std::bind(SA_InitSystems, SA_SYNCHRONOUS_COMMUNICATION), 3);      
    if (!result)
    {
        Comm::err << "ERROR: tried 3 times, but could not send command to initialize smaract controllers. Reason: " << result.getDescription() << endl;
        return false;
    }   
    else
    {
        Comm::out << "MCSController::initSystems() - Command_SA_InitSystems returned true" << endl;
    }
    _initialized = true;
    return true;
}

/**
 * Returns the number of initialized smaract systems within the mcscontrol driver. This method can only be called when the mcscontrol is in initialized state.
 * @param initializedSystems - a number of initialized units
 * @return True for success, false for failure.
 */
bool MCSController::numberOfInitializedSystems(unsigned int & initializedSystems)
{
    if (not _initialized) 
    {
        Comm::err << "ERROR: MCSController::numberOfInitializedSystems() can only be called if the mcscontrol system is already initialized." << endl;
        return false; //command can be used only if the mcscontrol driver has been initialized    
    }
    auto result = smaract::sendRepeat(std::bind(SA_GetNumberOfSystems, &initializedSystems), 3);
    if (!result)
    {
        Comm::err << "ERROR: MCSController::numberOfInitializedSystems() failed. Reason: " << result.getDescription() << endl;
        return false;
    }   
    else
    {
        Comm::out << "MCSController::numberOfInitializedSystems() - OK" << endl;
    }
    return true;
}

/**
 * Gets the systemID of a system addressed by systemIndex. For example, 
 * at systemID 0 there might be a smaract controller with systemID 4153533893.
 * @param systemIndex - Zero-based index of the controller to be queried.
 * @param systemID - pointer to an unsigned int systemID number (usually related to serial number of the controller box).
 * @return True for success, false for failure.
 */
bool MCSController::getSystemID(unsigned int systemIndex, unsigned int * systemID)
{   
    if (not _initialized) return false;
    auto result = smaract::sendRepeat(std::bind(SA_GetSystemID, systemIndex, systemID), 3);   
    if (!result)
    {
        Comm::err << "ERROR: tried 3 times, but could not send command get systemID. Reason: " << result.getDescription() << endl;
        return false;
    }
    return true;
}