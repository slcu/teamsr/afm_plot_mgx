/* 
 * File:   SmaractAxis.cpp
 * Author: cfmadmin
 * 
 * Created on August 20, 2013, 6:13 PM
 */

#include "SmaractAxis.hpp"
#include "SmaractHardwareController.hpp"
#include "Comm.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include "Robot.hpp"
#include "StateManager.hpp"
#include "MeasurementState.hpp"

REGISTER_ROBOT_SUBUNIT_TYPE(SmaractAxis);

constexpr Version SmaractAxis::version;

SmaractAxis::SmaractAxis(QString partName, QString name, Robot& robot) 
    : Axis(partName, name, robot)
    , _holdMs(100)
    , _timeoutMs(2000)
    , _precisionMargin(20)
{    
    registerOption("GOTO_TIMEOUT", GOTO_TIMEOUT, ReadWriteOption|NumberOption);
    registerOption("GOTO_PRECISION_MARGIN", GOTO_PRECISION_MARGIN, ReadWriteOption|NumberOption);
    registerOption("CLOSED_LOOP_MAX_FREQUENCY", CLOSED_LOOP_MAX_FREQUENCY, WriteOnlyOption|NumberOption); //there's no function to query this setting
    registerOption("CLOSED_LOOP_SPEED", CLOSED_LOOP_SPEED, ReadWriteOption|NumberOption);
    registerOption("CLOSED_LOOP_HOLD_TIME", CLOSED_LOOP_HOLD_TIME, ReadWriteOption|NumberOption);
    setDefault(CLOSED_LOOP_HOLD_TIME, _holdMs/1000.0, false);
    setDefault(GOTO_TIMEOUT, _timeoutMs/1000.0, false);
    setDefault(GOTO_PRECISION_MARGIN, _precisionMargin*1.0e-9, false);    
}
/**
 * Sets the maximum closed-loop driving frequency for the axis.
 * @param frequency - maximum driving frequency (Hz)
 * @return True for success, false for failure.
 */
bool SmaractAxis::setClosedLoopMaxFrequency(unsigned int frequency)
{//frequency is in Hz, no conversion needed
    return _controller->setClosedLoopMaxFrequency(_smaractAxisID, frequency);
}

/**
 * Sets closed-loop speed for the axis.
 * @param speedMPerS - Speed in meters per second. Exactly zero means no limit.
 * @return True for success, false for failure.
 */
bool SmaractAxis::setClosedLoopSpeed(double speedMPerS)
{
    if (speedMPerS > 0.1) speedMPerS = 0.1; //max supported by smaract
    if (speedMPerS < 0 ) return false;   
    return _controller->setClosedLoopSpeed(_smaractAxisID, (unsigned int)(speedMPerS*1.0e9));//conversion to nanometers per second
}
/**
 * Gets closed-loop speed for the axis. The value is retrieved from the hardware.
 * @param speedMPerS - reference to speed in meters/second
 * @return True for success, false for failure.
 */
bool SmaractAxis::getClosedLoopSpeed(double& speedMPerS)
{   unsigned int speedNmPerS = 0; 
    if (not _controller->getClosedLoopSpeed(_smaractAxisID, speedNmPerS))
    {
        return false;
    }
    speedMPerS = speedNmPerS*1.0e-9; //conversion from hardware-native nm/s to m/s
    return true;
}

/**
 * Gets SmaractAxisStatus of the axis. The value is retrieved from the hardware.
 * @param status - reference to the SmaractAxisStatus object
 * @return True for success, false for failure.
 */
bool SmaractAxis::getStatus(SmaractAxisStatus& status)
{
    return _controller->getStatus(_smaractAxisID, status);
}

/**
 * Stops axis motions including holding after closed-loop commands.
 * @return  True for success, false for failure.
 */
bool SmaractAxis::stop()
{
    return _controller->stop(_smaractAxisID);
}

/**
 * Reads Parms and takes a number of actions based on the values. 
 * Axis and its controller are created. 
 * 
 * @param parms
 * @return True for success, false for failure.
 */
bool SmaractAxis::readParameters(const Parms& parms)
{
    _valid = false;
    Comm::out << "Inside SmaractAxis::readParameters()" << endl;
    Comm::out << "Reading parameters for SmaractAxis" << endl;
    Comm::out << QString(" - axisName: %1").arg(name()) << endl;
    
    Version dataVersion;    
    if(parms(partName(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("SmaractAxis [%1]").arg(partName())))
            return false;
    
    if(!parms(partName(), "Controller", _controllerName)) return false;
    Comm::out << QString(" - hardwareControllerName: %1").arg(_controllerName) << endl;    
    
    _posField = robot().stateManager().registerFieldExc(name(), "m", false); //register position field
    
    _controller = robot().addPart<SmaractHardwareController>(_controllerName, _controllerName);
    if(!_controller)
    {
        Comm::err << "Error, cannot no Smaract controller defined as part '" << _controllerName << "'" << endl;
        return false;
    }
    
    if(!parms(partName(), "SmaractAxisID", _smaractAxisID)) return false;            
    
    _valid = _controller->registerAxis(this);
    if (not _valid) Comm::err << "Error, could not registerAxis " << partName() <<" to controller " << _controllerName << endl;
    return _valid;
}
/**
 * Checks if both axis and its controller are valid.
 * @return true/false
 */
bool SmaractAxis::valid() const
{
    return _valid and _controller->valid();
}
/**
 * Writes its parameters to given parms object.
 * @param parms
 * @return True for success, false for failue.
 */
bool SmaractAxis::writeParameters(Parms& parms) const
{
    bool isOK = true;
    //Comm::out << "Inside SmaractAxis::writeParameters()" << endl;
    isOK &= parms.set(partName(), "Version", version);
    isOK &= parms.set(partName(), "Controller", _controller->partName());
    return isOK;
}

/**
 * Gets option values.
 * @param option - The SmaractAxisOption option whose value is to be examined.
 * @param value - Reference to double to hold the result value.
 * @return True for success, false for failure.
 */
bool SmaractAxis::get(unsigned int option, double& value)
{
    switch(option) //TODO: implement options
    {      
        case GOTO_TIMEOUT:
            value = _timeoutMs/1000.0; //conversion from miliseconds
            break;
        case GOTO_PRECISION_MARGIN:
            value = _precisionMargin*1.0e-9; //conversion from hardware-native-nanometers
            break;
        case CLOSED_LOOP_HOLD_TIME:
            value = _holdMs/1000; //conversion from hardware-native ms to s
            break;
        case CLOSED_LOOP_SPEED:
            return getClosedLoopSpeed(value); //no conversion needed
            break;
      default:
        return false;
    }
    return true;
}
/**
 * Sets option values.
 * @param option - The SmaractAxisOption option whose value is to be set.
 * @param value - Value to be assigned.
 * @return True for success, false for failure.
 */
bool SmaractAxis::set(unsigned int option, double value)
{
    switch(option) //TODO: implement more options
    {
        case GOTO_TIMEOUT:
            _timeoutMs = value*1000.0; //conversion from seconds to miliseconds
            return true;
            break;
        case GOTO_PRECISION_MARGIN:
            _precisionMargin = value*1.0e9; //conversion from meters to hardware-native-nanometers
            return true;
            break;        
        case CLOSED_LOOP_HOLD_TIME:
            _holdMs = value*1000.0; //conversion from seconds to hardware-native-miliseconds
            return true;
            break;
        case CLOSED_LOOP_MAX_FREQUENCY:            
            return setClosedLoopMaxFrequency(value); //no conversion, Hz is both SI-conform and hardware-native
            break;
        case CLOSED_LOOP_SPEED:
            return setClosedLoopSpeed(value); //TODO:units !
            break;
        default:
            break;
    }
    return false;
}

/**
 * Initializes the axis by passing initialization request of its controller.
 * @param force - True asks the controller to assumes the initialization cannot fail, false is a normal honest initialization.
 * @return True for success, false for failure.
 */
bool SmaractAxis::initialize(bool force)
{
    Comm::out << "Inside SmaractAxis::initialize()" << endl;
    if (!_controller->initialize(force))
    {
        return false;
    }
    if (not setDefault(CLOSED_LOOP_MAX_FREQUENCY, 18500, true)) return false;
    if (not setDefault(CLOSED_LOOP_SPEED, 0, true)) return false; //zero is no limit
    
    return true;
}

/**
 * Sends a closed-loop goto command and waits for completion. 
 * First, axis status is observed: it should arrive at SA_STOPPED or SA_HOLDING.
 * Next, position is queried until the robot arrives within _precisionMargin 
 * from the target. If timeout elapses the method quits with false.
 *  
 * @param Position target position in meters.
 * @return True for success, false for error or timeout.
 */
bool SmaractAxis::gotoSync(double position)
{
    Timer timer;    
    Comm::out << "gotoSync, position: " << position << endl;
    if (isnan(position))
    {
        Comm::err << "Error: SmaractAxis::gotoSync(...) failed to go to a NaN position" << endl;
        return false;
    }
    int32_t targetPos;
    targetPos = position*1e9; //conversion from meters to nanometers    
    int32_t pos;
    
    timer.reset();
    if (not _controller->gotoSync(_smaractAxisID, targetPos, _holdMs))
    {
        Comm::err << "gotoSync, could not _controller->gotoSync" << endl;
        return false;    
    }
    //TODO: check realtime status of the axis, wait until stopped
    SmaractAxisStatus status;
    do 
    {
        if (not _controller->getStatus(_smaractAxisID, status))
        {            
            Comm::err << "gotoSync, could not _controller->getStatus" << endl;
            return false;
        }
        if (timer.timeElapsedMs() > _timeoutMs) break;
    }while(status.status() != SA_STOPPED_STATUS and status.status() != SA_HOLDING_STATUS);
        
    while (timer.timeElapsedMs() <= _timeoutMs)
    {        
        if (not _controller->getPosition(_smaractAxisID, pos))
        {
            Comm::err << "gotoSync, could not _controller->getPosition" << endl;
            return false;
        }        
        if (abs(pos - targetPos) <= _precisionMargin) 
        {
            Comm::out << "gotoSync, reached _precisionMargin" << endl;
            Comm::out << "pos: " << pos*1.0e-9 << endl;
            Comm::out << "targetPos: " << targetPos*1.0e-9 << endl;
            break;
        }
    }
    if (timer.timeElapsedMs() > _timeoutMs) //TODO: wrong, if can give a false alarm
    {
        Comm::out << "######################### gotoSync - timeout !" << endl;
    }
    //TODO:    
    //timeout ?
    //precisionMargin ?
    return true;
}

/**
 * Retrieves the axis position from the hardware and updates state. 
 * The native nanometer units become converted to meters, 
 * and so is written to the state.
 * @return True for success, false for failed communication with the device.
 */
bool SmaractAxis::updatePosition()
{
    int32_t pos;
    if (not _controller->getPosition(_smaractAxisID, pos)) 
    {
        Comm::err << "SmaractAxis::updatePosition() - could not _controller->getPosition()...)" << endl;
        return false; //TODO: invalidate state in case of failure ?
    }
    robot().stateManager().state()[_posField] = pos*1.0e-9; //convert from nanometers to meters        
    Comm::out << "SmaractAxis::updatePosition() - " << robot().stateManager().state()[_posField] << endl;
    return true;
}

SmaractAxisStatus::SmaractAxisStatus() {
}

SmaractAxisStatus::~SmaractAxisStatus() {
}

QMap<unsigned int, QString> SmaractAxisStatus::StatusMapDescription()
{
    QMap<SA_STATUS, QString> StatusMap;
    StatusMap.clear();
    StatusMap.insert(SA_STOPPED_STATUS, "The positioner or end effector is currently not performing active movement.");
    StatusMap.insert(SA_STEPPING_STATUS, "The positioner is currently performing a stepping movement.");
    StatusMap.insert(SA_SCANNING_STATUS, "The positioner is currently performing a scanning movement.");
    StatusMap.insert(SA_HOLDING_STATUS, "The positioner or end effector is holding its current target (see closed-loop commands, e.g. SA_GotoPositionAbsolute_S, SA_GotoAngleAbsolute_S, SA_GotoGripperForceAbsolute_S) or is holding the reference mark (see SA_FindReferenceMark_S).");
    StatusMap.insert(SA_TARGET_STATUS, "The positioner or end effector is currently performing a closed-loop movement.");
    StatusMap.insert(SA_MOVE_DELAY_STATUS, "The positioner is currently waiting for the sensors to power up before executing the movement command. This status may be returned if the the sensors are operated in power save mode.");
    StatusMap.insert(SA_CALIBRATING_STATUS, "The positioner or end effector is busy calibrating its sensor.");
    StatusMap.insert(SA_FINDING_REF_STATUS, "The positioner is moving to find the reference mark.");
    StatusMap.insert(SA_OPENING_STATUS, "The end effector (gripper) is closing or opening its jaws.");
    return StatusMap;
}

QMap<unsigned int, QString> SmaractAxisStatus::StatusMapName()
{
    QMap<SA_STATUS, QString> StatusMap;
    StatusMap.clear();
    StatusMap.insert(SA_STOPPED_STATUS, "SA_STOPPED_STATUS");
    StatusMap.insert(SA_STEPPING_STATUS, "SA_STEPPING_STATUS");
    StatusMap.insert(SA_SCANNING_STATUS, "SA_SCANNING_STATUS");
    StatusMap.insert(SA_HOLDING_STATUS, "SA_HOLDING_STATUS");
    StatusMap.insert(SA_TARGET_STATUS, "SA_TARGET_STATUS");
    StatusMap.insert(SA_MOVE_DELAY_STATUS, "SA_MOVE_DELAY_STATUS");
    StatusMap.insert(SA_CALIBRATING_STATUS, "SA_CALIBRATING_STATUS");
    StatusMap.insert(SA_FINDING_REF_STATUS, "SA_FINDING_REF_STATUS");
    StatusMap.insert(SA_OPENING_STATUS, "SA_OPENING_STATUS");
    return StatusMap;
}

/**
 * Gets the internally stored value, no communication with devices here.
 * @return the status
 */
unsigned int SmaractAxisStatus::status()
{
    return _status;
}
/**
 * Gets description of the state.
 * @return A narrative description of the status.
 */
QString SmaractAxisStatus::description()
{
    return StatusMapDescription().value(_status);
}
/**
 * Gets name of the state.
 * @return Short name of the status.
 */
QString SmaractAxisStatus::name()
{
    return StatusMapName().value(_status);
}