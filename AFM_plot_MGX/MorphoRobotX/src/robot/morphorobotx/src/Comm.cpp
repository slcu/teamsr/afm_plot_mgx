#define COMM_CPP
#include "Comm.hpp"
#include <stdlib.h>
#include <QFile>
#include <QDateTime>
#include <sys/time.h>
#include <QTime>

namespace
{
QTextStream termError(stderr);

QFile errorFile;
QFile outputFile;

void setOutputFile(QTextStream& stream, QFile& file, void (*reset)(), QString filename, bool append, QString streamName)
{
    if(file.isOpen())
        file.close();
    file.setFileName(filename);
    QIODevice::OpenMode flags = QIODevice::WriteOnly;
    if(append)
        flags |= QIODevice::Append;
    if(!file.open(QIODevice::WriteOnly, flags))
    {
        termError << "Impossible to open file '" << filename << "' for writing" << endl;
        termError << "Redirecting " << streamName << " to the terminal" << endl;
        reset();
        return;
    }
    stream.setDevice(&file);
}

QTextStream in(stdin, QIODevice::ReadOnly);

QTime timer;
}

namespace Comm
{
QTextStream user(stdout, QIODevice::WriteOnly);
QTextStream out(stdout, QIODevice::WriteOnly);
QTextStream err(stderr, QIODevice::WriteOnly);


void setError(QString filename, bool append)
{
    setOutputFile(err, errorFile, setStdError, filename, append, "err");
}

void setOutput(QString filename, bool append)
{
    setOutputFile(out, outputFile, setStdOutput, filename, append, "out");
}

void setStdError()
{
    if(errorFile.isOpen())
        errorFile.close();
    errorFile.open(stderr, QIODevice::WriteOnly);
    err.setDevice(&errorFile);
}

void setStdOutput()
{
    if(outputFile.isOpen())
        outputFile.close();
    outputFile.open(stdout, QIODevice::WriteOnly);
    out.setDevice(&outputFile);
}

void pause(QString text)
{
    if(!text.isEmpty())
        user << text << endl;
    user << "Press Enter to continue..." << endl;
    in.readLine();
}

QString ask(QString text)
{
    if(!text.isEmpty())
        user << text << endl;
    return in.readLine();
}

void _init_log()
{
    timer.start();
}

void _log(const char filename[], size_t line_number, const char function[], QString text)
{
    auto time = QString::number(timer.elapsed());
    //time = time.leftJustified(10);
    //out << time << ", " << function << ": " << line_number << "\n ";
    out << text << endl;
}

const char* QStringException::what() const throw()
{
    if(!_w)
    {
        auto err = text();
        auto ba = err.toLocal8Bit();
        _w = new char[ba.size()+1];
        memcpy(_w, ba.data(), ba.size());
        _w[ba.size()] = 0;
    }
    return _w;
}

}

