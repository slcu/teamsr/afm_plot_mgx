/* 
 * File:   ComediSensor.hpp
 * Author: cfmadmin
 *
 * Created on August 15, 2013, 11:42 AM
 */

#ifndef COMEDISENSOR_HPP
#define	COMEDISENSOR_HPP

#include "Sensor.hpp"
#include <QString>
class Robot;
class ComediHardwareController;
typedef std::shared_ptr<ComediHardwareController> ComediHardwareControllerPtr;

class ComediSensor : public Sensor
{
public:
    ComediSensor(QString partName, QString name, Robot& robot);
    double integrationTime() const
    {
        return _integrationTime;
    }
    
    //virtual ~ComediSensor();
    
    bool readParameters(const Parms& parms) override;
    bool writeParameters(Parms& parms) const override;

    int ADChannel() const
    {
        return _ADChannel;
    }
    
    bool initialize(bool force = false) override;
    bool valid() const override;
    
    
protected:
    double _integrationTime = -1.0;
    int _ADChannel = -1;
    bool _valid = false;
    ComediHardwareControllerPtr _controller;
};

#endif	/* COMEDISENSOR_HPP */

