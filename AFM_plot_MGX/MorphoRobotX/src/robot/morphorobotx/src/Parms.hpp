#ifndef __PARMS_H__
#define __PARMS_H__
/**
 * \file parms.h
 *
 * Defines the util::Parms class
 */
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <set>
#include <list>
#include <QString>
#include <iterator>
#include <QTextStream>
#include <QStringList>
#include <QHash>

QTextStream& operator>>(QTextStream& ss, bool b);
QTextStream& operator<<(QTextStream& ss, bool b);

/**
 * \class Parms parms.h <util/parms.h>
 * \brief A utility class to parse L-Studio like parameter files
 *
 * <h2>Format of the parameter file</h2>
 *
 * The basic information in the parameter file is a key associated to
 * a value, possibly with a C++-like comment:
 *
 * \code
 * key= value ; Comment
 * \endcode
 *
 * Keys can be organized in sections:
 *
 * \code
 * [Section1]
 * key1= value1
 * key2= value2
 * ...
 *
 * [Section2]
 * key1= value3
 * key2= value4
 * ...
 * \endcode
 *
 * Empty line or comment-only line are ignored, any other line will raise an
 * error. The default section is named with an empty string "".
 *
 * <h2> Usage example of util::Parm </h2>
 *
 * Here is an example:
 *
 * \code
 * int a,b;
 * QString s1, s2;
 * std::vector<double> all_d;
 * util::Parms parms( "view.v", 2 );
 *
 * // First read simple parameters
 * parms( "", "a", a );
 * parms( "Main", "b", b, 0 );
 * parms( "Main", "string1", s1 );
 * parms( "Main", "string2", s2 );
 *
 * // Then read all the keys "double" in section "Values"
 * parms.all( "Values, "double", all_d);
 * \endcode
 *
 * <h2>Reading typed parameters</h2>
 *
 * To read user-defined typed as parameter, it is enough to overload the
 * function
 * \code QTextStream& operator>>( QTextStream&, const T& ). \endcode
 * \p T being the type of the parameter to read.
 *
 * <h2>Key duplicates</h2>
 *
 * If the same key is used may times in the same section, either all the
 * values can be retrieved using the Parms::all() method, or only the last
 * can be retrieved using the normal Parms::operator()().
 *
 * <h2> \anchor ParmVerbosity Verbosity</h2>
 *
 * The class accepts 5 verbosity levels:
 * - 0: No output
 * - 1: Errors only
 * - 2: Errors and warnings
 * - 3: User information
 * - 4: Debug information
 *
 * Debug information output the raw string before evaluation to set up
 * a parameter.
 *
 */
class Parms
{
public:
    static QTextStream err;

    enum VerbosityLevels
    {
        NO_OUTPUT = 0,
        ERRORS_ONLY,
        ERRORS_WARNINGS,
        USER_INFORMATION,
        DEBUG_INFORMATION
    };

    /**
     * Default constructor, with no file
     */
    explicit Parms(int verboseLevel = 1);

    /**
     * Constructor of the parameter file.
     */
    Parms(QString parmFile, int verboseLevel = ERRORS_ONLY);

    ~Parms();

    /**
     * Read the content from a file
     */
    bool read(QString parmFile);

    /**
     * Write the content to a file
     *
     * If a list of sections is provided, they will appear on the top of the file, in that order
     */
    bool write(QString parmFile, const QStringList& sectionOrder = QStringList()) const;

    /**
     * Change the verbosity level.
     *
     * \see \ref ParmVerbosity "Verbosity"
     */
    void verboseLevel( int vl ) { VerboseLevel = ( vl < 0 ) ? 0 : vl; }

    /**
     * Returns true if the parameter object has been correctly loaded
     */
    bool isLoaded() const { return loaded; }

    /**
     * This operator retrieve a single parameter.
     * \param section Section in which the parameter is looked for
     * \param key Key to look for
     * \param value Variable to set up if possible
     *
     * If the [section]key exists, \c value is set up to the parameter value.
     * Any key placed before the first section is considered in the first
     * secion. If the [section]key is multiply defined, only the last one is
     * taken and a warning is issued (see \ref ParmVerbosity "Verbosity"). If
     * the [section]key does not exist, or its content cannot be interpreted as
     * the requested type, then ar error is issued.
     *
     * \returns True if there was no error while converting the different
     * parameters from their string representation and the [section]key
     * exists.
     */
    template <typename T>
    bool operator()( QString section, QString key, T& value ) const;

    /**
     * This operator writes a single parameter.
     * \param section Section in which the parameter is looked for
     * \param key Key to look for
     * \param value Variable to set up if possible
     * \param comment set the comment for this value of the [section]key
     *
     * If the [section]key already exists, \c value will be the new value.
     *
     * \returns True if there was no error while converting the different
     * parameters to their string representation.
     */
    template <typename T>
    bool set(QString section, QString key, const T& value, const QStringList& comment = QStringList());

    bool set(QString section, QString key, const bool& value, const QStringList& comment = QStringList());
    bool set(QString section, QString key, const int& value, const QStringList& comment = QStringList());
    bool set(QString section, QString key, const float& value, const QStringList& comment = QStringList());
    bool set(QString section, QString key, const double& value, const QStringList& comment = QStringList());
    bool set(QString section, QString key, const std::string& value, const QStringList& comment = QStringList());
    bool set(QString section, QString key, QString value, const QStringList& comment = QStringList());

    /**
     * \brief setComment set the comment on a section
     * \param section Section to set the comment of
     * \param comment Comment to add. The strings must not contain new lines.
     * \return True if the section exists.
     */
    bool setComment(QString section, const QStringList& comment);
    /**
     * \brief setComment set the comment on a section
     * \param section Section to set the comment of
     * \param comment Comment to add. The string is split on the new lines.
     * \return True if the section exists.
     */
    bool setComment(QString section, const QString& comment);

    /**
     * \brief setComments set the comments for a set of existing [section]key
     * \param section Section containing the keys
     * \param key Key to edit
     * \param comments List of comments, one per existing value associated with the key
     * \return True if [section]key exists and \c comments is of the right size
     */
    bool setComments(QString section, QString key, const QList<QStringList>& comments);
    /**
     * \brief setComment set the comments of the first [section]key
     * \param section Section containing the key
     * \param key Key to edit
     * \param comment Comment to add. The strings must not contain new lines
     * \return True if [section]key exists
     */
    bool setComment(QString section, QString key, const QStringList& comment);
    /**
     * \brief setComment set the comments of the first [section]key
     * \param section Section containing the key
     * \param key Key to edit
     * \param comment Comment to add. The string will be split on the new lines.
     * \return True is [section]key exists.
     */
    bool setComment(QString section, QString key, const QString& comment);

    /**
     * This operator add a single parameter.
     * \param section Section in which the parameter is looked for
     * \param key Key to look for
     * \param value Variable to set up if possible
     * \param comment set the comment for this value of the [section]key
     *
     * If the [section]key already exists, \c value will be added to the value set.
     *
     * \returns True if there was no error while converting the different
     * parameters to their string representation.
     */
    template <typename T>
    bool add(QString section, QString key, const T& value, const QStringList& comment = QStringList());

    bool add(QString section, QString key, const bool& value, const QStringList& comment = QStringList());
    bool add(QString section, QString key, const int& value, const QStringList& comment = QStringList());
    bool add(QString section, QString key, const float& value, const QStringList& comment = QStringList());
    bool add(QString section, QString key, const double& value, const QStringList& comment = QStringList());
    bool add(QString section, QString key, const std::string& value, const QStringList& comment = QStringList());
    bool add(QString section, QString key, QString value, const QStringList& comment = QStringList());

    /**
     * Retrieve a single parameter.
     *
     * Boolean value must read "true" or "false", case being meaningless.
     *
     * \see operator()( QString section, QString key, T& value )
     */
    bool operator()( QString section, QString key, bool& value ) const;
    /**
     * Retrieve a single parameter.
     *
     * \see operator()( QString section, QString key, T& value )
     */
    bool operator()( QString section, QString key, int& value ) const;
    /**
     * Retrieve a single parameter.
     *
     * \see operator()( QString section, QString key, T& value )
     */
    bool operator()( QString section, QString key, float& value ) const;
    /**
     * Retrieve a single parameter.
     *
     * \see operator()( QString section, QString key, T& value )
     */
    bool operator()( QString section, QString key, double& value ) const;
    /**
     * Retrieve a single parameter.
     *
     * For string, the value is the whole line with whitespaces and comment
     * stripped before and after the parameter.
     *
     * \see operator()( QString section, QString key, T& value )
     */
    bool operator()( QString section, QString key, std::string& value ) const;

    /**
     * Retrieve a single parameter.
     *
     * For string, the value is the whole line with whitespaces and comment
     * stripped before and after the parameter.
     *
     * \see operator()( QString section, QString key, T& value )
     */
    bool operator()( QString section, QString key, QString& value ) const;

    /**
     * Variation on the previous, but if the [section]key is not found, an
     * information message is issued (instead of an error) and \c value is set
     * up to \c def.
     */
    template <typename T>
    bool operator()( QString section, QString key, T& value, const T& def ) const;

    /**
     * This operator retrieves all parameters with same [section]key.
     * \param section Section in which the parameter is looked for
     * \param key Key to look for
     * \param value Variable to set up
     *
     * \c value is filled with the different values found having same
     * [section]key. If none, \c value is simply empty. The only error that can
     * arise is a reading error, if one parameter has invalid value. This
     * parameter will simply be ignored, all other parameters being read.
     *
     * \returns True if there was no error while converting the different
     * parameters from their string representation.
     */
    template <typename Container>
    bool all( QString section, QString key, Container& value ) const;

    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, std::vector<bool>& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, std::vector<int>& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, std::vector<float>& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, std::vector<double>& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, std::vector<std::string>& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, std::vector<QString>& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QString key, std::vector<T>& value )
     */
    bool all( QString section, QString key, QStringList& value ) const;

    /**
     * This operator write a set of parameters with same [section]key.
     * \param section Section in which the parameter is looked for
     * \param key Key to look for
     * \param value Variable to set up
     * \param comments List of comments for each value in the [second]key
     *
     * If [section]key already exists, they are to be replaced by the current set.
     *
     * \returns True if there was no error while converting the different
     * parameters from their string representation.
     */
    template <typename Container>
    bool setAll( QString section, QString key, const Container& value, const QList<QStringList>& comments = QList<QStringList>() );

    bool setAll(QString section, QString key, const std::vector<bool>& value, const QList<QStringList>& comments = QList<QStringList>());
    bool setAll(QString section, QString key, const std::vector<int>& value, const QList<QStringList>& comments = QList<QStringList>());
    bool setAll(QString section, QString key, const std::vector<float>& value, const QList<QStringList>& comments = QList<QStringList>());
    bool setAll(QString section, QString key, const std::vector<double>& value, const QList<QStringList>& comments = QList<QStringList>());
    bool setAll(QString section, QString key, const std::vector<std::string>& value, const QList<QStringList>& comments = QList<QStringList>());
    bool setAll(QString section, QString key, const std::vector<QString>& value, const QList<QStringList>& comments = QList<QStringList>());
    bool setAll(QString section, QString key, const QStringList& value, const QList<QStringList>& comments = QList<QStringList>());

    /**
     * This operator retrieves all parameters with same [section].
     * \param section Section in which the parameter is looked for
     * \param value Variable to set up
     *
     * \c value is filled with the different keys found in [section] and, for
     * each key, the vector of all the values associated to it. If none, \c
     * value is simply empty. The only error that can arise is a reading error,
     * if one parameter has invalid value. This parameter will simply be
     * ignored, all other parameters being read.
     *
     * \returns True if there was no error while converting the different
     * parameters from their string representation.
     */
    template <typename T, typename Container>
    bool all( QString section, QHash<QString, Container>& values) const;

    /**
     * Retrieve a all parameters with same [section].
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, std::vector<bool> >& value ) const;
    /**
     * Retrieve a all parameters with same [section].
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, std::vector<int> >& value ) const;
    /**
     * Retrieve a all parameters with same [section].
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, std::vector<float> >& value ) const;
    /**
     * Retrieve a all parameters with same [section].
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, std::vector<double> >& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, std::vector<std::string> >& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, std::vector<QString> >& value ) const;
    /**
     * Retrieve a all parameters with same [section]key.
     *
     * \see all( QString section, QHash<QString, std::vector<T> >& value )
     */
    bool all( QString section, QHash<QString, QStringList>& value ) const;

    QStringList listSections() const;

    /**
     * Remove all the fields in the \c section with the \c key
     */
    bool erase(QString section, QString key);

    /**
     * Remove a whole section from the parameters
     */
    bool erase(QString section);

    struct value_t
    {
        value_t() { }
        value_t(QString v, QStringList c)
            : value(v)
            , comment(c)
        { }
        QString value;
        QStringList comment;
    };

    typedef QList<value_t> values_t;

    struct section_t
    {
        typedef QHash<QString,values_t>::const_iterator const_iterator;
        typedef QHash<QString,values_t>::iterator iterator;

        QHash<QString,values_t> values;
        QStringList comment;

        values_t& operator[](const QString& key) { return values[key]; }
        const values_t value(const QString& key) const { return values.value(key); }
        QStringList keys() const { return values.keys(); }
        iterator begin() { return values.begin(); }
        iterator end() { return values.end(); }
        const_iterator begin() const { return values.begin(); }
        const_iterator end() const { return values.end(); }
        iterator find(const QString& key) { return values.find(key); }
        const_iterator find(const QString& key) const { return values.find(key); }

        bool empty() const { return values.empty(); }
        bool size() const { return values.size(); }
        iterator erase(iterator pos) { return values.erase(pos); }
    };

    typedef QHash<QString, section_t> parameters_t;

private:
    bool setValues( QString section, QString key, const values_t& values );
    bool setValue(QString section, QString key, const value_t& value)
    {
        return setValues(section, key, values_t() << value);
    }
    bool addValue(QString section, QString key, const value_t& value);

    /**
     * Extract a value as a string given \c section and \c key.
     *
     * \returns True if [section]key exist.
     */
    bool extractValues( QString section, QString key, QStringList& values, bool checkExist ) const;

    /**
     * Read a value from a string.
     *
     * If the value to read is a boolean, the input string has to be equal to
     * "true" or "false" (case insensitive).
     *
     * \returns True if the reading succeeded
     */
    bool readValue( QString value, bool& variable ) const;
    /**
     * Read a value from a string.
     *
     * If the value to read is a string, the input one is simply copied.
     *
     * \returns True if the reading succeeded
     */
    bool readValue( QString value, std::string& variable ) const;
    /**
     * Read a value from a string.
     *
     * If the value to read is a string, the input one is simply copied.
     *
     * \returns True if the reading succeeded
     */
    bool readValue( QString value, QString& variable ) const;
    /**
     * Read a vector of values from a string.
     *
     * Default is to use operator>> on a \c istringstream defined on the input
     * string. Print an error is the flux operator fails.
     *
     * \returns True if the reading succeeded
     */
    template <typename T>
    bool readValue( QString value, std::vector<T>& variable ) const;
    /**
     * Read a list of values from a string.
     *
     * Default is to use operator>> on a \c istringstream defined on the input
     * string. Print an error is the flux operator fails.
     *
     * \returns True if the reading succeeded
     */
    template <typename T>
    bool readValue( QString value, std::list<T>& variable ) const;
    /**
     * Read a set of values from a string.
     *
     * Default is to use operator>> on a \c istringstream defined on the input
     * string. Print an error is the flux operator fails.
     *
     * \returns True if the reading succeeded
     */
    template <typename T>
    bool readValue( QString value, std::set<T>& variable ) const;
    /**
     * Read a value from a string.
     *
     * Default is to use operator>> on a \c istringstream defined on the input
     * string. Print an error is the flux operator fails.
     *
     * \returns True if the reading succeeded
     */
    template <typename T>
    bool readValue( QString value, T& variable ) const;

    template <typename T, typename InsertIterator>
    bool readContainer( QString value, InsertIterator container) const;

    bool writeValue( QString& value, const bool& variable );
    bool writeValue( QString& value, const std::string& variable );
    bool writeValue( QString& value, QString variable );
    template <typename T>
    bool writeValue( QString& value, const std::vector<T>& variable );
    template <typename T>
    bool writeValue( QString& value, const std::list<T>& variable );
    template <typename T>
    bool writeValue( QString& value, const std::set<T>& variable );
    template <typename T>
    bool writeValue( QString& value, const T& variable );

    template <typename Iterator>
    bool writeContainer( QString& value, Iterator first, Iterator last);

    template <typename T>
    bool get(QString section, QString key, T& value, bool checkExist) const;

    /**
     * Map of all the parameters in string form as read from the file. The
     * strings are already stripped from whitespaces and comments.
     */
    parameters_t Parameters;

    /**
     * Current verbosity level
     */
    int VerboseLevel;

    /**
     * set to True if the existence of a key output an error. Used with the
     * default value to avoid printing an error in that case.
     */
    bool CheckExist;

    /**
     * Make sure the parameter file is correctly loaded
     */
    bool loaded;
};

template <typename T, typename InsertIterator>
bool Parms::readContainer( QString value, InsertIterator it) const
{
    QString val(value);
    QTextStream iss(&val, QIODevice::ReadOnly);
    while(!iss.atEnd() and iss.status() == QTextStream::Ok)
    {
        T v;
        iss >> v;
        *it++ = v;
    }
    return iss.status() == QTextStream::Ok;
}

template <typename T>
bool Parms::readValue( QString value, std::vector<T>& variable ) const
{
    return readContainer<T>(value, std::back_insert_iterator<std::vector<T> >(variable));
}

template <typename T>
bool Parms::readValue( QString value, std::list<T>& variable ) const
{
    return readContainer<T>(value, std::back_insert_iterator<std::list<T> >(variable));
}

template <typename T>
bool Parms::readValue( QString value, std::set<T>& variable ) const
{
    return readContainer<T>(value, std::insert_iterator<std::set<T> >(variable, variable.end()));
}

template <typename T>
bool Parms::readValue( QString value, T& variable ) const
{
    QString val(value);
    QTextStream iss(&val, QIODevice::ReadOnly);
    iss >> variable;
    return iss.status() == QTextStream::Ok;
}

template <typename T>
bool Parms::operator()( QString section, QString key, T& value ) const
{
    return get(section, key, value, true);
}

template <typename T>
bool Parms::get( QString section, QString key, T& value, bool checkExist) const
{
    QStringList values;
    if( !extractValues( section, key, values, checkExist ) )
        return false;

    if( ( values.size() > 1 ) && ( VerboseLevel >= ERRORS_WARNINGS ) )
    {
        err << "Parms::operator():Warning multiple value for key [" << section << "]"
            << key << ", last one used.\n" << flush;
    }

    if( !readValue( values.back(), value ) )
    {
        if( VerboseLevel >= ERRORS_ONLY )
        {
            err << "Parms::operator():Error getting value for key [" << section << "]" <<  key
                << " value " << values.back() << "\n" << flush;
        }
        return false;
    }
    return true;
}

template <typename T>
bool Parms::operator()( QString section, QString key, T& value, const T& def ) const
{
    bool found = true;
    if( !get( section, key, value, false ) )
    {
        found = false;
        if( VerboseLevel >= USER_INFORMATION )
        {
            err << "Parms::operator()::Info key [" << section << "]"
                << key << " not found, using default value" << endl;
        }
        value = def;
    }
    return found;
}

template <typename Container>
bool Parms::all( QString section, QString key, Container& value ) const
{
    bool valid = true;
    typedef typename Container::value_type T;
    QStringList values;
    if( !extractValues( section, key, values, false ) )
        return false;
    value.clear();
    std::insert_iterator<Container> it(value, value.end());
    foreach( QString val, values )
    {
        T single_value;
        if( readValue( val, single_value ) )
        {
            *it++ = (const T&)single_value;
        }
        else
        {
            if( VerboseLevel >= USER_INFORMATION )
            {
                err << "Parms::all:Error reading key [" << section << "]" << key
                    << " with value " << val << "\n" << flush;
            }
            valid = false;
        }
    }
    return valid;
}

template <typename T, typename Container>
bool Parms::all( QString section, QHash<QString, Container>& result ) const
{
    typename parameters_t::const_iterator found = Parameters.find(section);
    if(found == Parameters.end()) return true;
    const section_t& sec = *found;
    result.clear();
    bool valid = true;
    for(section_t::const_iterator it = sec.begin() ; it != sec.end() ; ++it)
    {
        QString key = it.key();
        Container& value = result[key];
        value.clear();
        foreach( const value_t& val, it.value() )
        {
            T single_value;
            if( readValue( val.value, single_value ) )
            {
                value.push_back( single_value );
            }
            else
            {
                if( VerboseLevel >= USER_INFORMATION )
                {
                    err << "Parms::all:Error reading key [" << section << "]" << key
                        << " with value " << val.value << endl;
                }
                valid = false;
            }
        }
    }
    return valid;
}

template <typename T>
bool Parms::writeValue(QString& value, const std::vector<T>& variable)
{
    return writeContainer(value, variable.begin(), variable.end());
}

template <typename T>
bool Parms::writeValue(QString& value, const std::list<T>& variable)
{
    return writeContainer(value, variable.begin(), variable.end());
}

template <typename T>
bool Parms::writeValue(QString& value, const std::set<T>& variable)
{
    return writeContainer(value, variable.begin(), variable.end());
}

template <typename T>
bool Parms::writeValue(QString& value, const T& variable)
{
    value = QString();
    QTextStream oss(&value, QIODevice::WriteOnly);
    oss << variable;
    bool result = oss.status() == QTextStream::Ok;
    return result;
}

template <typename T>
bool Parms::set(QString section, QString key, const T& variable, const QStringList& comment)
{
    QString value;
    if(!writeValue(value, variable))
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << "Parms::set:Error writing value for key [" << section << "]" << key << "\n" << flush;
        return false;
    }
    return setValue(section, key, value_t(value, comment));
}

template <typename T>
bool Parms::add(QString section, QString key, const T& variable, const QStringList& comment)
{
    QString value;
    if(!writeValue(value, variable))
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << "Parms::set:Error writing value for key [" << section << "]" << key << "\n" << flush;
        return false;
    }
    return addValue(section, key, value_t(value, comment));
}

template <typename Iterator>
bool Parms::writeContainer(QString& value, Iterator first, Iterator end)
{
    value = QString();
    QTextStream oss(&value, QIODevice::WriteOnly);
    while(first != end)
    {
        oss << *first++;
        if(first != end) oss << " ";
    }
    return oss.status() == QTextStream::Ok;
}

template <typename Container>
bool Parms::setAll( QString section, QString key, const Container& variable, const QList<QStringList>& comments)
{
    values_t values;
    typedef typename Container::value_type T;
    int i = 0;
    foreach(const T& t, variable)
    {
        QString value;
        if(!writeValue(value, t))
            return false;
        if(!comments.empty())
        {
            if(comments.size() <= i) return false;
            values << value_t(value, comments[i]);
        }
        else
            values << value_t(value, QStringList());
        ++i;
    }
    return setValues(section, key, values);
}

#endif
