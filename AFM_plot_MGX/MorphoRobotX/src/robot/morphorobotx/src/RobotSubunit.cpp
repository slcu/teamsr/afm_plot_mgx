/*
 * File:   RobotSubunit.cpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 7:34 PM
 */

#include "RobotSubunit.hpp"
#include "HardwareController.hpp"
#include "StateManager.hpp"
#include "Comm.hpp"

constexpr double RobotSubunit::double_nan;

RobotSubunit::RobotSubunit(QString pn, QString n, Robot& rob)
    : _partName(pn)
    , _name(n)
    , _robot(rob)
{ }

bool RobotSubunit::registerOption(QString name, unsigned int id, OptionType type)
{
    bool ok = true;
    if(id == INVALID_OPTION)
    {
        Comm::err << "Error, INVALID_OPTION (i.e. " << INVALID_OPTION << ") is not a valid option identifier" << endl;
        ok = false;
    }
    if((type & ReadWriteOption) == 0)
    {
        Comm::err << "Error, you need to specify if an option can be read or written" << endl;
        ok = false;
    }
    if((type & (NumberOption | StringOption)) == 0)
    {
        Comm::err << "Error, you need to specify if an option is values or number (or both)" << endl;
        ok = false;
    }
    if(!ok) return false;
    const OptionId& prev = _options.value(name, OptionId{0,INVALID_OPTION});
    if(prev.id != INVALID_OPTION)
    {
        if((prev.id != id) or (prev.type != type))
        {
            Comm::err << "Error, option '" << name << "' already exists with a different id or type" << endl;
            return false;
        }
        return true;
    }
    _options[name] = OptionId{type, id};
    return true;
}

bool RobotSubunit::removeOption(QString name)
{
    return (bool)_options.remove(name);
}

QStringList RobotSubunit::optionList() const
{
    return _options.keys();
}

unsigned int RobotSubunit::optionId(QString name) const
{
    return _options.value(name, OptionId{0, INVALID_OPTION}).id;
}

unsigned int RobotSubunit::optionId(QString name, OptionType constraints) const
{
    OptionId id = _options.value(name, OptionId{0, INVALID_OPTION});
    if((id.type & constraints) != constraints) return INVALID_OPTION;
    return id.id;
}

QString RobotSubunit::optionName(unsigned int id) const
{
    for(auto it = _options.begin() ; it != _options.end() ; ++it)
    {
        if(it->id == id)
            return it.key();
    }
    return QString();
}

RobotSubunit::OptionType RobotSubunit::optionType(QString name) const
{
    return _options.value(name, OptionId{0, INVALID_OPTION}).type;
}


const double& RobotSubunit::OptionSet::value(unsigned int id) const
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException{id};
    if(found->numeric_option)
        return _number_options[found->pos];
    throw InvalidOptionTypeException(id, true);
}

const QString& RobotSubunit::OptionSet::string(unsigned int id) const
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException(id);
    if(not found->numeric_option)
        return _string_options[found->pos];
    throw InvalidOptionTypeException(id, false);
}

bool RobotSubunit::OptionSet::numeric(unsigned int id) const
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException(id);
    return found->numeric_option;
}

QList<unsigned int> RobotSubunit::OptionSet::stringOptions() const
{
    QList<unsigned int> result;
    for(auto it = _options.begin() ; it != _options.end() ; ++it)
    {
        if(not it->numeric_option)
            result << it.key();
    }
    return result;
}

QList<unsigned int> RobotSubunit::OptionSet::numberOptions() const
{
    QList<unsigned int> result;
    for(auto it = _options.begin() ; it != _options.end() ; ++it)
    {
        if(it->numeric_option)
            result << it.key();
    }
    return result;
}

void RobotSubunit::OptionSet::unset(unsigned int id)
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException(id);
    if(found->numeric_option)
        _number_options[found->pos] = double_nan;
    else
        _string_options[found->pos] = QString();
}

bool RobotSubunit::OptionSet::isSet(unsigned int id) const
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException(id);
    if(found->numeric_option)
        return not std::isnan(_number_options[found->pos]);
    else
        return not _string_options[found->pos].isNull();
}

double& RobotSubunit::OptionSet::value(unsigned int id)
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException(id);
    if(found->numeric_option)
        return _number_options[found->pos];
    throw InvalidOptionTypeException(id, true);
}

QString& RobotSubunit::OptionSet::string(unsigned int id)
{
    auto found = _options.find(id);
    if(found == _options.end())
        throw UnknownOptionIdException(id);
    if(not found->numeric_option)
        return _string_options[found->pos];
    throw InvalidOptionTypeException(id, false);
}

void RobotSubunit::OptionSet::addValue(unsigned int id, double value)
{
    size_t next = _number_options.size();
    _options[id] = OptionPlace{true, next};
    _number_options.push_back(value);
}

void RobotSubunit::OptionSet::addString(unsigned int id, QString value)
{
    size_t next = _string_options.size();
    _options[id] = OptionPlace{false, next};
    _string_options.push_back(value);
}

RobotSubunit::OptionSet RobotSubunit::defaults()
{
    OptionSet opts(partName());
    for(auto it = _options.begin() ; it != _options.end() ; ++it)
    {
        auto id = it->id;
        auto type = it->type;
        if(type & WriteOnlyOption)
        {
            if(type & NumberOption)
                opts.addValue(id, _default_values.value(id, double_nan));
            else
                opts.addString(id, _default_strings.value(id, QString()));
        }
    }
    return opts;
}

bool RobotSubunit::setDefault(unsigned int option, double value, bool set)
{
    _default_values[option] = value;
    if(set)
        return this->set(option, value);
    return true;
}

bool RobotSubunit::setDefault(unsigned int option, QString value, bool set)
{
    _default_strings[option] = value;
    if(set)
        return this->set(option, value);
    return true;
}

bool RobotSubunit::setAll(const OptionSet& options)
{
    if(options.partName() != partName())
        throw InvalidOptionSet(options.partName(), partName());
    QList<unsigned int> opts = options.options();
    qSort(opts);
    for(unsigned int id: opts) if(options.isSet(id))
    {
        if(options.numeric(id))
        {
            if(!set(id, options.value(id)))
               return false;
        }
        else
        {
            if(!set(id, options.string(id)))
               return false;
        }
    }
    return true;
}

QString RobotSubunit::InvalidOptionTypeException::text() const throw()
{
    auto str = QString("%2 option '%1' used as %3 option").arg(_name);
    if(_is_numeric)
        str = str.arg("Numeric").arg("string");
    else
        str = str.arg("String").arg("numeric");
    return str;
}

QString RobotSubunit::UnknownOptionNameException::text() const throw()
{
    return QString("Unknown option named '%1'").arg(_name);
}

QString RobotSubunit::UnknownOptionIdException::text() const throw()
{
    return QString("Unknown option id '%1'").arg(_id);
}
