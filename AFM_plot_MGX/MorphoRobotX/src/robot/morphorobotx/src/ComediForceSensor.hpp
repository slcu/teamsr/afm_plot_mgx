/*
 * File:   ComediForceSensor.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 9:07 PM
 */

#ifndef COMEDIFORCESENSOR_HPP
#define COMEDIFORCESENSOR_HPP

#include "ComediSensor.hpp"

class Robot;
class Parms;
class StateManager;

class ComediForceSensor : public ComediSensor
{

public:
    ComediForceSensor(QString partName, QString name, Robot& robot);
    
    bool update() override;
    
    bool initialize(bool force = false) override;

    bool readParameters(const Parms& parms) override;    
    bool writeParameters(Parms& parms) const override;
    
    bool get(unsigned int id, double& val) override;
    bool get(unsigned int id, QString& val) override;
    
protected:
    double _gain = 0;
    int _voltageField = -1;
    int _forceField = -1;
    StateManager *_stateManager;
    double _minReadForce;
    double _maxReadForce;
    double _minBreakForce;
    double _maxBreakForce;
    bool _direction;
    QString _axis;
};

typedef std::shared_ptr<ComediForceSensor> ComediForceSensorPtr;

#endif /* COMEDIFORCESENSOR_HPP */

