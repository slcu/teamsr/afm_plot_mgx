/*
 * File:   CFMFileManager.h
 * Author: huflejt
 *
 * Created on October 25, 2012, 7:30 PM
 */

#ifndef CFMFILEMANAGER_H
#define CFMFILEMANAGER_H

#include "Parms.hpp"
#include <QString>
#include <QFile>
#include <QDir>


class FileManager {

public:

    FileManager();
    FileManager(const FileManager&) = delete;
    virtual ~FileManager();

    bool setSessionPath(QString path);

    QDir sessionDir() const
    {
        return _sessionDir;
    }

    void setExperimentPrefix(QString prefix)
    {
        _experimentPrefix = prefix;
        _initialized = false;
    }

    QString outputDataPath() const
    {
        return _experimentDir.absoluteFilePath("data.csv");
    }

    QDir experimentDir() const
    {
        return _experimentDir;
    }

    bool readParameters(const Parms& parms);
    bool writeParameters(Parms& parms) const;

    bool prepareNewDirectory();

    bool valid() const { return _initialized; }
    operator bool() const { return valid(); }

private:
    QDir _experimentPath;
    QDir _sessionDir;
    QString _experimentPrefix;
    QDir _experimentDir;
    bool _initialized;
};

#endif /* CFMFILEMANAGER_H */
