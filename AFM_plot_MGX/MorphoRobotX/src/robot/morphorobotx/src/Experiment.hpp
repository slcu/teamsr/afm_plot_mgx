/*
 * File:   Experiment.hpp
 * Author: cfmadmin
 *
 * Created on December 11, 2012, 6:01 PM
 */

#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP

#include "FileManager.hpp"
#include "StateManager.hpp"
#include "HardwareManager.hpp"
#include "DataLogger.hpp"
#include "Protocol.hpp"
#include "Robot.hpp"
#include <set>
#include <exception>

//class Robot;
//class HardwareManager;

struct CannotCreateExperiment : public std::exception
{
    ~CannotCreateExperiment() throw() override { }

    const char* what() const throw() override
    {
        return "The experiment could not be created, check the error output for details.";
    }
};

struct CalledWhileRunning : public Comm::QStringException
{
    CalledWhileRunning(const char m[])
      : Comm::QStringException()
      , method(m)
    { }

    ~CalledWhileRunning() throw() override { }

    QString text() const throw() override
    {
        return QString("Error, called %1 while the experiment is running").arg(method);
    }

    const char* method;
};

struct ErrorCannotSuspendProtocol : public std::exception
{
    ~ErrorCannotSuspendProtocol() throw() override { }

    const char* what() const throw() override
    {
        return "Error, cannot suspend protocol execution";
    }
};

struct ErrorCannotResumeProtocol : public std::exception
{
    ~ErrorCannotResumeProtocol() throw() override { }

    const char* what() const throw() override
    {
        return "Error, cannot resum protocol execution";
    }
};

class Experiment {
public:
    Experiment();
    /*Experiment(const Experiment& orig);*/
    virtual ~Experiment();
    /**
     * <initializes the experiment>
     * @return true for success, false for failure
     */
    bool initialize();
    bool writeParameters();
    bool run();
    FileManager& fileManager();
    StateManager& stateManager();
     ///needs to be able to initialize logger with path suppplied by fileManager
    Robot& robot();
    ProtocolPtr mainProtocol();

    class Pause
    {
        Experiment *experiment;
        friend class Experiment;

        Pause(Experiment *exp)
          : experiment(exp)
        {
            if(experiment)
               if(!experiment->suspend())
                   throw ErrorCannotSuspendProtocol();
        }

    public:
        Pause() = delete; // No default constructor
        Pause(const Pause&) = delete; // Cannot copy!
        Pause(Pause&& s)
          : experiment(s.experiment)
        { s.experiment = 0; }

        ~Pause()
        {
            if(experiment)
               if(!experiment->configure())
                   throw ErrorCannotResumeProtocol();
        }

    };

    unsigned long timeElapsedMs()
    {
        return _timer.timeElapsedMs();
    }

    const Parms& parms()
    {
        return _parms;
    }

    /**
     * Retrieve a protocol from its section name and initialize it.
     * 
     * \param sectionName Name of the section containing the protocol
     * \returns A pair where the first element is the protocol and the second is true if it has been correctly initialized
     */
    std::pair<ProtocolPtr, bool> protocol(QString sectionName);

    const Parms& robotParms() const { return _robotParms; }
    Parms& robotParms() { return _robotParms; }

    const Parms& protocolParms() const { return _parms; }
    Parms& protocolParms() { return _parms; }

    /**
     * Suspend the experiment.
     *
     * During a run, this call warns the experiment new fields and protocols may be created.
     * After this, you cannot write anything on the fields.
     */
    bool suspend();

    /**
     * This configures the experiment and allows running
     *
     * This has to be called before the experiment run, or after suspend. It will create a new folder with the new data
     * and a new copy of the experiment.ini and robot.ini
     */
    bool configure();

    /**
     * Pause the experiment as long as the returned object is alive
     */
    Pause pause()
    {
        return Pause(this);
    }

protected:
   FileManager _fileManager;
   StateManager _stateManager;
   Timer _timer;
   Robot _robot;
   ProtocolPtr _protocol;
   std::set<ProtocolPtr> _protocols;
   Parms _parms, _robotParms;
   bool _running = false;
};

#endif /* EXPERIMENT_HPP */

