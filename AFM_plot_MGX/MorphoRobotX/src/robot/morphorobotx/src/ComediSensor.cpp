/* 
 * File:   ComediSensor.cpp
 * Author: cfmadmin
 * 
 * Created on August 15, 2013, 11:43 AM
 */

#include "ComediSensor.hpp"
#include "Comm.hpp"
#include "Robot.hpp"
#include "Parms.hpp"
#include "ComediHardwareController.hpp"

ComediSensor::ComediSensor(QString partName, QString name, Robot& robot)
    : Sensor(partName, name, robot)
{    
}

bool ComediSensor::readParameters(const Parms& parms)
{
    QString controllerName;
    if (not parms(partName(), "Controller", controllerName)) return false;
    _controller = robot().addPart<ComediHardwareController>(controllerName, controllerName);
    if (not _controller)
    {
        Comm::err << "Error: ComediSensor [" << partName() << "] no ComediHardwareController named " << controllerName << endl;
        return false;
    }
    
    if (not parms(partName(), "IntegrationTime", _integrationTime)) return false;
    if (not parms(partName(), "ADChannel", _ADChannel)) return false;
    if (_ADChannel < 0 or _ADChannel > 15)
    {
        Comm::err << "Error: ComediSensor [" << partName() << "] ADChannel must be between 0 and 15 (incl.)" << endl;
        return false;
    }
    
    _valid = _controller->registerSensor(this);    
    return _valid;
}
bool ComediSensor::writeParameters(Parms& parms) const
{
    if (not parms.set(partName(), "IntegrationTime", _integrationTime)) return false;
    if (not parms.setComment(partName(), "IntegrationTime", "Preferred integration time for this sensor (in seconds)"));
    return true;
}

bool ComediSensor::initialize(bool force)
{
    return true;
}

bool ComediSensor::valid() const
{
    return _valid and _controller->valid();
}

//ComediSensor::~ComediSensor() {
//}

