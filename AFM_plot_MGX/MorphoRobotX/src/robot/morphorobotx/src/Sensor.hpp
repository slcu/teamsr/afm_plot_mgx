/*
 * File:   Sensor.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 4:09 PM
 */

#ifndef SENSOR_HPP
#define SENSOR_HPP

#include "RobotSubunit.hpp"
#include <memory>

class Sensor : public RobotSubunit
{
public:
    Sensor(QString partName, QString name, Robot& robot);

    virtual bool update() = 0;
};

typedef std::shared_ptr<Sensor> SensorPtr;

#endif /* SENSOR_HPP */

