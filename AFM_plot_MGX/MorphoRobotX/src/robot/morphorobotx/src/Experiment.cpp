/*
 * File:   Experiment.cpp
 * Author: cfmadmin
 *
 * Created on December 11, 2012, 6:01 PM
 */

#include "Experiment.hpp"
#include "Protocol.hpp"
#include "Comm.hpp"
#include <QDir>

Experiment::Experiment()
    : _robot(*this)
    , _parms("experiment.ini")
    , _robotParms("robot.ini")
{
    if(!_parms.isLoaded())
    {
        Comm::err << "Error, could read 'experiment.ini'" << endl;
        throw CannotCreateExperiment();
    }
    if(!_robotParms.isLoaded())
    {
        Comm::err << "Error, could read 'robot.ini'" << endl;
        throw CannotCreateExperiment();
    }

}

Experiment::~Experiment()
{
}

FileManager& Experiment::fileManager()
{
    return _fileManager;
}

StateManager& Experiment::stateManager()
{
    return _stateManager;
}

Robot& Experiment::robot()
{
    return _robot;
}

ProtocolPtr Experiment::mainProtocol()
{
    return _protocol;
}

bool Experiment::initialize()
{
    Comm::out << "reading robot parameters..." << endl;
    if(!_robot.readParameters(_robotParms))
    {
        Comm::err << "ERROR: Robot::readParameters failed." << endl;
        return false;
    }

    if (!_robot.initialize())
    {
        Comm::err << "ERROR: robot.initialize() failed" << endl;
        return false;
    }


    QString protocolName;
    if(!_parms("Experiment", "Protocol", protocolName))
    {
        Comm::err << "ERROR: no protocol name specified" << endl;
        return false;
    }

    QString protocolType;
    if(!_parms(protocolName, "Type", protocolType))
    {
        Comm::err << QString("ERROR: no protocol type specified in section '%1'").arg(protocolName) << endl;
        return false;
    }

    //ProtocolPtr proc = protocol(protocolName, state);
    bool is_init;
    std::tie(_protocol, is_init) = protocol(protocolName); //looks ugly, I take the value of this and pass it as a reference
    if(!_protocol)
    {
        Comm::err << QString("Error, no protocol named '%1'.").arg(protocolType) << endl;
        return false;
    }

    if(!is_init)
    {
        Comm::err << "Error, cannot read protocol parameters. init(parms) failed." << endl;
        return false;
    }

    if(!_fileManager.readParameters(_parms))
    {
        Comm::err << "ERROR: FileManager.readParameters failed" << endl;
        return false;
    }

    return true;
}

bool Experiment::writeParameters()
{
    QDir session = _fileManager.experimentDir();
    Parms parms, robotParms;

    if(!parms.set("Experiment", "Protocol", _protocol->name())) return false;
    if(!parms.setComment("Experiment", "Protocol", "Name of the main protocol")) return false;
    if(!parms.setComment("Experiment", "Configuration of the experiment")) return false;

    // **PBdR**
    // We need to think if the parameters may be influenced by running the process. If not, this is the best position.
    // If so, the writeParameters need to be placed after the proc->run
    for(ProtocolPtr prot: _protocols)
    {
        if(!prot->writeParameters(parms))
        {
            Comm::err << "Error, cannot write protocol parameters." << endl;
            return false;
        }
        QString type = protocolFactory().instanceType(prot->name());
        if(type.isEmpty())
        {
            Comm::err << "Error, cannot find the type of the protocol '" << prot->name() << "'" << endl;
            return false;
        }
        if(!parms.set(prot->name(), "Type", type)) return false;
    }

    if(!_stateManager.writeParameters(parms)) return false;
    if(!_fileManager.writeParameters(parms)) return false;

    if(!_robot.writeParameters(robotParms))
        return false;

    // **PBdR** and here, we are writing the parameters to a file
    QStringList parmsSections;
    parmsSections << "Experiment" << "StorageSettings" << "Data";
    QString protocolFile = session.filePath("experiment.ini");
    if(!parms.write(protocolFile, parmsSections))
    {
        Comm::err << "Error, cannot write parameter file '" << protocolFile << "'" << endl;
        return false;
    }
    QString robotFile = session.filePath("robot.ini");
    if(!robotParms.write(robotFile, QStringList() << "Robot"))
    {
        Comm::err << "Error, cannot write robot file '" << robotFile << "'" << endl;
        return false;
    }

    return true;
}

bool Experiment::run()
{
    _timer.reset();
    return _protocol->run();
}

bool Experiment::suspend()
{
    _running = false;
    _stateManager.suspend();
    return true;
}

bool Experiment::configure()
{
    LOG("Entering function");
    if (!_fileManager.prepareNewDirectory())
    {
        Comm::err << "ERROR: fileManager.prepareNewDirectory() failed" << endl;
        return false;
    }
    //_stateManager.logger().setFilename(_fileManager.outputDataPath());
    _stateManager.configure(_fileManager.outputDataPath());

    Comm::out << "Checking what fileManager found:" << endl;
    Comm::out << QString("fileManager.getExperimentDirAbsolute() = %1").arg(_fileManager.experimentDir().canonicalPath()) << endl;
    Comm::out << QString("fileManager.getCurrentSessionPath() = %1").arg(_fileManager.sessionDir().canonicalPath()) << endl;
    if(!writeParameters()) return false;
    _running = true;
    LOG("Leaving function");
    return true;
}

std::pair<ProtocolPtr, bool> Experiment::protocol(QString sectionName)
{
    if(_running)
        throw CalledWhileRunning(__PRETTY_FUNCTION__);
    std::pair<ProtocolPtr, bool> result{nullptr, false};
    QString type;
    if(_parms(sectionName, "Type", type))
    {
        result.first = protocolFactory()(type, sectionName, *this);
        if(result.first)
        {
           if(_protocols.insert(result.first).second) // If the protocol was newly inserted
               result.second = result.first->init(_parms);
           else
               result.second = true;
        }
        else
            Comm::err << "Error, could not create protocol " << sectionName << endl;
    }
    else
        Comm::err << "Error, protocol " << sectionName << " has no type or there is no such section" << endl;
    return result;
}

