/*
 * File:   ComediHardwareController.cpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 3:04 PM
 */

#include "ComediHardwareController.hpp"
#include "ComediSensor.hpp"
#include "Parms.hpp"
#include "Comm.hpp"
#include "HardwareManager.hpp"
#include "Timer.hpp"

#include <comedilib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>
#include <QString>
#include <QByteArray>
#include <cmath>
#include <cstdint>

REGISTER_ROBOT_SUBUNIT_TYPE(ComediHardwareController);

constexpr Version ComediHardwareController::version;

namespace
{
typedef enum
{
    ACQUISITION_MODE_NONE,       //nothing valid, initial state
    ACQUISITION_MODE_BURST,      //finite number of samples at specified frequency
    ACQUISITION_MODE_CONTINUOUS  //infinite number of samples at specified frequency, cancel later using comedi_cancel()
} ACQUISITION_MODE;

const double double_nan = std::numeric_limits<double>::quiet_NaN();

char const * const cmdtest_messages[]={
        "success",
        "invalid source",
        "source conflict",
        "invalid argument",
        "argument conflict",
        "invalid chanlist",
    };

QString cmd_src(int src)
{
    QStringList cmd;

    if(src & TRIG_NONE) cmd << "none";
    if(src & TRIG_NOW) cmd << "now";
    if(src & TRIG_FOLLOW) cmd << "follow";
    if(src & TRIG_TIME) cmd << "time";
    if(src & TRIG_TIMER) cmd << "timer";
    if(src & TRIG_COUNT) cmd << "count";
    if(src & TRIG_EXT) cmd << "ext";
    if(src & TRIG_INT) cmd << "int";
#ifdef TRIG_OTHER
    if(src & TRIG_OTHER) cmd << "other";
#endif

    if(cmd.empty())
        return QString("unknown(0x%1) ").arg(src, 8, 16, QChar('0'));

    return cmd.join("|").leftJustified(20);
}

QString dump_cmd(comedi_cmd const & cmd)
{
    QString output;

    output += QString("subdevice:      %1\n").arg(cmd.subdev);
    output += QString("start:      %1 %2\n").arg(cmd_src(cmd.start_src)).arg(cmd.start_arg);
    output += QString("scan_begin: %1 %2\n").arg(cmd_src(cmd.scan_begin_src)).arg(cmd.scan_begin_arg);
    output += QString("convert:    %1 %2\n").arg(cmd_src(cmd.convert_src)).arg(cmd.convert_arg);
    output += QString("scan_end:   %1 %2\n").arg(cmd_src(cmd.scan_end_src)).arg(cmd.scan_end_arg);
    output += QString("stop:       %1 %2\n").arg(cmd_src(cmd.stop_src)).arg(cmd.stop_arg);

    return output;
}


}
//static char * const default_filename = "/dev/comedi0"; ///TODO: this is going to be configurable from the robotRecipe.ini

#define     N_CHANS 16

class ComediHardwareControllerPrivate
{
public:
    ComediHardwareControllerPrivate()
    {
        bzero(&command, sizeof(command));
    }
    comedi_t *dev = 0;
    comedi_cmd  command;    
    comedi_polynomial_t polynomial_converter[N_CHANS];
    unsigned int chanlist[N_CHANS];
    int n_chan; //the examples are not consistent if it should be signed or unsigned
    comedi_range * range_info[N_CHANS];
    lsampl_t maxdata[N_CHANS];
    ACQUISITION_MODE acquisitionMode = ACQUISITION_MODE_NONE;
    double integrationTime = 0.0;
   
    struct ParsedOptions
    {
        QString filename;
        double value = 0.;
        int subdevice = 0;
        int channel = 0;
        int aref = 0;
        int range = 0;
        int physical = 0;
        bool verbose = true;
        int n_chan = 0;
        int n_scan = 0;
        double freq = 0.0;
    } options;
    
    bool prepareCmdLib()
    {
        bzero(&command,sizeof(command));
        int ret;
        unsigned scan_period_ns = unsigned(1e9 / options.freq);
        ret = comedi_get_cmd_generic_timed(dev, options.subdevice, &command, 
                                           n_chan, scan_period_ns);
        
        if(ret<0){
            Comm::out << "comedi_get_cmd_generic_timed failed" << endl;
            return false;
        }
        switch(acquisitionMode)
        {
            case ACQUISITION_MODE_BURST:
                command.stop_src = TRIG_COUNT;
                command.stop_arg = options.n_scan;                
                break;
            case ACQUISITION_MODE_CONTINUOUS:
                command.stop_src = TRIG_NONE;
                command.stop_arg = 0;
            default:
                Comm::err << "ERROR: invalid acquisition mode" << endl;
                return false;
        }
        command.chanlist = chanlist;
        command.chanlist_len = n_chan;
        
        return true;
    }
    
    
    
};

ComediHardwareController::ComediHardwareController(QString partName, QString name, Robot& robot)
    : RobotSubunit(partName, name, robot)
    , p(new ComediHardwareControllerPrivate)    
{
    for(int i = 0 ; i < 16 ; ++i)
    {
        _channelMap[i] = -1;
        _channels[i] = -1;
    }
}

bool ComediHardwareController::readParameters(const Parms& parms)
{
    Comm::out << "called constructor ComediHardwareController::ComediHardwareController() " << endl;
    _valid = false;
    //TODO: parse device specific parameters (device path etc.)
    //and set _isOBjectValid accordingly

    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, "ComediHardwareController"))
            return false;


    if(!parms(partName(), "DevPath", _devPath)) return false;
    Comm::out << QString("parsed DevPath %1").arg(_devPath) << endl;
    
    if(!parms(partName(), "CalibrationFile", _calibrationFile)) return false;
    Comm::out << QString("parsed CalibrationFile %1").arg(_calibrationFile) << endl;
    
    if(!parms(partName(), "SamplingFrequency", _samplingFrequency)) return false;
    if(!parms(partName(), "SubDevice", _subdevice)) return false;
    if(_subdevice < 0)
    {
        Comm::err << "Error ComediHardwareController [" << partName() << "], the subdevice cannot be negative" << endl;
        return false;
    }
    if(!parms(partName(), "VoltageValidityTime", _voltageValidityTime)) return false;
    _valid = true;
    return true;
}

bool ComediHardwareController::writeParameters(Parms& parms) const
{
    bool isOk = true;
    isOk &= parms.set(partName(), "DevPath", _devPath);
    parms.setComment(partName(), "DevPath",
                     "Path to the comedi device.\n"
                     "usually is /dev/comediN where N is the device number, starting at 0.");
    isOk &= parms.set(partName(), "CalibrationFile", _calibrationFile);    
    isOk &= parms.set(partName(), "SamplingFrequency", _samplingFrequency);
    isOk &= parms.set(partName(), "Version", version);
    isOk &= parms.set(partName(), "SubDevice", _subdevice);
    parms.setComment(partName(), "SubDevice",
                     "Set which sub-device to use.\n"
                     "Analog Input sub-device is typically 0.\n"
                     "More details can be obtained with the command 'comedi_board_info -v'");
    isOk &= parms.set(partName(), "VoltageValidityTime", _voltageValidityTime);
    parms.setComment(partName(), "VoltageValidityTime",
                     "Minimum time between two starts of acquisition.\n"
                     "If voltages are requested faster than this, the same values will be used.\n"
                     "Mostly useful if you have more than one channel.");
    return isOk;
}

/*
 *Sensor* ComediHardwareController::buildSensor(const Parms& parms, QString sensorName)
 *{
 *    Sensor* sensor = 0;
 *    SensorFactory factory;
 *    sensor = factory.sensor(parms, sensorName, this);
 *    Comm::out << "ComediHardwareController::buildSensor" << endl;
 *    if (sensor)
 *    {
 *        _sensors.push_back(sensor);
 *        _integrationTimeMs = sensor->IntegrationTimeMs();
 *        Comm::out << QString("_integrationTimeMs = %1").arg(_integrationTimeMs) << endl;
 *    }
 *    else
 *    {
 *        Comm::out << "ERROR: building sensor failed" << endl;
 *    }
 *    return sensor;
 *}
 */

double ComediHardwareController::integrationTime() const
{
    double integrationTime = -1.0;
    
    for (const ComediSensor* sensor: _sensors)
    {
        if (integrationTime < sensor->integrationTime())
        {
            integrationTime = sensor->integrationTime();
        }
    }
    
    return integrationTime;    
}

bool ComediHardwareController::initialize(bool force)
{
    if(_initialized and not force) return true;  
    _initialized = false;
    
    Comm::out << "inside ComediHardwareController::initialize()" << endl;    

    if (_sensors.size() < 1)
    {
        Comm::out << "ERROR: no sensors registered" << endl;
        return false;
    }

    //TODO: implement

    //- open device
    //- get registered sensors and prepare the acquisiton card
    //- send commands

    //TODO: use adcard.initMultichannel and provide the list of channels
    //The card (ADCard) knows nothing about the sensors it only delivers voltages related to channels
    //it is up to the ComediHardwareController to know which sensor is registered to which channel and how to update voltages, timestamps, status and forces
    _voltages.resize(0);
    for (size_t i=0; i<_sensors.size(); i++)
        _channels[i] = _sensors[i]->ADChannel();
    p->n_chan = _sensors.size();

    //isOK &= adcard.initMultichannel(_devPath, FORCE_ACQUISITION_MODE_BURST, _integrationTimeMs, _samplingFrequency, &_channels[0], _nchan);
    
    p->acquisitionMode = ACQUISITION_MODE_BURST;
    for (int i=0; i<(p->n_chan); i++)
    {
        p->chanlist[i] = this->_channels[i];
    }

    p->integrationTime = integrationTime();
    if (p->integrationTime <=0.0)
    {
        Comm::err << "Error: wrong integration time: " << p->integrationTime << endl;
        return false;
    }
    
    
    p->options = ComediHardwareControllerPrivate::ParsedOptions();
    p->options.freq = _samplingFrequency;
    p->options.n_scan = int(std::ceil(p->integrationTime * _samplingFrequency));
    p->options.subdevice = _subdevice;
    p->options.aref = AREF_GROUND;
    p->options.range = 1;
    
    p->dev = comedi_open(_devPath.toLocal8Bit());
    if (!p->dev)
    {
        comedi_perror(_devPath.toLocal8Bit());
        return false;
    }
    
    comedi_set_global_oor_behavior(COMEDI_OOR_NUMBER);
    
    for(int i = 0; i < p->n_chan; i++){
        //TODO: be careful, this overwrites the original array of integer channels, now the numbers no longer represent the original content
        int ch = p->chanlist[i];
        p->chanlist[i] = CR_PACK(ch, p->options.range, p->options.aref);
        p->range_info[i] = comedi_get_range(p->dev, p->options.subdevice, ch, p->options.range);
        p->maxdata[i] = comedi_get_maxdata(p->dev, p->options.subdevice, ch);
    }
    
    if (not p->prepareCmdLib()) return false;
    
    Comm::err << "initial dump of the command\n" << dump_cmd(p->command) << endl;
            
    
    int ret = comedi_command_test(p->dev, &p->command);
    if (ret<0)
    {
        comedi_perror("comedi_command_test");
        return false;
    }
    if (ret>0)
    {
        Comm::err << "Error: comedi_command_test modified the original command" << endl;
        Comm::err << dump_cmd(p->command) << endl;
        return false;
    }
    
    
    comedi_calibration_t* cal = comedi_parse_calibration_file(_calibrationFile.toLocal8Bit());
    if (cal == 0) //if no calibration available
    {
        Comm::out << _calibrationFile << " doesn't exist or is damaged. Trying to recreate..." << endl;
        QString cmd = QString("comedi_soft_calibrate -f %1 -S %2").arg(_devPath).arg(_calibrationFile);
        system(cmd.toLocal8Bit());
        cal = comedi_parse_calibration_file(_calibrationFile.toLocal8Bit());//try again
        if (cal == 0)
        {
            Comm::out << _calibrationFile << " doesn't exist or is damaged" << endl;
            Comm::out << "try the following command: sudo " << cmd << endl;
            return false;
        }
    }
    
    for (int i=0; i<p->n_chan; i++)
    {
        //TODO: each channel has its own converter, not only channel 15
        int res = comedi_apply_parsed_calibration(p->dev, p->options.subdevice, _channels[i], p->options.range, p->options.aref, cal);
        if (res)
        {
            Comm::out << "comedi_apply_parsed_calibration returned ERROR" << endl;
            //exit(1);
            return false;
        }
        res = comedi_get_softcal_converter(p->options.subdevice, _channels[i], p->options.range, COMEDI_TO_PHYSICAL, cal, (p->polynomial_converter + i));
        if (res)
        {
            Comm::out << "get_softcal_converter returned ERROR" << endl;
            //exit(1);
            return false;
        }
    }

    //isOK &= adcard.init(_devPath, FORCE_ACQUISITION_MODE_BURST, _integrationTimeMs, _samplingFrequency);
    Comm::out << QString("_devPath = %1").arg(_devPath) << endl;
    Comm::out << QString("_integrationTime = %1").arg(p->integrationTime) << endl;
    Comm::out << QString("_samplingFrequency = %1").arg(_samplingFrequency) << endl;

    return true;
}

bool ComediHardwareController::getConsistentSamplingFrequency(double * samplingFrequency)
{
    return false; //false means not consistent sampling frequency (or no sensors)
}

bool ComediHardwareController::getConsistentIntegrationTime(double * integrationTimeMs)
{
    return false; //false means no consistent integration time (or no sensors)
}

///TODO: allow more channels to be sampled, remember about timestamps
bool ComediHardwareController::voltage(int adchannel, double& voltage)
{
    if(!updateVoltages()) return false;
    voltage = _voltages[_channelMap[adchannel]];
    return true;
}


bool ComediHardwareController::registerSensor(ComediSensor* sensor)
{
    Comm::out << "ComediHardwareController::registerSensorForSampling(Sensor* sensor)" << endl;
    if (!sensor)
    {
        Comm::out << "ERROR: trying to register sensor with a null pointer" << endl;
        return false;
    }
    //check if exists already
    for(ComediSensor *s: _sensors)
    {
        if(sensor == s)
        {
            Comm::err << "Trying to register the same sensor twice" << endl;
            return false;
        }
    }
    if(_sensors.size() == 16)
    {
        Comm::err << "There are only 16 channels, you registered more than 16 sensors" << endl;
        return false;
    }
    int channel = sensor->ADChannel();
    if(channel < 0 or channel >= 16)
    {
        Comm::err << "Error, channel must be between 0 and 15 included. Provided: " << channel << endl;
        return false;
    }
    if(_channelMap[channel] >= 0)
    {
        Comm::err << "Error, channel " << channel
                    << " has already been registered by sensor "
                    << _sensors[_channelMap[channel]]->partName() << endl;
    }
    _channels[_sensors.size()] = channel; //channel number addressed by channel index
    _channelMap[channel]= _sensors.size();
    _sensors.push_back(sensor);
    return true;
}

bool ComediHardwareController::updateVoltages()
{
    size_t nbChannels = _sensors.size();

    if(_voltages.size() == nbChannels and acquisitionTime.timeElapsed() < _voltageValidityTime)
        return false;

    acquisitionTime.reset();

    std::valarray<double> avgVoltages(0.0, nbChannels);
    size_t totalReads = 0;

    int ret = comedi_command(p->dev, &p->command);
    if(ret < 0)
    {        
        comedi_perror("comedi_command");     
        return false;
    }
    
    int subdev_flags = comedi_get_subdevice_flags(p->dev, p->options.subdevice);
    size_t bufsize = 1000;
    int bytes_per_sample = (subdev_flags & SDF_LSAMPL) ? sizeof(lsampl_t) : sizeof(sampl_t);
    bufsize *= bytes_per_sample*nbChannels;
    std::vector<uint8_t> buf(bufsize);
    size_t shift = 0;
    size_t currentChannel = 0;
    lsampl_t raw=0;
    
    ret = 1;
    while(ret > 0)
    {
        ret = read(comedi_fileno(p->dev), &buf[0] + shift, buf.size()-shift);
        if (ret<0)
        {
            perror("read on comedi device");
            return false;
        }
        else if(ret>0)
        {
            ret += shift;
            size_t end = size_t(ret) / bytes_per_sample;
            shift = size_t(ret) - (end*bytes_per_sample);
            for(size_t i = 0 ; i < end ; ++i)
            {
                if(subdev_flags & SDF_LSAMPL)
                    raw = ((lsampl_t *)&buf[0])[i];
                else
                    raw = ((sampl_t *)&buf[0])[i];
                avgVoltages[currentChannel] += comedi_to_physical(raw, &(p->polynomial_converter[currentChannel]));
                totalReads++;
                currentChannel++;
                if(currentChannel >= nbChannels)
                    currentChannel = 0;
            }
            if(shift != 0)
                memcpy(&buf[0], &buf[end*bytes_per_sample], shift);
        }
    }
    if(totalReads == 0)
    {
        Comm::err << "Error, no data received" << endl;
        return false;
    }
    if(totalReads % nbChannels != 0)
    {
        Comm::err << "Error, the number of reads is not a multiple to the number of channel" << endl;
        return false;
    }
    totalReads /= nbChannels;
    avgVoltages /= totalReads;

    std::swap(avgVoltages, _voltages);

    return true; //TODO: return something meaningful
}
