/*
 * File:   StateManager.hpp
 * Author: cfmadmin
 *
 * Created on December 11, 2012, 6:02 PM
 */

#ifndef STATEMANAGER_HPP
#define STATEMANAGER_HPP

#include "MeasurementState.hpp"
#include "DataLogger.hpp"
#include <QString>
#include <exception>
#include "Comm.hpp"
//#include "Experiment.hpp"

struct FieldError : public Comm::QStringException
{
    enum Error
    {
        DUPLICATE_FIELD,
        INVALID_NAME,
        EXPERIMENT_RUNNING,
        UNKNOWN
    } error = UNKNOWN;

    QString name;

    FieldError(Error err, QString n)
        : Comm::QStringException()
        , error(err)
        , name(n)
    { }

    ~FieldError() throw() { }

    QString text() const throw () override;
};

//class Experiment; //forward declaration to avoid a loop

class StateManager {

public:
    class Field
    {
    public:
        Field()
            : _id(-1)
            , _sm(0)
        { }

        Field(int id, StateManager* sm)
            : _id(id)
            , _sm(sm)
        { }

        double& operator()()
        {
            return (*_sm)[_id];
        }

        const double& operator()() const
        {
            return (*_sm)[_id];
        }

        explicit operator bool() const
        {
            return _id >= 0;
        }

        QString name() const
        {
            return _sm->name(_id);
        }

        QString unit() const
        {
            return _sm->unit(_id);
        }

        bool stable() const
        {
            return _sm->stable(_id);
        }
    protected:
        int _id;
        StateManager* _sm;
    };
    StateManager();
    /*StateManager(const StateManager& orig);*/
    virtual ~StateManager();

    MeasurementState& state()
    {
        return _state;
    }

    DataLogger& logger()
    {
        return _logger;
    }

    bool renew();

    ///TODO: call registerField and throw an exception on error
    int registerFieldExc(QString name, QString unit, bool stable = true);

    ///TODO: return the key with -1 if error
    int registerField(QString name, QString unit, bool stable = true);

    Field field(QString name);
    Field field(int id);

    int fieldIndex(QString name);

    QString name(int id) const
    {
        return _state.name(id);
    }

    bool writeParameters(Parms& parms) const;
    double& operator[](int id) {return _state[id];}
    const double& operator[](int id) const {return _state[id];}

    bool stable(int id) const
    {
        return _state.stable(id);
    }

    QString unit(int key) const
    {
        return _state.unit(key);
    }

    /**
     * Suspend execution.
     *
     * New fields can be added after this,
     */
    bool suspend();

    /**
     * Configure the state manager to allow for data to be written
     */
    bool configure(QString filename);

protected:

    MeasurementState _state;
    DataLogger _logger;
};

#endif /* STATEMANAGER_HPP */

