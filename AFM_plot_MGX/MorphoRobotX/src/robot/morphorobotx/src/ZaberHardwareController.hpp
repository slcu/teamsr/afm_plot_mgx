/*
 * File:   ZaberHardwareController.hpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 3:05 PM
 */

#ifndef ZABERHARDWARECONTROLLER_HPP
#define ZABERHARDWARECONTROLLER_HPP

#include "RobotSubunit.hpp"
#include "Version.hpp"
#include <memory>
#include <cstdint>

class Robot;
class ZaberHardwareControllerPrivate;

typedef unsigned int uint;
class ZaberAxis;

class ZaberHardwareController : public RobotSubunit
{
public:
    ZaberHardwareController(QString partName, QString name, Robot& robot);

    virtual bool readParameters(const Parms& parms) override;
    virtual bool writeParameters(Parms& parms) const override;

    bool get(unsigned int option, double& value) override; ///TODO: require specific type for the option
    bool set(unsigned int option, double value) override; ///TODO: require specific type for the option
    bool initialize(bool force = false) override;
    bool valid() const override;

    static constexpr Version version = Version{1,0};

    bool registerAxis(ZaberAxis* axis);

    bool openPort();
    bool flushInputBuffer(uint msecTimeout);
    bool setDeviceMode(uint8_t device, uint32_t mode);
    bool setAcceleration(uint8_t device, uint32_t acceleration);
    bool setMicrostepResolution(uint8_t device, uint8_t microsteps);
    bool gotoAbsolute(uint8_t device, int32_t position);
    bool goHome(uint8_t device);
    bool renumber();
    bool firmwareVersion(uint32_t& version);
    bool returnSetting(uint8_t device, uint32_t& setting);
    bool currentPosition(uint8_t device, int32_t& position);
    bool knowsHome(uint8_t device, bool& result);
    bool findHome(uint8_t);
    bool disablePotentiometer();
    bool enablePotentiometer();

    double accelerationLimit() const;

protected:
    struct Command
    {
        uint8_t deviceByte;
        uint8_t commandID;
        uint8_t _data[4];
        uint32_t data() const;
        void setData(uint32_t d);
    };

    int receive6Bytes(uint8_t *buf, uint mstimeout);
    bool sendCommand(uint8_t device, uint8_t command, uint32_t value, bool waitForReply, uint32_t *reply = 0);
    bool sendCommand(uint8_t device, uint8_t command, int32_t value, bool waitForReply, uint32_t *reply = 0);
    bool sendCommand(uint8_t device, uint8_t command, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, bool waitForReply, uint32_t *reply = 0);
    bool sendCommand(const Command& cmd, Command& response, bool waitForReply);
    bool getResponse(Command& response);

    std::unique_ptr<ZaberHardwareControllerPrivate> p; //it's going to be properly deallocated
    bool _valid;
};

#endif /* ZABERHARDWARECONTROLLER_HPP */

