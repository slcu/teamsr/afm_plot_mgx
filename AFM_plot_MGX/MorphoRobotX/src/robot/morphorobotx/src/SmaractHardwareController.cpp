/* 
 * File:   SmaractHardwareController.cpp
 * Author: cfmadmin
 * 
 * Created on August 20, 2013, 10:20 PM
 */

#include "SmaractHardwareController.hpp"
#include "SmaractAxis.hpp"
#include "Comm.hpp"
#include "Robot.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include "MCSController.hpp"
#include "SmaractCommands.hpp"
#include <functional>

using namespace std::placeholders;

REGISTER_ROBOT_SUBUNIT_TYPE(SmaractHardwareController);

constexpr Version SmaractHardwareController::version;

SmaractHardwareController::SmaractHardwareController(QString partName, QString name, Robot& robot)
    : RobotSubunit(partName, name, robot)
    , _valid(false)
    , _initialized(false)
    , _hardwareID(0)
    , _systemIndex(0)       
{
    
}

bool SmaractHardwareController::readParameters(const Parms& parms)
{
    Comm::out << "Inside SmaractHardwareController::readParameters()" << endl;
    _valid = false;
    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
    {
        if(not version.canReadData(dataVersion, QString("SmaractHardwareController [%1]").arg(partName())))
        {
            Comm::out << "Inside SmaractHardwareController::readParameters() - cannot read data due to version" << endl;
            return false;
        }    
    }
    else
    {
        Comm::out << "Inside SmaractHardwareController::readParameters() - Version not specified" << endl;
    }
    //TODO: more params here    
        
    if (!parms(partName(), "HardwareID", _hardwareID)) return false;
    if (!parms(partName(), "Controller", _mcsControllerName)) return false;
        
    _mcsController = robot().addPart<MCSController>(_mcsControllerName, _mcsControllerName);
    
    if (not _mcsController->registerSmaractController(this))
    {
        Comm::err << "Error, SmaractHardwareController::readParameters() - cannot register to MCSControl" << endl;
        return false;
    }
    
    Comm::out << "Inside SmaractHardwareController::readParameters() - everything OK" << endl;
    _valid = true;
    return true;
}

bool SmaractHardwareController::writeParameters(Parms& parms) const
{
    Comm::out << "Inside SmaractHardwareController::writeParameters()" << endl;
    bool isOK = true;
    isOK &= parms.set(partName(), "Version", version);
    //TODO: more params here
    return isOK;
}

bool SmaractHardwareController::set(unsigned int option, double value)
{
    //TODO: implement
    return false;
}

bool SmaractHardwareController::get(unsigned int option, double& value)
{
    //TODO: implement
    return false;
}

bool SmaractHardwareController::initialize(bool force)
{
    Comm::out << "Inside SmaractHardwareController::initialize()" << endl;
    if (not _mcsController) return false;    
    if (not _mcsController->initialize(force)) return false; //TODO: actually there's not point in forcing it...
    //now, see if this controller can find its own id in the list of initialized systems    
    //first, get the number of initialized systems
    unsigned int initializedSystems;
    if (not _mcsController->numberOfInitializedSystems(initializedSystems)) return false;
    unsigned int hwID=0;
    bool foundMyself = false;    
    for (unsigned int i=0; i<initializedSystems; i++)
    {
        if (not _mcsController->getSystemID(i, &hwID)) return false;        
        if (hwID == hardwareID())
        {
            foundMyself = true;
            _systemIndex = i; // your i, thats your systemIndex assigned by the top smaract library
            _initialized = true;
            break; //when found, no need to query further
        }
    }
    if (!foundMyself)
    {
        Comm::out << QString("ERROR: SmarActHardwareController::initialize() failed: could not find hardwareID %1 in the list of successfully initialized systems").arg(hardwareID()) << endl;
        return false;
    }
            
    //_valid = true;
    return true;//TODO: return true
}

bool SmaractHardwareController::valid() const
{
    return _valid;
}

bool SmaractHardwareController::registerAxis(SmaractAxis* axis)
{   if (not axis)
    {
        Comm::err << "Error, SmaractHardwareController::registerAxis(), trying to register a null pointer" << endl;
        return false;
    }
    _axes.push_back(axis);
    //TODO: what else besides adding to a list ?        
    return true; 
}

bool SmaractHardwareController::getPosition(unsigned int axisID, int32_t& position)
{
    //SmaractResult result = SA_GetPosition_S(_systemIndex, axisID, &position);
    //auto cmd = std::bind(SA_GetPosition_S, _1, axisID, &position);
    auto result = smaract::sendRepeat(std::bind(SA_GetPosition_S, _systemIndex, axisID, &position), 3);
    if(!result)
    {
        Comm::err << "Error calling getPosition(): " << result.getDescription() << endl;
        return false;
    }
    return true;
}

bool SmaractHardwareController::gotoSync(unsigned int axisID, int32_t position, unsigned int holdMs)
{
    auto result = smaract::sendRepeat(std::bind(SA_GotoPositionAbsolute_S, _systemIndex, axisID, position, holdMs), 3);    
    if (not result)
    {
        Comm::err << "Error calling gotoPositionAbsolute(): " << result.getDescription() << endl;
        return false;
    }
    return true;
}

bool SmaractHardwareController::getStatus(unsigned int axisID, SmaractAxisStatus& status)
{    
    unsigned int axisStatus;
    auto result = smaract::sendRepeat(std::bind(SA_GetStatus_S, _systemIndex, axisID, &axisStatus), 3);        
    if (not result)
    {
        Comm::err << "Error calling getStatus(): " << result.getDescription() << endl;
        return false;
    }
    status.setStatus(axisStatus);
    return true;
}

bool SmaractHardwareController::setClosedLoopMaxFrequency(unsigned int axisID, unsigned int frequency)
{
    Comm::out << "SmaractHardwareController::setClosedLoopMaxFrequency(..) setting frequency " << frequency << " Hz" << endl;
    auto result = smaract::sendRepeat(std::bind(SA_SetClosedLoopMaxFrequency_S, _systemIndex, axisID, frequency), 3);
    if (not result)
    {
        Comm::err << "Error calling setClosedLoopMaxFrequency(...): " << result.getDescription() << endl;
        return false;
    }
    return true;
}

bool SmaractHardwareController::setClosedLoopSpeed(unsigned int axisID, unsigned int speedNmPerS)
{
    Comm::out << "SmaractHardwareController::setClosedLoopSpeed(..) setting speed " << speedNmPerS << " nanometers per second (hardware-native unit)" << endl;
    auto result = smaract::sendRepeat(std::bind(SA_SetClosedLoopMoveSpeed_S, _systemIndex, axisID, speedNmPerS), 3); //nanometers per second
    if (not result)
    {
        Comm::err << "Error calling setClosedLoopSpeed(...): " << result.getDescription() << endl;
        return false;   
    }
    return true;
}

bool SmaractHardwareController::getClosedLoopSpeed(unsigned int axisID, unsigned int& speedNmPerS)
{
    Comm::out << "SmaractHardwareController::getClosedLoopSpeed(..) getting speed" << endl;
    auto result = smaract::sendRepeat(std::bind(SA_GetClosedLoopMoveSpeed_S, _systemIndex, axisID, &speedNmPerS), 3);
    if (not result)
    {
        Comm::err << "Error calling getClosedLoopSpeed(...): " << result.getDescription() << endl;
        return false;   
    }
    Comm::out << "SmaractHardwareController::getClosedLoopSpeed(..) got speed " << speedNmPerS << " nanometers per second (hardware-native unit)" << endl;
    return true;
}

bool SmaractHardwareController::stop(unsigned int axisID)
{
    Comm::out << "SmaractHardwareController::stop(...)" << endl;
    auto result = smaract::sendRepeat(std::bind(SA_Stop_S, _systemIndex, axisID),3);
    if (not result)
    {
        Comm::err << "Error calling stop(...): " << result.getDescription() << endl;
        return false;   
    }
    Comm::out << "SmaractHardwareController::stop(...) successfully stopped axis " << axisID << endl;
    return true;
    
    return true;
}