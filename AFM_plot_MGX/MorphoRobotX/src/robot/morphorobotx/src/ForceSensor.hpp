/*
 * File:   ForceSensor.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 9:07 PM
 */

#ifndef FORCESENSOR_HPP
#define FORCESENSOR_HPP

#include "Sensor.hpp"
#include <exception>

struct ForceSensorError : public std::exception
{
    enum MISSING_FIELDS : unsigned int
    {
        NONE = 0,
        DIRECTION = 0x1,
        AXIS = 0x2
    };
    ForceSensorError(unsigned int mis)
        : std::exception()
        , missing(mis)
    { }
    
    char const * what() const throw ();
    
    unsigned int missing = 0;
};

class ForceSensor
{
public:
    ForceSensor(SensorPtr sensor = SensorPtr()) throw (ForceSensorError);
    ForceSensor(const ForceSensor& copy) = default;

    bool direction() const;
    QString axis() const;
    double minReadForce() const;
    double maxReadForce() const;
    double minBreakForce() const;
    double maxBreakForce() const;
    
    QString name() const { return _sensor->name(); }

    bool hasMinReadForce() const;
    bool hasMaxReadForce() const;
    bool hasMinBreakForce() const;
    bool hasMaxBreakForce() const;
    
    SensorPtr sensor() { return _sensor; }
    
    bool update() const
    {
        return _sensor->update();
    }
    
    ForceSensor& operator=(const ForceSensor& f) = default;
    bool operator==(const ForceSensor& other) const
    {
        return _sensor == other._sensor;
    }
    bool operator!=(const ForceSensor& other) const
    {
        return _sensor != other._sensor;
    }
    
    explicit operator bool() const { return (bool)_sensor; }

protected:
    SensorPtr _sensor;
    unsigned int _axis = 0;
    unsigned int _direction = 0;
    unsigned int _minReadForce = 0;
    unsigned int _maxReadForce = 0;
    unsigned int _minBreakForce = 0;
    unsigned int _maxBreakForce = 0;
};

#endif /* FORCESENSOR_HPP */

