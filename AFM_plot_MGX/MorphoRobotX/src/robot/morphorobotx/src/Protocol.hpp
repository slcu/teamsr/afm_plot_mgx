#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP

#include <QHash>
#include <QString>
#include <memory>
//#include "Context.hpp"
#include <stdlib.h>
//#include "MeasurementState.hpp"
#include <typeinfo>
#include "Factory.hpp"

class Parms;
class Experiment;
class StateManager;

/**
 * \class Protocol Protocol.hpp "Protocol.hpp"
 *
 * Abstract base class of the protocols.
 */
struct Protocol
{
    /**
     * Constructor, with the shared experiment
     */
    Protocol(QString name, Experiment& experiment);
    /**
     * Virtual destructor
     *
     * This is mandatory but should be left empty, and will probably not be overriden.
     */
    virtual ~Protocol() { };
    /**
     * run method
     *
     * This is the main entry point of a protocol, in which the magic should happen.
     * You have to override this method to be able to create a protocol.
     */
    virtual bool run() = 0;
    //virtual void run(Context& context) = 0;

    /**
     * Function that will read the parameters from the given object and will create/get its subprotocols
     */
    virtual bool init(const Parms& parms) = 0;

    /**
     * Function that will write extra parameters to the object
     */
    virtual bool writeParameters(Parms& parms) const = 0;

    Experiment& experiment()
    {
        return _experiment;
    };

    const QString& name() const
    {
        return _name;
    };

    StateManager& state() { return _state; }

protected:
    Experiment& _experiment;
    QString _name;///refers to section name in the param file
    StateManager& _state;
};

typedef std::shared_ptr<Protocol> ProtocolPtr;

extern Protocol* makeProtocol(QString, Experiment&);
extern const char protocol_name[];

MAKE_INSTANCE_FACTORY(ProtocolFactory, protocol_name, makeProtocol);

/**
 * Short-cut to access the protocol factory
 */
inline ProtocolFactory& protocolFactory() { return ProtocolFactory::instance(); }

/**
 * \def REGISTER_PROTOCOL(name, makeFct)
 * Register the function \c makeFct as a protocol maker.
 */
#define REGISTER_PROTOCOL(name, makerFct) REGISTER_FACTORY_OBJECT(ProtocolFactory, name, makerFct)

/**
 * \def REGISTER_PROTOCOL_TYPE(ProtocolClass)
 * Register the class \c ProtocolClass as a protocol
 */
#define REGISTER_PROTOCOL_TYPE(ProtocolClass) REGISTER_FACTORY_OBJECT_TYPE(ProtocolFactory, ProtocolClass)

#endif // PROTOCOL_HPP

