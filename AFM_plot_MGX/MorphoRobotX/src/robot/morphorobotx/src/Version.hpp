#ifndef VERSION_HPP
#define VERSION_HPP

#include <QString>

class QTextStream;

class Version
{
    unsigned int _major;
    unsigned int _minor;
public:

    /**
     * Creates an invalid version
     */
    constexpr Version()
      : _major(0)
      , _minor(0)
    { }

    /**
     * Create a version from the numeric values.
     *
     * The default values correspond to an INVALID version number!
     */
    constexpr Version(unsigned int M, unsigned int m)
      : _major(M)
      , _minor(m)
    { }

    /**
     * Create a version from a string
     *
     * If the string is invalid, then an invalid version is created.
     */
    explicit Version(const QString& v);

    /**
     * Copy the version
     */
    Version(const Version& other);

    /**
     * Check if the version is valid
     */
    constexpr explicit operator bool()
    {
        return (_major > 0) or (_minor > 0);
    }

    /**
     * Check if the data will be readable.
     * The data is readable if:
     *
     * 1. Both versions are valid
     * 2. The major is the same
     * 3. The minor of the data is smaller than the one of the code
     */
    constexpr bool canReadData(const Version& dataVersion)
    {
        return bool(dataVersion) and bool(*this) and
               (major() == dataVersion.major()) and
               (minor() >= dataVersion.minor());
    }

    /**
     * Checks if the data will be readable and output meaningful error in case it doesn't
     */
    bool canReadData(const Version& dataVersion, QString objectName) const;

    /**
     * Change the version based on the string
     *
     * \returns True if this was a valid version (i.e. on the format MAJOR.MINOR)
     */
    Version& operator=(const QString& v);

    /**
     * Convert into a string on the form MAJOR.MINOR
     */
    explicit operator QString() const
    {
        return QString("%1.%2").arg(_major).arg(_minor);
    }

    /**
     * Assignement operator
     */
    Version& operator=(const Version& other)
    {
        _major = other._major;
        _minor = other._minor;
        return *this;
    }

    /**
     * Reset the version to an invalid one
     */
    void clear();

    constexpr bool operator==(const Version& other)
    {
        return _major == other._major and _minor == other._minor;
    }

    constexpr bool operator!=(const Version& other)
    {
        return _major != other._major or _minor != other._minor;
    }

    constexpr bool operator<=(const Version& dataVersion)
    {
        return (_major < dataVersion._major) or
               ((_major == dataVersion._major) and
                (_minor <= dataVersion._minor));
    }

    constexpr bool operator>=(const Version& dataVersion)
    {
        return (_major > dataVersion._major) or
               ((_major == dataVersion._major) and
                (_minor >= dataVersion._minor));
    }

    constexpr bool operator<(const Version& dataVersion)
    {
        return (_major < dataVersion._major) or
               ((_major == dataVersion._major) and
                (_minor < dataVersion._minor));
    }

    constexpr bool operator>(const Version& dataVersion)
    {
        return (_major > dataVersion._major) or
               ((_major == dataVersion._major) and
                (_minor > dataVersion._minor));
    }

    /**
     * Get the major number
     */
    constexpr unsigned int major() { return _major; }

    /**
     * Get the minor number
     */
    constexpr unsigned int minor() { return _minor; }

};

extern QTextStream& operator<<(QTextStream& ss, const Version& version);
extern QTextStream& operator>>(QTextStream& ss, Version& version);

#endif // VERSION_HPP
