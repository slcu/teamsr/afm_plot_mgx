/*
 * File:   ComediForceSensor.cpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 9:07 PM
 */

#include "ComediForceSensor.hpp"
#include "MeasurementState.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include "ComediHardwareController.hpp"
#include "StateManager.hpp"
#include "Comm.hpp"
#include "Robot.hpp"
#include "StateManager.hpp"

namespace
{
const double double_nan = std::numeric_limits<double>::quiet_NaN();
}

REGISTER_ROBOT_SUBUNIT_TYPE(ComediForceSensor);

ComediForceSensor::ComediForceSensor(QString partName, QString name, Robot& robot)
    : ComediSensor(partName, name, robot)
    , _minReadForce(double_nan)
    , _maxReadForce(double_nan)
    , _minBreakForce(double_nan)
    , _maxBreakForce(double_nan)
    , _direction(true)
{
    registerOption("Axis", 1000, StringOption | ReadOnlyOption);
    registerOption("Direction", 1001, NumberOption | ReadOnlyOption);
    registerOption("MinBreakForce", 1002, NumberOption | ReadOnlyOption);
    registerOption("MaxBreakForce", 1003, NumberOption | ReadOnlyOption);
    registerOption("MinReadForce", 1004, NumberOption | ReadOnlyOption);
    registerOption("MaxReadForce", 1005, NumberOption | ReadOnlyOption);
}

bool ComediForceSensor::get(unsigned int id, double& val)
{
    switch(id)
    {
        case 1001:
            val = (_direction ? 1.0 : 0.0);
            return true;
        case 1002:
            val = _minBreakForce;
            return true;
        case 1003:
            val = _maxBreakForce;
            return true;
        case 1004:
            val = _minReadForce;
            return true;
        case 1005:
            val = _maxReadForce;
            return true;
        default:
            return false;
    }
}

bool ComediForceSensor::get(unsigned int id, QString& val)
{
    if(id == 1000)
    {
        val = _axis;
        return true;
    }
    return false;
}

bool ComediForceSensor::update()
{
    double voltage;
    if(!_controller->voltage(_ADChannel, voltage)) return false;
    _stateManager->state()[_voltageField] = voltage;
    _stateManager->state()[_forceField] = _gain*voltage;
    return true;
}

bool ComediForceSensor::readParameters(const Parms& parms)
{
    _valid = false;
    if(!parms(partName(), "Gain", _gain)) return false;
    _forceField = robot().stateManager().registerFieldExc(name(), "N", false);
    _voltageField = robot().stateManager().registerFieldExc(QString("%1_Voltage").arg(name()), "V", false);
    if(!parms(partName(), "Axis", _axis)) return false;
    if(!parms(partName(), "Direction", _direction)) return false;
    if(!parms(partName(), "MinBreakForce", _minBreakForce)) return false;
    if(!parms(partName(), "MaxBreakForce", _maxBreakForce)) return false;
    if(!parms(partName(), "MinReadForce", _minReadForce)) return false;
    if(!parms(partName(), "MaxReadForce", _maxReadForce)) return false;
    return ComediSensor::readParameters(parms);
}

bool ComediForceSensor::writeParameters(Parms& parms) const
{
    bool isOk = true;
    isOk &= parms.set(partName(), "Gain", _gain);
    parms.setComment(partName(), "Gain", "Gain transforming voltage into relative force.");
    isOk &= parms.set(partName(), "Axis", _axis);
    isOk &= parms.set(partName(), "Direction", _direction);
    parms.setComment(partName(), "Direction", "If true, positives force correspond to forces with the same direction that the axis. If false, forces are opposite.");
    isOk &= parms.set(partName(), "MinBreakForce", _minBreakForce);
    isOk &= parms.set(partName(), "MaxBreakForce", _maxBreakForce);
    isOk &= parms.set(partName(), "MinReadForce", _minReadForce);
    isOk &= parms.set(partName(), "MaxReadForce", _maxReadForce);
    return ComediSensor::writeParameters(parms);
}

bool ComediForceSensor::initialize(bool force)
{
    _stateManager = &robot().stateManager();
    return ComediSensor::initialize(force);
}

#if 0
    
bool ComediForceSensor::writeParameters(Parms& parms) const
{
    return false;
}
bool ComediForceSensor::readParameters(const Parms& parms) 
{
    _valid = false;
    offsetVoltage = 0.0;

    bool isOK = true;
    _name = sensorName;//TODO: consider if this should be moved to base class constructor

    QString sensorSectionName;

    isOK &= parms(sensorName, "SensorSectionName", sensorSectionName);

    //TODO: parse parms, use sensorName etc.
    isOK &= parms(sensorSectionName, "ADChannel", _ADChannel);
    isOK &= parms(sensorSectionName, "DAChannel", _DAChannel);
    isOK &= parms(sensorSectionName, "DAVoltage", _DAVoltage);
    isOK &= parms(sensorSectionName, "IntegrationTime", _integrationTime);
    Comm::out << "inside ComediForceSensor::ComediForceSensor(...)" << endl;
    Comm::out << QString("_IntegrationTime = %1").arg(_integrationTime) << endl;
    isOK &= parms(sensorSectionName, "MaxForce", _maxForce);
    isOK &= parms(sensorSectionName, "Gain", _gain);
    isOK &= parms(sensorSectionName, "InvertedGain", _invertedGain);
    isOK &= parms(sensorSectionName, "Axis", _axisName);
    isOK &= parms(sensorSectionName, "AxisPositivePushing", _axisPositivePushing);

    //TODO: contact the state manager and register sensor fields
    _fieldIDForce = this->hardwareController()->stateManager().registerFieldExc(sensorName+"_Force", "(uN)", false);
    _fieldIDOffsetVoltage = this->hardwareController()->stateManager().registerFieldExc(sensorName+"_OffsetVoltage", "(V)", true);
    _fieldIDOffsetForce = this->hardwareController()->stateManager().registerFieldExc(sensorName+"_OffsetForce", "(uN)", true);
    _fieldIDVoltage = this->hardwareController()->stateManager().registerFieldExc(sensorName+"_Voltage", "(V)", false);
    //TODO: _fieldIDOffset missing

    _valid = isOK;
    return false; //TODO: ! return isOK ?
}

//ComediForceSensor::ComediForceSensor(const ComediForceSensor& orig) {
//}

ComediForceSensor::~ComediForceSensor() {
}
/*
double ComediForceSensor::getForce(double voltage)
{
    double force = 0.0;

    force = voltage*gain - offsetForce;

    return force;
}*/

double ComediForceSensor::getForce()
{    //TODO: get the force from stateMangaer, take timestamps into accout etc...
    double force = 0.0;
    force = (getVoltage() - offsetVoltage)*this->_Gain;
    this->hardwareController()->stateManager().state().update(_fieldIDForce, force);
    //Comm::out << QString("Force %1 (uN), offsetVoltage = %2, gain = %3 ").arg(force).arg(offsetVoltage).arg(this->_Gain) << endl;
    return force;
}

///TODO: dangerous, no error handling
///TODO: add state manager in between
///TODO: consider: shall every sensor update its voltage in state manager, or the comedi controller should do that for each sensor it samples ?
double ComediForceSensor::getVoltage()
{
    double voltage = 0.0;
    //Comm::out << "calling ComediForceSensor::getVoltage()" << endl;
    //((ComediHardwareController*)this->_itsHardwareController)->getVoltage(voltage);
    ((ComediHardwareController*)this->_itsHardwareController)->getVoltage(this->ADChannel(), voltage);
    this->hardwareController()->stateManager().state().update(_fieldIDVoltage, voltage);
    //Comm::out << QString("ComediForceSensor::getVoltage() returns %1").arg(voltage) << endl;
    return voltage;
}

bool ComediForceSensor::get(unsigned int option, double& value)
{
    return false; //nothing supported
}

bool ComediForceSensor::set(unsigned int option, double value)
{
    return false; //nothing supported
}

bool ComediForceSensor::initialize()
{
    Comm::out << "Inside ComediForceSensor::initialize()" << endl;
    if( !_valid)
    {
        Comm::out << "ERROR: cannot initialize: instance not valid" << endl;
        return false;
    }

    bool isOK = true;
    //initialize its controller if needed. The acqusition card should already have the list of connected sensors to know how to access them (Register).

    if (!this->_itsHardwareController->isInitialized())
    {
        isOK &= _itsHardwareController->initialize();
        if (!isOK)
        {
            Comm::out << "ERROR: failed to initialize hardware controller" << endl;
            return false;
        }
    }

    //power the sensor up (should be possible individually for each output channel)
    PowerUp();

    return isOK;
}

bool ComediForceSensor::PowerUp()
{
    //TODO: implement
    //use:
    //this->_DAChannel
    //this->_DAVoltage
    //ask comedi to set the voltage out

    //check the voltage and maybe wait until stable (if possible)
    //alternatively, make external waiting for stable reading, and no waiting here

    return false;
}

bool ComediForceSensor::PowerDown()
{
    //TODO: the same as power up, but no need to wait until its ready
    return false;
}

bool ComediForceSensor::registerSensorToHardwareController()
{
    Comm::out << "inside of ComediForceSensor::registerSensorToHardwareController" << endl; //TODO: remove
    return ((ComediHardwareController*)this->hardwareController())->registerSensorForSampling(this);
}

bool ComediForceSensor::isPushingPositive()
{
    return _AxisPositivePushing; //TODO: ugly name, rename
}

void ComediForceSensor::setOffsetVoltage(double offsetVoltage);
{
    this->offsetVoltage = offsetVoltage;

    robot().stateManager().state().update(_fieldIDOffsetVoltage, offsetVoltage);
    robot().stateManager().state().update(_fieldIDOffsetForce, getOffsetForce());
}
#endif