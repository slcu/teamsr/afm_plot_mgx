/*
 * File:   Robot.hpp
 * Author: cfmadmin
 *
 * Created on February 7, 2013, 9:42 PM
 */

#ifndef ROBOT_HPP
#define ROBOT_HPP

#include <QString>
#include <memory>
#include "RobotSubunit.hpp"
#include "Version.hpp"


class StateManager;
class Experiment;
class Parms;
class Axis;
class Sensor;
enum AxisOption: unsigned int; ///this is legal only in C++0x, forward declaration of enum

typedef std::shared_ptr<Axis> AxisPtr;
typedef std::shared_ptr<Sensor> SensorPtr;

typedef struct {
    unsigned int robotAxisID;
    unsigned int actuatorSystemID;
    unsigned int localAxisID;
    QString name;
    signed int flip; ///+1 or -1, a multiplier for the coordinates ///TODO: change into true false
} RobotRecipeEntry;

///TODO: replace that with simply parameter file (robot recipe file)
typedef struct
{
    std::vector<RobotRecipeEntry> entries;
} RobotRecipe;

class Robot {
public:
    Robot(Experiment& experiment);
    virtual ~Robot();
    bool readParameters(const Parms& parms);
    bool writeParameters(Parms& parms) const;
    bool initialize();
    StateManager& stateManager()
    {
        return _stateManager;
    }

    RobotSubunitPtr addPart(QString partName, QString name);
    
    template <typename T>
    std::shared_ptr<T> addPart(QString partName, QString name)
    {
        return std::dynamic_pointer_cast<T>(addPart(partName, name));
    }

    bool addPart(RobotSubunitPtr part);

    bool hasPart(QString name) const;

    RobotSubunitPtr part(QString name);

    template <typename T>
    std::shared_ptr<T> part(QString name)
    {
        return std::dynamic_pointer_cast<T>(this->part(name));
    }

    bool remove(RobotSubunitPtr part);

    SensorPtr sensor(QString sensorName);
    AxisPtr axis(QString axisName);

    AxisPtr indentationAxis();
    SensorPtr indentationSensor();
    
    Experiment& experiment() { return _experiment; }

    static constexpr Version version = Version(1,0);

protected:
    Experiment& _experiment;
    std::vector<RobotSubunitPtr> _parts;
    StateManager& _stateManager;
};

#endif /* ROBOT_HPP */

