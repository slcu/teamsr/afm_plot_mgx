/*
 * File:   ZaberAxis.cpp
 * Author: cfmadmin
 *
 * Created on March 5, 2013, 11:55 AM
 */

#include "ZaberAxis.hpp"
#include "ZaberHardwareController.hpp"
#include "Comm.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include "Robot.hpp"
#include "StateManager.hpp"
#include "MeasurementState.hpp"

REGISTER_ROBOT_SUBUNIT_TYPE(ZaberAxis);

constexpr Version ZaberAxis::version;

enum ZaberAxisOption //TODO: should this go to hpp ?
{
    HOME_STATUS = 1000,
    NEED_HOME = 1001
};

ZaberAxis::ZaberAxis(QString partName, QString name, Robot& robot)
    : Axis(partName, name, robot)
{
    registerOption("GotoTimeout", GOTO_TIMEOUT, ReadOnlyOption | NumberOption);
    registerOption("GotoPrecisionMargin", GOTO_PRECISION_MARGIN, ReadOnlyOption | NumberOption);
    registerOption("AccelerationLimit", ACCELERATION_LIMIT, ReadOnlyOption | NumberOption);
    registerOption("Home", HOME_STATUS, ReadWriteOption | NumberOption);
    registerOption("NeedHome", NEED_HOME, ReadOnlyOption | NumberOption);
}

bool ZaberAxis::goHome()
{
    return set(HOME_STATUS, 1);
}

bool ZaberAxis::knowsHome(bool& result)
{
    double res;
    if(!get(HOME_STATUS, res)) return false;
    result = bool(res);
    return true;
}

bool ZaberAxis::setAcceleration(double val)
{
    return set(ACCELERATION_LIMIT, val);
}

bool ZaberAxis::setGotoPrecisionMargin(double val)
{
    return set(GOTO_PRECISION_MARGIN, val);
}

bool ZaberAxis::setGotoTimeout(double val)
{
    return set(GOTO_TIMEOUT, val);
}

bool ZaberAxis::readParameters(const Parms& parms)
{
    _valid = false;
    //TODO: what if it fails? The same problem as with constructor of SmarActAxis
    Comm::out << "Reading parameters for ZaberAxis" << endl;
    Comm::out << QString(" - axisName: %1").arg(name()) << endl;

    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("ZaberAxis [%1]").arg(partName())))
            return false;

    if(!parms(partName(), "Controller", _controllerName)) return false;
    Comm::out << QString(" - hardwareControllerName: %1").arg(_controllerName) << endl;

    if(!parms(partName(), "MicrostepSize", _microstepSize)) return false;

    _rawPosField = robot().stateManager().registerFieldExc(QString("%1_Microsteps").arg(name()), "microstep", false);
    _realPosField = robot().stateManager().registerFieldExc(name(), "m", false);

    _controller = robot().addPart<ZaberHardwareController>(_controllerName, _controllerName);
    if(!_controller)
    {
        Comm::err << "Error, cannot no Zaber controller defined as part '" << _controllerName << "'" << endl;
        return false;
    }
    _valid = _controller->registerAxis(this);
    return _valid;
}

bool ZaberAxis::valid() const
{
    return _valid and _controller->valid();
}

bool ZaberAxis::writeParameters(Parms& parms) const
{
    bool isOK = true;
    isOK &= parms.set(partName(), "Version", version);
    isOK &= parms.set(partName(), "Controller", _controller->partName());
    isOK &= parms.set(partName(), "MicrostepSize", _microstepSize);
    isOK &= parms.setComment(partName(), "MicrostepSize", "Distance moved for a microstep (in meters)");
    return isOK;
}

bool ZaberAxis::get(unsigned int option, double& value)
{
    switch(option)
    {
      case GOTO_TIMEOUT:
        value = 20;
        break;
      case GOTO_PRECISION_MARGIN:
        value = 0;
        break;
      case ACCELERATION_LIMIT:
        value = _controller->accelerationLimit();
        break;
      case HOME_STATUS:
        {
           bool res;
           if(!_controller->knowsHome(0, res)) return false;
           value = (res ? 1.0 : 0.0);
           return true;
        }
      case NEED_HOME:
        value = 1.0;
        return true;
      default:
        return false;
    }
    return true;
}

bool ZaberAxis::set(unsigned int option, double value)
{
    switch(option)
    {
      case HOME_STATUS:
        if(value != 0) return _controller->goHome(0);
        else return true;
      default:
        break;
    }
    return false;
}

bool ZaberAxis::initialize(bool force)
{
    Comm::out << "Inside ZaberAxis::initialize()" << endl;
    return _controller->initialize(force);
}

bool ZaberAxis::gotoSync(double position)
{
    int32_t pos = int32_t(position / _microstepSize);
    return _controller->gotoAbsolute(0, pos);
}

bool ZaberAxis::updatePosition()
{
    int32_t pos;
    if(!_controller->currentPosition(0, pos)) return false;
    robot().stateManager().state()[_rawPosField] = pos;
    robot().stateManager().state()[_realPosField] = pos*microstepSize();
    return true;
}

bool ZaberAxis::disablePotentiometer()
{
    return _controller->disablePotentiometer();
}

bool ZaberAxis::enablePotentiometer()
{
    return _controller->enablePotentiometer();
}
