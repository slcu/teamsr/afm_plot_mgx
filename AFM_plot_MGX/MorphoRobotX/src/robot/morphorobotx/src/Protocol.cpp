#include "Protocol.hpp"
#include "Experiment.hpp"
#include <iostream>
#include <QTextStream>
#include <stdio.h>
#include "Comm.hpp"

const char protocol_name[] = "protocol";

Protocol::Protocol(QString name, Experiment& e)
  : _experiment(e)
  , _name(name)
  , _state(e.stateManager())
{
}

