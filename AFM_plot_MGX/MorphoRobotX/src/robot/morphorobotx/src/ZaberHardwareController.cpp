/*
 * File:   ZaberHardwareController.cpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 3:05 PM
 */

#include "ZaberHardwareController.hpp"
#include "ZaberAxis.hpp"
#include "Comm.hpp"
#include "Robot.hpp"
#include "HardwareManager.hpp"
#include "Parms.hpp"
#include <QFile>

#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <climits>
#include <array>

#include <QtEndian>

/* baudrate settings are defined in <asm/termbits.h>, which is
   included by <termios.h> */
#define BAUDRATE B9600

REGISTER_ROBOT_SUBUNIT_TYPE(ZaberHardwareController);

constexpr Version ZaberHardwareController::version;

typedef unsigned int uint;

namespace
{

uint64_t timespecDiffMs(struct timespec *timeEnd_p, struct timespec *timeStart_p)
{
    return (timeEnd_p->tv_sec - timeStart_p->tv_sec)*1000 + 
            (timeEnd_p->tv_nsec - timeStart_p->tv_nsec)/1000000;
}

}

/// Note: we assume we are using a little endian architecture
uint32_t ZaberHardwareController::Command::data() const
{
	// On the left, we'll have 0s because we are working with *unsigned* integers
	return ((uint32_t(_data[0])) |
			(uint32_t(_data[1]) << 8) |
			(uint32_t(_data[2]) << 16) |
			(uint32_t(_data[3]) << 24));
}

void ZaberHardwareController::Command::setData(uint32_t d)
{
	d = qFromLittleEndian<uint32_t>(d);
    uint8_t *dd = reinterpret_cast<uint8_t*>(&d); // <=> in C = (uint8_t*)&d
    _data[0] = dd[0];
    _data[1] = dd[1];
    _data[2] = dd[2];
    _data[3] = dd[3];
}

class ZaberHardwareControllerPrivate
{
public:
    ZaberHardwareControllerPrivate()
      : fd(-1)
      , verbose(false)
      , initialized(false)
      , acceleration(20)
    { }

    std::vector<ZaberAxis*> axes;
    QByteArray modemDevice;
    int fd;
    termios oldtio, newtio;
    bool verbose;
    bool initialized;
    int acceleration;
};

ZaberHardwareController::ZaberHardwareController(QString partName, QString name, Robot& robot)
    : RobotSubunit(partName, name, robot)
    , p(new ZaberHardwareControllerPrivate)
    , _valid(false)
{
    static_assert(sizeof(Command) == 6, "The size of a command must be 6 bytes");
}

bool ZaberHardwareController::readParameters(const Parms& parms)
{
    _valid = false;
    Version dataVersion;
    if(parms(partName(), "Version", dataVersion, version))
        if(not version.canReadData(dataVersion, QString("ZaberHardwareController [%1]").arg(partName())))
            return false;
    QString modem;
    if(not parms(partName(), "ModemDevice", modem)) return false;
    QFile modemFile(modem);
    if(not modemFile.exists())
    {
        Comm::err << "Error, trying to access model device '"<< modem << "', but the device doesn't exist" << endl;
        return false;
    }
    parms(partName(), "Verbose", p->verbose, false);
    if(!parms(partName(), "Acceleration", p->acceleration)) return false;
    p->modemDevice = modem.toLocal8Bit();
    _valid = true;
    return true;
}

bool ZaberHardwareController::writeParameters(Parms& parms) const
{
    bool isOK = true;
    isOK &= parms.set(partName(), "Version", version);
    isOK &= parms.set(partName(), "ModemDevice", p->modemDevice);
    isOK &= parms.set(partName(), "Verbose", p->verbose);
    isOK &= parms.setComment(partName(), "Control all the Zaber devices.\n"
                                         "Link to documentation: http://www.zaber.com/products/product_detail.php?detail=T-LSR150B#tabs");
    return isOK;
}

bool ZaberHardwareController::set(unsigned int option, double value)
{
    //TODO: implement
    return false;
}

bool ZaberHardwareController::get(unsigned int option, double& value)
{
    //TODO: implement
    return false;
}

bool ZaberHardwareController::openPort()
{
    p->fd = open(p->modemDevice, O_RDWR | O_NOCTTY  | O_NDELAY );
    if (p->fd < 0)
    {
        perror(p->modemDevice);
        return false;
    }
    fcntl(p->fd, F_SETFL, FNDELAY);
    if (p->verbose)
    {
            Comm::out << "open fd " << p->fd << endl;
    }
    tcgetattr(p->fd,&p->oldtio); /* save current serial port settings */
    bzero(&p->newtio, sizeof(termios)); /* clear struct for new port settings */
    /*
       BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
       CRTSCTS : output hardware flow control (only used if the cable has
       all necessary lines. See sect. 7 of Serial-HOWTO)
       CS8     : 8n1 (8bit,no parity,1 stopbit)
       CLOCAL  : local connection, no modem contol
       CREAD   : enable receiving characters
       */
    p->newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;

    cfsetispeed(&(p->newtio), B9600);
    cfsetospeed(&(p->newtio), B9600);

    //o parity (8N1):

    p->newtio.c_cflag &= ~PARENB;
    p->newtio.c_cflag &= ~CSTOPB;
    p->newtio.c_cflag &= ~CSIZE;
    p->newtio.c_cflag |= CS8;

    /*
       IGNPAR  : ignore bytes with parity errors
       ICRNL   : map CR to NL (otherwise a CR input on the other computer
       will not terminate input)
       otherwise make device raw (no other input processing)
       */
    //p->newtio.c_iflag = IGNPAR | ICRNL;

    /*
       Raw output.
       */
    p->newtio.c_oflag = 0;
    //Raw Comm
    //Raw output is selected by resetting the OPOST option in the c_oflags member:

    p->newtio.c_oflag &= ~OPOST;
    //When the OPOST option is disabled, all other option bits in c_oflags are ignored.


    // Raw input is unprocessed, so they may be used as they are read.
    // Our device sent raw data.
    p->newtio.c_lflag &= ~(ICANON | ECHO | ISIG); //disable ICANON ECHO and ISIG for raw binary mode

    // Whether you use canonical or raw input, make sure you never enable input
    // echo when connected to a computer/device which is echoing characters to you.
    // Refer to section 14 for local mode constants.

    //char buf[255];
    /*
       Open modem device for reading and writing and not as controlling tty
       because we don't want to get killed if linenoise sends CTRL-C.
       */

    /*
       ICANON  : enable canonical input
       disable all echo functionality, and don't send signals to calling program
       */
    //p->newtio.c_lflag = ICANON; // don't set self flag for binary transmission!

    /*
       initialize all control characters
       default values can be found in /usr/include/termios.h, and are given
       in the comments, but we don't need them here
       */
    //        p->newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */
    //        p->newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
    //        p->newtio.c_cc[VERASE]   = 0;     /* del */
    //        p->newtio.c_cc[VKILL]    = 0;     /* @ */
    //        p->newtio.c_cc[VEOF]     = 4;     /* Ctrl-d */
    //        p->newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
    //        p->newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
    //        p->newtio.c_cc[VSWTC]    = 0;     /* '\0' */
    //        p->newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */
    //        p->newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
    //        p->newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
    //        p->newtio.c_cc[VEOL]     = 0;     /* '\0' */
    //        p->newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
    //        p->newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
    //        p->newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
    //        p->newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
    //        p->newtio.c_cc[VEOL2]    = 0;     /* '\0' */

    /*
       now clean the modem line and activate the settings for the port
       */
    tcflush(p->fd, TCIFLUSH);
    tcsetattr(p->fd,TCSANOW,&(p->newtio));

    return true;
}

bool ZaberHardwareController::flushInputBuffer(uint msecTimeout)
{
    uint64_t timeElapsed = 0;
    struct timespec start;
    struct timespec end;
    clock_gettime(CLOCK_REALTIME, &start);
    char b[100];
    int nread;
    int total = 0;
    if (p->verbose)
    {
        Comm::out << "flushing input buffer...\n<" << flush;
    }
    nread = read(p->fd, b, 100);
    if (p->verbose)
        Comm::out << "== flushInputBuffer read " << nread << " bytes" << endl;
    while((nread == 100) and timeElapsed < msecTimeout)
    {
        nread = read(p->fd, b, 100);
        //printf("nread: %d\n", nread);
        clock_gettime(CLOCK_REALTIME, &end);
        timeElapsed = timespecDiffMs(&end, &start);
        total += nread;
        if (p->verbose)
            Comm::out << "== flushInputBuffer read " << nread << " bytes" << endl;
            //for (i=0; i<nread; i++)
            //    Comm::out << QString::number((unsigned int)b[i]);
        usleep(1000);
    }
    if (p->verbose)
        Comm::out << ">\nreceived total " << total << endl;
    return true;
}

bool ZaberHardwareController::disablePotentiometer()
{
    uint32_t mode;
    if(not returnSetting(0, mode)) return false;
    usleep(10000);
    mode |= 0x8;
    return setDeviceMode(0, mode);
}

bool ZaberHardwareController::enablePotentiometer()
{
    uint32_t mode;
    if(not returnSetting(0, mode)) return false;
    usleep(10000);
    mode &= ~0x8;
    return setDeviceMode(0, mode);
}

bool ZaberHardwareController::setDeviceMode(uint8_t device, uint32_t mode)
{
    //uint8_t data0 = 32+1; // with 33 you get no automatic answers
    //if(homeStatus)
    //    data0 += 128; //set home status known
    //uint8_t data1 = 8; //enable circular phase microstepping , bit 11

    if (p->verbose)
        Comm::out << "sending SetDeviceMode with 0x" << QString::number(mode, 16) << "..." << endl;
    return sendCommand(device, 40, mode, false);
}

bool ZaberHardwareController::setAcceleration(uint8_t device, uint32_t acceleration)
{
    if (p->verbose)
        Comm::out << "sending SetAcceleration..." << endl;
    return sendCommand(device, 43, acceleration, false);
}

bool ZaberHardwareController::setMicrostepResolution(uint8_t device, uint8_t microsteps)
{
    if (p->verbose)
        Comm::out << "sending SetMicrosteps..." << endl;
    return sendCommand(device, 37, microsteps, 0, 0, 0, false);
}

bool ZaberHardwareController::gotoAbsolute(uint8_t device, int32_t position)
{
    if (p->verbose)
        Comm::out << "sending GotoAbsolute..." << endl;
    bool retval = sendCommand(device, 20, position, false);
    if (not retval) return false;
    int32_t actualPosition = INT32_MIN;
    int32_t lastPosition = actualPosition;
    do
    {
        usleep(30000);
        retval = currentPosition(device, actualPosition);
        if (not retval) return false;
        if(p->verbose)
            Comm::out << "Target: " << position << ", actual: " << actualPosition << endl;
        if(lastPosition == actualPosition) // retry if no progress
        {
            if(p->verbose)
                Comm::out << "!!!!!!!!!!!!!!! RETRY !!!!!!!!!!!!!!!" << endl;
            retval = sendCommand(device, 20, position, true);
            if (not retval) return false;
        }
        lastPosition = actualPosition;
    } while(position != actualPosition);
    flushInputBuffer(10);
    return retval;
}

bool ZaberHardwareController::goHome(uint8_t device)
{
    bool homeFound = false;
    bool timeoutElapsed = false;
    uint retries = 0;
    do
    {
        if(p->verbose)
            Comm::out << "sending GoHome..." << endl;
        sendCommand(device, 1, 0u, false);
        int32_t position = INT32_MIN;
        uint64_t timeElapsed;
        std::array<int,4> positions = {{INT32_MAX, INT32_MAX, INT32_MAX, INT32_MAX}};
        uint lastPos = 0;
        homeFound = timeoutElapsed = false;
        struct timespec start;
        struct timespec end;
        clock_gettime(CLOCK_REALTIME, &start);
        uint64_t mstimeout = 20000;
        do
        {
            usleep(100000);
            if(p->verbose)
                Comm::out << "calling currentPosition" << endl;
            currentPosition(device, position);
            if(p->verbose)
                Comm::out << "position = " << position << endl;
            positions[lastPos] = position;
            lastPos = (lastPos+1)%4;
            if(p->verbose)
                Comm::out << QString("positions = {%1, %2, %3, %4}").arg(positions[lastPos])
                                                                      .arg(positions[(lastPos+1)%4])
                                                                      .arg(positions[(lastPos+2)%4])
                                                                      .arg(positions[(lastPos+3)%4]) << endl;
            homeFound = (positions[0] == 0) and (positions[1] == 0) and (positions[2] == 0) and (positions[3] == 0);
            if(homeFound) break;

            clock_gettime(CLOCK_REALTIME, &end);
            timeElapsed = timespecDiffMs(&end, &start);
            timeoutElapsed = (timeElapsed > mstimeout);
        } while(not timeoutElapsed);
        retries++;
    } while((not homeFound) and (retries < 4));
    flushInputBuffer(10);
    if(p->verbose)
        Comm::out << "done. retval = " << homeFound << endl; // This is still uncertain
    return homeFound;
}

bool ZaberHardwareController::renumber()
{
    if (p->verbose)
        Comm::out << "sending Renumber..." << endl;
    return sendCommand(0, 2, 0u, true);
}

bool ZaberHardwareController::firmwareVersion(uint32_t& version)
{
    if (p->verbose)
        Comm::out << "sending GetFirmwareVersion..." << endl;
    return sendCommand(0, 51, 0u, true, &version);
}

bool ZaberHardwareController::returnSetting(uint8_t device, uint32_t& setting)
{
    if (p->verbose)
        Comm::out << "sending ReturnSetting..." << endl;
    flushInputBuffer(100);
    return sendCommand(device, 53, 40u, true, &setting);
}

bool ZaberHardwareController::knowsHome(uint8_t device, bool& result)
{
    uint32_t setting;
    bool isOK = returnSetting(device, setting);
    if(!isOK) return false;
    result = setting & 128u;
    return true;
}

bool ZaberHardwareController::currentPosition(uint8_t device, int32_t& position)
{
    if (p->verbose)
        Comm::out << "sending CurrentPosition..." << endl;
    return sendCommand(device, 60, 0u, true, reinterpret_cast<uint32_t*>(&position));
}


bool ZaberHardwareController::initialize(bool force)
{
    if(not force and p->initialized) return true;
    _valid = false;
    p->initialized = false;
    uint32_t setting = 0;
    if(not openPort()) return false;
    //get home status and don't forget it
    flushInputBuffer(100);
    if(not returnSetting(0, setting)) return false;
    uint8_t homeStatus = 0;
    if(setting & 0x80 /*128*/) // if home known
        homeStatus = 1; //home is known
    usleep(10000);
    // 0x1 = Disable auto-reply
    // 0x20 = disable manual move tracking
    // 0x8 = disable potentiometer
    uint32_t mode = 0x1 | 0x20;// | 0x8;
    if(homeStatus)
        mode |= 0x80;
    if(not setDeviceMode(0, mode)) return false; //self overwrites home status
    // Check device mode
    if(not setAcceleration(0, p->acceleration)) return false;
    if(not setMicrostepResolution(0, 128)) return false; //128 means 100000 steps is 25mm, 64 means 100000 steps is 50mm
    //128 gives 1 step ~= 2.5um

    uint32_t firmware;
    if(not firmwareVersion(firmware)) return false;
    Comm::out << "Firmware version = " << (firmware / 100) << "." << (firmware % 100) << endl;

    _valid = true;
    p->initialized = true;
    return true;
}

bool ZaberHardwareController::valid() const
{
    return _valid;
}

bool ZaberHardwareController::registerAxis(ZaberAxis* axis)
{
    if(!p->axes.empty())
    {
        Comm::err << "Error, current implementation of the Zaber controller doesn't allow for more than a single axis" << endl;
        return false;
    }
    p->axes.push_back(axis);
    return true;
}

int ZaberHardwareController::receive6Bytes(uint8_t *buf, uint mstimeout)
{
    uint64_t timeElapsed;
    struct timespec start;
    struct timespec end;
    clock_gettime(CLOCK_REALTIME, &start);

    int nsofar=0;
    int nread=0;
    do
    {
        nread = read(p->fd, buf+nsofar, 6-nsofar);
        if (nread >=0)
            nsofar += nread;
        clock_gettime(CLOCK_REALTIME, &end);
        timeElapsed = timespecDiffMs(&end, &start);
        if (timeElapsed > mstimeout) // timeout elapsed, something went wrong
            return nsofar;
        if (nsofar<5)
            usleep((6-nsofar)*1050); // one byte need 1.04 milisecond
    } while(nsofar < 5); //TODO: why 5 ?!

    return nsofar;
}

bool ZaberHardwareController::sendCommand(uint8_t device, uint8_t command,
                                          uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
                                          bool waitForReply, uint32_t *reply)
{
    uint8_t data[4] = {d0, d1, d2, d3};
    return sendCommand(device, command, qFromLittleEndian<uint32_t>(data), waitForReply, reply);
}

bool ZaberHardwareController::sendCommand(uint8_t device, uint8_t cid, int32_t value,
                                          bool waitForReply, uint32_t* reply)
{
    uint32_t uval = *reinterpret_cast<uint32_t*>(&value);
    return sendCommand(device, cid, uval, waitForReply, reply);
}

namespace
{
struct ErrorCode
{
    uint32_t errorId;
    char const * const name;
    char const * const description;
};

ErrorCode errorCodes[] = {
        { 1  , "Cannot Home", "Home - Device has traveled a long distance without triggering the home sensor. Device may be stalling or slipping." },
        { 2  , "Device Number Invalid ", "Renumbering data out of range."},
        { 14  , "Voltage Low ", "Power supply voltage too low."},
        { 15  , "Voltage High ", "Power supply voltage too high."},
        { 18  , "Stored Position Invalid ", "The position stored in the requested register is no longer valid. This is probably because the maximum range was reduced."},
        { 20  , "Absolute Position Invalid ", "Move Absolute - Target position out of range."},
        { 21  , "Relative Position Invalid ", "Move Relative - Target position out of range."},
        { 22  , "Velocity Invalid ", "Constant velocity move. Velocity out of range."},
        { 36  , "Peripheral Id Invalid ", "Restore Settings - peripheral id is invalid. Please use one of the peripheral ids listed in the user manual, or 0 for default."},
        { 37  , "Resolution Invalid ", "Invalid microstep resolution. Resolution may only be 1, 2, 4, 8, 16, 32, 64, 128."},
        { 38  , "Run Current Invalid ", "Run current out of range. See command 38 for allowable values."},
        { 39  , "Hold Current Invalid ", "Hold current out of range. See command 39 for allowable values."},
        { 40  , "Mode Invalid ", "Set Device Mode - one or more of the mode bits is invalid."},
        { 41  , "Home Speed Invalid ", "Home speed out of range. The range of home speed is determined by the resolution."},
        { 42  , "Speed Invalid ", "Target speed out of range. The range of target speed is determined by the resolution."},
        { 43  , "Acceleration Invalid ", "Target acceleration out of range. The range of target acceleration is determined by the resolution."},
        { 44  , "Maximum Range Invalid ", "The maximum range may only be set between 1 and the resolution limit of the stepper controller, which is 16,777,215."},
        { 45  , "Current Position Invalid ", "Current position out of range. Current position must be between 0 and the maximum range."},
        { 46  , "Maximum Relative Move Invalid ", "Max relative move out of range. Must be between 0 and 16,777,215."},
        { 47  , "Offset Invalid ", "Home offset out of range. Home offset must be between 0 and maximum range."},
        { 48  , "Alias Invalid ", "Alias out of range."},
        { 49  , "Lock State Invalid ", "Lock state must be 1 (locked) or 0 (unlocked)."},
        { 50  , "Device Id Unknown ", "The device id is not included in the firmware's list."},
        { 53  , "Setting Invalid ", "Return Setting - data entered is not a valid setting command number. Valid setting command numbers are the command numbers of any 'Set ...' instructions."},
        { 64  , "Command Invalid ", "Command number not valid in this firmware version."},
        { 255  , "Busy ", "Another command is executing and cannot be pre-empted. Either stop the previous command or wait until it finishes before trying again."},
        { 1600  , "Save Position Invalid ", "Save Current Position register out of range (must be 0-15)."},
        { 1601  , "Save Position Not Homed ", "Save Current Position is not allowed unless the device has been homed."},
        { 1700  , "Return Position Invalid ", "Return Stored Position register out of range (must be 0-15)."},
        { 1800  , "Move Position Invalid ", "Move to Stored Position register out of range (must be 0-15)."},
        { 1801  , "Move Position Not Homed ", "Move to Stored Position is not allowed unless the device has been homed."},
        { 2146  , "Relative Position Limited ", "Move Relative (command 20) exceeded maximum relative move range. Either move a shorter distance, or change the maximum relative move (command 46)."},
        { 3600  , "Settings Locked ", "Must clear Lock State (command 49) first. See the Set Lock State command for details."},
        { 4008  , "Disable Auto Home Invalid ", "Set Device Mode - this is a linear actuator; Disable Auto Home is used for rotary actuators only."},
        { 4010  , "Bit 10 Invalid ", "Set Device Mode - bit 10 is reserved and must be 0."},
        { 4012  , "Home Switch Invalid ", "Set Device Mode - this device has integrated home sensor with preset polarity; mode bit 12 cannot be changed by the user."},
        { 4013  , "Bit 13 Invalid ", "Set Device Mode - bit 13 is reserved and must be 0. "}
};
}

bool ZaberHardwareController::sendCommand(uint8_t device, uint8_t cid, uint32_t value,
                                          bool waitForReply, uint32_t* reply)
{
    Command command;
    Command response;

    command.deviceByte = device;
    command.commandID = cid;
    command.setData(value);

    bool retval = sendCommand(command, response, waitForReply);
    if(!retval) return false;

    while(response.commandID == 255 and response.data() == 255) // ==> Busy
    {
        if(p->verbose)
            Comm::out << " -- Device is BUSY -- " << endl;
        usleep(1000);
        retval = sendCommand(command, response, waitForReply);
        if(!retval) return false;
    }
    if(response.commandID == 255)
    {
        Comm::err << "reply-only error message received" << endl;
        for(size_t i = 0 ; i < sizeof(errorCodes) / sizeof(ErrorCode) ; ++i)
            if(errorCodes[i].errorId == response.data())
            {
                Comm::err << errorCodes[i].name << " -- " << errorCodes[i].description << endl;
                return false;
            }
    }

    
    if (p->verbose)
        Comm::out << "done. retval : " << retval
                    << "\nresponseValue " << response.data()
                    << " (0x" << QString::number(response.data(), 16) << ")"
                    << endl;

    if(reply) *reply = response.data();

    return retval;
}

bool ZaberHardwareController::sendCommand(const Command& cmd, Command& response, bool waitForReply)
{
    bool retval = true;
    uint nretries = (waitForReply ? 0 : -1);
    if(p->verbose)
        Comm::out << "beginning of SendCommand, waitForReply is " << waitForReply << endl;
    do
    {
        int nwritten = write(p->fd, &cmd, 6);
        if(nwritten != 6)
        {
            Comm::err << "Error, could not send the command" << endl;
            retval = false;
        }
        else if (waitForReply)
        {
            if (p->verbose)
                Comm::out << "Waiting for reply" << endl;
            usleep(30000);
            retval = getResponse(response);
        }
        nretries++;
    } while((not retval) && (nretries < 2)); // retry up to 3 times

    return retval;
}

bool ZaberHardwareController::getResponse(Command& response)
{
    int nread = receive6Bytes(reinterpret_cast<quint8*>(&response), 1000); //1 second timeout
    if (nread < 6)
        return false;
    return true;
}

// TODO: Find the conversion from int to m/s^2
double ZaberHardwareController::accelerationLimit() const
{
    return p->acceleration;
}
