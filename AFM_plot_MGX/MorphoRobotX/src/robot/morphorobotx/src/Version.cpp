#include "Version.hpp"

#include <QString>
#include <QStringList>
#include <QTextStream>
#include "Comm.hpp"

Version::Version(const QString& version)
{
    *this = version;
}

Version& Version::operator=(const QString& v)
{
    clear();
    QStringList fields = v.split(".");
    if(fields.size() == 2)
    {
        bool ok;
        unsigned int M,m;
        M = fields[0].toUInt(&ok);
        if(!ok)
            return *this;
        m = fields[1].toUInt(&ok);
        if(!ok)
            return *this;
        _major = M;
        _minor = m;
    }
    return *this;
}

QTextStream& operator<<(QTextStream& ss, const Version& version)
{
    ss << (QString)version;
    return ss;
}

QTextStream& operator>>(QTextStream& ss, Version& version)
{
    QString v;
    ss >> v;
    version = v;
    return ss;
}

void Version::clear()
{
    _major = _minor = 0;
}

bool Version::canReadData(const Version& dataVersion, QString objectName) const
{
    if(!dataVersion)
    {
        Comm::err << "Error, data version of " << objectName << " (" << dataVersion << ") is invalid" << endl;
        return false;
    }
    if(canReadData(dataVersion)) return true;
    Comm::err << "Error, versions of " << objectName << " are incompatible."
                << "\n  Code version: " << (*this)
                << "\n  Data version: " << dataVersion << endl;
    return false;
}
