/*
 * File:   HardwareManager.hpp
 * Author: cfmadmin
 *
 * Created on February 1, 2013, 7:14 PM
 */

#ifndef HARDWAREMANAGER_HPP
#define HARDWAREMANAGER_HPP

#include "RobotSubunit.hpp"
#include "Factory.hpp"


extern RobotSubunit* makeRobotSubunit(QString partName, QString name, Robot& robot);
extern const char robotsubunit_name[];

MAKE_INSTANCE_FACTORY(HardwareManager, robotsubunit_name, makeRobotSubunit);

inline HardwareManager& hardwareManager() { return HardwareManager::instance(); }

/**
 * \def REGISTER_ROBOT_SUBUNIT(name, makeFct)
 * Register the function \c makeFct as a maker of name \c name for robot subunits
 */
#define REGISTER_ROBOT_SUBUNIT(name, makeFct) REGISTER_FACTORY_OBJECT(HardwareManager, name, makeFct)

/**
 * \def REGISTER_ROBOT_SUBUNIT_TYPE(SubunitClass)
 * Register the class \c Class as a robot subunit
 */
#define REGISTER_ROBOT_SUBUNIT_TYPE(SubunitClass) REGISTER_FACTORY_OBJECT_TYPE(HardwareManager, SubunitClass)

#endif /* HARDWAREMANAGER_HPP */

