/*
 * File:   MeasurementState.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 3:54 PM
 */

#ifndef MEASUREMENTSTATE_HPP
#define MEASUREMENTSTATE_HPP

#include <QString>
#include <QStringList>
#include "Timer.hpp"
#include <exception>

class Parms;

struct UnknownKeyException : public std::exception
{
    const char* what() const throw() override;
};

class MeasurementState
{
public:
    ///TODO: remove domain knowledge from the state, put in in the protocol itself. The protocol is going to register fields through state manager or something like that.
    static constexpr double double_nan = std::numeric_limits<double>::quiet_NaN();

    explicit MeasurementState();
    virtual ~MeasurementState();

    QStringList getHeaders() const; ///returns names and units
    bool dumpCurrentValues(QStringList& stringList); ///gets a list of values converted to text
    double get(int key) const { return (*this)[key]; }

    double& operator[](int key);
    const double& operator[](int key) const;
    void invalidate(const int key)
    {
        values.at(key) = double_nan;
    }
    bool stable(int key) const { return stability.at(key); }
    QString name(int key) const { return names[key]; }

    /**
     * Register a data field
     * @param name Name of the field, must start with a letter and contains only letters, numbers and underscores
     * @param unit Name of the unit used
     * @param stable If false, the unit will be reset for each data line
     * @return the field number or a negative error code.
     *
     * Error codes are:
     *  - -1 for duplicate field
     *  - -2 for invalid name
     */
    int registerField(QString name, QString unit, bool stable = true);
    int fieldIndex(QString name);

    void renew(); // to remove

    bool writeParameters(Parms& parms) const;

    QString unit(int key) const
    {
        return units[key];
    }

protected:

    std::vector<double> values;
    std::vector<QString> names;
    std::vector<QString> units;
    std::vector<bool> stability;
};

#endif /* MEASUREMENTSTATE_HPP */

