/*
 * File:   CFMFileManager.cpp
 * Author: huflejt
 *
 * Created on October 25, 2012, 7:30 PM
 */

#include "FileManager.hpp"
#include "Comm.hpp"

FileManager::FileManager() {
    _initialized = false;
}

FileManager::~FileManager() {
}

bool FileManager::prepareNewDirectory()
{
    _initialized = false;

    for(uint suffix = 0 ; suffix < 100000 ; ++suffix)
    {
        QString exp = QString("%1%2").arg(_experimentPrefix).arg(suffix, 5, 10, QChar('0'));
        if(not _sessionDir.exists(exp) and _sessionDir.mkdir(exp))
        {
            _experimentDir = _sessionDir;
            _experimentDir.cd(exp);
            _experimentDir.makeAbsolute();
            _initialized = true;
            return true;
        }
    }

    Comm::err << "Couldn't find a folder for the experiment" << endl;
    return false;
}

bool FileManager::setSessionPath(QString path)
{
    QDir dir(path);
    if(not dir.exists() and not dir.mkpath("."))
    {
        Comm::err << "Error, cannot create path '" << dir.absolutePath() << "'" << endl;
        return false;
    }
    _sessionDir = dir;
    return true;
}

bool FileManager::readParameters(const Parms& par)
{
    _initialized = false;

    QString path;
    if(!par("StorageSettings", "SessionPath", path)) return false;
    if(!setSessionPath(path)) return false;
    if(!par("StorageSettings", "ExperimentPrefix", _experimentPrefix)) return false;
    _initialized = true;
    return true;
}

bool FileManager::writeParameters(Parms& parms) const
{
    if(!parms.set("StorageSettings", "SessionPath", _sessionDir.path())) return false;
    if(!parms.setComment("StorageSettings", "SessionPath", "Path to the set of sessions")) return false;
    if(!parms.set("StorageSettings", "ExperimentPrefix", _experimentPrefix)) return false;
    if(!parms.setComment("StorageSettings", "ExperimentPrefix", "Prefix for the name of the experiment's folders")) return false;
    if(!parms.setComment("StorageSettings", "Define where the experiments are stored")) return false;
    return true;
}
