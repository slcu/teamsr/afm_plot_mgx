/* 
 * File:   SmaractHardwareController.hpp
 * Author: cfmadmin
 *
 * Created on August 20, 2013, 10:20 PM
 */

#ifndef SMARACTHARDWARECONTROLLER_HPP
#define	SMARACTHARDWARECONTROLLER_HPP

#include "RobotSubunit.hpp"
#include "Version.hpp"
#include "SmaractAxis.hpp" //because I need SmaractAxisPtr
#include "SmaractCommands.hpp" //because I need SmaractResult

class Robot;
//class SmaractHardwareControllerPrivate; //useful or not ?
class MCSController;

typedef std::shared_ptr<MCSController> MCSControllerPtr;

class SmaractHardwareController : public RobotSubunit
{
public:
    SmaractHardwareController(QString partName, QString name, Robot& robot);
    
    virtual bool readParameters(const Parms& parms) override;
    virtual bool writeParameters(Parms& parms) const override;
    
    bool get(unsigned int option, double& value) override;
    bool set(unsigned int option, double value) override;
    bool initialize(bool force = false) override;
    bool valid() const override;
    
    static constexpr Version version = Version{1,0};
    
    bool registerAxis(SmaractAxis* axis);
    
    /**
     * hardwareID is the number derived from serial number of each smaract controller box.
     * Useful during initialization of controllers.
     * 
     * \returns hardwareID
     */
    unsigned int hardwareID()
    {
        return _hardwareID;
    }
    
    /**
     * SystemIndex is the zero-based index identifying each smaract controller box connecte to the computer. 
     * SystemIndex is required by most smaract commands. The main mcscontrol library assigns systemIndex 
     * values dynamically during init, therefore it is a read-only value.
     *
     * \returns systemIndex
     */
    unsigned int systemIndex()
    {
        return _systemIndex;
    }
    ///TODO: should these motion methods be protected ? They only need to be accessed from SmaractAxis class.    
    bool getPosition(unsigned int axisID, int32_t& position);
    bool gotoSync(unsigned int axisID, int32_t position, unsigned int holdMs);
    bool getStatus(unsigned int axisID, SmaractAxisStatus& status);
    bool setClosedLoopMaxFrequency(unsigned int axisID, unsigned int frequency);
    bool setClosedLoopSpeed(unsigned int axisID, unsigned int speedNmPerS);
    bool getClosedLoopSpeed(unsigned int axisID, unsigned int& speedNmPerS);
    bool stop(unsigned int axisID);
    
protected:
            
    //std::unique_ptr<SmaractHardwareControllerPrivate> p; //in case something should be private
    bool _valid;
    bool _initialized;
    std::vector<SmaractAxis*> _axes;
    //MCSControl* _controller; //TODO: which one is better ?
    MCSControllerPtr _mcsController;
    QString _mcsControllerName;
    unsigned int _hardwareID;
    unsigned int _systemIndex;

private:
  
};



#endif	/* SMARACTHARDWARECONTROLLER_HPP */

