/*
 * File:   RobotSubunit.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 7:34 PM
 */

#ifndef ROBOTSUBUNIT_HPP
#define ROBOTSUBUNIT_HPP

#include <QString>
#include <memory>
#include <QStringList>
#include <QHash>
#include <QFlags>
#include <exception>
#include "Comm.hpp"

class Robot;
class Parms;

/**
 * \class RobotSubunit RobotSubunit.hpp "RobotSubunit.hpp"
 *
 * This class represent a part of the robot. This can be an axis, a sensor, a controller, or anything that will be
 * created and requires an independent section in the Robot description file.
 */
class RobotSubunit {
public:
    static constexpr double double_nan = std::numeric_limits<double>::quiet_NaN();

    enum Option : unsigned int
    {
        INVALID_OPTION = 0
    };

    /**
     * \enum OptionTypeFlag
     */
    enum OptionTypeFlag
    {
        StringOption    = 0x1, ///< The option is a string
        NumberOption    = 0x2, ///< The option is a number
        ReadOnlyOption  = 0x4, ///< The option can be queried
        WriteOnlyOption = 0x8, ///< The option can be set
        ReadWriteOption = ReadOnlyOption | WriteOnlyOption ///< The option can be set and queried
    };

    /**
     * \typedef OptionType
     *
     * Bit-mask for option type
     *
     * \relates OptionTypeFlag
     */
    Q_DECLARE_FLAGS(OptionType, OptionTypeFlag)

    /**
     * Exception launched if an unknown exception name is used
     */
    struct UnknownOptionNameException : public Comm::QStringException
    {
        UnknownOptionNameException(QString name)
          : _name(name)
        { }
        ~UnknownOptionNameException() throw() {}

        QString text() const throw() override;

        QString _name;
    };

    /**
     * Exception launched if an invalid id is used
     */
    struct UnknownOptionIdException : public Comm::QStringException
    {
        UnknownOptionIdException(unsigned int id)
          : _id(id)
        { }
        ~UnknownOptionIdException() throw() {}

        QString text() const throw() override;

        unsigned int _id;
    };

    /**
     * Exception launched if the wrong type is used for an option
     */
    struct InvalidOptionTypeException : public Comm::QStringException
    {
        InvalidOptionTypeException(unsigned int name, bool is_numeric)
          : _name(name)
          , _is_numeric(is_numeric)
        { }
        ~InvalidOptionTypeException() throw() { }

        QString text() const throw() override;

        unsigned int _name;
        bool _is_numeric;
    };


    /**
     * \class RobotSubunit::OptionSet RobotSubunit.hpp "RobotSubunit.hpp"
     *
     * Class defining a set of options to set at once.
     */
    class OptionSet
    {
        friend class RobotSubunit;
        OptionSet(QString name)
          : _partName(name)
        { }

        struct OptionPlace
        {
            bool numeric_option;
            size_t pos;
        };

    public:
        OptionSet(const OptionSet& copy)
          : _options(copy._options)
          , _number_options(copy._number_options)
          , _string_options(copy._string_options)
        { }

        OptionSet(OptionSet&& copy)
          : _options(std::move(copy._options))
          , _number_options(std::move(copy._number_options))
          , _string_options(std::move(copy._string_options))
        { }

        /**
         * Get the value of an option
         *
         * \throws UnknownOptionIdException
         * \throws InvalidOptionTypeException
         */
        const double& value(unsigned int id) const;
        /**
         * Get the value of an option
         *
         * \throws UnknownOptionIdException
         * \throws InvalidOptionTypeException
         */
        const QString& string(unsigned int id) const;

        /**
         * Get the value of an option
         *
         * \throws UnknownOptionIdException
         * \throws InvalidOptionTypeException
         */
        double& value(unsigned int id);
        /**
         * Get the value of an option
         *
         * \throws UnknownOptionIdException
         * \throws InvalidOptionTypeException
         */
        QString& string(unsigned int id);

        /**
         * Set the value of a numeric option
         */
        void set(unsigned int id, double val) { value(id) = val; }

        /**
         * Set the value of a numeric option interpreted as boolean
         */
        void set(unsigned int id, bool val) { value(id) = (val ? 1 : 0); }

        /**
         * Set the value of a string option
         */
        void set(unsigned int id, QString val) { string(id) = val; }

        /**
         * Remove the value for an option.
         *
         * An unset option will not be set by RobotSubunit::setAll().
         *
         * When unset, numeric options will get the value NaN and string will be null.
         */
        void unset(unsigned int id);

        /**
         * Name of the part for which these options are
         */
        QString partName() const { return  _partName; }

        /**
         * Returns true if the option is numeric
         */
        bool numeric(unsigned int id) const;

        /**
         * Return the list of options known to the structure
         */
        QList<unsigned int> options() const
        {
            return _options.keys();
        }

        /**
         * Return the list of string options
         */
        QList<unsigned int> stringOptions() const;

        /**
         * Return the list of numeric options
         */
        QList<unsigned int> numberOptions() const;

        /**
         * Test if an option is present in the set
         */
        bool contains(unsigned int id) const
        {
            return _options.contains(id);
        }

        /**
         * Check if the option is set
         */
        bool isSet(unsigned int id) const;


    private:
        /**
         * Add a numeric option to the set
         */
        void addValue(unsigned int id,
                      double value = double_nan);

        /**
         * Add a string option to the set
         */
        void addString(unsigned int id,
                       QString value = QString());

        /// Map the option id to a place of the option
        QHash<unsigned int, OptionPlace> _options;
        QList<double> _number_options;
        QStringList _string_options;
        QString _partName;
    };

    /**
     * Create a new Robot Subunit
     *
     * \param partName Name of the section describing the part
     * \param name Logical name of the subunit for reference by other parts
     * \param robot Robot including the part
     */
    RobotSubunit(QString partName, QString name, Robot& robot);

    virtual ~RobotSubunit() { }

    /**
     * Read the parameters from a Parms object.
     *
     * If any other sub-parts are to be created, it is there.
     * However, you cannot assume the robot to be complete at that point.
     */
    virtual bool readParameters(const Parms& parms) = 0;
    /**
     * Write the parameters to the parms.
     *
     * \note the parameters should be written so that, * if given back to readParameters it will create the same object.
     */
    virtual bool writeParameters(Parms& parms) const = 0;

    /**
     * Initialize the part.
     *
     * Be careful that this method could be called more than once. If a part require another one to be initialized for
     * example, it may be called by this other part and by the robot itself.
     */
    virtual bool initialize(bool force = false)=0;

    OptionSet defaults();

    /**
     * Set the default value for an option
     *
     * \param option If of the option to get (see optionId() for details)
     * \param value Value to use as default
     * \param set If true, the option will also be set (i.e. calling set())
     */
    bool setDefault(unsigned int option, double value, bool set = true);

    /**
     * Set the default value for an option
     *
     * \param option If of the option to get (see optionId() for details)
     * \param value Value to use as default
     * \param set If true, the option will also be set (i.e. calling set())
     */
    bool setDefault(unsigned int option, QString value, bool set = true);

    /**
     * Generic method to set an option.
     *
     * \param option Id of the option to get (see optionId() for details)
     * \param value New value of the option
     *
     * \note This can be used to set a floating point value or an integer up to 32 bits.
     * \note In the case of hardware-related option, this can translate to actual hardware call.
     * \returns true if the option exists and can be set
     */
    virtual bool set(unsigned int option, double value) { return false; }
    /**
     * Generic method to get values
     *
     * \note This can be used to set a floating point value or an integer up to 32 bits.
     * \returns true if the option exists and can be queries
     */
    virtual bool get(unsigned int option, double& value) { return false; }

    /**
     * Overload get to retrieve boolean value
     */
    bool get(unsigned int option, bool& value)
    {
        double val;
        bool ret = this->get(option, val);
        if(not ret) return false;
        value = bool(val);
        return true;
    }

    /**
     * Generic method to set an option.
     *
     * \param option Id of the option to get (see optionId() for details)
     * \param value New value of the option
     *
     * \note This can be used to set a floating point value or an integer up to 32 bits.
     * \note In the case of hardware-related option, this can translate to actual hardware call.
     * \returns true if the option exists and can be set
     */
    virtual bool set(unsigned int option, QString value) { return false; }

    /**
     * Exception launched if an option set is used on the wrong subunit
     */
    struct InvalidOptionSet : public std::exception
    {
        InvalidOptionSet(QString setName, QString subunitName)
          : _setName(setName)
          , _subunitName(subunitName)
        { }
        ~InvalidOptionSet() throw() { }

        const char* what() const throw()
        {
            return "Trying to use an option set with the wrong part";
        }

        QString _setName, _subunitName;
    };

    /**
     * Set all the options at once.
     *
     * The default behavior is to set all the options, in numerical order, skipping any 'unset' option.
     *
     * The OptionSet has to come from the current robot sub-unit to be valid.
     *
     * \throws InvalidOptionSet
     */
    virtual bool setAll(const OptionSet& options);

    /**
     * Overload set to send boolean value
     */
    bool set(unsigned int option, bool value)
    {
        return this->set(option, (value ? 1.0 : 0.0));
    }

    /**
     * Generic method to get a value as a string
     *
     * \param option Id of the readable option
     * \param value Reference to a string where the result will be stored if the get succeeded
     *
     * If the get fails (for hardware options), the value may or may not have changed.
     *
     * \returns true if the option exists and can be queries
     */
    virtual bool get(unsigned int option, QString& value) { return false; }

    /**
     * Return the list of available options
     */
    virtual QStringList optionList() const;

    /**
     * Get the id for a given option
     *
     * \note It is recommended to use the option optionId(QString, OptionType) method.
     */
    virtual unsigned int optionId(QString name) const;

    /**
     * Get the name of a given option id
     * \returns the name of the option or an empty string if it doesn't exist
     */
    virtual QString optionName(unsigned int id) const;

    /**
     * Get the id for a given option only if the constraints are respected
     *
     * The constraints is an AND mask on the type.
     * For example
     *
     * constraints = ReadOnlyOption | NumberOption
     *
     * will match
     *
     * type = ReadWriteOption | NumberOption
     *
     * but not
     *
     * type = WriteOnlyOption | NumberOption
     */
    virtual unsigned int optionId(QString name, OptionType constraints) const;

    /**
     * Get the type of the option
     */
    virtual OptionType optionType(QString name) const;

    /**
     * Name of the part
     *
     * This is typically the name of the section describing the part
     */
    QString partName() const { return _partName; }

    /**
     * Name of the sub-unit
     *
     * This is the logical name used by other parts and protocols.
     */
    QString name() const { return _name; }

    /**
     * Return true if the subunit is valid.
     *
     * An subunit is valid if it has been created and there hasn't been any errors yet.
     *
     * \note This doesn't imply the sub-unit has been initialized! However, if initialized, it can be true only if the
     * initialization succeeded.
     */
    virtual bool valid() const = 0;

    /**
     * Equivalent to calling valid()
     */
    operator bool() const { return valid(); }

    /**
     * Access to the robot that created the part
     */
    Robot& robot() { return _robot; }
    /**
     * Access to the robot that created the part
     */
    const Robot& robot() const { return _robot; }

protected:
    /**
     * Add a fix option to the sub-unit
     */
    bool registerOption(QString name, unsigned int id, OptionType type);

    /**
     * Remove an option from the list
     */
    bool removeOption(QString name);

private:
    QString _partName;
    QString _name;
    Robot& _robot;
    struct OptionId
    {
        OptionType type;
        unsigned int id;
    };
    QHash<QString, OptionId> _options;
    QHash<unsigned int, QString> _default_strings;
    QHash<unsigned int, double> _default_values;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(RobotSubunit::OptionType)

typedef std::shared_ptr<RobotSubunit> RobotSubunitPtr;

#endif /* ROBOTSUBUNIT_HPP */

