/*
 * File:   ForceSensor.cpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 9:07 PM
 */

#include "ForceSensor.hpp"
#include "Comm.hpp"
#include <climits>
namespace
{
    const double double_nan = std::numeric_limits<double>::quiet_NaN();
}

char const * ForceSensorError::what() const throw ()
{
    switch(missing)
    {
        case DIRECTION:
            return "Direction field is missing.";
        case AXIS:
            return "Axis field is missing.";
        case DIRECTION|AXIS:
            return "Direction and axis fields are missing.";
        default:
            return "Unknown error";
    }
}

ForceSensor::ForceSensor(SensorPtr sensor) throw (ForceSensorError)
    : _sensor(sensor)
{
    if(not _sensor) return;
    _direction = _sensor->optionId("Direction", Sensor::NumberOption | Sensor::ReadOnlyOption);
    _axis = _sensor->optionId("Axis", Sensor::StringOption | Sensor::ReadOnlyOption);
    if(_direction == 0 or _axis == 0)
    {
        unsigned int mis = 0;
        if(_direction == 0)
            mis |= ForceSensorError::DIRECTION;
        if(_axis == 0)
            mis |= ForceSensorError::AXIS;
        throw ForceSensorError{mis};
    }
    _minBreakForce = _sensor->optionId("MinBreakForce", Sensor::NumberOption | Sensor::ReadOnlyOption);
    _maxBreakForce = _sensor->optionId("MaxBreakForce", Sensor::NumberOption | Sensor::ReadOnlyOption);
    _minReadForce = _sensor->optionId("MinReadForce", Sensor::NumberOption | Sensor::ReadOnlyOption);
    _maxReadForce = _sensor->optionId("MaxReadForce", Sensor::NumberOption | Sensor::ReadOnlyOption);
}

bool ForceSensor::direction() const
{
    bool dir = true;
    _sensor->get(_direction, dir);
    return dir;
}

QString ForceSensor::axis() const
{
    QString axis;
    _sensor->get(_axis, axis);
    return axis;
}

double ForceSensor::minReadForce() const
{
    double val = double_nan;
    if(_minReadForce > 0)
        _sensor->get(_minReadForce, val);
    return val;
}
double ForceSensor::maxReadForce() const
{
    double val = double_nan;
    if(_maxReadForce > 0)
        _sensor->get(_maxReadForce, val);
    return val;
}
double ForceSensor::minBreakForce() const
{
    double val = double_nan;
    if(_minBreakForce > 0)
        _sensor->get(_minBreakForce, val);
    return val;
}
double ForceSensor::maxBreakForce() const
{
    double val = double_nan;
    if(_maxBreakForce > 0)
        _sensor->get(_maxBreakForce, val);
    return val;
}

bool ForceSensor::hasMinReadForce() const
{
    return _minReadForce > 0;
}
bool ForceSensor::hasMaxReadForce() const
{
    return _maxReadForce > 0;
}
bool ForceSensor::hasMinBreakForce() const
{
    return _minBreakForce > 0;
}
bool ForceSensor::hasMaxBreakForce() const
{
    return _maxBreakForce > 0;
}
