#!/bin/bash
apt-get -y update
apt-get -y install git synaptic autoconf swig libtool python-dev bison flex qt4-dev-tools libgsl0-dev gsl-bin libboost-dev libboost-program-options-dev gfortran libblas-dev  liblapack-dev doxygen doxygen-latex cmake cmake-qt-gui python-pip nautilus-open-terminal nautilus-actions gnome-shell g++ gcc
if [[ $(g++ -dumpversion) < "4.8" ]]; then
	add-apt-repository -y ppa:ubuntu-toolchain-r/test
	apt-get -y update
	apt-get -y install g++-4.8
	update-alternatives --remove cpp /usr/bin/cpp
	update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.6 60 \
		--slave /usr/bin/g++ g++ /usr/bin/g++-4.6 \
		--slave /usr/bin/cpp cpp /usr/bin/cpp-4.6 \
		--slave /usr/bin/gcov gcov /usr/bin/gcov-4.6
	update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 40 \
		--slave /usr/bin/g++ g++ /usr/bin/g++-4.8 \
		--slave /usr/bin/cpp cpp /usr/bin/cpp-4.8 \
		--slave /usr/bin/gcov gcov /usr/bin/gcov-4.8
	update-alternatives --set gcc /usr/bin/gcc-4.8
fi
ldconfig
pip install pandas pyqtgraph path.py

