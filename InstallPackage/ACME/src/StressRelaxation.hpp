/* 
 * File:   StressRelaxation.hpp
 * Author: cfmadmin
 *
 * Created on May 7, 2013, 12:49 PM
 */

#ifndef STRESSRELAXATION_HPP
#define	STRESSRELAXATION_HPP

#include "Experiment.hpp"
#include "QString"

class StressRelaxation :public Protocol{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    StressRelaxation(Experiment& experiment, QString sectionName);
    virtual ~StressRelaxation();
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;
    
    
protected:
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);    
    
    enum ProtocolFlag{
        TareSensor=0,
        GoingToTargetForce=1,
        Holding=2
    };
    
    void UpdateProtocolFlag(ProtocolFlag flag)
    {
        experiment().stateManager().state().Update(_fieldIdProtocolFlag, flag);
    }
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    double _stretchStepMicrons;
    double _maxTravelMicrons;
    double _maxForceAbsMicronewtons;
    double _targetTravelMicrons;
    double _holdingTimeS;

};

#endif	/* STRESSRELAXATION_HPP */

