/* 
 * File:   CreepWithCalibration.cpp
 * Author: cfmadmin
 * 
 * Created on May 27, 2013, 1:13 PM
 */

#include <morphorobotx/Robot.hpp>
#include <stdlib.h>
#include "CreepWithCalibration.hpp"
#include "StiffnessMonitor.hpp"

REGISTER_PROTOCOL("CreepWithCalibration", CreepWithCalibration);

CreepWithCalibration::CreepWithCalibration(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{
        _fieldIdProtocolFlag = this->experiment().stateManager().RegisterFieldExc(QString("ProtocolFlag"), QString("(flag)"), false);
}

CreepWithCalibration::~CreepWithCalibration() 
{
}

bool CreepWithCalibration::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    /*Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }*/
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    //StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    /*primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);*/
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    QTextStream in(stdin);
    QTextStream out(stdout);
    
    out << endl;
    out << "#########################################" << endl;
    out << "Make sure the force sensor is not loaded" << endl;
    out << "If necessary, move the robot manually to unload the sensor" << endl;
    out << "Press <ENTER> to confirm..." << endl;

    QString line = in.readLine();
    
    /*how to make a memo file:*/    
    /*
    {
      QFile memoTxt;
      //memoTxt.setFileName(fileManager->getExperimentDirAbsolute() + "/info.txt");
      memoTxt.setFileName(experiment().fileManager().getExperimentDirAbsolute() + "/info.txt");
      QTextStream memoStream;

      if(!memoTxt.open(QIODevice::WriteOnly)) // **PBdR** You want to reset the file everytime or add at the end?
      {
        out << "Error, cannot open file '" << memoTxt.fileName() << "' for writing" << endl;
        return false;
      }
      //QTextStream textStream(&file);
      memoStream.setDevice(&memoTxt);
      memoStream << line << endl;   
    }
    */       
        
    double indentationAxisPosition = 0.0;
    double d = 0.0;
    
    
    sm.state().Renew();   
    double forceMicronewtons = 0.0;
    myForceSensor->TareSensor(); //how to tare the sensor ?
    UpdateProtocolFlag(ProtocolFlag::TareSensor);
        
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
    double initialIndentationPositionMicrons = indentationAxisPosition;
    isOK &= sm.logger().WriteCurrentState(); 
        
    if (_findZeroPosition)
    {
        UpdateProtocolFlag(ProtocolFlag::AdjustingJaws);
        isOK &= sm.logger().WriteCurrentState(); 
        //here, close the jaws to find the zero position, if required by parameters
        //reset the coordinates        
        isOK &= GoDownUntilStiffness(indentationAxis, myForceSensor, isPushingPositive); //close the jaws       
        isOK &= GoUpUntilNoStiffness(indentationAxis, myForceSensor, isPushingPositive); //open the jaws a bit
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        _zeroPosition = indentationAxisPosition;
        UpdateProtocolFlag(ProtocolFlag::ZeroPosition); //mark that this is zero position
        isOK &= sm.logger().WriteCurrentState(); 
        
        //here, reset the smaract
        //isOK &= indentationAxis->Set(AxisOption::STOP_NOW, 0);
        //isOK &= indentationAxis->Set(AxisOption::SET_ZERO_POSITION, 0);
        //if (!isOK)
        //{
        //      out << "Setting zero position failed..." << endl;  
        //}        
        UpdateProtocolFlag(ProtocolFlag::AdjustingJaws); //mark that this is zero position        
        isOK &= StepUp(indentationAxis, _defaultJawOpeningMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        isOK &= sm.logger().WriteCurrentState();                         
    }
    //else
    //{ //don't do it, the zero is not really zero.
    //   isOK &= indentationAxis->GotoSync(_defaultJawOpeningMicrons);
    //}        
    
    out << endl;
    out << "#########################################" << endl;
    out << "Now you can mount the plant" << endl;    
    out << "Press <ENTER> to start stretching ..." << endl;

    line = in.readLine();
        
    UpdateProtocolFlag(ProtocolFlag::GoingToTargetForce1);
    isOK &= sm.logger().WriteCurrentState(); 
    
    out << "Pre-relaxing..." << endl;
    isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, 0.0, 10.0, ProtocolFlag::PreRelaxing, ProtocolFlag::PreRelaxing, 1000000.0);
    out << "Pre-relaxing done." << endl;
    out << "Stretching..." << endl;
    isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, _targetForceMicronewtons, _holdingTimeS, ProtocolFlag::GoingToTargetForce1, ProtocolFlag::Holding1, _maxForceAbsMicronewtons);
    out << "Relaxing..." << endl;
    isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, 0.0, _relaxingTimeS, ProtocolFlag::GoingToTargetForce2, ProtocolFlag::Holding2, _maxForceAbsMicronewtons);
    
    
    isOK &= sm.logger().WriteCurrentState();         
    
    out << endl;
    out << "#########################################" << endl;
    out << "Now, remove the plant" << endl;    
    out << "Make sure the sensor is not loaded" << endl;    
    out << "Press <ENTER> to confirm..." << endl;

    line = in.readLine();
    
    
    //here, measure the force with the same sensor offset as the whole experiment
    
    myForceSensor->TareSensor();
    UpdateProtocolFlag(ProtocolFlag::TareSensor);        
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);    
    isOK &= sm.logger().WriteCurrentState();     
    
    return isOK;
}

bool CreepWithCalibration::Regulate(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive, double targetForceMicronewtons, double timeoutSeconds, ProtocolFlag flagGoingTo, ProtocolFlag flagHolding, double maxForceMicronewtons)
{
    bool isOK = true;
    bool keepGoing = true;
    double indentationAxisPosition;
    double step = 0.0;
    double initialIndentationPositionMicrons;
    double forceMicronewtons;
    double forceError=0.0;
    double avgForceError = 0.0;
    Timer timer = Timer();
    timer.Reset();
    StateManager& sm = this->experiment().stateManager();
    
    int phase = 1;    
    const double alpha = 20.0;
    isOK &= indentationAxis->GetPositionSync(initialIndentationPositionMicrons);
    while(keepGoing)
    {   
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = forceSensor->GetForce(); 
        forceError = forceMicronewtons - targetForceMicronewtons;
        avgForceError = 0.8*avgForceError + 0.2*forceError;
        if ((forceMicronewtons >= targetForceMicronewtons - _forceMarginMicronewtons)
           && 
           (forceMicronewtons <= targetForceMicronewtons + _forceMarginMicronewtons))
        {                 
            if (phase == 1)
            {
               phase = 2; //holding                   
               timer.Reset();
            }            
        }       
        //determine step size:
        if (phase == 2)
        {
            step = _stretchStepMicrons;
            if (fabs(avgForceError) < fabs(_forceMarginMicronewtons*(alpha-1)))
            {
                //step = _stretchStepMicrons*(fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha) -fabs(_forceMarginMicronewtons));
                step = _stretchStepMicrons*(fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha) );
                if (step < 0.0) {
                    step = 0;
                }
            }
        }
        else
        {
            step = _stretchStepMicrons;
            //if (abs(avgForceError) < abs(_forceMarginMicronewtons*10))
            //{
            //step = _stretchStepMicrons/3;
            //}
            if (fabs(avgForceError) < fabs(_forceMarginMicronewtons*(alpha-1)))
            {
                //step = _stretchStepMicrons*( fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha)  -fabs(_forceMarginMicronewtons));
                step = _stretchStepMicrons*( fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha) );
                if (step < 0.0)
                {
                    step = 0;
                }
            }
        }
        if (forceMicronewtons < targetForceMicronewtons - _forceMarginMicronewtons)
        {
            isOK &= StepDown(indentationAxis, step, isPushingPositive);
        }        
        if (forceMicronewtons > targetForceMicronewtons + _forceMarginMicronewtons)
        {          
            isOK &= StepUp(indentationAxis, step, isPushingPositive);            
        }   
        if (timer.TimeElapsedMs() > timeoutSeconds*1000)
        {             
            if (phase == 2)
            {
                keepGoing = false;
            }                  
            else
            {
                timer.Reset();
            }            
        }      
        if (fabs(forceMicronewtons) > fabs(maxForceMicronewtons))
        {
            //out << "Max force exceeded, aborting..." << endl;    
            keepGoing = false;
        }
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        {
            //out << "Max travel exceeded, aborting..." << endl;    
            keepGoing = false;
        }        
        if (phase == 1)
        {
            UpdateProtocolFlag(flagGoingTo);
            printf("going to force: %f, \t force: %f, \t timer: %f \t phase:%d \t step:%f\n", targetForceMicronewtons, forceMicronewtons, timer.TimeElapsedMs()/1000.0, phase, step);
            timer.Reset();
        }        
        if (phase == 2)
        {
            UpdateProtocolFlag(flagHolding);
            printf("holding force: %f, \t force: %f, \t timer: %f \t phase:%d \t step:%f\n", targetForceMicronewtons, forceMicronewtons, timer.TimeElapsedMs()/1000.0, phase, step);
        }        
        isOK &= sm.logger().WriteCurrentState();      
        if (!isOK)
        {
            keepGoing = false;
        }
    }    
    return isOK;
}

bool CreepWithCalibration::GoDownUntilStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;
    bool feelingStiffness = false;    
    //TODO: observe max force
    //TODO: observe max travel (useful for vertical hypocotyls in the air (Gabriella))    
    do
    {
        isOK &= StepDown(indentationAxis, _stretchStepMicrons*10, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();    //   printf("stiffness = %f\n", stiffness);
        //UpdateFlag(3); //like the old coarse approach
        if (stiffness > _stiffnessThreshold)
        {            
            feelingStiffness = true;            
        }       
        sm.logger().WriteCurrentState();        
    }while(!feelingStiffness);
    //TODO: check errors    
    return isOK;
 }

bool CreepWithCalibration::GoUpUntilNoStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;
    bool feelingStiffness = true;
    do
    {
        isOK &= StepUp(indentationAxis, _stretchStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();  //printf("stiffness = %f\n", stiffness);
        if (stiffness < _stiffnessThreshold)
        {       /*if (stiffness > 0.0) //otherwise, negative stiffness might indicate water meniscus broke and nothing is sure anymore
                {*///in other words, if you experience meniscus rupture, don't assume the tip is out of contact with the sample, keep moving up
            feelingStiffness = false;              //}
        }
        //UpdateFlag(3); //like the old coarse approach
        sm.logger().WriteCurrentState();        
    }while(feelingStiffness);
    //TODO: check errors
    //TODO: maybe go up a little bit more ?
    return isOK;
 }
 


bool CreepWithCalibration::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    isOK &= parms(_sectionName, "StretchStepMicrons", _stretchStepMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _stretchStepMicrons);        
    
    isOK &= parms(_sectionName, "MaxTravelMicrons", _maxTravelMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "MaxForceAbsMicronewtons", _maxForceAbsMicronewtons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "TargetTravelMicrons", _targetTravelMicrons);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "HoldingTimeSeconds", _holdingTimeS);
   
    isOK &= parms(_sectionName, "RelaxingTimeSeconds", _relaxingTimeS);
    
    isOK &= parms(_sectionName, "TargetForceMicronewtons", _targetForceMicronewtons);
    
    isOK &= parms(_sectionName, "ForceMarginMicronewtons", _forceMarginMicronewtons);
    
    isOK &= parms(_sectionName, "FindZeroPosition", _findZeroPosition);
    
    isOK &= parms(_sectionName, "StiffnessThreshold", _stiffnessThreshold);
    
    isOK &= parms(_sectionName, "DefaultJawOpening", _defaultJawOpeningMicrons);
    
    return isOK;
}

bool CreepWithCalibration::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

//TODO: consider if this could be a method of Axis ?
bool CreepWithCalibration::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}

bool CreepWithCalibration::StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}
