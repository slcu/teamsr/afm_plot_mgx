/* 
 * File:   Indentation.hpp
 * Author: cfmadmin
 *
 * Created on March 21, 2013, 6:01 PM
 */

#ifndef INDENTATION_HPP
#define	INDENTATION_HPP

#include "Experiment.hpp"
//class Experiment;

class Indentation : public Protocol{
public:
    Indentation(Experiment& experiment, QString sectionName);
    virtual ~Indentation();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;
    
protected:
    
    void UpdateFlag(unsigned int flag);
    void InvalidateFlag();
    
    int _fieldIDIndentationFlag;
    double _indentationStepMicrons;
    double _maxForceMicronewtons;
    double _maxDepthMicrons;
    double _minDepthMicrons;
    //int _maxBadWigglesPerPoint;
    unsigned int _loadingCycles;
    QString _indentationForceSensorName;    
    QString _indentationAxisName;
    
};

#endif	/* INDENTATION_HPP */

