/* 
 * File:   Creep.cpp
 * Author: cfmadmin
 * 
 * Created on May 7, 2013, 2:17 PM
 */
#include <morphorobotx/Robot.hpp>
#include <stdlib.h>

#include "Creep.hpp"

REGISTER_PROTOCOL("Creep", Creep);

Creep::Creep(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{
    _fieldIdProtocolFlag = this->experiment().stateManager().RegisterFieldExc(QString("ProtocolFlag"), QString("(flag)"), false);        
}

Creep::~Creep() {
}

bool Creep::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    /*Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }*/
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    //StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    /*primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);*/
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
        
    double indentationAxisPosition = 0.0;
    double d = 0.0;
    
    sm.state().Renew();   
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
    double initialIndentationPositionMicrons = indentationAxisPosition;
    double forceMicronewtons = 0.0;
    myForceSensor->TareSensor(); //how to tare the sensor ?
    UpdateProtocolFlag(ProtocolFlag::TareSensor);
    isOK &= sm.logger().WriteCurrentState(); 
    UpdateProtocolFlag(ProtocolFlag::GoingToTargetForce1);
    isOK &= sm.logger().WriteCurrentState(); 
    
    Timer timer = Timer();
    timer.Reset();
    bool keepGoing = true;
    int phase = 1;
    double step = 0;
    bool firstTarget = true;
    while(keepGoing)
    {            
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();        
        if ((forceMicronewtons >= _targetForceMicronewtons - _forceMarginMicronewtons)
           && 
           (forceMicronewtons <= _targetForceMicronewtons + _forceMarginMicronewtons))
        {     
             if (firstTarget)  
             {
                 if (phase == 1)
                 {
                    phase = 2; //holding first target
                    //firstTarget = false;
                    timer.Reset();
                 }
             }
             else
             {
                 if (phase == 3)
                 {
                    phase = 4; //holding of the second target (zero)
                 }
             }
        }     
        if (forceMicronewtons < _targetForceMicronewtons - _forceMarginMicronewtons)
        {
            if ((phase == 2)||(phase == 4)) //smaller step while holding target
            {
                step = _stretchStepMicrons/5;
            }
            else
            {
                step = _stretchStepMicrons;
            }
            isOK &= StepDown(indentationAxis, step, isPushingPositive);
            if (phase == 1)
            {
                phase = 2;
            }
        }        
        if (forceMicronewtons > _targetForceMicronewtons + _forceMarginMicronewtons)
        {
            if (phase == 2)
            {
                step = _stretchStepMicrons/5;
            }
            else
            {
                step = _stretchStepMicrons;
            }
            isOK &= StepUp(indentationAxis, step, isPushingPositive);
            if (phase == 3)
            {
                phase = 4;
            }
        }                                
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        {
            keepGoing = false;
        }
        if (fabs(forceMicronewtons) > _maxForceAbsMicronewtons)
        {
            keepGoing = false;
        }
        if (timer.TimeElapsedMs() > _holdingTimeS*1000)
        {             
            if (firstTarget)
            {
                phase = 3;
            }
            _targetForceMicronewtons = 0.0;
            firstTarget = false;
        }        
        
        if (phase == 1)
        {
            UpdateProtocolFlag(ProtocolFlag::GoingToTargetForce1);
        }
        
        if (phase == 2)
        {
            UpdateProtocolFlag(ProtocolFlag::Holding1);
        }
        
        if (phase == 3)
        {
            UpdateProtocolFlag(ProtocolFlag::GoingToTargetForce2);            
        }
        
        if (phase == 4)
        {
            UpdateProtocolFlag(ProtocolFlag::Holding2);
        }

        
        isOK &= sm.logger().WriteCurrentState();         
    }
    return isOK;
}


bool Creep::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    isOK &= parms(_sectionName, "StretchStepMicrons", _stretchStepMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _stretchStepMicrons);        
    
    isOK &= parms(_sectionName, "MaxTravelMicrons", _maxTravelMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "MaxForceAbsMicronewtons", _maxForceAbsMicronewtons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "TargetTravelMicrons", _targetTravelMicrons);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "HoldingTimeSeconds", _holdingTimeS);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "TargetForceMicronewtons", _targetForceMicronewtons);
    
    isOK &= parms(_sectionName, "ForceMarginMicronewtons", _forceMarginMicronewtons);
    
    return isOK;
}

bool Creep::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

//TODO: consider if this could be a method of Axis ?
bool Creep::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}

bool Creep::StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}