/* 
 * File:   Dynamic.cpp
 * Author: cfmadmin
 * 
 * Created on May 10, 2013, 4:00 PM
 */

#include <morphorobotx/Robot.hpp>
#include <stdlib.h>

#include "Dynamic.hpp"

REGISTER_PROTOCOL("Dynamic", Dynamic);

Dynamic::Dynamic(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{
        _fieldIdProtocolFlag = this->experiment().stateManager().RegisterFieldExc(QString("ProtocolFlag"), QString("(flag)"), false);        
}


Dynamic::~Dynamic() {
}

bool Dynamic::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    /*Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }*/
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    //StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    /*primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);*/
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
        
    double indentationAxisPosition = 0.0;
    double d = 0.0;
    
    sm.state().Renew();   
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
    double targetPos = indentationAxisPosition;
    double initialIndentationPositionMicrons = indentationAxisPosition;
    double forceMicronewtons = 0.0;
    myForceSensor->TareSensor(); //how to tare the sensor ?
    isOK &= sm.logger().WriteCurrentState(); 
    Timer timer = Timer();
    timer.Reset();
    bool keepGoing = true;
    while(keepGoing)
    {            
        targetPos = initialIndentationPositionMicrons - _strainAmplitudeMicrons/2 + _strainAmplitudeMicrons/2 * sin(6.2832*timer.TimeElapsedMs()/_cycleTimeMiliseconds);
        isOK &= indentationAxis->GotoSync(targetPos);
        
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();
        
        /*if (forceMicronewtons < _targetForceMicronewtons - _forceMarginMicronewtons)
        {
            isOK &= StepDown(indentationAxis, _stretchStepMicrons, isPushingPositive);
        }*/
        
        /*if (forceMicronewtons > _targetForceMicronewtons + _forceMarginMicronewtons)
        {
            isOK &= StepUp(indentationAxis, _stretchStepMicrons, isPushingPositive);
        }*/
                                
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        {
            keepGoing = false;
        }
        if (fabs(forceMicronewtons) > _maxForceAbsMicronewtons)
        {
            keepGoing = false;
        }
        /*if (timer.TimeElapsedMs() > _holdingTimeS*1000)
        {
            _targetForceMicronewtons = 0.0;
        }*/
        
        
        
        
        
        isOK &= sm.logger().WriteCurrentState();         
    }
     

    return isOK;
}


bool Dynamic::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    isOK &= parms(_sectionName, "StretchStepMicrons", _stretchStepMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _stretchStepMicrons);        
    
    isOK &= parms(_sectionName, "MaxTravelMicrons", _maxTravelMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "MaxForceAbsMicronewtons", _maxForceAbsMicronewtons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "TargetTravelMicrons", _targetTravelMicrons);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "HoldingTimeSeconds", _holdingTimeS);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "TargetForceMicronewtons", _targetForceMicronewtons);
    
    //isOK &= parms(_sectionName, "ForceMarginMicronewtons", _forceMarginMicronewtons);
    
    isOK &= parms(_sectionName, "CycleTimeMiliseconds", _cycleTimeMiliseconds);
    
    isOK &= parms(_sectionName, "StrainAmplitudeMicrons", _strainAmplitudeMicrons);
    
    return isOK;
}

bool Dynamic::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

//TODO: consider if this could be a method of Axis ?
bool Dynamic::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}

bool Dynamic::StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}