/* 
 * File:   Indentation.cpp
 * Author: cfmadmin
 * 
 * Created on March 21, 2013, 6:01 PM
 */

#include "Indentation.hpp"
//#include "Experiment.hpp"

REGISTER_PROTOCOL("Indentation", Indentation);

Indentation::Indentation(Experiment& experiment, QString sectionName) : Protocol(experiment, sectionName)
{
    _fieldIDIndentationFlag = experiment.stateManager().RegisterFieldExc("IndentationFlag", "(flag)", false);//TODO: consider if flags should be volatile
}

Indentation::~Indentation() {
}


bool Indentation::run()
{
    bool isOK = true;
    printf("Inside of Indentation::run()\n");
    
    //TODO: this should be done at the beginning (in Init() ) because it may fail and give null pointer in the middle of an experiment
    //Axis* indentationAxis = this->experiment().robot().axis(_indentationAxisName); //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    Axis* indentationAxis = this->experiment().robot().indentationAxis();
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }        

    //ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().GetSensorPtrByName("LoadCellFutek89844"); //TODO: this is wrong, the sensor name refers
    //TODO: this should be done at the beginning (in Init() ) because it may fail and give null pointer in the middle of an experiment
    //ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().sensor(_indentationForceSensorName);   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    StateManager& sm = experiment().stateManager();
    double force = 0.0;
    
    //tare the sensor
    //force = myForceSensor->GetForce();
    myForceSensor->TareSensor(); //weird, the sensor has been tared just a while ago
    sm.logger().WriteCurrentState();
    
    double position = 0.0;
    isOK &= indentationAxis->GetPositionSync(position);
    if (!isOK)
    {
        printf("GetPositionSync failed\n");
    }
    //TARE_SENSOR FLAG
    UpdateFlag(0); //sm.state().Update(_fieldIDIndentationFlag, 0);
    sm.logger().WriteCurrentState();
    
    double targetPosition = position;
    //for (int i=0; i<30; i++)   
    
    const bool isPushingPositive = myForceSensor->isPushingPositive();
    
    double indentationDepth = 0.0;
    double initialPosition = position;
    while(1)
    {                        
        sm.state().Update(_fieldIDIndentationFlag, 2);
        isOK &= indentationAxis->GetPositionSync(position);
        if (!isOK)
        {
            printf("GetPositionSync failed\n");
        }
        if (isPushingPositive)
        {
            targetPosition = position + _indentationStepMicrons;
        }
        else
        {
            targetPosition = position - _indentationStepMicrons;
        }
        
        isOK &= indentationAxis->GotoSync(targetPosition);
        if (!isOK)
        {
           printf("GotoSync failed\n");
        }
        isOK &= indentationAxis->GetPositionSync(position);
        if (!isOK)
        {
            printf("GetPositionSync failed\n");
        }
        force = myForceSensor->GetForce();                
        sm.logger().WriteCurrentState();
        if (force > _maxForceMicronewtons)
        {
            break;
        }
        indentationDepth = fabs(position - initialPosition);
        if (indentationDepth > _maxDepthMicrons)
        {
            break;
        }
        //TODO: check errors
        if (!isOK)
        {
            break;
        }
    }
    //for (int i=0; i<30; i++)
    
    //UpdateFlag(4); //sm.state().Update(_fieldIDIndentationFlag, 4); //mark max force
    //sm.logger().WriteCurrentState();
    /*
    while(force>_maxForceMicronewtons/100) //TODO: go back to initial position rather than to a force, meniscus or offset can make it go up infinitely
    {   
        UpdateFlag(5); //sm.state().Update(_fieldIDIndentationFlag, 5);        
        indentationAxis->GetPositionSync(position);
        if (isPushingPositive)
        {
            targetPosition = position - _indentationStepMicrons;
        }
        else
        {
            targetPosition = position + _indentationStepMicrons;
        }
        indentationAxis->GotoSync(targetPosition);
        indentationAxis->GetPositionSync(position);
        
        if(isPushingPositive)
        {
            if (position < initialPosition)
            {
                break;
            }
        }
        else
        {
            if (position > initialPosition)
            {
                break;
            }        
        }        
        
        force = myForceSensor->GetForce();        
        sm.logger().WriteCurrentState();
    }*/
    //TODO: if is pushing positive
    /* //TODO: make it configurable from parameters
    if (isPushingPositive)
    {
        targetPosition = position - 500;
    } 
    else
    {
        targetPosition = position + 500;
    }
      */
    
    isOK &= indentationAxis->GetPositionSync(position);
    if (!isOK)
    {
        printf("GetPositionSync failed\n");
    }
    if (isPushingPositive)
    {
        targetPosition = position - _indentationStepMicrons;
    }
    else
    {
        targetPosition = position + _indentationStepMicrons;
    }
    isOK &= indentationAxis->GotoSync(targetPosition);
    if (!isOK)
    {
        printf("GotoSync failed\n");
    }
     
    isOK &= indentationAxis->GetPositionSync(position);
    if (!isOK)
    {
        printf("GetPositionSync failed\n");
    }
    force = myForceSensor->GetForce();        
    UpdateFlag(5); //"scan up" flag, quite fake
    sm.logger().WriteCurrentState();    
    InvalidateFlag();
    //sm.logger().WriteCurrentState();    
    return isOK;
}

bool Indentation::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    
//    isOK &= parms(_sectionName, "IndentationAxis", _indentationAxisName);
//    printf("Indentation::readParameters, _indentationAxisName = %s\n", _indentationAxisName.toLocal8Bit().data());    
    
    isOK &= parms(_sectionName, "IndentationStep", _indentationStepMicrons);
    printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _indentationStepMicrons);    
   
    isOK &= parms(_sectionName, "MaxForce", _maxForceMicronewtons);
    printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _indentationStepMicrons);   
    
//    isOK &= parms(_sectionName, "IndentationForceSensor", _indentationForceSensorName);
//    printf("Indentation::readParameters, _indentationForceSensorName = %s\n", _indentationForceSensorName.toLocal8Bit().data());   
    
    isOK &= parms(_sectionName, "MaxDepth", _maxDepthMicrons);
    printf("Indentation::readParameters, _maxDepthMicrons =%f\n", _maxDepthMicrons);
    
    isOK &= parms(_sectionName, "MinDepth", _minDepthMicrons);
    printf("Indentation::readParameters, _minDepthMicrons =%f\n", _minDepthMicrons);
    /*
    isOK &= parms(_sectionName, "MaxBadWigglesPerPoint", _maxBadWigglesPerPoint);
    printf("Indentation::readParameters, _maxBadWigglesPerPoint =%d\n", _maxBadWigglesPerPoint);   
    */
    return isOK;
}


bool Indentation::writeParameters(Parms& parms) const
{
    //TODO: implement
    return true;
}

 void Indentation::UpdateFlag(unsigned int flag)
 {
     StateManager& sm = experiment().stateManager();
     sm.state().Update(_fieldIDIndentationFlag, flag);
 }
 
 void Indentation::InvalidateFlag()
 {
     StateManager& sm = experiment().stateManager();
     sm.state().Invalidate(_fieldIDIndentationFlag);
 }