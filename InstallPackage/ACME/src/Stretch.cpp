/* 
 * File:   Stretch.cpp
 * Author: cfmadmin
 * 
 * Created on May 3, 2013, 3:29 PM
 */
#include <morphorobotx/Robot.hpp>

#include <stdlib.h>
#include "Stretch.hpp"

REGISTER_PROTOCOL("Stretch", Stretch);

Stretch::Stretch(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName)  {
}

Stretch::~Stretch() {
}

bool Stretch::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    /*Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }*/
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    //StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    /*primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);*/
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
        
    double indentationAxisPosition = 0.0;
    double d = 0.0;
    
    sm.state().Renew();   
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
    double initialIndentationPositionMicrons = indentationAxisPosition;
    double forceMicronewtons = 0.0;
    myForceSensor->TareSensor(); //how to tare the sensor ?
    isOK &= sm.logger().WriteCurrentState(); 
    
    bool keepGoing = true;
    while(keepGoing)
    {            
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();
        isOK &= StepUp(indentationAxis, _stretchStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        {
            keepGoing = false;
        }
        if (fabs(forceMicronewtons) > _maxForceAbsMicronewtons)
        {
            keepGoing = false;
        }
        isOK &= sm.logger().WriteCurrentState(); 
    }
    
    keepGoing = true;
    /*
    while(keepGoing)
    {            
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();
        isOK &= StepDown(indentationAxis, _stretchStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) )
        {
            keepGoing = false;
        }
        
        if (fabs(forceMicronewtons) > _maxForceAbsMicronewtons)
        {
            keepGoing = false;
        }
        isOK &= sm.logger().WriteCurrentState(); 
    }
    */
    
    
    
    return isOK;
}


bool Stretch::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    isOK &= parms(_sectionName, "StretchStepMicrons", _stretchStepMicrons);
    printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _stretchStepMicrons);        
    
    isOK &= parms(_sectionName, "MaxTravelMicrons", _maxTravelMicrons);
    printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "MaxForceAbsMicronewtons", _maxForceAbsMicronewtons);
    printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    return isOK;
}

bool Stretch::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

//TODO: consider if this could be a method of Axis ?
bool Stretch::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}
