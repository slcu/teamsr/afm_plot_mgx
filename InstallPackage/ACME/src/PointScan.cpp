/* 
 * File:   PointScan.cpp
 * Author: cfmadmin
 * 
 * Created on February 18, 2013, 7:40 PM
 */

#include "PointScan.hpp"
#include "Indentation.hpp"
#include "StiffnessMonitor.hpp"
#include "CandidateList.hpp"
//#include "Experiment.hpp"

REGISTER_PROTOCOL("PointScan", PointScan);

PointScan::PointScan(Experiment& experiment, QString sectionName):  Protocol(experiment, sectionName) 
{
    _fieldIDPointScanFlag       = experiment.stateManager().RegisterFieldExc("PointScanFlag", "(flag)", false);
    _fieldIDPointScanWiggleID   = experiment.stateManager().RegisterFieldExc("PointScanWiggleID", "()", false);    
    _fieldIDPointScanContactPosition = experiment.stateManager().RegisterFieldExc("PointScanContactPosition", "()", false); 
}

/*PointScan::PointScan(const PointScan& orig) {
}*/

PointScan::~PointScan() {
}

/// TODO: implement
bool PointScan::run()
{        
  
    bool isOK = true;
    //Axis* indentationAxis = this->experiment().robot().GetAxisPtrByName("ZAxis");//old, wrong way
    //Axis* indentationAxis = this->experiment().robot().axis(_indentationAxisName); //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    Axis* indentationAxis = this->experiment().robot().indentationAxis();
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }        
    //ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().GetSensorPtrByName("LoadCellFutek89844"); //TODO: this is wrong, the sensor name refers
    //ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().axis(_indentationForceSensorName);   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor(); 
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    StateManager& sm = experiment().stateManager();    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    double force = 0.0;    
    //tare the sensor
    //force = myForceSensor->GetForce();
    //myForceSensor->TareSensor(); // too early, first go up until no stiffness
    sm.logger().WriteCurrentState();    
    double position = 0.0;
    indentationAxis->GetPositionSync(position);
    UpdateFlag(0);   //sm.state().Update(_fieldIDPointScanFlag, 0);
    sm.logger().WriteCurrentState();        
    //NOTE: the original software goes up until no stiffness only if the first step had stiffness higher than water threshold
    isOK &= GoUpUntilNoStiffness(indentationAxis, myForceSensor, isPushingPositive); //to make sure you have a good start   
    if(!isOK)
    {
        printf("GoUpUntilNoStiffness failed\n");
        return false;
    }
    /*
    isOK &= GoDownUntilStiffness(indentationAxis, myForceSensor, isPushingPositive); //to find sample
    if(!isOK)
    {
        return false;
    }*/    
    isOK &= CoarseApproach(indentationAxis, myForceSensor, isPushingPositive);    
    
    indentationAxis->GetPositionSync(position);
    sm.state().Update(_fieldIDPointScanContactPosition, position); //mark the collision coordinate as surface position
    
    isOK &= GoUpUntilNoStiffness(indentationAxis, myForceSensor, isPushingPositive); //prepare for offset reset      
    if(!isOK)
    {
        printf("GoUpUntilNoStiffness failed\n");
        return false;
    }        
    int wiggleID = 0;
    int goodWiggleCounter = 0;
    int badWiggleCounter = 0;
    while(goodWiggleCounter < _loadingCycles)
    {    
        myForceSensor->TareSensor(); //set zero force from current water offset
        //TODO: GetForce too ?
        UpdateFlag(0); //what for ? //TODO: make an enum for the flags or something
        sm.state().Update(_fieldIDPointScanWiggleID, wiggleID);
        sm.logger().WriteCurrentState();                                    
        UpdateFlag(2); //what for ? //TODO: make an enum for the flags or something
        //std::shared_ptr<Protocol> indentationPtr = factory().protocol(experiment(), _indentationProtocolName);
        printf("inside of Pointscan::run(), about to launch indentationPrr->run()\n");
        if (indentationPtr==nullptr)
        {
            printf("ERROR: indentationPtr is null\n");
            return false;
        }
        isOK &= indentationPtr->run();
        if(!isOK)
        {
            printf("Indentation failed\n");
            return false;
        }
        isOK &= GoUpUntilNoStiffness(indentationAxis, myForceSensor, isPushingPositive); //to make sure you have a good start       
        if(!isOK)
        {
            printf("GoUpUntilNoStiffness failed\n");
            return false;
        }
        force = myForceSensor->GetForce(); //at this point the force would be zero if the meniscus was stable
        if ( fabs(force) > _forceDriftThreshold )
        {
            printf("bad wiggle!\n");
            badWiggleCounter++;
            _badWiggleBuffer.Push(1);
        }
        else
        {
            goodWiggleCounter++;
            _badWiggleBuffer.Push(0);            
        }
        wiggleID++;
        if (badWiggleCounter > _maxBadWigglesPerPoint)
        {
            //give up wiggling, too many problems
            printf("too many bad wiggles per point\n");
            break;
        }
        if (_badWiggleBuffer.Sum() > _maxBadWigglesPerBuffer) //TODO: how to notify the parent protocol to abort
        {
            printf("too many bad wiggles in the buffer\n");            
            isOK = false;
            break;
        }
        
    }
    InvalidateFlag();
    //sm.logger().WriteCurrentState();
    //normally the protocol quits having the sensor not attached to sample    
    if (!isOK) printf("PointScan::run(), about to return false\n");
    if (isOK) printf("PointScan::run(), about to return true\n");
    return isOK; 
}

bool PointScan::Init(const Parms& parms)
{
    bool isOK  = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
        
    isOK &= parms(_sectionName, "CoarseApproachStep", _coarseApproachStepMicrons);
    printf("PointScan::Init, _coarseApproachStepMicrons = %f\n", _coarseApproachStepMicrons);
    
    //isOK &= parms(_sectionName, "IndentationForceSensor", _indentationForceSensorName);  //TODO: consider it this should be global (for example: experiment.IndentationForceSensorName() )
    //printf("PointScan::Init, _indentationForceSensorName = %s\n", _indentationForceSensorName.toLocal8Bit().data());
    
    //isOK &= parms(_sectionName, "IndentationAxis", _indentationAxisName);  //TODO: consider it this should be global (for example: experiment.IndentationAxisName() )
    //printf("PointScan::Init, _indentationAxisName = %s\n", _indentationAxisName.toLocal8Bit().data());
    
    isOK &= parms(_sectionName, "IndentationAction", _indentationProtocolName);
    printf("PointScan::Init, _indentationProtocolName = %s\n", _indentationProtocolName.toLocal8Bit().data());
    
    isOK &= parms(_sectionName, "WaterStiffnessThreshold", _waterStiffnessThreshold);
    printf("PointScan::Init, _waterStiffnessThreshold = %f\n", _waterStiffnessThreshold);
    
    isOK &= parms(_sectionName, "SampleStiffnessThreshold", _sampleStiffnessThreshold);
    printf("PointScan::Init, _sampleStiffnessThreshold = %f\n", _sampleStiffnessThreshold);   
    
    isOK &= parms(_sectionName, "MaxBadWigglesPerPoint", _maxBadWigglesPerPoint);
    printf("PointScan::Init, _maxBadWigglesPerPoint = %d\n", _maxBadWigglesPerPoint);   
    
    isOK &= parms(_sectionName, "MaxBadWigglesPerBuffer", _maxBadWigglesPerBuffer);
    printf("PointScan::Init, _maxBadWigglesPerBuffer = %d\n", _maxBadWigglesPerBuffer);   
    
    isOK &= parms(_sectionName, "BadWiggleBufferSize", _badWiggleBufferSize);
    printf("PointScan::Init, _badWiggleBufferSize = %d\n", _badWiggleBufferSize);       
    
    isOK &= parms(_sectionName, "LoadingCycles", _loadingCycles);
    printf("PointScan::Init, _loadingCycles = %d\n", _loadingCycles);   
    
    isOK &= parms(_sectionName, "ForceDriftThreshold", _forceDriftThreshold);
    printf("PointScan::Init, _forceDriftThreshold = %f\n", _forceDriftThreshold);   
        
    indentationPtr = factory().protocol(experiment(), _indentationProtocolName);
    if (indentationPtr == nullptr)
    {
        printf("ERROR: could not create protocol. Factory returned null pointer\n");
        return false;
    }
    isOK &= indentationPtr->Init(parms);
    if(!isOK)
    {
        printf("ERROR: Indentation::readParameters failed\n");
    }    
            
    _badWiggleBuffer.Init(_badWiggleBufferSize);        
    
    return isOK; //TODO: implement
}

bool PointScan::writeParameters(Parms& parms) const
{
    return false; //TODO: implement
}

//TODO: consider if this could be a method of Axis ?
bool PointScan::StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}

//TODO: consider if this could be a method of Axis ?
bool PointScan::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{    
    printf("PointScan::StepUp by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;
}

 bool PointScan::GoUpUntilNoStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;
    bool feelingStiffness = true;
    do
    {
        isOK &= StepUp(indentationAxis, _coarseApproachStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();  //printf("stiffness = %f\n", stiffness);
        if (stiffness < this->_waterStiffnessThreshold)
        {       /*if (stiffness > 0.0) //otherwise, negative stiffness might indicate water meniscus broke and nothing is sure anymore
                {*///in other words, if you experience meniscus rupture, don't assume the tip is out of contact with the sample, keep moving up
            feelingStiffness = false;              //}
        }
        UpdateFlag(3); //like the old coarse approach
        sm.logger().WriteCurrentState();        
    }while(feelingStiffness);
    //TODO: check errors
    //TODO: maybe go up a little bit more ?
    return isOK;
 }
 
 bool PointScan::GoDownUntilStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;
    bool feelingStiffness = false;    
    //TODO: observe max force
    //TODO: observe max travel (useful for vertical hypocotyls in the air (Gabriella))    
    do
    {
        isOK &= StepDown(indentationAxis, _coarseApproachStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();    //   printf("stiffness = %f\n", stiffness);
        UpdateFlag(3); //like the old coarse approach
        if (stiffness > this->_sampleStiffnessThreshold)
        {            
            feelingStiffness = true;            
        }       
        sm.logger().WriteCurrentState();        
    }while(!feelingStiffness);
    //TODO: check errors    
    return isOK;
 }
 
 void PointScan::UpdateFlag(unsigned int flag)
 {
     StateManager& sm = experiment().stateManager();
     sm.state().Update(_fieldIDPointScanFlag, flag);
 }
 
 void PointScan::InvalidateFlag()
 {
     StateManager& sm = experiment().stateManager();
     sm.state().Invalidate(_fieldIDPointScanFlag);
 }
 
 bool PointScan::CoarseApproach(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    CandidateList candidateList;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;        
    bool feelingStiffness = false;
    do
    {
        isOK &= StepDown(indentationAxis, _coarseApproachStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();
     //   printf("stiffness = %f\n", stiffness);
        UpdateFlag(3); //like the old coarse approach
        if (stiffness > this->_sampleStiffnessThreshold)
        {            
            feelingStiffness = true;            
        }
        else
        {
            if (stiffness > this->_waterStiffnessThreshold)
            {
                bool isConfirmed = candidateList.CheckCandidate(positionMicrons, _coarseApproachStepMicrons/2.0);
                if (isConfirmed)
                {
                    feelingStiffness = true;
                }
                else
                {
                    candidateList.AddCandidate(positionMicrons);
                    isOK &= StepUp(indentationAxis, _coarseApproachStepMicrons*2.0, isPushingPositive);
                    isOK &= indentationAxis->GetPositionSync(positionMicrons);
                    forceMicronewtons = forceSensor->GetForce();
                    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);                    
                }                
            }                                    
        }                
        //TODO: check for max force
        //TODO: check for max travel                
        sm.logger().WriteCurrentState();        
    }while(!feelingStiffness);            
    return isOK;
 }