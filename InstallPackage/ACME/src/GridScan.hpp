/* 
 * File:   GridScan.hpp
 * Author: cfmadmin
 *
 * Created on February 8, 2013, 9:40 PM
 */

#ifndef GRIDSCAN_HPP
#define	GRIDSCAN_HPP

//#include "Factory.hpp"
//#include "Robot.hpp"
//#include "ForceMeasurement.hpp"
//#include "HardwareManager.hpp"
#include "Experiment.hpp"
#include "QString"

class GridScan : public Protocol {

public:

    enum ProtocolFlag{
        TareSensor,
        IndentationStep
    };
    
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    
    GridScan(Experiment& experiment, QString sectionName);
    //GridScan(const GridScan& orig);
    virtual ~GridScan();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;
    void UpdateTime()
    { 
        experiment().stateManager().state().Update(_fieldIdTime, experiment().TimeElapsedMs());
    }
    void UpdateProtocolFlag(ProtocolFlag flag)
    {
        experiment().stateManager().state().Update(_fieldIdProtocolFlag, flag);
    }
               
protected:
    
    bool generateCoordinates(std::vector<double>& gridCoordsX, std::vector<double>& gridCoordsY, QString poiFileName);
    bool gnuplotGridPoints(std::vector<double>& gridCoordsX, std::vector<double>& gridCoordsY, QString filename); //TODO: rename to something more serious
    bool readPoiFile(std::vector<double>& poiX, std::vector<double>& poiY, QString poiFileName);
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    int _fieldIdGridPointId;
    double experimentMaxForce;
    double microstepSize;
    double sensorGain;
    double sensorMaxForce;
    int integrationTimeMs;
    
    int _GridPointsPrimary;
    int _GridPointsSecondary;
    double _GridStepMicrons;
    
    QString _pointActionProtocolName;
    //QString _indentationForceSensorName; //TODO: consider it this should be global (for example: experiment.ForceSensorName() )
    QString _indentationAxisSectionName; //TODO: consider it this should be global (for example: experiment.IndentationAxisName() )
    QString _primaryAxisSectionName;
    QString _secondaryAxisSectionName;
    QString _gridOrder;
    
};

#endif	/* GRIDSCAN_HPP */

