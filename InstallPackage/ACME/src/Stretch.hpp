/* 
 * File:   Stretch.hpp
 * Author: cfmadmin
 *
 * Created on May 3, 2013, 3:29 PM
 */

#ifndef STRETCH_HPP
#define	STRETCH_HPP
#include "Experiment.hpp"
#include "QString"

class Stretch : public Protocol{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    
    
    Stretch(Experiment& experiment, QString sectionName);
    
    virtual ~Stretch();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;
    void UpdateTime()
    { 
        experiment().stateManager().state().Update(_fieldIdTime, experiment().TimeElapsedMs());
    }
/*    void UpdateProtocolFlag(ProtocolFlag flag)
    {
        experiment().stateManager().state().Update(_fieldIdProtocolFlag, flag);
    }*/
    
protected:
    
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    double _stretchStepMicrons;
    double _maxTravelMicrons;
    double _maxForceAbsMicronewtons;
    

};

#endif	/* STRETCH_HPP */

