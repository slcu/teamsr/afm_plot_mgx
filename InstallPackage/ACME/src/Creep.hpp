/* 
 * File:   Creep.hpp
 * Author: cfmadmin
 *
 * Created on May 7, 2013, 2:17 PM
 */

#ifndef CREEP_HPP
#define	CREEP_HPP

#include "Experiment.hpp"
#include "QString"

class Creep : public Protocol{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    Creep(Experiment& experiment, QString sectionName);
    virtual ~Creep();
        
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;        
    
protected:
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);    
    
    enum ProtocolFlag{
        TareSensor=0,
        GoingToTargetForce1=1,
        Holding1=2,
        GoingToTargetForce2=3,        
        Holding2=4
    };
    
    void UpdateProtocolFlag(ProtocolFlag flag)
    {
        experiment().stateManager().state().Update(_fieldIdProtocolFlag, flag);
    }
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    double _stretchStepMicrons;
    double _maxTravelMicrons;
    double _maxForceAbsMicronewtons;
    double _targetTravelMicrons;
    double _holdingTimeS;
    double _targetForceMicronewtons;
    double _forceMarginMicronewtons;

};

#endif	/* CREEP_HPP */

