/* 
 * File:   ForceReading.hpp
 * Author: cfmadmin
 *
 * Created on July 3, 2013, 5:24 PM
 */

#ifndef FORCEREADING_HPP
#define	FORCEREADING_HPP

#include "Experiment.hpp"
#include "QString"

class ForceReading : public Protocol {
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    ForceReading(Experiment& experiment, QString sectionName);        
    virtual ~ForceReading();
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;        
    
protected:

};

#endif	/* FORCEREADING_HPP */

