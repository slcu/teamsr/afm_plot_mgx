/* 
 * File:   StressRelaxation.cpp
 * Author: cfmadmin
 * 
 * Created on May 7, 2013, 12:49 PM
 */
#include <morphorobotx/Robot.hpp>
#include <stdlib.h>

#include "StressRelaxation.hpp"

REGISTER_PROTOCOL("StressRelaxation", StressRelaxation);

StressRelaxation::StressRelaxation(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{
        _fieldIdProtocolFlag = this->experiment().stateManager().RegisterFieldExc(QString("ProtocolFlag"), QString("(flag)"), false);        
}


StressRelaxation::~StressRelaxation() {
}

bool StressRelaxation::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    /*Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }*/
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    //StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    /*primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);*/
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
        
    double indentationAxisPosition = 0.0;
    double d = 0.0;
    
    sm.state().Renew();   
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
    double initialIndentationPositionMicrons = indentationAxisPosition;
    double forceMicronewtons = 0.0;
    myForceSensor->TareSensor(); //how to tare the sensor ?
    UpdateProtocolFlag(ProtocolFlag::TareSensor);
    isOK &= sm.logger().WriteCurrentState(); 
    
    bool keepGoing = true;
    bool keepHolding = false;
    while(keepGoing)
    {            
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();
        isOK &= StepUp(indentationAxis, _stretchStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        {
            keepGoing = false;
        }
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _targetTravelMicrons)
        {
            keepGoing = false;
            keepHolding = true;
        }        
        if (fabs(forceMicronewtons) > _maxForceAbsMicronewtons)
        {
            keepGoing = false;
        }
        UpdateProtocolFlag(ProtocolFlag::GoingToTargetForce);
        isOK &= sm.logger().WriteCurrentState(); 
    }
    
    keepGoing = true;
    
    Timer timer = Timer();
    timer.Reset();
    while(keepHolding)
    {
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();
        if (timer.TimeElapsedMs() > _holdingTimeS*1000)
        {
            keepHolding = false;
        }
        UpdateProtocolFlag(ProtocolFlag::Holding);
        isOK &= sm.logger().WriteCurrentState(); 
    }
    
    /*
    while(keepGoing)
    {            
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = myForceSensor->GetForce();
        isOK &= StepDown(indentationAxis, _stretchStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        
        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) )
        {
            keepGoing = false;
        }
        
        if (fabs(forceMicronewtons) > _maxForceAbsMicronewtons)
        {
            keepGoing = false;
        }
        isOK &= sm.logger().WriteCurrentState(); 
    }
    */            
    return isOK;
}
 
bool StressRelaxation::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    isOK &= parms(_sectionName, "StretchStepMicrons", _stretchStepMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _stretchStepMicrons);        
    
    isOK &= parms(_sectionName, "MaxTravelMicrons", _maxTravelMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "MaxForceAbsMicronewtons", _maxForceAbsMicronewtons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "TargetTravelMicrons", _targetTravelMicrons);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "HoldingTimeSeconds", _holdingTimeS);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    return isOK;
}

bool StressRelaxation::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

//TODO: consider if this could be a method of Axis ?
bool StressRelaxation::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}

bool StressRelaxation::StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);        
    return isOK;            
}