/* 
 * File:   CreepWithCalibration.hpp
 * Author: cfmadmin
 *
 * Created on May 27, 2013, 1:13 PM
 */

#ifndef CREEPWITHCALIBRATION_HPP
#define	CREEPWITHCALIBRATION_HPP

#include "Experiment.hpp"
#include "QString"

class CreepWithCalibration : public Protocol{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    CreepWithCalibration(Experiment& experiment, QString sectionName);    
    virtual ~CreepWithCalibration();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;        
    
protected:
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);  
    bool GoDownUntilStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive);
    bool GoUpUntilNoStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive);    
    
    enum ProtocolFlag{
        TareSensor=0,
        GoingToTargetForce1=1,
        Holding1=2,
        GoingToTargetForce2=3,        
        Holding2=4,
        PreRelaxing=10,
        AdjustingJaws=100,
        ZeroPosition=101
    };
    
    bool Regulate(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive, double targetForceMicronewtons, double timeoutSeconds, ProtocolFlag flagGoingTo, ProtocolFlag flagHolding, double maxForceMicronewtons);
    
    void UpdateProtocolFlag(ProtocolFlag flag)
    {
        experiment().stateManager().state().Update(_fieldIdProtocolFlag, flag);
    }
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    double _stretchStepMicrons;
    double _maxTravelMicrons;
    double _maxForceAbsMicronewtons;
    double _targetTravelMicrons;
    double _holdingTimeS;
    double _relaxingTimeS;
    double _targetForceMicronewtons;
    double _forceMarginMicronewtons;
    double _stiffnessThreshold;
    double _defaultJawOpeningMicrons;
    double _zeroPosition;
    bool _findZeroPosition = false;
    

};

#endif	/* CREEPWITHCALIBRATION_HPP */

