/* 
 * File:   MultiStep.cpp
 * Author: cfmadmin
 * 
 * Created on June 14, 2013, 3:08 PM
 */

#include <morphorobotx/Robot.hpp>
#include <stdlib.h>
#include "MultiStep.hpp"
#include "StiffnessMonitor.hpp"

REGISTER_PROTOCOL("MultiStep", MultiStep);

MultiStep::MultiStep(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{
    _fieldIdProtocolFlag = this->experiment().stateManager().RegisterFieldExc(QString("ProtocolFlag"), QString("(flag)"), false);
    _fieldIdSequenceStep = this->experiment().stateManager().RegisterFieldExc(QString("SequenceStep"), QString(""), false);
    _steps.clear();    
}

MultiStep::~MultiStep() {
}

bool MultiStep::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    ForceSensor* myForceSensorRef = 0;
    if (_observeRefForce)
    {
        myForceSensorRef = (ForceSensor*)experiment().robot().sensor("IndentationForceSensor2");// get the second sensor by name
    }
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    if (_observeRefForce)
    {
        if (!myForceSensorRef)
        {
            printf("ERROR: indentationSensor2 is null\n");
            return false;   
        }
    }
    /*Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }*/
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }    
    const bool isPushingPositive = myForceSensor->isPushingPositive();    
    //StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    /*primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);*/
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    double indentationAxisPosition = 0.0;
    QTextStream in(stdin);
    QTextStream out(stdout);
    bool goodCommand = false;
    QString line = "";
    if (!_ignoreUserInput)
    {
        do 
        {    
            out << endl;
            out << "#########################################" << endl;
            out << "type: calibrate<ENTER> to calibrate zero force" << endl;
            out << "type: skip<ENTER> to skip calibration" << endl;

            line = in.readLine();
            if ((line.toLower() != "calibrate")&&(line.toLower() != "skip"))
            {
                out << "wrong command ! try again (calibrate or skip)" << endl;
            }
            else
            {
                goodCommand = true;
            }
        }while (!goodCommand);
    }
    else
    {
        line = "calibrate"; //in automated tests always calibrate
    }
    if (line.toLower() == "calibrate")
    {    
        out << "Make sure the force sensor is not loaded" << endl;
        out << "If necessary, move the robot manually to unload the sensor" << endl;
        out << "Press <ENTER> to confirm..." << endl;
        
        if (!_ignoreUserInput)
        {
                QString line = in.readLine();
        }
        /*how to make a memo file:*/    
        /*
        {
          QFile memoTxt;
          //memoTxt.setFileName(fileManager->getExperimentDirAbsolute() + "/info.txt");
          memoTxt.setFileName(experiment().fileManager().getExperimentDirAbsolute() + "/info.txt");
          QTextStream memoStream;

          if(!memoTxt.open(QIODevice::WriteOnly)) // **PBdR** You want to reset the file everytime or add at the end?
          {
            out << "Error, cannot open file '" << memoTxt.fileName() << "' for writing" << endl;
            return false;
          }
          //QTextStream textStream(&file);
          memoStream.setDevice(&memoTxt);
          memoStream << line << endl;   
        }
        */
        
        double d = 0.0;

        sm.state().Renew();   
        double forceMicronewtons = 0.0;
        myForceSensor->TareSensor(); //how to tare the sensor ?
        if (_observeRefForce)
        {
                myForceSensorRef->TareSensor();
        }
        UpdateProtocolFlag(ProtocolFlag::TareSensor);

        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        double initialIndentationPositionMicrons = indentationAxisPosition;
        _globalInitialPosition = initialIndentationPositionMicrons;
		//zero position not found, assume the current position to be the zero
		_zeroPosition = _globalInitialPosition;
        isOK &= sm.logger().WriteCurrentState();
        if (_findZeroPosition)
        {
            UpdateProtocolFlag(ProtocolFlag::AdjustingJaws);
            isOK &= sm.logger().WriteCurrentState(); 
            //here, close the jaws to find the zero position, if required by parameters
            //reset the coordinates        
            isOK &= GoDownUntilStiffness(indentationAxis, myForceSensor, isPushingPositive); //close the jaws       
            isOK &= GoUpUntilNoStiffness(indentationAxis, myForceSensor, isPushingPositive); //open the jaws a bit
            isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
            _zeroPosition = indentationAxisPosition;
            UpdateProtocolFlag(ProtocolFlag::ZeroPosition); //mark that this is zero position
            isOK &= sm.logger().WriteCurrentState(); 
            //here, reset the smaract
            //isOK &= indentationAxis->Set(AxisOption::STOP_NOW, 0);
            //isOK &= indentationAxis->Set(AxisOption::SET_ZERO_POSITION, 0);
            //if (!isOK)
            //{
            //      out << "Setting zero position failed..." << endl;  
            //}        
            UpdateProtocolFlag(ProtocolFlag::AdjustingJaws); //mark that this is zero position        
            isOK &= StepUp(indentationAxis, _defaultJawOpeningMicrons, isPushingPositive);
            isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
            isOK &= sm.logger().WriteCurrentState();                         
        }
        else
        {
			//here: read last calibration to retrieve zero position
			double offset = 0;
			double zeroPos = 0;
			isOK &= ReadLastCalibration(offset, zeroPos);
			if (!isOK)
			{
				printf("ERROR: ReadLastCalibration failed\n");
				return false;
			}
			_zeroPosition = zeroPos;
		}
        //else
        //{ //don't do it, the zero is not really zero.
        //   isOK &= indentationAxis->GotoSync(_defaultJawOpeningMicrons);
        //}        
        isOK &= WriteCalibration(myForceSensor->GetOffsetVoltage(), _zeroPosition);                        
    }
    else
    {
        //read zero position from file
        double sensorOffsetVoltage = 0.0;
        double zeroPositionMicrons = 0.0;
        isOK &= ReadLastCalibration(sensorOffsetVoltage, zeroPositionMicrons);
        if (!isOK)
        {
            printf("ERROR: ReadLastCalibration failed\n");
            return false;
        }
        else
        {
            myForceSensor->SetOffsetVoltage(sensorOffsetVoltage);
            _zeroPosition = zeroPositionMicrons;
            _globalInitialPosition = _zeroPosition;
        }
    }
    out << endl;
    out << "#########################################" << endl;
    out << "Now you can mount the plant" << endl;    
    out << "Press <ENTER> to start stretching ..." << endl;
    
    if (!_ignoreUserInput)
    {
        line = in.readLine();
    }
        
    UpdateProtocolFlag(ProtocolFlag::GoingToTargetForce1);
    isOK &= sm.logger().WriteCurrentState(); 
    
    for (int i=0; i<_steps.size(); i++)
    {
        //go through the steps, look at type and launch an action accordingly (goto force or goto position)
        out << "about to perform step " <<i<< ":"<<_steps.at(i).type<<", "<<_steps.at(i).value<<", "<<_steps.at(i).duration<<endl;
        UpdateSequenceStep(i);
        if (_steps.at(i).type == "F")
        {    
                isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, _steps.at(i).value, _steps.at(i).duration, ProtocolFlag::GoingToTargetForce1, ProtocolFlag::Holding1, _maxForceAbsMicronewtons);
        }
        else
        {
            if (_steps.at(i).type == "P")
            {
                isOK &= KeepPosition(indentationAxis, myForceSensor, isPushingPositive, _steps.at(i).value, _steps.at(i).duration, ProtocolFlag::GoingToTargetPosition1, ProtocolFlag::HoldingTargetPosition1, _maxForceAbsMicronewtons);
            }
            else
            {
                isOK = false;
                out<<"ERROR: unknown step type:"<<_steps.at(i).type<<endl;
                break;
            }
        }
    }
       
    /*
    out << "Pre-relaxing..." << endl;
    isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, 0.0, 10.0, ProtocolFlag::PreRelaxing, ProtocolFlag::PreRelaxing, 1000000.0);
    out << "Pre-relaxing done." << endl;
    out << "Stretching..." << endl;
    isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, _targetForceMicronewtons, _holdingTimeS, ProtocolFlag::GoingToTargetForce1, ProtocolFlag::Holding1, _maxForceAbsMicronewtons);
    out << "Relaxing..." << endl;
    isOK &= Regulate(indentationAxis, myForceSensor, isPushingPositive, 0.0, _relaxingTimeS, ProtocolFlag::GoingToTargetForce2, ProtocolFlag::Holding2, _maxForceAbsMicronewtons);
    */
    isOK &= sm.logger().WriteCurrentState();         
    
    out << endl;
    out << "#########################################" << endl;
    out << "Now, remove the plant" << endl;    
    out << "Make sure the sensor is not loaded" << endl;    
    out << "Press <ENTER> to confirm..." << endl;

    if (!_ignoreUserInput)
    {
        line = in.readLine();    
    }
    
    //here, measure the force with the same sensor offset as the whole experiment
    
    myForceSensor->TareSensor();
    UpdateProtocolFlag(ProtocolFlag::TareSensor);        
    isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);    
    isOK &= sm.logger().WriteCurrentState();     
    
    return isOK;
}

bool MultiStep::WriteCalibration(double sensorOffsetVoltage, double zeroPosition)
{
    //open file and append
    QFile calibrationTxt;
    //memoTxt.setFileName(fileManager->getExperimentDirAbsolute() + "/info.txt");
    _calibrationFilename = experiment().fileManager().getCurrentSessionPath()+"calibration.csv";
    calibrationTxt.setFileName(_calibrationFilename);
    QTextStream calibrationStream;

    if(!calibrationTxt.open(QIODevice::Append)) // **PBdR** You want to reset the file everytime or add at the end?
    {
      //out << "Error, cannot open file '" << calibrationTxt.fileName() << "' for writing" << endl;
      return false;
    }
    //QTextStream textStream(&file);
    calibrationStream.setDevice(&calibrationTxt);
    QString line = "";
    line  = line + QString("date:") + QString(",") + QString::number(sensorOffsetVoltage) + QString(",") + QString::number(zeroPosition);
    calibrationStream << line << endl;   
    calibrationStream.flush();    
    return true;
}

bool MultiStep::ReadLastCalibration(double &sensorOffsetVoltage, double &zeroPositionMicrons)
{
    //open file and append
    QFile calibrationTxt;
    //memoTxt.setFileName(fileManager->getExperimentDirAbsolute() + "/info.txt");
    _calibrationFilename = experiment().fileManager().getCurrentSessionPath()+"calibration.csv";
    calibrationTxt.setFileName(_calibrationFilename);
    QTextStream calibrationStream;

    if(!calibrationTxt.open(QIODevice::ReadOnly)) // **PBdR** You want to reset the file everytime or add at the end?
    {
      //out << "Error, cannot open file '" << calibrationTxt.fileName() << "' for writing" << endl;
      return false;
    }
    //QTextStream textStream(&file);    
    calibrationStream.setDevice(&calibrationTxt);
    QString line = "";
    QString prevLine = line;
    
    double offsetVoltage = 0.0;
    double zeroPosition = 0.0;
    
    bool keepReading = true;
    bool ok1 = false;
    bool ok2 = false;
    do 
    {
        line = calibrationStream.readLine();
        //printf("read: %s\n", line.toLocal8Bit().data());
        QStringList list = line.split(",");
        //printf("list count is: %d\n", list.count());
        //for (int i=0; i<list.count(); i++)
        //{
            //printf("-- %s\n", list.at(i).toLocal8Bit().data());
        //}
        if (list.count() == 3)
        {
                offsetVoltage = list.at(1).toDouble(&ok1);
                zeroPosition = list.at(2).toDouble(&ok2);        
        }
        else
        {
            ok1 = false;
            ok2 = false;
        }
        if (ok1 && ok2)
        {
            prevLine = line;
        }
        else
        {
            keepReading = false;
            line = prevLine; //not needed
        }       
    } while (keepReading);
    QStringList list = line.split(",");
    //printf("there are %d elements in the list \n", list.count());
    if (list.count() == 3)
    {
        offsetVoltage = list.at(1).toDouble(&ok1);
        zeroPosition = list.at(2).toDouble(&ok2);        
        if (ok1 && ok2)
        {
            sensorOffsetVoltage = offsetVoltage;
            zeroPositionMicrons = zeroPosition;            
        }
    } 
    else
    {
        return false;    
    }
    
    return true;
}



bool MultiStep::Regulate(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive, double targetForceMicronewtons, double timeoutSeconds, ProtocolFlag flagGoingTo, ProtocolFlag flagHolding, double maxForceMicronewtons)
{
    bool isOK = true;
    bool keepGoing = true;
    double indentationAxisPosition;
    double step = 0.0;
    double initialIndentationPositionMicrons;
    double forceMicronewtons;
    double forceError=0.0;
    double avgForceError = 0.0;
    bool perfectionistMode = true; //use narrow tolerance
    Timer timer = Timer();
    timer.Reset();
    StateManager& sm = this->experiment().stateManager();
    
    int phase = 1;    
    const double alpha = 20.0;
    isOK &= indentationAxis->GetPositionSync(initialIndentationPositionMicrons);
    ForceSensor* myForceSensorRef = 0;
    if (_observeRefForce)
    {
        myForceSensorRef = (ForceSensor*)experiment().robot().sensor("IndentationForceSensor2");// get the second sensor by name
    }
    _forceMarginMicronewtons = _forceMarginSmallMicronewtons; //at first, adjust exactely
    perfectionistMode = true;
    while(keepGoing)
    {   
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        forceMicronewtons = forceSensor->GetForce(); 
        if (_observeRefForce)
        {
                myForceSensorRef->GetForce();
        }
        forceError = forceMicronewtons - targetForceMicronewtons;
        avgForceError = 0.8*avgForceError + 0.2*forceError;
        if ((forceMicronewtons >= targetForceMicronewtons - _forceMarginMicronewtons)
           && 
           (forceMicronewtons <= targetForceMicronewtons + _forceMarginMicronewtons))
        {   
            if (perfectionistMode) //switch back to tolerant mode
            {
                perfectionistMode = false;
                _forceMarginMicronewtons = _forceMarginLargeMicronewtons;
            }                        
            if (phase == 1)
            {
               phase = 2; //holding                   
               timer.Reset();
            }            
        }
        else //if outside of tolerance
        {
            if (!perfectionistMode) //if outside of tolerance and not perfectionist, then it's time to become stricter
            {
                perfectionistMode = true;
                _forceMarginMicronewtons = _forceMarginSmallMicronewtons;
            }
        }
        //determine step size:
        if (phase == 2)
        {
            step = _stretchStepMicrons;
            if (fabs(avgForceError) < fabs(_forceMarginMicronewtons*(alpha-1)))
            {
                //step = _stretchStepMicrons*(fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha) -fabs(_forceMarginMicronewtons));
                step = _stretchStepMicrons*(fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha) );
                if (step < 0.0) {
                    step = 0;
                }
            }
        }
        else
        {
            step = _stretchStepMicrons;
            //if (abs(avgForceError) < abs(_forceMarginMicronewtons*10))
            //{
            //step = _stretchStepMicrons/3;
            //}
            if (fabs(avgForceError) < fabs(_forceMarginMicronewtons*(alpha-1)))
            {
                //step = _stretchStepMicrons*( fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha)  -fabs(_forceMarginMicronewtons));
                step = _stretchStepMicrons*( fabs(avgForceError)/fabs(_forceMarginMicronewtons*alpha) );
                if (step < 0.0)
                {
                    step = 0;
                }
            }
        }
        if (forceMicronewtons < targetForceMicronewtons - _forceMarginMicronewtons)
        {
            isOK &= StepDown(indentationAxis, step, isPushingPositive);
        }        
        if (forceMicronewtons > targetForceMicronewtons + _forceMarginMicronewtons)
        {          
            isOK &= StepUp(indentationAxis, step, isPushingPositive);            
        }   
        if (timer.TimeElapsedMs() > timeoutSeconds*1000)
        {             
            if (phase == 2)
            {
		printf("timer elapsed\n");
                keepGoing = false;
            }                  
            else
            {
                timer.Reset();
            }            
        }      
        if (fabs(forceMicronewtons) > fabs(maxForceMicronewtons))
        {
            //out << "Max force exceeded, aborting..." << endl;  
	    printf("max force exceeded\n");  
            keepGoing = false;
        }
//        if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        if (fabs(indentationAxisPosition - _zeroPosition) > _maxTravelMicrons) //zero position is the "contact point" when both jaw touch
        {
            //out << "Max travel exceeded, aborting..." << endl;    
	    printf("max travel exceeded\n");
            keepGoing = false;
        }        
        if (phase == 1)
        {
            UpdateProtocolFlag(flagGoingTo);
            printf("going to force: %f, \t force: %f, \t timer: %f \t phase:%d \t step:%f\n", targetForceMicronewtons, forceMicronewtons, timer.TimeElapsedMs()/1000.0, phase, step);
            timer.Reset();
        }        
        if (phase == 2)
        {
            UpdateProtocolFlag(flagHolding);
            printf("holding force: %f, \t force: %f, \t timer: %f \t phase:%d \t step:%f\n", targetForceMicronewtons, forceMicronewtons, timer.TimeElapsedMs()/1000.0, phase, step);
        }        
        isOK &= sm.logger().WriteCurrentState();      
        if (!isOK)
        {
		printf("something went wrong, aborting\n");			
            keepGoing = false;
        }
    }    
    return isOK;
}

bool MultiStep::KeepPosition(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive, double relativePositionMicrons, double timeoutSeconds, ProtocolFlag flagGoingTo, ProtocolFlag flagHolding, double maxForceMicronewtons)
{
    bool isOK = true;
    bool keepGoing = true;
    double indentationAxisPosition;
    double step = 0.0;
    double initialIndentationPositionMicrons;
    double forceMicronewtons;
    double forceError=0.0;
    double avgForceError = 0.0;
    double posError = 0.0;
    double targetPos = 0.0;
    double sign = 0.0;
    bool result = false;
    
    Timer timer = Timer();
    timer.Reset();
    StateManager& sm = this->experiment().stateManager();
    
    int phase = 1;        
    isOK &= indentationAxis->GetPositionSync(initialIndentationPositionMicrons);
    
    ForceSensor* myForceSensorRef = 0;
    if (_observeRefForce)
    {
        myForceSensorRef = (ForceSensor*)experiment().robot().sensor("IndentationForceSensor2");// get the second sensor by name
    }
    
    printf("started...\n");
    while(keepGoing)
    {           
        isOK &= indentationAxis->GetPositionSync(indentationAxisPosition);
        if ( fabs(indentationAxisPosition - (initialIndentationPositionMicrons + relativePositionMicrons)) > _keepPositionToleranceMicrons) //10nm tolerance
        {
            printf("position correction...\n");
            //printf("abs(posError) = %f\n", fabs(indentationAxisPosition - (initialIndentationPositionMicrons + relativePositionMicrons)));
                posError = indentationAxisPosition - (initialIndentationPositionMicrons + relativePositionMicrons);
                if (fabs(posError) > fabs(_stretchStepMicrons))
                {                    
                    if (posError >= 0.0)
                    {
                        sign = 1.0;                        
                    }
                    else
                    {
                        sign = -1.0;
                    }
                    targetPos = indentationAxisPosition - sign*fabs(_stretchStepMicrons);
                }
                else
                {
                    targetPos = initialIndentationPositionMicrons + relativePositionMicrons;
                }                
                result = indentationAxis->GotoSync(targetPos);
                if (!result)
                {
                        isOK &= indentationAxis->GotoSync(targetPos);
                }
        }
        else
        {
            if (phase == 1) //we count time for the first time position enters the target range
            {
                printf("holding begun...\n");
                timer.Reset(); 
            }
            phase = 2;
        }
        forceMicronewtons = forceSensor->GetForce(); 
        if (_observeRefForce)
        {
                myForceSensorRef->GetForce();
        }   
        if (timer.TimeElapsedMs() > timeoutSeconds*1000)
        {             
            if (phase == 2)
            {
                keepGoing = false;
            }                  
            else
            {
                timer.Reset();
            }            
        }      
        
        if (fabs(forceMicronewtons) > fabs(maxForceMicronewtons))
        {
            //out << "Max force exceeded, aborting..." << endl;    
            printf("Max force exceeded, aborting...\n");
            keepGoing = false;
        }
        //if (fabs(indentationAxisPosition-initialIndentationPositionMicrons) > _maxTravelMicrons)
        if (fabs(indentationAxisPosition - _zeroPosition) > _maxTravelMicrons) //zero position is the point where the jaws touch
        {
            //out << "Max travel exceeded, aborting..." << endl;
            printf("Max travel exceeded, aborting...\n");
            keepGoing = false;
        }        
        if (phase == 1)
        {
            UpdateProtocolFlag(flagGoingTo);
            printf("going to position: %f, \t force: %f, \t timer: %f \t phase:%d \t step:%f\n", relativePositionMicrons, forceMicronewtons, timer.TimeElapsedMs()/1000.0, phase, step);
            timer.Reset();
        }        
        if (phase == 2)
        {
            UpdateProtocolFlag(flagHolding);
            printf("holding position delta: %f, \t force: %f, \t timer: %f \t phase:%d \t step:%f\n", relativePositionMicrons, forceMicronewtons, timer.TimeElapsedMs()/1000.0, phase, step);
        }        
        isOK &= sm.logger().WriteCurrentState();      
        if (!isOK)
        {
            keepGoing = false;
        }
    }    
    return isOK;
}


bool MultiStep::GoDownUntilStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;
    bool feelingStiffness = false;    
    //TODO: observe max force
    //TODO: observe max travel  
    do
    {
        isOK &= StepDown(indentationAxis, _calibrationStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();    //   printf("stiffness = %f\n", stiffness);
        //UpdateFlag(3); //like the old coarse approach
        if (stiffness > _stiffnessThreshold)
        {            
            feelingStiffness = true;            
        }       
        sm.logger().WriteCurrentState();        
    }while(!feelingStiffness);
    //TODO: check errors    
    return isOK;
 }

bool MultiStep::GoUpUntilNoStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive)
 {
    bool isOK = true;
    double positionMicrons = 0.0;    
    StateManager& sm = this->experiment().stateManager();
    StiffnessMonitor stiffnessMonitor(10, isPushingPositive);    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }    
    double forceMicronewtons = 0.0;
    forceMicronewtons = forceSensor->GetForce();
    stiffnessMonitor.Reset();
    stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);    
    double stiffness = 0.0;
    bool feelingStiffness = true;
    do
    {
        isOK &= StepUp(indentationAxis, _stretchStepMicrons, isPushingPositive);
        isOK &= indentationAxis->GetPositionSync(positionMicrons);
        forceMicronewtons = forceSensor->GetForce();
        stiffnessMonitor.UpdateStiffness(positionMicrons, forceMicronewtons);
        stiffness = stiffnessMonitor.Stiffness();  //printf("stiffness = %f\n", stiffness);
        if (stiffness < _stiffnessThreshold)
        {       /*if (stiffness > 0.0) //otherwise, negative stiffness might indicate water meniscus broke and nothing is sure anymore
                {*///in other words, if you experience meniscus rupture, don't assume the tip is out of contact with the sample, keep moving up
            feelingStiffness = false;              //}
        }
        //UpdateFlag(3); //like the old coarse approach
        sm.logger().WriteCurrentState();        
    }while(feelingStiffness);
    //TODO: check errors
    //TODO: maybe go up a little bit more ?
    return isOK;
 }

bool MultiStep::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    isOK &= parms(_sectionName, "IgnoreUserInput", _ignoreUserInput);
    
    isOK &= parms(_sectionName, "StretchStepMicrons", _stretchStepMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _stretchStepMicrons);        
    
    isOK &= parms(_sectionName, "CalibrationStepMicrons", _calibrationStepMicrons);
    
    isOK &= parms(_sectionName, "MaxTravelMicrons", _maxTravelMicrons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    isOK &= parms(_sectionName, "MaxForceAbsMicronewtons", _maxForceAbsMicronewtons);
    //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "TargetTravelMicrons", _targetTravelMicrons);
   //printf("Indentation::readParameters, _indentationStepMicrons = %f\n", _maxTravelMicrons);        
    
    //isOK &= parms(_sectionName, "HoldingTimeSeconds", _holdingTimeS);
   
    //isOK &= parms(_sectionName, "RelaxingTimeSeconds", _relaxingTimeS);
    
    //isOK &= parms(_sectionName, "TargetForceMicronewtons", _targetForceMicronewtons);
    
    isOK &= parms(_sectionName, "ForceMarginLargeMicronewtons", _forceMarginLargeMicronewtons);
    isOK &= parms(_sectionName, "ForceMarginSmallMicronewtons", _forceMarginSmallMicronewtons);
    isOK &= parms(_sectionName, "KeepPositionToleranceMicrons", _keepPositionToleranceMicrons);
    
    isOK &= parms(_sectionName, "FindZeroPosition", _findZeroPosition);
    
    isOK &= parms(_sectionName, "ObserveReferenceForce", _observeRefForce);
    
    isOK &= parms(_sectionName, "StiffnessThreshold", _stiffnessThreshold);
    
    isOK &= parms(_sectionName, "DefaultJawOpening", _defaultJawOpeningMicrons);
    
    QString _stepSequenceName;
    isOK &= parms(_sectionName, "StepSequence", _stepSequenceName);
    
    QStringList _stepSequenceSteps;        
    isOK &= parms.all(_stepSequenceName, "Step", _stepSequenceSteps);
    
    for (int i=0; i<_stepSequenceSteps.count(); i++)
    {
        //printf("%s\n", _forceSequenceSteps.at(i).toLocal8Bit().data());
        QStringList tokens = _stepSequenceSteps.at(i).split(","); //QRegExp("\\s+")
        if (tokens.count()!=3)
        {
            printf("ERROR: The should be 3 entries in a step (parameters.ini->[MultiStep].StepSequence)\n");
            isOK = false;
            return isOK;
        }
        step st;
        QString type = "";
        for (int j=0; j<tokens.count(); j++)
        {
            //printf("%s\n", tokens.at(j).trimmed().toLocal8Bit().data());
            int tmp = 0;            
            int tmpDouble = 0.0;
            bool ok = true;            
            switch(j)
            {
                case 0:
                    type = tokens.at(j).trimmed();                    
                    if (ok)
                    {
                        st.type = type;                        
                    }
                    else
                    {
                        printf("ERROR: Step type to be a string: P for position or F for force\n");
                        isOK = false;
                        return isOK;
                    }
                    break;
                case 1:
                    tmpDouble = tokens.at(j).trimmed().toDouble(&ok);
                    if (ok)
                    {
                        st.value = tmpDouble;                        
                    }
                    else
                    {
                        printf("ERROR: Step target value needs to be a double number (double in floating point sense)\n");
                        isOK = false;
                        return isOK;
                    }
                    break;
                case 2:
                    tmpDouble = tokens.at(j).trimmed().toDouble(&ok);
                    if (ok)
                    {                        
                        st.duration = tmpDouble;
                    }
                    else
                    {
                        printf("ERROR: Step holding time needs to be a double number (double in floating point sense)\n");
                        isOK = false;
                        return isOK;
                    }                    
                    break;                    
            }
                  
        }
        _steps.push_back(st);      
    }    
    return isOK;
}

bool MultiStep::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

//TODO: consider if this could be a method of Axis ?
bool MultiStep::StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
	printf("ERROR: GetPositionSync failed to receive position from the robot\n");
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons); 
    if(!isOK)
    {
	printf("ERROR: GotoSync failed\n");
    }       
    return isOK;            
}

bool MultiStep::StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive)
{
   // printf("PointScan::StepDown by :%f microns\n", stepAbsMicrons);
    double positionMicrons = 0.0;    
    bool isOK = true;    
    isOK &= indentationAxis->GetPositionSync(positionMicrons);
    if(!isOK)
    {
	printf("ERROR: GetPositionSync failed to receive position from the robot\n");
        return false;
    }
    if (isPushingPositive)
    {
        positionMicrons += fabs(stepAbsMicrons);
    }
    else
    {
        positionMicrons -= fabs(stepAbsMicrons);
    }
    isOK &= indentationAxis->GotoSync(positionMicrons);
    if(!isOK)
    {
	printf("ERROR: GotoSync failed\n");
    }
    return isOK;            
}
