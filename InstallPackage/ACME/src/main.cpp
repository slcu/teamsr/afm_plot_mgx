/* 
 * File:   main.cpp
 * Author: huflejt
 *
 * Created on September 18, 2012, 3:40 PM
 */

//#include <cstdlib>
//#include <cstdio>

#include "parms.hpp"
//#include "TEFRobot.hpp"
#include "FileManager.hpp"
#include "ForceSensor.hpp"
#include "ForceMeasurement.hpp"
#include "MeasurementState.hpp"
#include "DataLogger.hpp"
//#include "BasicTEFIndentation.hpp"
#include "Experiment.hpp"
//#include "SmarActActuatorSystem.hpp"
#include "HardwareManager.hpp"
#include "Robot.hpp"

//using namespace std;

/*
 * 
 */
int main(int argc, char* argv[]) {
        
    Experiment experiment;

    bool isOK = true;
    isOK &= experiment.Initialize();
    if (isOK)
    {
        isOK &= experiment.Run();
        if (!isOK)
        {
            printf("ERROR Experiment.Run() failed\n");
        }
    }
    else
    {
        printf("ERROR: Experiment.Initialize() failed\n");
    }
        
    return (isOK ? 0 : 1);  
       
}

