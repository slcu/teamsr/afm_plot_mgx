/* 
 * File:   StiffnessMonitor.hpp
 * Author: cfmadmin
 *
 * Created on March 26, 2013, 7:05 PM
 */

#ifndef STIFFNESSMONITOR_HPP
#define	STIFFNESSMONITOR_HPP

#include "vector"
#include <limits>
#include <cmath>
#include "stdio.h"


class StiffnessMonitor {
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    StiffnessMonitor(unsigned int maxElem, bool pushingIsPositive);
    
    //StiffnessMonitor(const StiffnessMonitor& orig);
    virtual ~StiffnessMonitor();
    
    void Reset()
    {
        _positions.clear();
        _forces.clear();
    }
    void UpdateStiffness(double positionMicrons, double forceMicronewtons)
    {
        //printf("updating stiffness...: positionMicrons= %f \t forceMicronewtons = %f\n", positionMicrons, forceMicronewtons);
        _positions.push_back(positionMicrons);
        _forces.push_back(forceMicronewtons);
        if (_forces.size() > maxElem)
        {
            _forces.erase(_forces.begin());
            _positions.erase(_positions.begin());
        }
    }
    double Stiffness()
    {
        double s = 0.0;        
        if (_positions.size() > 1)
        {
            double d = _positions[_positions.size()-1] - _positions[_positions.size()-2];        
            //printf("d = %f\t", d);
            if (fabs(d) > 0.020) //20 nanometers is really small
            {   
                s = (_forces[_forces.size()-1]-_forces[_forces.size()-2]) / d;            
                if (!pushingIsPositive)
                {
                    s = -1.0*s;
                }                        
            }
            else
            {                
                printf("ERROR: fabs(d) <= 0.020 microns\n");
                s = double_nan;
            }
        }    
        else
        {
            printf("ERROR: _positions.size <= 1\n");
            s = double_nan;
        }       
        return s;
    }    
    
protected:
    unsigned int maxElem;
    bool pushingIsPositive;
    std::vector<double> _positions;    
    std::vector<double> _forces;
};

#endif	/* STIFFNESSMONITOR_HPP */

