/* 
 * File:   ForceReading.cpp
 * Author: cfmadmin
 * 
 * Created on July 3, 2013, 5:24 PM
 */

#include <morphorobotx/Robot.hpp>
#include <stdlib.h>
#include "ForceReading.hpp"

REGISTER_PROTOCOL("ForceReading", ForceReading);

ForceReading::ForceReading(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{
    
}



ForceReading::~ForceReading() {
}

bool ForceReading::run()
{
    bool isOK = true;
    StateManager &sm = experiment().stateManager(); //use sm later on
    ForceSensor* forceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!forceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }
    QTextStream in(stdin);
    QTextStream out(stdout);
    
    out << endl;
    out << "#########################################" << endl;
    out << "Make sure the force sensor is not loaded" << endl;
    
    out << "Press <ENTER> to confirm..." << endl;

    QString line = in.readLine();
    double forceMicronewtons = 0.0;
    double forceVoltage = 0.0;
    //forceSensor->TareSensor(); //how to tare the sensor ?
    double forceVoltageIIR = 0.0;
    
    while (1)
    {
        //forceMicronewtons = forceSensor->GetForce();
        forceVoltage = forceSensor->GetVoltage();
        forceVoltageIIR = 0.9*forceVoltageIIR + 0.1*forceVoltage;
        
        out << "Voltage: " << QString::number(forceVoltage, 'f', 10) <<  "V"<< "IIR: " << QString::number(forceVoltageIIR, 'f', 10) <<  "V"<<endl;
        
    }
    
    
    
}

bool ForceReading::writeParameters(Parms& parms) const
{
    bool isOK = true;    
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}

bool ForceReading::Init(const Parms& parms)
{
    bool isOK = true;
    
    return isOK;
}