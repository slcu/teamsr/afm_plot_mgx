/* 
 * File:   PointScan.hpp
 * Author: cfmadmin
 *
 * Created on February 18, 2013, 7:40 PM
 */

#ifndef POINTSCAN_HPP
#define	POINTSCAN_HPP

//#include "Factory.hpp"
//#include "ForceMeasurement.hpp"
//#include "Robot.hpp"

#include "Experiment.hpp"
#include "CandidateList.hpp"
//class Experiment;
class Axis;
class CircularSummingBuffer;

class PointScan : public Protocol{
public:
    
    PointScan(Experiment& experiment, QString sectionName);
    /*PointScan(const PointScan& orig);*/
    virtual ~PointScan();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;
    
    int fieldIDPointScanContactPosition()
    {
        return _fieldIDPointScanContactPosition;
    }    
            
protected:
    
    bool StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool GoUpUntilNoStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive);
    bool GoDownUntilStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive);
    bool CoarseApproach(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive); //the old way of doing coarse approach
                
    void UpdateFlag(unsigned int flag);
    void InvalidateFlag();
    
    //ForceMeasurement forceMeasurement;
    //Robot _robot;
    //HardwareManager& _hardwareManager; //TODO: this should be a reference to the central one (global / singleton ?)
    int _fieldIDPointScanFlag;
    int _fieldIDPointScanWiggleID;
    int _fieldIDPointScanContactPosition;
    
    int _maxBadWigglesPerPoint;
    int _maxBadWigglesPerBuffer;
    int _badWiggleBufferSize;
    int _loadingCycles;
    double _coarseApproachStepMicrons;
    double _waterStiffnessThreshold;
    double _sampleStiffnessThreshold;
    double _forceDriftThreshold;
    QString _indentationForceSensorName;
    QString _indentationAxisName;
    QString _indentationProtocolName;        
    
    CircularSummingBuffer _badWiggleBuffer; //TODO: try to parametrize the capacity, or allow flexible capacity
    
    std::shared_ptr<Protocol> indentationPtr;
    
};

#endif	/* POINTSCAN_HPP */

