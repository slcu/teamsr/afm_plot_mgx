/* 
 * File:   GridScan.cpp
 * Author: cfmadmin
 * 
 * Created on February 8, 2013, 9:40 PM
 */

#include <morphorobotx/Robot.hpp>

#include <stdlib.h>

#include "GridScan.hpp"
#include "PointScan.hpp"
#include "QProcess"


REGISTER_PROTOCOL("GridScan", GridScan);

GridScan::GridScan(Experiment& experiment, QString sectionName): Protocol(experiment, sectionName) 
{        
    //_fieldIdProtocolFlag = this->experiment().stateManager().RegisterFieldExc(QString("ProtocolFlag"), QString("(enum)"), false);        
    _fieldIdGridPointId = this->experiment().stateManager().RegisterFieldExc(QString("GridPointId"), QString("(enum)"), false);        
}

GridScan::~GridScan() {
}


bool GridScan::readPoiFile(std::vector<double>& poiX, std::vector<double>& poiY, QString poiFileName)
{
    bool isOK = true;
    Parms poiParms(poiFileName);
    
    QStringList pointStringList;
    isOK &= poiParms.all("PointsOfInterest", "point", pointStringList);
    foreach (QString pointString, pointStringList)
    {
        QStringList splittedCoordinates = pointString.split(",");
        if (splittedCoordinates.size() == 2)
        {            
            QString xString = splittedCoordinates.at(0);
            QString yString = splittedCoordinates.at(1);
            double xCoord = xString.toDouble(&isOK);
            double yCoord = yString.toDouble(&isOK);
            if (isOK)
            {
                poiX.push_back(xCoord);
                poiY.push_back(yCoord);
            }
            else
            {
                break;
            }
        }
        else            
        {
            break;
        }
    }        
    
    return isOK;
}

bool GridScan::generateCoordinates(std::vector<double>& gridCoordsX, std::vector<double>& gridCoordsY, QString poiFileName)
{
    bool isOK = true;
    int i=0;
    int j=0;
    int x=0;
    int y=0;
    int npointsx = _GridPointsPrimary;
    int npointsy = _GridPointsSecondary;
    double gridStep = _GridStepMicrons;        
    
    double gridMiddleX = 0.0;
    double gridMiddleY = 0.0;
    
    double gridStartX = 0.0;
    double gridStartY = 0.0;            
    
    std::vector<double> poiX;
    std::vector<double> poiY;
    isOK &= readPoiFile(poiX, poiY, poiFileName);
    
    int npois = poiX.size();
    for (int poiId=0; poiId < npois; poiId++)
    {
        gridMiddleX = poiX[poiId];
        gridMiddleY = poiY[poiId];
        
        gridStartX = gridMiddleX - npointsx*gridStep/2; //corner coordinate from middle coordinate and grid size
        gridStartY = gridMiddleY - npointsy*gridStep/2; //corner coordinate from middle coordinate and grid size

        for (i=0; i<npointsx; i++)
        {
            x = i*gridStep;
            for (j=0; j<npointsy; j++)
            {
                if (i%2 == 0)
                {
                    y=j*gridStep;
                }
                else
                {
                    y=gridStep*( (npointsy-1) -j );
                }
                gridCoordsX.push_back(x+gridStartX);
                gridCoordsY.push_back(y+gridStartY);
            }
        }
    }
    gnuplotGridPoints(gridCoordsX, gridCoordsY, "grid.png");
    return isOK;
}

bool GridScan::gnuplotGridPoints(std::vector<double>& gridCoordsX, std::vector<double>& gridCoordsY, QString filename)
{    
/*    QString cmd = "set term png medium; set style line 1 lt 3 pt 4;";
    cmd += "set output \"" + filename + "\";";
    cmd += "set title \"Planned grid points\"; set grid; set pointsize; ";
    cmd += "plot '-';";
    int npoints = gridCoordsX.size();
    for (int i=0; i<npoints; i++)
    {
        cmd += QString::number(gridCoordsX.at(i)) + " " + QString::number(gridCoordsY.at(i)) + "\n";
    }                
    cmd +="EOF\n";
    cmd+= "exit";
  */  
    /*printf("cmd = %s\n", cmd.toLocal8Bit().data());
    system(cmd.toLocal8Bit().data());
    system(filename.toLocal8Bit().data());*/
    QProcess gnuplot;
    QStringList commands;
    
    //commands << 
    commands << "set term png size 1600,1200\n";
    commands << "set style line 1 lt 3 pt 4\n";
    commands << "set output \"" + filename + "\"\n";
    commands << "set title \"Planned grid points\"\n";
    commands << "set grid\n";
    commands << "set pointsize\n";
    commands << "plot '-'\n";
    int npoints = gridCoordsX.size();
    for (int i=0; i<npoints; i++)
    {
        commands << QString::number(gridCoordsX.at(i)) + " " + QString::number(gridCoordsY.at(i)) + "\n";
    }                
    commands << "EOF\n";            
    
    commands << "quit\n";
    gnuplot.start("gnuplot", QIODevice::ReadWrite);
    foreach (QString cm, commands)
    {
        gnuplot.write(cm.toUtf8());
    }    
    gnuplot.waitForFinished();               
    
    QProcess eog;
    eog.start("eog " + filename);
    //eog.waitForFinished()        
    return false;
}

bool GridScan::run()
{
    //return false;//TODO: remove
    printf("GridScan::run() starts\n");
    bool isOK = true;
  //printf("from the point of view of the protocol:\n");
  /*printf("%s\n", experiment().robot().GetAxisName(0).toLocal8Bit().data());
    printf("%s\n", experiment().robot().GetAxisName(1).toLocal8Bit().data());
    printf("%s\n", experiment().robot().GetAxisName(2).toLocal8Bit().data());
    printf("%s\n", experiment().robot().GetAxisName(3).toLocal8Bit().data());    
    */
    
    std::vector<double> gridCoordsX;
    std::vector<double> gridCoordsY;
    
    isOK &= generateCoordinates(gridCoordsX, gridCoordsY, "poi.ini");
    if (!isOK)
    {
        printf("could not generateCoordinates\n");
        return false;
    }
        
    StateManager &sm = experiment().stateManager(); //use sm later on
        
    //ForceSensor * myForceSensor = (ForceSensor*)this->_experiment.robot().GetSensorPtrByName("LoadCellFutek89844");
    //ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().sensor(_indentationForceSensorName);   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    ForceSensor* myForceSensor = (ForceSensor*)experiment().robot().indentationSensor();   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor)
    {
        printf("ERROR: indentationSensor is null\n");
        return false;   
    }       
    //TODO: remove !
    /*ForceSensor* myForceSensor0 = (ForceSensor*)experiment().robot().GetSensorPtrByName("ForceSensorChannel0");   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor0)
    {    
        printf("ERROR: sensor not found by name\n");
        return false;
    }    
    ForceSensor* myForceSensor15 = (ForceSensor*)experiment().robot().GetSensorPtrByName("ForceSensorChannel15");   //TODO: consider it this should be global (for example: experiment.IndentationAxis() )
    if (!myForceSensor15)
    {    
        printf("ERROR: sensor not found by name\n");
        return false;
    }*/        
    
    Axis* primaryAxis = this->experiment().robot().axis(_primaryAxisSectionName);
    if (!primaryAxis)
    {
        printf("ERROR: primaryAxis is null\n");
        return false;
    }
    Axis* secondaryAxis = this->experiment().robot().axis(_secondaryAxisSectionName);
    if (!secondaryAxis)
    {
        printf("ERROR: secondaryAxis is null\n");
        return false;
    }
    Axis* indentationAxis = this->experiment().robot().axis(myForceSensor->AxisName()); //The sensor knows to which axis it is mounted to, this is your indentation axis
    if (!indentationAxis)
    {
        printf("ERROR: indentationAxis is null\n");
        return false;
    }        
    
    printf("primaryAxis :%s\n", _primaryAxisSectionName.toLocal8Bit().data());      
    printf("secondaryAxis :%s\n", _secondaryAxisSectionName.toLocal8Bit().data());      
    printf("indentationAxis :%s\n", indentationAxis->Name().toLocal8Bit().data());      
    
    primaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    primaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    primaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    secondaryAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    secondaryAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    secondaryAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);
    
    indentationAxis->Set(AxisOption::GOTO_PRECISION_MARGIN_MICRONS, 0.01);
    indentationAxis->Set(AxisOption::FREQUENCY_LIMIT_HZ, 18500);
    indentationAxis->Set(AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND, 100000000);                       

    /*
    double v1 = 0.0;
    double v2 = 0.0;
    for (i=0; i<1000; i++)
    {   sm.state().Renew();
        v1 = myForceSensor0->GetForce();
        v2 = myForceSensor15->GetForce();
        printf("force1 = %f,\tforce2 = %f\n", v1, v2);
        sm.logger().WriteCurrentState();
    }  
    return false; //TODO: remove !
    */
    double primaryAxisPosition = 0.0;
    double secondaryAxisPosition = 0.0;
    double indentationAxisPosition = 0.0;    
    double previousSurfacePosition = double_nan;
    //sm.state().Get( pointActionPtr.fieldIDPointScanContactPosition() );
    
    std::shared_ptr<Protocol> pointActionPtr = factory().protocol(experiment(), _pointActionProtocolName);
    
    for (int i=0; i<gridCoordsX.size(); i++)
    {
        sm.state().Renew();   
        sm.state().Update(_fieldIdGridPointId, i);
        int x = gridCoordsX[i];
        int y = gridCoordsY[i];        
        primaryAxis->GetPositionSync(primaryAxisPosition);
        secondaryAxis->GetPositionSync(secondaryAxisPosition);
        double d = (primaryAxisPosition-x)*(primaryAxisPosition-x) + (secondaryAxisPosition-y)*(secondaryAxisPosition-y);
        d = sqrt(d);        
        indentationAxis->GetPositionSync(indentationAxisPosition);
        double slopeCorrectionTerm = 0.0;        
        /*if (!double.isnan( lastSurfacePosition ))
        {
            if (myForceSensor->isPushingPositive())
            {
                slopeCorrectionTerm = lastSurfacePosition - previousSurfacePosition;             
            }
            else
            {
                slopeCorrectionTerm = lastSurfacePosition - previousSurfacePosition;
            }
        }*/
        //TODO: determine slopeCorrectionTerm
        d = d+slopeCorrectionTerm;        
        printf("going up before going to next X-Y point...\n");
        if (myForceSensor->isPushingPositive())
        {   
            indentationAxis->GotoSync(indentationAxisPosition - d); //TODO: use poking versor
        }
        else
        {   
            indentationAxis->GotoSync(indentationAxisPosition + d); //TODO: use poking versor
        }                 
                        
        primaryAxis->GotoSync(x);
        secondaryAxis->GotoSync(y);
        isOK &= sm.logger().WriteCurrentState();        
        if (!isOK)
        {
            printf("could not WriteCurrentState() \n");
        }
        if(isOK) printf("before point action: isOK is true\n");
        if(!isOK) printf("before point action: isOK is false\n");
        isOK &= pointActionPtr->run();        
        if(isOK) printf("after point action: isOK is true\n");
        if(!isOK) printf("after point action: isOK is false\n");
        if (!isOK)
        {
            printf("something went wrong, going up, out of the water...\n");
            //TODO: go up and break;
            indentationAxis->GetPositionSync(indentationAxisPosition);
            if (myForceSensor->isPushingPositive())
            {   
                indentationAxis->GotoSync(indentationAxisPosition - 1000000); //TODO: use poking versor
            }
            else
            {   
                indentationAxis->GotoSync(indentationAxisPosition + 1000000); //TODO: use poking versor
            }
            isOK &= sm.logger().WriteCurrentState();        
            break;
        }        
    }          
    printf("GridScan::run() ends\n");
    return isOK;//TODO: returning false causes the whole program to stop. Return true to acknowledge that everything is OK
}

bool GridScan::Init(const Parms& parms)
{
    bool isOK = true;
    
    if (experiment().robot().indentationAxis() == 0)
    {
        printf("ERROR: robot does not have indentation axis\n");
        return false;
    }
    //TODO: parse relevant parameters:    
    //isOK &= parms("ExperimentSettings", "Travel", experimentTravel, 0.);
    //isOK &= parms("ExperimentSettings", "MaxForce", experimentMaxForce, 0.);
    //isOK &= parms("ExperimentSettings", "IndentationStep", experimentStep, 0.);
    //isOK &= parms("RobotSettings", "MicrostepSize", microstepSize, 0.);
    //isOK &= parms("SensorSettings", "Gain", sensorGain, 0.);
    //isOK &= parms("SensorSettings", "MaxForce", sensorMaxForce, 0.);
    //isOK &= parms("SensorSettings", "IntegrationTime", integrationTimeMs, 10);
    //isOK &= parms("RobotSettings", "RobotAcceleration", robotAcceleration, 2);
    //isOK &= fileManager->readParameters(parms);//TODO: are you sure ? This is already done in Experiment.Initialize()    
    ///TODO: get protocol name (section name) and parse further params
    //without protocol name it is not possible to read from the right section    
    isOK &= parms(_sectionName, "PointAction", _pointActionProtocolName); //get the section name of the point action sub protocol
    //isOK &= parms(_sectionName, "IndentationForceSensor", _indentationForceSensorName); //get the section name of the point action sub protocol       
    //isOK &= parms(_sectionName, "IndentationAxis", _indentationAxisSectionName);
    //isOK &= parms(_sectionName, "PrimaryAxis", _primaryAxisSectionName);
    //isOK &= parms(_sectionName, "SecondaryAxis", _secondaryAxisSectionName);
    isOK &= parms(_sectionName, "GridPointsPrimary", _GridPointsPrimary);
    isOK &= parms(_sectionName, "GridPointsSecondary",_GridPointsSecondary);
    isOK &= parms(_sectionName, "GridStep", _GridStepMicrons);    
    isOK &= parms(_sectionName, "GridOrder", _gridOrder);
    
    if (_gridOrder.count() != 2)
    {
        printf("ERROR: wrong GridOrder\n");
        isOK = false;
        return isOK;
    }
    else
    {
        _primaryAxisSectionName = _gridOrder.at(0);
        _secondaryAxisSectionName = _gridOrder.at(1);
    }
    
    if (_primaryAxisSectionName == experiment().robot().indentationAxis()->Name())
    {
        printf("ERROR: primary axis equals indentation axis\n");
        isOK = false;
    }
    if (_secondaryAxisSectionName == experiment().robot().indentationAxis()->Name())
    {
        printf("ERROR: secondary axis equals indentation axis\n");
        isOK = false;
    }
    if (_primaryAxisSectionName == _secondaryAxisSectionName)
    {
        printf("ERROR: primary and secondary axes are equal\n");
        isOK = false;
    }
    QRegExp rx("[XYZ]");
    if (rx.indexIn(_primaryAxisSectionName) == -1)
    {
        printf("ERROR: primary axis not in regexp([XYZ])\n");
        isOK = false;
    }
    if (rx.indexIn(_secondaryAxisSectionName) == -1)
    {
        printf("ERROR: secondary axis not in regexp([XYZ])\n");
        isOK = false;
    }            
    
    //isOK &= parms()    
    if (!isOK)
    {
        printf("ERROR: GridScan::readParameters(const Parms& parms) failed\n");
        return false;
    }
    // **PBdR**: Doing stuff only if all the parameter reading went well        
    std::shared_ptr<Protocol> prot = factory().protocol(experiment(), _pointActionProtocolName);    
    if (prot==nullptr)
    {
        return false;
    }
    isOK &= prot->Init(parms);
    return isOK;
}

bool GridScan::writeParameters(Parms& parms) const
{
    bool isOK = true;
    //printf("within BAsicTEFIndentation.writeParameters: fileManager.getOutParamsPath() = %s\n", experiment.fileManager.getOutParamsPath().toLocal8Bit().data());
    isOK &= parms.write(experiment().fileManager().getOutParamsPath()); // TODO: check result
    return isOK;        
}