/* 
 * File:   MultiCreep.hpp
 * Author: cfmadmin
 *
 * Created on June 10, 2013, 3:27 PM
 */

#ifndef MULTICREEP_HPP
#define	MULTICREEP_HPP

#include "Experiment.hpp"
#include "QString"

class MultiCreep: public Protocol{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    MultiCreep(Experiment& experiment, QString sectionName);    
    virtual ~MultiCreep();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;        
    
protected:
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);  
    bool GoDownUntilStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive);
    bool GoUpUntilNoStiffness(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive);    
    
    enum ProtocolFlag{
        TareSensor=0,
        GoingToTargetForce1=1,
        Holding1=2,
        GoingToTargetForce2=3,        
        Holding2=4,
        PreRelaxing=10,
        AdjustingJaws=100,
        ZeroPosition=101
    };
    
    bool Regulate(Axis* indentationAxis, ForceSensor* forceSensor, bool isPushingPositive, double targetForceMicronewtons, double timeoutSeconds, ProtocolFlag flagGoingTo, ProtocolFlag flagHolding, double maxForceMicronewtons);
    
    void UpdateProtocolFlag(ProtocolFlag flag)
    {
        experiment().stateManager().state().Update(_fieldIdProtocolFlag, flag);
    }
    
    void UpdateSequenceStep(int stepId)
    {
        experiment().stateManager().state().Update(_fieldIdSequenceStep, stepId);
    }
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    int _fieldIdSequenceStep;
    
    double _stretchStepMicrons;
    double _maxTravelMicrons;
    double _maxForceAbsMicronewtons;
    double _targetTravelMicrons;
    double _holdingTimeS;
    double _relaxingTimeS;
    double _targetForceMicronewtons;
    double _forceMarginMicronewtons;
    double _stiffnessThreshold;
    double _defaultJawOpeningMicrons;
    double _zeroPosition;
    double _globalInitialPosition;
    bool _findZeroPosition = false;
    std::vector<double> _forceSequenceMicronewtons;
    std::vector<double> _holdingSequenceSeconds;
    std::vector<int> _forceSequenceStepIds;
    
    
   

};

#endif	/* MULTICREEP_HPP */

