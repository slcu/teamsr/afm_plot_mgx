/* 
 * File:   CandidateList.hpp
 * Author: cfmadmin
 *
 * Created on April 3, 2013, 5:43 PM
 */

#ifndef CANDIDATELIST_HPP
#define	CANDIDATELIST_HPP

class CandidateList {
public:
    CandidateList();
    //CandidateList(const CandidateList& orig);
    virtual ~CandidateList();
    void Clear();
    void AddCandidate(double position);
    bool CheckCandidate(double position, double margin);
private:
    std::vector<double> _candidatePositions;

};

///Not thread-safe
class CircularSummingBuffer
{
public:
    
    CircularSummingBuffer();
    virtual ~CircularSummingBuffer();
    
    void Init(int nitems)
    {
        _nitems = nitems;
        _items.clear();
        _items.assign(nitems, 0);
        _index = 0;
    }
    
    void Push(int item)
    {        
        _items.at(_index) = item;
        _index++;
        if (_index >= _items.size()) //wrap around
        {
            _index = 0;
        }
    }
    
    int Sum()
    {   
        int s=0;
        for (int i=0; i<_nitems; i++)
        {
            s += _items.at(i);
        }
        return s;        
    }
    
protected:
    int _nitems;
    int _index;
    std::vector<int>_items;
    
};


#endif	/* CANDIDATELIST_HPP */

