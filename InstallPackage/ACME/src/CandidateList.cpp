/* 
 * File:   CandidateList.cpp
 * Author: cfmadmin
 * 
 * Created on April 3, 2013, 5:43 PM
 */


#include <vector>
#include <cmath>
#include "CandidateList.hpp"


CandidateList::CandidateList() {
}

CandidateList::~CandidateList() {
}

void CandidateList::Clear()
{
    _candidatePositions.clear();
}

void CandidateList::AddCandidate(double position)
{
    _candidatePositions.push_back(position);
}

bool CandidateList::CheckCandidate(double position, double margin)
{
    bool isConfirmed = false;
    for (int i=0; i<_candidatePositions.size(); i++)
    {
        if ( fabs(_candidatePositions[i] - position) <= margin )
        {
            isConfirmed = true;
            break;
        }
    }
    return isConfirmed;
}

CircularSummingBuffer::CircularSummingBuffer()
{       

}

CircularSummingBuffer::~CircularSummingBuffer()
{
    
}

