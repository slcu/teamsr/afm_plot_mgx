/* 
 * File:   Dynamic.hpp
 * Author: cfmadmin
 *
 * Created on May 10, 2013, 4:00 PM
 */

#ifndef DYNAMIC_HPP
#define	DYNAMIC_HPP

#include "Experiment.hpp"
#include "QString"

class Dynamic : public Protocol{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN(); 
    Dynamic(Experiment& experiment, QString sectionName);      
    virtual ~Dynamic();
    
    virtual bool run();
    virtual bool Init(const Parms& parms);
    virtual bool writeParameters(Parms& parms) const;
    
protected:
    bool StepUp(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);
    bool StepDown(Axis* indentationAxis, double stepAbsMicrons, bool isPushingPositive);    
    
    int _fieldIdProtocolFlag;
    int _fieldIdTime;
    double _stretchStepMicrons;
    double _maxTravelMicrons;
    double _maxForceAbsMicronewtons;
    double _targetTravelMicrons;
    double _holdingTimeS;
    double _targetForceMicronewtons;
    double _forceMarginMicronewtons;
    double _strainAmplitudeMicrons;
    double _cycleTimeMiliseconds;

};

#endif	/* DYNAMIC_HPP */

