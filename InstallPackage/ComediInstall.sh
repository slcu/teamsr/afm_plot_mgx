#!/bin/bash

rm -rf comedi
rm -rf comedilib
rm -rf comedi_calibrate
rm -rf comedi-nonfree-firmware

#git clone git://comedi.org/git/comedi/comedi.git
#git clone git://comedi.org/git/comedi/comedilib.git
#git clone git://comedi.org/git/comedi/comedi_calibrate.git
#git clone git://comedi.org/git/comedi/comedi-nonfree-firmware.git
git clone https://github.com/Linux-Comedi/comedi.git
git clone https://github.com/Linux-Comedi/comedilib.git
git clone https://github.com/Linux-Comedi/comedi_calibrate.git
git clone https://github.com/Linux-Comedi/comedi-nonfree-firmware.git



cd comedi

./autogen.sh
autoconf
./configure
make

sudo make install
depmod -a

cd ..

cd comedilib
sudo make install
./autogen.sh
./configure

make

sudo make install


cd ..

cd comedi_calibrate

./autogen.sh
./configure
make


sudo make install



/sbin/modprobe ni_pcimio

sudo comedi_config /dev/comedi0 ni_pcimio


cd ..

sudo cp 99-extensometer.rules  /etc/udev/rules.d/ 

sudo ldconfig










