            ================================
                  MCS Control Library
                  Linux Version Notes
                (c) 2011 by SmarAct GmbH
            ================================


************************************************************
*  This Linux version of the MCS software is pre-release.  *
*  The development is unfinished and it may be unstable!   *
*  If you have problems with the software or suggestions   *
*  don't hesitate to contact us - your feedback is welcome *
************************************************************


CHANGES TO LAST PRE-RELEASE
===========================
The function SA_WaitForNextPacket has been removed. Waiting
for packets is now covered by other functions (see below).
An installer script has been added.



REQUIREMENTS
============

This version of the MCS Control Library requires Linux for 
32bit/x86 architecture.


INSTALLATION
============
The library package consists of the shared library libmcscontrol,
C header files and documentation. The library requires 3rd party
libraries which are also included (libftd2xx + libftchipid).
All libraries must be installed on the target computer.

The shell script 'install' installs the libraries and C headers
automatically to a user-definable destination path. The files
are installed in sub-directories as /usr/lib and /usr/include etc.
Call:
    install         - w/o arguments to install under /usr
    install -c      - to remove previous installations before installing
    install <path>  - to pass an installation path other than /usr
If you install to a system path (e.g. the default path), you 
must execute install with sufficient privileges, e.g.
    sudo install ...


SYSTEM CONFIGURATION
====================
When an MCS controller is connected to the computer or switched on,
it is possible that the ftdi_sio driver is automatically loaded
for that device. In this case the application cannot connect to
the MCS. The ftdi_sio driver (kernel module) must be unloaded before
launching your application or blocked from loading, e.g. by 
blacklisting the ftdi_sio module in /etc/modprobe.d/.

The MCS Control library needs write access to the USB port the MCS 
device is connected to. To automatically set r/w permissions when 
an MCS is connected, a udev rule can be added to /etc/udev/rules.d/

   ATTR{idVendor}=="0403", ATTR{idProduct}=="6001", MODE="666"

The rule sets the r/w permission for everyone. If this is not
acceptable, adjust the MODE argument.


PROGRAMMING
===========
Several asynchronous mode functions that wait for the next packet
received from an MCS have a timeout parameter:
    SA_LookAtNextPacket_A,
    SA_ReceiveNextPacket_A and
    SA_ReceiveNextPacketIfChannel_A
For increased reliability, under Linux the timeout is overwritten
internally with 1000 milliseconds if the parameter value is smaller.
This means that if you call one of the functions with a small 
timeout value and no packet is received from the MCS for several
seconds, the function does not return after the timeout you have
specified, but after about 1 second at the earliest. If a packet
is received before the timeout, the function returns immediately
of course.

In the Windows version of the MCS Control Library it is possible to
register a system Event to get notifications when data packets are
received from an MCS controller. This notification mechanism is not 
available under Linux and using Events with the MCS Control library 
under Windows is *deprecated now*. 

From now on it is recommened to call one of the SA_...NextPacket... 
functions (see above) to wait for the next packet. These functions
now understand the new timeout constant SA_TIMEOUT_INFINITE. A
function called with an infinite timeout will not return until a
packet has been received or the call is canceled with the new 
function
    SA_CancelWaitForPacket_A 
from a different thread.
When a waiting ...NextPacket... function is canceled it returns with
the SA_CANCELED_ERROR status. SA_CancelWaitForPacket_A has an effect
only if it is called while a function is waiting for a packet.
If it is called before the function, it has no effect.

It is still possible (although not recommended) to use the Event 
based notification mechanism in Windows programs. If an 
Event has been set with SA_SetReceiveNotification_A, 
SA_CancelWaitForPacket_A returns with an error and has no effect. 
Using SA_...NextPacket... functions for waiting has advantages over
the Event mechanism: you can write programs that can be compiled
under Windows and Linux without platform specific adaptations.

