from path import path

class INIFile(object):
    """
    Read a INI or INI-like file.

    :Properties"
        - `filename`: string or None
            Path to the file to load. When set, the file is loaded automatically.
        - `comment`: string
            String identifying the start of a comment
        - `assignment`: string
            String separating a key from its value
    """
    def __init__(self, filename = None, **kwords):
        """
        :Parameters:
            - `filename`: string or None
                Path to a filename to load

        :Keywords:
            - `comment`: string
                String identifying a comment (default: ';')
            - `assignment`: string
                String separating the key from the value (default: '=')
            - `read_mgxv`: bool
                If True, set comment to '//' and assignment to ':'

        """
        self._sections = {}
        self._filename = None
        self.comment = kwords.get("comment", ";")
        self.assignment = kwords.get("assignment", "=")
        if kwords.get("read_mgxv", False):
            self.comment = "//"
            self.assignment = ":"
        if filename is not None:
            self.filename = filename

    def _set_filename(self, filename):
        if filename is None:
            del self.filename
            return
        pth = path(filename)
        if pth.exists() and pth.isfile():
            self._filename = filename
            self.load()
        else:
            raise ValueError("Path '{0}' doesn't exist or isn't a file".format(filename))

    def _get_filename(self):
        return self._filename

    def _del_filename(self):
        self._filename = None
        self._sections = {}

    filename = property(_get_filename, _set_filename, _del_filename)

    def load(self):
        fn = self._filename
        assignment = self.assignment
        assign_size = len(assignment)
        comment = self.comment
        sections = self._sections
        if fn is None:
            raise ValueError("Error, you need to set a valid filename before trying to load the file")
        with open(fn, "rt") as f:
            current_section = None
            line_num = 0
            while True:
                line = f.readline()
                line_num += 1
                if not line:
                    break # Finished reading
                try:
                    idx = line.index(comment)
                    line = line[:idx]
                except ValueError:
                    pass
                line = line.strip()
                if line:
                    if line[0] == '[' and line[-1] == ']':
                        current_section = line[1:-1].strip()
                        sections.setdefault(current_section, {})
                    else:
                        try:
                            idx = line.index(assignment)
                        except ValueError:
                            raise ValueError("Error in file '{0}' on line {1}: the line is neither empty, a section or a key.".format(fn, line_num))
                        key = line[:idx].strip()
                        value = line[idx+assign_size:].strip()
                        sections[current_section].setdefault(key, []).append(value)

    def __getitem__(self, (section, key)):
        return self._sections[section][key][-1]

