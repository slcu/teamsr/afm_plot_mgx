import sys
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import pandas as pd
import glob
import os
import os.path
from pyqtgraph.ptime import time
import read_ini

def GetProtocolType(csvPath):
	s = os.path.split(csvPath)
	directory = s[0]
	parametersIniFilename = os.path.join(directory, "parameters.ini")
	#print parametersIniFilename
	parameters = read_ini.INIFile(parametersIniFilename)
	protocolName = parameters.__getitem__(('Protocol','Name'))
	protocolType = parameters.__getitem__((protocolName,'Type'))
	#hypocotylDiameter = parameters.__getitem__(('HypocotylDimensions', 'diameter'))
	
	#targetForce = parameter.__getitem__((protocolSection,'TargetForce'))
	#stepSize = parameter.__getitem__((protocolSection,'StepSize'))
	return protocolType	


app = QtGui.QApplication([])
mw = QtGui.QMainWindow()
mw.resize(1100,800)

#csvPath = '/home/cfmadmin/Desktop/Practicals/ExtensometerResults/2013_11_19_new_amplifier_test_00008/data.csv'
csvPath = sys.argv[1]

view = pg.GraphicsLayoutWidget()
mw.setCentralWidget(view)
mw.show()
mw.setWindowTitle(csvPath)

label = pg.LabelItem(justify='center', color='FFFF00')
view.addItem(label, row=0, col=0)
label.setText('')


p = view.addPlot(row=1, col=0)
p.setLabel('left', "force (uN)")
p.setLabel('bottom', "time (min)")

view.nextRow()
p2 = view.addPlot(row=2, col=0)
p2.setLabel('left', "position (um)")
p2.setLabel('bottom', "time (min)")
#p2.setXLink(p)
p.setXLink(p2)



counter = 0
data = pd.read_csv(csvPath)

if (len(data[~np.isnan(data['Y_Position(um)'])]) == 0):
	mainAxisName = 'Z_Position(um)'
else:
	mainAxisName = 'Y_Position(um)'

print GetProtocolType(csvPath)

if (GetProtocolType(csvPath) == 'MultiStep'):
	MultiStep = True
else:
	MultiStep = False

#if (len(data[data['ProtocolFlag(flag)'] == 2]) == 0):
#	MultiStep = True
#else:
#	MultiStep = False

if (MultiStep):
	time = data['LoggerTime(ms)'][(data['ProtocolFlag(flag)'] == 6) | (data['ProtocolFlag(flag)'] == 5)|(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
	pos = data[mainAxisName][(data['ProtocolFlag(flag)'] == 6) | (data['ProtocolFlag(flag)'] == 5)|(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
	force = data['IndentationForceSensor_Force(uN)'][(data['ProtocolFlag(flag)'] == 6) | (data['ProtocolFlag(flag)'] == 5)|(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
else:
	time = data['LoggerTime(ms)'][(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
	pos = data[mainAxisName][(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
	force = data['IndentationForceSensor_Force(uN)'][(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
		
time = time[~np.isnan(force)]
pos = pos[~np.isnan(force)]
force = force[~np.isnan(force)]

linePos1 = 0
linePos2 = 0

def ExtensometerUpdate():
	global p, p2, counter, time, force, pos, text1, text2, linePos1, linePos2, label
	data = pd.read_csv(csvPath)
	if (MultiStep):
		time = data['LoggerTime(ms)'][(data['ProtocolFlag(flag)'] == 6) | (data['ProtocolFlag(flag)'] == 5)|(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
		pos = data[mainAxisName][(data['ProtocolFlag(flag)'] == 6) | (data['ProtocolFlag(flag)'] == 5)|(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
		force = data['IndentationForceSensor_Force(uN)'][(data['ProtocolFlag(flag)'] == 6) | (data['ProtocolFlag(flag)'] == 5)|(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]		
	else:
		time = data['LoggerTime(ms)'][(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
		pos = data[mainAxisName][(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
		force = data['IndentationForceSensor_Force(uN)'][(data['ProtocolFlag(flag)'] == 2) | (data['ProtocolFlag(flag)'] == 1)]
		
	time = time[~np.isnan(force)]
	pos = pos[~np.isnan(force)]
	force = force[~np.isnan(force)]
	
	p.clear()
	p2.clear()
	
    #curve = pg.ScatterPlotItem(x=data[ptr%50], y=data[(ptr+1)%50], pen='w', brush='b', size=size, pxMode=ui.pixelModeCheck.isChecked())    
	#curve = pg.ScatterPlotItem(x = np.array(time)/60000.0, y=np.array(force), pen='w', brush='w', size=0.5)
	#curve = p.plot(x = np.array(time)/60000.0, y=np.array(force), pen='w', brush='w', size=1)
	#p.addItem(curve)
	p.showAxis('right', True)
	p.showAxis('top', True)
	p.showGrid(x=True, y=True, alpha=0.3)
	p.plot(x = np.array(time)/60000.0, y=np.array(force))
	#trying to add markers:
	#print "linePos (old):"
	#print linePos
	line1 = pg.InfiniteLine(pos=linePos1,angle=0, movable=True)
	line2 = pg.InfiniteLine(pos=linePos2,angle=0, movable=True)
	text1 = pg.TextItem("", anchor=(0, 0), color=(255,255,0), angle=0)
	text2 = pg.TextItem("", anchor=(0, 0), color=(255,255,0), angle=0)

	def posChanged1():
		global text1, linePos1, linePos2, label
		linePos1 = line1.pos()[1]
		labelPos1 = line1.pos()[0]
		print "linePos1:"		
		print linePos1			
		#label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: yellow'>y1=%0.1f</span>" % (linePos, linePos))
		text1.setPos(0, linePos1)	
		text1.setText('[%0.3f]' % (linePos1))
		label.setText('f1 = %0.3f uN,\n f2 = %0.3f uN, \t abs(deltaf) = %0.3f uN' %(linePos1, linePos2, np.abs(linePos1-linePos2)))	
	def posChanged2():
		global text2, linePos1, linePos2, label
		linePos2 = line2.pos()[1]
		print "linePos2:"
		print linePos2
		#label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: yellow'>y1=%0.1f</span>" % (linePos, linePos))
		text2.setPos(0, linePos2)	
		text2.setText('[%0.3f]' % (linePos2))	
		label.setText('f1 = %0.3f uN,\n f2 = %0.3f uN, \t abs(deltaf) = %0.3f uN' %(linePos1, linePos2, np.abs(linePos1-linePos2)))
	line1.sigPositionChangeFinished.connect(posChanged1)
	line2.sigPositionChangeFinished.connect(posChanged2)
	p.addItem(line1)
	p.addItem(line2)
	p.addItem(text1)
	p.addItem(text2)

	
	#curve2 = pg.ScatterPlotItem(x = np.array(time)/60000.0, y=np.array(pos), pen='w', brush='w', size=0.5)
	#curve2 = p2.plot(x = np.array(time)/60000.0, y=np.array(pos), pen='w', brush='w', size=1)
	#p2.addItem(curve2)	
	p2.showAxis('right', True)
	p2.showAxis('top', True)
	p2.showGrid(x=True, y=True, alpha=0.3)
	p2.plot(x = np.array(time)/60000.0, y=np.array(pos))
	
	   
    #p.setTitle('%0.2f fps' % fps)
    #p.enableAutoScale()
    #p2.enableAutoScale()
    
	#p.repaint()
	#p2.repaint()
    #app.processEvents()  ## force complete redraw for every plot
#timer = QtCore.QTimer()
#timer.timeout.connect(update)
#timer.start(2000)
ExtensometerUpdate()
#win.show()
    
#QtGui.QApplication.processEvents()
QtGui.QApplication.exit()
## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
	import sys
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		csvPath = sys.argv[1]
		QtGui.QApplication.instance().exec_()
        
