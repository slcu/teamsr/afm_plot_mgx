#!/bin/bash

#dialog --title "ACME Install" \
#--backtitle "ACME INSTALL" \
#--yesno "This script is going to install all ACME components and compile software.

#The following directories are going to be deleted prior to installation:
#/comedi
#/comedilib
#/comedi_calibration
#/comedi-nonfree-firmware
#/morphorobotx/build
#/ACME/build

#Calibration files, ini files in /ACME/build are going to be deleted.

#Are you sure you want to permanently overwrite the installation ?" 7 60
 
# Get exit status
# 0 means user hit [yes] button.
# 1 means user hit [no] button.
# 255 means user hit [Esc] key.
#response=$?
#if $response=0

chmod 755 ExtensometerInstall.sh
chmod 755 ComediInstall.sh
chmod 755 SmaractInstall.sh
chmod 755 SoftwareInstall.sh

sudo ./ExtensometerInstall.sh | tee ExtensometerInstall.log
./ComediInstall.sh | tee ComediInstall.log
./SmaractInstall.sh | tee SmaractInstall.log
./SoftwareInstall.sh | tee SoftwareInstall.log

#fi
