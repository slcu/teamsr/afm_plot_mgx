# Install script for directory: /home/lisa/Desktop/InstallPackage/morphorobotx/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so"
         RPATH "")
  ENDIF()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/lib/libmorphorobotx.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES "/home/lisa/Desktop/InstallPackage/morphorobotx/build/src/libmorphorobotx.so")
  IF(EXISTS "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so")
    FILE(RPATH_REMOVE
         FILE "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/lib/libmorphorobotx.so")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/include/morphorobotx/Axis.hpp;/usr/local/include/morphorobotx/ADCard.hpp;/usr/local/include/morphorobotx/ComediHardwareController.hpp;/usr/local/include/morphorobotx/DataLogger.hpp;/usr/local/include/morphorobotx/Experiment.hpp;/usr/local/include/morphorobotx/Factory.hpp;/usr/local/include/morphorobotx/FileManager.hpp;/usr/local/include/morphorobotx/ForceMeasurement.hpp;/usr/local/include/morphorobotx/ForceSensor.hpp;/usr/local/include/morphorobotx/HardwareManager.hpp;/usr/local/include/morphorobotx/HardwareController.hpp;/usr/local/include/morphorobotx/HardwareControllerFactory.hpp;/usr/local/include/morphorobotx/MeasurementState.hpp;/usr/local/include/morphorobotx/parms.hpp;/usr/local/include/morphorobotx/Robot.hpp;/usr/local/include/morphorobotx/RobotSubunit.hpp;/usr/local/include/morphorobotx/Sensor.hpp;/usr/local/include/morphorobotx/SensorFactory.hpp;/usr/local/include/morphorobotx/SmarActAxis.hpp;/usr/local/include/morphorobotx/SmarActAxisStatus.hpp;/usr/local/include/morphorobotx/SmarActCommand.hpp;/usr/local/include/morphorobotx/SmarActCommands.hpp;/usr/local/include/morphorobotx/SmarActHardwareController.hpp;/usr/local/include/morphorobotx/SmarActResult.hpp;/usr/local/include/morphorobotx/StateManager.hpp;/usr/local/include/morphorobotx/Timer.hpp;/usr/local/include/morphorobotx/ZaberAxis.hpp;/usr/local/include/morphorobotx/ZaberHardwareController.hpp")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/include/morphorobotx" TYPE FILE FILES
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/Axis.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/ADCard.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/ComediHardwareController.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/DataLogger.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/Experiment.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/Factory.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/FileManager.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/ForceMeasurement.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/ForceSensor.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/HardwareManager.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/HardwareController.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/HardwareControllerFactory.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/MeasurementState.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/parms.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/Robot.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/RobotSubunit.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/Sensor.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SensorFactory.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SmarActAxis.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SmarActAxisStatus.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SmarActCommand.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SmarActCommands.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SmarActHardwareController.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/SmarActResult.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/StateManager.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/Timer.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/ZaberAxis.hpp"
    "/home/lisa/Desktop/InstallPackage/morphorobotx/src/ZaberHardwareController.hpp"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

