FILE(REMOVE_RECURSE
  "CMakeFiles/morphorobotx.dir/ADCard.cpp.o"
  "CMakeFiles/morphorobotx.dir/Axis.cpp.o"
  "CMakeFiles/morphorobotx.dir/ComediHardwareController.cpp.o"
  "CMakeFiles/morphorobotx.dir/DataLogger.cpp.o"
  "CMakeFiles/morphorobotx.dir/Experiment.cpp.o"
  "CMakeFiles/morphorobotx.dir/Factory.cpp.o"
  "CMakeFiles/morphorobotx.dir/FileManager.cpp.o"
  "CMakeFiles/morphorobotx.dir/ForceMeasurement.cpp.o"
  "CMakeFiles/morphorobotx.dir/ForceSensor.cpp.o"
  "CMakeFiles/morphorobotx.dir/HardwareManager.cpp.o"
  "CMakeFiles/morphorobotx.dir/HardwareController.cpp.o"
  "CMakeFiles/morphorobotx.dir/HardwareControllerFactory.cpp.o"
  "CMakeFiles/morphorobotx.dir/MeasurementState.cpp.o"
  "CMakeFiles/morphorobotx.dir/parms.cpp.o"
  "CMakeFiles/morphorobotx.dir/Robot.cpp.o"
  "CMakeFiles/morphorobotx.dir/RobotSubunit.cpp.o"
  "CMakeFiles/morphorobotx.dir/Sensor.cpp.o"
  "CMakeFiles/morphorobotx.dir/SensorFactory.cpp.o"
  "CMakeFiles/morphorobotx.dir/SmarActAxis.cpp.o"
  "CMakeFiles/morphorobotx.dir/SmarActAxisStatus.cpp.o"
  "CMakeFiles/morphorobotx.dir/SmarActCommand.cpp.o"
  "CMakeFiles/morphorobotx.dir/SmarActCommands.cpp.o"
  "CMakeFiles/morphorobotx.dir/SmarActHardwareController.cpp.o"
  "CMakeFiles/morphorobotx.dir/SmarActResult.cpp.o"
  "CMakeFiles/morphorobotx.dir/StateManager.cpp.o"
  "CMakeFiles/morphorobotx.dir/Timer.cpp.o"
  "CMakeFiles/morphorobotx.dir/ZaberAxis.cpp.o"
  "CMakeFiles/morphorobotx.dir/ZaberHardwareController.cpp.o"
  "libmorphorobotx.pdb"
  "libmorphorobotx.so"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang CXX)
  INCLUDE(CMakeFiles/morphorobotx.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
