/* 
 * File:   ComediCardHardwareController.hpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 3:04 PM
 */

#ifndef COMEDIHARDWARECONTROLLER_HPP
#define	COMEDIHARDWARECONTROLLER_HPP
#include "HardwareController.hpp"
#include "ADCard.hpp"
#include "QHash"
class Sensor;
class Parms;

class ComediHardwareController : public HardwareController{
public:
    const double double_nan = std::numeric_limits<double>::quiet_NaN();   
    ComediHardwareController(const Parms& parms, QString name, StateManager& stateManager);    
    virtual ~ComediHardwareController();
    bool Get(int option, double& value); ///TODO: require specific type for the option 
    bool Set(int option, double value); ///TODO: require specific type for the option 
   // bool BuildFromRecipe(const Parms& parms, QString section); 
    Sensor* BuildSensor(const Parms& parms, QString name);
    bool Initialize();
    bool GetVoltage(int ADChannel, double& voltage);
    bool RegisterSensorForSampling(Sensor * sensor);
    
protected:
    bool GetConsistentSamplingFrequency(double * samplingFrequency);
    bool GetConsistentIntegrationTime(double * integrationTimeMs);
    
   // ADCard card;
    QString _devPath;
    unsigned int _samplingFrequency;
    ADCard adcard;
    double _integrationTimeMs;
    int _channels[16]; ///structure holding channels to be sampled by comedi, pointer to it needed during init
    int _nchan; ///number of entries in _channels[16]
    QHash <int, int> _channelMap; //maps channel number to the index of placeholder in _channels, this one is being built during
    std::vector<double> _voltages;
};

#endif	/* COMEDICARDHARDWARECONTROLLER_HPP */

