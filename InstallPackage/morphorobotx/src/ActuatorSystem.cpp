/* 
 * File:   ActuatorSystem.cpp
 * Author: cfmadmin
 * 
 * Created on January 31, 2013, 9:16 PM
 */

#include <MCSControl.h>

#include "ActuatorSystem.hpp"
#include "ActuatorAxis.hpp"

ActuatorSystem::ActuatorSystem(unsigned int systemID, unsigned int localSystemID): _systemID(systemID), _localSystemID(localSystemID){
}

/*ActuatorSystem::ActuatorSystem(const ActuatorSystem& orig) {
}*/

ActuatorSystem::~ActuatorSystem() {
}

/*bool ActuatorSystem::Goto(SA_INDEX channelIndex, signed int position)
{
    //TODO: implement within subclasses
        
    //int holdMs = 100;
    //SA_GotoPositionAbsolute_S(this->_systemIndex, channelIndex, position, holdMs);
    
    return false;
}*/

ActuatorAxis* ActuatorSystem::GetAxisPointer(unsigned int localAxisID)
{    
    for (int i=0; i<actuatorAxes.size(); i++)
    {
        printf("...... checking axis %d in actuator system, its localID is %d\n", i, actuatorAxes.at(i)->LocalID() );
        if (actuatorAxes.at(i)->LocalID() == localAxisID)
        {            
            return actuatorAxes.at(i);
        }
    }
    return 0;
}