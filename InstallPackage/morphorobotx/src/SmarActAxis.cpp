/* 
 * File:   SmarActAxis.cpp
 * Author: cfmadmin
 * 
 * Created on March 1, 2013, 7:44 PM
 */

#include "SmarActAxis.hpp"
#include "SmarActHardwareController.hpp"
#include "parms.hpp"
#include "Timer.hpp"
#include "SmarActAxisStatus.hpp"
#include "SmarActCommands.hpp"
#include "StateManager.hpp"

///TODO: what if there is error in constructor  ?
///TODO: use isValid flag to indicate if constructor worked successfully
SmarActAxis::SmarActAxis(const Parms& parms, QString axisName, SmarActHardwareController* hardwareController):Axis(hardwareController) {
    printf("called contructor SmarActAxis::SmarActAxis\n");
    printf(" - axisName: %s\n", axisName.toLocal8Bit().data());
    printf(" - hardwareControllerName: %s\n", hardwareController->Name().toLocal8Bit().data());
    _name = axisName;
    _controllerName = hardwareController->Name();    //TODO: can this be moved to base class ?
    _marginMicrons = 0.012; //default margin is 12 nanometers
    //here, handle smaract-specific parameters:
    bool isOK = parms(axisName, "LocalAxisID", _LocalAxisID); ///TODO: what if there is error ? Make a zombie object with invalid flag ?
    printf(" - LocalAxisID: %d\n", _LocalAxisID );
    _SystemID = 10000; //safe default value, no command will succeed with it. Valid value will be added after successful Initialize() 
    _holdMs = 100; //default value, can be changed by calling set        
    //TODO: it is controversial what name should the axis register as its column name: no w it is the axis section name from robotRecipe.ini
    QString axisGivenName;
	isOK &= parms(axisName, "Name", axisGivenName); //this is how the User named it, it is not the section name
    _fieldIdPositionMicrons = hardwareController->stateManager().RegisterFieldExc(axisGivenName + "_Position", "(um)", false); //here, change the naming convention, use the name given by user in robotRecipe.ini
}

SmarActAxis::~SmarActAxis() {
}

bool SmarActAxis::Set(unsigned int option, double value)
{
    return SetOption((AxisOption)option, value);    //casts int into AxisOption
}

bool SmarActAxis::Get(unsigned int option, double& value)
{    
    return false;
}

bool SmarActAxis::Initialize()
{
    printf("Inside SmarActAxis::Initialize()\n");
    bool isOK = true;
    if ( ! _itsHardwareController->isInitialized())
    {    
        isOK &= _itsHardwareController->Initialize();    
        if (!isOK)
        {
            return false;
        }
    }
    //TODO: check if axis with this->_LocalAxisID is present (connected) or something like this
    //here the systems should be already initialized and talking. So, now we can ask if the actual hardware axis is of the type required, if is connected, etc.                 
    //ask if the actual smaract axis is connected;
    _SystemID = ((SmarActHardwareController*)_itsHardwareController)->systemID();
        
    ///TODO: create a commmand object for this command (in file SmarActCommands.cpp and .hpp)
    SA_STATUS status = SA_GetChannelType(_SystemID, _LocalAxisID, &_channelType);
    if (status != SA_OK)
    {
        printf("ERROR: cannot get channel type for axis\n");
        return false;
    }
    
    if (_channelType != SA_POSITIONER_CHANNEL_TYPE)
    {
        printf("ERROR: smaract axis (channel) is not of type SA_POSITIONER_CHANNEL_TYPE\n");
        return false;
    }
    
    status = SA_GetSensorType_S(_SystemID, _LocalAxisID, &_sensorType);
    if (status != SA_OK)
    {
        printf("ERROR: cannot get sensor type for axis\n");
        return false;
    }
    
    if (_sensorType == SA_NO_SENSOR_TYPE)
    {
        printf("ERROR: SmarAct axis %d on controller %d is not connected (or is defective)\n", _LocalAxisID, _SystemID);
        return false;
    }
    
    printf("Axis %d on controller %d has channel type %d and sensor type %d\n", _LocalAxisID, _SystemID, _channelType, _sensorType);
    
    return isOK;            
}
///TODO: this routine is ugly, but it works in most cases. Check timeouts and find a better solution.
bool SmarActAxis::GotoSync(double positionMicrons)
{
    bool result = GotoAsync(positionMicrons);
    Timer timer;    
    if (result)
    {   
        unsigned int axisStatus;        
        SmarActAxisStatus s;
        Command_SA_GetStatus_S cmd;
        timer.Reset();
        do
        {
            if (!GetStatusSync(axisStatus))
            {
                return false;
            }
            s.SetStatus(axisStatus); //printf("checking axis status: %s\n", s.GetName().toLocal8Bit().data()); //TODO: ugly, this will output the whole story
            if (timer.TimeElapsedMs() > 60000) //TODO: it's ugly (and arbitrary)
            {
                printf("Timer elapsed, give up waiting...\n"); //TODO: call SA_Stop_S to make sure it doesn't try to move
                break;                
            }
        }
        while( (axisStatus != SA_STOPPED_STATUS)&&(axisStatus != SA_HOLDING_STATUS) );        //TODO: ugly, difficult to read        
        //TODO: add timeout        
        //TODO: wait until position achieved
        //signed int actualPositionNanometers;
        double actualPositionMicrons;
        Command_SA_GetPosition_S cmd_getpos;
        bool repeat = true;
        timer.Reset();
        do
        {
            if (!GetPositionSync(actualPositionMicrons))
            {
                return false;
            }
            //printf("checking position: %f\n", actualPositionMicrons);
            if (fabs(positionMicrons - actualPositionMicrons) < _marginMicrons) //TODO: check if abs is the right function to call (double vs. int)
            {
                repeat = false;
            }
            if (timer.TimeElapsedMs() > 100)
            {
                printf("Timer elapsed, give up waiting...\n");
                repeat = false;
            }
        }while(repeat); //TODO: add timeout
        StopSync(); //to make sure it's not moving, holding or whatever
    }
    else
    {
        return false;
    }
    return true;
}

bool SmarActAxis::GotoAsync(double positionMicrons)
{
    SA_STATUS status = SA_OK;    
    Command_SA_GotoPositionAbsolute_S cmd;
    //printf("called GotoAsync...\n");
    
    cmd.Send(_SystemID, _LocalAxisID, positionMicrons*1000, _holdMs);
    if (cmd.Failed())    
    {   
        if (cmd.RetryOnceAllowed())
        {// printf("retry send...\n");
            cmd.Send(_SystemID, _LocalAxisID, positionMicrons*1000, _holdMs);
        }//TODO: rubbish comes out of it...
        if (cmd.Failed())
        {
            printf("ERROR: %s\n", cmd.GetStatusDescription().toLocal8Bit().data());        
            return false;
        }
    }    
    return true;
}

bool SmarActAxis::GetStatusSync(unsigned int& status) //TODO: consider if SmarActAxisStatus fits better than unsigned int
{
    //TODO: implement    
    unsigned int axisStatus;
    SmarActAxisStatus s;
    Command_SA_GetStatus_S cmd;
    cmd.Send(_SystemID, _LocalAxisID, &axisStatus);
    if (cmd.Failed()) {
        if (cmd.RetryOnceAllowed()) {
            cmd.Send(_SystemID, _LocalAxisID, &axisStatus);
            if (cmd.Failed()) {
                return false;
            }
        } else {
            return false;
        }
    }
    s.SetStatus(axisStatus);
    status = axisStatus; //TODO: consider returning s as output
    //printf("checking axis status: %s\n", s.GetName().toLocal8Bit().data()); //TODO: ugly, this        
    return true;
}

bool SmarActAxis::StopSync()
{
    Command_SA_Stop_S cmd_stop;
    Timer timer;
    cmd_stop.Send(_SystemID, _LocalAxisID);
    if (cmd_stop.Failed())
    {
        if (cmd_stop.RetryOnceAllowed())
        {
            cmd_stop.Send(_SystemID, _LocalAxisID);
            if (cmd_stop.Failed())
            {
                return false;
            }
        }
    }
    unsigned int axisStatus;        
    Command_SA_GetStatus_S cmd;
    SmarActAxisStatus s;
    timer.Reset();
    do
    {
        if (!GetStatusSync(axisStatus))
        {
            return false;
        } 
        s.SetStatus(axisStatus);
        //printf("checking axis status: %s\n", s.GetName().toLocal8Bit().data()); //TODO: ugly, this will output the whole story
        if (timer.TimeElapsedMs() > 1000)
        {
            printf("Timer elapsed, give up waiting...\n");
            //TODO: call SA_Stop_S to make sure it doesn't try to move
            return false; 
            break;                            
        }
    }
    while( (axisStatus != SA_STOPPED_STATUS) );    
    return true;
}

bool SmarActAxis::SetOption(AxisOption option, double value)
{
    switch(option)
    {
        case (AxisOption::ACCELERATION_LIMIT_MICRONS_PER_SECOND_SQUARE):
            return false;
            break;
            
        case (AxisOption::GOTO_HOLD_MS):
            SetHoldMs(value);            
            break;
            
        case (AxisOption::GOTO_PRECISION_MARGIN_MICRONS):
            SetMarginMicrons(value);
            break;
            
        case (AxisOption::GOTO_TIMEOUT_MS):
            return false;
            break;
            
        case (AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND):
            return SetClosedLoopMoveSpeed(value);
            break;
            
        case (AxisOption::FREQUENCY_LIMIT_HZ):
            return SetClosedLoopMaxFrequency(value);
            break;
            
        case (AxisOption::SET_ZERO_POSITION):
            return SetZeroPosition();
            break;
            
        case (AxisOption::STOP_NOW):            
            return StopSync();
            break;
            
        default:            
            return false;
    }
    return true;
}

bool SmarActAxis::SetZeroPosition()
{    
    Command_SA_SetZeroPosition cmd;    
    cmd.Send(_SystemID, _LocalAxisID);
    if (cmd.Failed())
    {
        if (cmd.RetryOnceAllowed())
        {
            cmd.Send(_SystemID, _LocalAxisID);
            if (cmd.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }                   
    return true;
}

void SmarActAxis::SetHoldMs(unsigned int holdMsValue)
{
    _holdMs = holdMsValue;            
}
void SmarActAxis::SetMarginMicrons(double marginMicrons) //defines precision margin within which the Goto must arrive
{
    _marginMicrons = marginMicrons;
}
bool SmarActAxis::SetClosedLoopMaxFrequency(double frequencyHz)
{   
    Command_SA_SetClosedLoopMaxFrequency_S cmd;    
    cmd.Send(_SystemID, _LocalAxisID, frequencyHz);
    if (cmd.Failed())
    {
        if (cmd.RetryOnceAllowed())
        {
            cmd.Send(_SystemID, _LocalAxisID, frequencyHz);
            if (cmd.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }                   
    return true;
}

bool SmarActAxis::SetClosedLoopMoveSpeed(double speedMicronsPerSecond)
{
    Command_SA_SetClosedLoopMoveSpeed_S cmd;    
    cmd.Send(_SystemID, _LocalAxisID, speedMicronsPerSecond*1000);
    if (cmd.Failed())
    {
        if (cmd.RetryOnceAllowed())
        {
            cmd.Send(_SystemID, _LocalAxisID, speedMicronsPerSecond*1000);
            if (cmd.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }                   
    return true;
}


bool SmarActAxis::GetPositionSync(double& positionMicrons) //TODO: rubbish comes out of it...
{
    Command_SA_GetPosition_S cmd_getpos;
            
    signed int actualPositionNanometers;
    
    cmd_getpos.Send(_SystemID, _LocalAxisID, &actualPositionNanometers);
    
    if (cmd_getpos.Failed())
    {
        if (cmd_getpos.RetryOnceAllowed())
        {
            cmd_getpos.Send(_SystemID, _LocalAxisID, &actualPositionNanometers);
            if (cmd_getpos.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    //printf("got signed int actualPositionNanometers: %d\n", actualPositionNanometers);
    
    positionMicrons = actualPositionNanometers/1000.0; 
    
    //TODO: update state
    
    stateManager().state().Update(_fieldIdPositionMicrons, positionMicrons);
    
    //printf("after conversion to double microns: %f\n", positionMicrons);
    return true;
}
