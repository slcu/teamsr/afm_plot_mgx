/* 
 * File:   ComediHardwareController.cpp
 * Author: cfmadmin
 * 
 * Created on February 27, 2013, 3:04 PM
 */

#include "ComediHardwareController.hpp"
#include "Sensor.hpp"
#include "parms.hpp"
#include "SensorFactory.hpp"

ComediHardwareController::ComediHardwareController(const Parms& parms, QString name, StateManager& stateManager) : HardwareController(name, stateManager) {
    printf("called constructor ComediHardwareController::ComediHardwareController() \n");
    _isInstanceValid = false;
    //TODO: parse device specific parameters (device path etc.)
    //and set _isOBjectValid accordingly
    QString devPath;
    bool isOK = true;
    
    isOK &= parms(name, "DevPath", devPath);
    if (isOK)
    {
        _devPath = devPath;
    }
    printf("parsed DevPath %s\n", devPath.toLocal8Bit().data());
    unsigned int fs;        
    isOK &= parms(name, "SamplingFrequency", fs);
    if (isOK)
    {
       _samplingFrequency = fs;
    }
    _isInstanceValid = isOK;    
}

ComediHardwareController::~ComediHardwareController() {
}

bool ComediHardwareController::Set(int option, double value)
{
    //TODO: implement
    return false;
}

bool ComediHardwareController::Get(int option, double& value)
{
    //TODO: implement
    return false;
}
/*
bool ComediHardwareSystem::BuildFromRecipe(const Parms& parms, QString section)
{
    printf("within ComediHardwareSystem::BuildFromRecipe(parms, QString section== %s\n", section.toLocal8Bit().data());
    return false; ///TODO: implement
}*/

Sensor* ComediHardwareController::BuildSensor(const Parms& parms, QString sensorName)
{
    Sensor* sensor = 0;    
    SensorFactory factory;
    sensor = factory.sensor(parms, sensorName, this);    
    printf("ComediHardwareController::BuildSensor\n");
    if (sensor)
    {        
        _sensors.push_back(sensor);
        _integrationTimeMs = sensor->IntegrationTimeMs();    
        printf("_integrationTimeMs = %f\n", _integrationTimeMs);       
    }
    else
    {
        printf("ERROR: building sensor failed\n");
    }
    return sensor;
}

bool ComediHardwareController::Initialize()
{
    printf("inside ComediHardwareController::Initialize()\n");
    bool isOK = true;     
    
    if (_sensors.size() < 1)
    {
        printf("ERROR: no sensors registered\n");
        return false;
    }    
    
    //TODO: implement
    
    //- open device
    //- get registered sensors and prepare the acquisiton card
    //- send commands            
    
    //TODO: use adcard.InitMultichannel and provie the list of channels
    //The card (ADCard) knows nothing about the sensors it only delivers voltages related to channels
    //it is up to the ComediHardwareController to know which sensor is registered to which channel and how to update voltages, timestamps, status and forces
    _voltages.clear();
    for (int i=0; i<_sensors.size(); i++)
    {
        _voltages.push_back(double_nan);
        _channels[i] = _sensors[i]->ADChannel();
    }
    _nchan = _sensors.size();    
    
    isOK &= adcard.InitMultichannel(_devPath, FORCE_ACQUISITION_MODE_BURST, _integrationTimeMs, _samplingFrequency, &_channels[0], _nchan);
    
    //isOK &= adcard.Init(_devPath, FORCE_ACQUISITION_MODE_BURST, _integrationTimeMs, _samplingFrequency);
    printf("_devPath = %s\n", _devPath.toLocal8Bit().data());
    printf("_integrationTimeMs = %f\n", _integrationTimeMs);
    printf("_samplingFrequency = %d\n", _samplingFrequency);            
    if (isOK)
    {
        printf("OK, card initialized\n");
    }
    else
    {
        printf("ERROR, card not initialized\n");
    }    
    return isOK;
}

bool ComediHardwareController::GetConsistentSamplingFrequency(double * samplingFrequency)
{
    return false; //false means not consistent sampling frequency (or no sensors)
}

bool ComediHardwareController::GetConsistentIntegrationTime(double * integrationTimeMs)
{
    return false; //false means no consistent integration time (or no sensors)
}

///TODO: allow more channels to be sampled, remember about timestamps
bool ComediHardwareController::GetVoltage(int adchannel, double& voltage) 
{
   //TODO: get voltage from adchannel
   //adcard.GetVoltage(voltage);   
    
   //TODO: everytime a single sensor get voltage all other sensors are sampled, do something about this.
   bool isOK = adcard.GetVoltageMultichannel(_voltages);
   /*printf("_channelMap.value(adchannel) = %d\n", _channelMap.value(adchannel));
   for (int i=0; i<_voltages.size(); i++)
   {
       printf("_voltages[%d] = %f \t", i, _voltages[i]);
   }
   printf("\n");*/
   voltage = _voltages[_channelMap.value(adchannel)];
   return isOK;
}


bool ComediHardwareController::RegisterSensorForSampling(Sensor* sensor)
{    
    printf("ComediHardwareController::RegisterSensorForSampling(Sensor* sensor)\n");
    if (!sensor)
    {
        printf("ERROR: trying to register sensor with a null pointer\n");
    }
    //check if exists already       
    bool found = false;
    for (int i=0; i< _nchan; i++)
    {
        if (_channels[i] == sensor->ADChannel())
        {
            found = true;
            printf("ERROR: trying to register a sensor to already taken AD channel\n");
            return false; //this means someone is trying to register the same sensor twice, or two different sensor to the same channel
        }
    }
    if (!found)
    {        
        _channels[_nchan] = sensor->ADChannel(); //channel number addressed by channel index
        _channelMap.insert(sensor->ADChannel(), _nchan); //channel index adressed by channel number        
        _nchan++;        
    }        
    
    printf("ComediHardwraeController: the following sensors are registered for sampling:\n");
    for (int i=0; i<_nchan; i++)
    {
        printf("_channels[%d] == %d\n", i, _channels[i]);
        
        printf("_channelMap[%d] == %d\n", _channels[i], _channelMap.value(_channels[i]));
    }
    return true;
}