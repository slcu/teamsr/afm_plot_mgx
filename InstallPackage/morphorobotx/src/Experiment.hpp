/* 
 * File:   Experiment.hpp
 * Author: cfmadmin
 *
 * Created on December 11, 2012, 6:01 PM
 */

#ifndef EXPERIMENT_HPP
#define	EXPERIMENT_HPP

#include "FileManager.hpp"
#include "StateManager.hpp"
#include "HardwareManager.hpp"
#include "DataLogger.hpp"
#include "Factory.hpp"
#include "Robot.hpp"

//class Robot;
//class HardwareManager;

class Experiment {
public:
    Experiment();
    /*Experiment(const Experiment& orig);*/
    virtual ~Experiment();
    /**
     * <Initializes the experiment>
     * @return true for success, false for failure
     */
    bool Initialize();
    bool Run();
    QString getExperimentDirAbsolute()
    {
        return _fileManager.getExperimentDirAbsolute();
    }
    FileManager& fileManager();
    StateManager& stateManager();
     ///needs to be able to initialize logger with path suppplied by fileManager    
    HardwareManager& hardwareManager()  //TODO: can we avoid making it public .. ?
    {
        return _hardwareManager;
    }
    Robot& robot();
    
    unsigned long TimeElapsedMs()
    {
        return _timer.TimeElapsedMs();
    }
    
    const Parms& parms()
    {
        return _parms;
    }
        
protected:
   FileManager _fileManager;
   StateManager _stateManager;       
   Timer _timer;
   HardwareManager _hardwareManager;
   Robot _robot;
   ProtocolPtr protocol;  
   Parms _parms;
};

#endif	/* EXPERIMENT_HPP */

