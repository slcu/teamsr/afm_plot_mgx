/* 
 * File:   HardwareManager.hpp
 * Author: cfmadmin
 *
 * Created on February 1, 2013, 7:14 PM
 */

#ifndef HARDWAREMANAGER_HPP
#define	HARDWAREMANAGER_HPP

#include <vector>
#include "MCSControl.h"
#include "QHash"

class ActuatorAxis;
class ActuatorSystem;
class StateManager;
class Experiment;
class Robot;
class HardwareController;
class Parms;
class Axis; //TODO: implement
class Sensor; //TODO: implement

class HardwareManager {
public:
    HardwareManager(Experiment& experiment); //TODO: does it really need the state manager to be passed this way ? why not through Experiment ?
    //HardwareManager(const HardwareManager& orig);
    virtual ~HardwareManager();
    bool AcquireHardware();   //TODO: better name? //TODO: add parameter: a list of systems to acquire
    //TODO: Zaber is a bit more difficult, it needs to be initialized to say which type it is...
    //codes are here: www.zaber.com/support/?tab=Device ids
 //   ActuatorAxis* GetAxisPointer(unsigned int systemID, unsigned int localAxisID); // returns null if nothing found
    bool BuildRobot(const Parms& parms, Robot& robot);
    StateManager& stateManager()
    {
        return _stateManager;
    }    
    
protected:
    StateManager& _stateManager;
    bool AcquireSmaractDevices();
    bool AcquireZaberDevices();
    bool QueryComediDevices();
    std::vector<ActuatorSystem*> _actuatorSystems;  ///vector of pointers. Cannot store by value, because ActuatorSystem is abstract.
    std::vector<HardwareController*> _hardwareControllers;
    std::vector<Axis*> _axes;
    std::vector<Sensor*> _sensor;
    QHash<QString, HardwareController*> _hardwareControllersHash;
};

#endif	/* HARDWAREMANAGER_HPP */

