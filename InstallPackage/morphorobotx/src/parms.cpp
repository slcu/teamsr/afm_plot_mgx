// Parameter reader class
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <cctype>
#include <algorithm>
#include <iterator>
#include "parms.hpp"
#include <qdir.h>

#ifdef __unix__
#  include <fcntl.h>
#  include <sys/file.h>
#  include <sys/stat.h>
#endif

QTextStream Parms::err(stderr);

using std::ostream_iterator;
using std::copy;

Parms::Parms(int vl )
    : loaded(true)
{
    verboseLevel( vl );
}

Parms::Parms(QString parmFile, int vl )
{
    verboseLevel( vl );
    loaded = read(parmFile);
}

bool Parms::read(QString parmFile)
{
#ifdef __unix__
    // First, obtain the lock
    QByteArray ba = parmFile.toLocal8Bit();
    int fd = open(ba.data(), O_RDONLY);
    flock(fd, LOCK_SH);
#endif
    QFile fIn(parmFile);
    if(!fIn.open(QIODevice::ReadOnly))
    {
        if( VerboseLevel > 0 )
            err << "Parms::Parms:Error opening " << parmFile << endl;
        return false;
    }
    QTextStream ss(&fIn);
    unsigned int line = 0;
    int pos;
    QString buff;
    QStringList current_comment;
    QString section;
    while(!ss.atEnd() and ss.status() == QTextStream::Ok) {
        line++;
        // read in line
        buff = ss.readLine();
        // find INI style comments
        pos = buff.indexOf(";");
        // and remove to end of line
        if(pos != -1)
        {
            current_comment << buff.mid(pos+1);
            buff = buff.mid(0, pos);
        }
        // remove leading and trailing whitespace
        buff = buff.trimmed();
        // skip line if blank
        if(buff.length() == 0)
            continue;
        // Look for section
        if(buff[0] == '[' && buff.right(1)[0] == ']') {
            section = buff.mid(1, buff.length() - 2);
            if(( section.length() == 0) && ( VerboseLevel > 0 ) )
                err << "Parms::Parms:Error on line " << line << ", []" << endl;
            if(!current_comment.empty())
                Parameters[section].comment = current_comment;
            current_comment.clear();
            continue;
        }
        // split key and value
        pos = buff.indexOf("=");
        // error if no : delimiter
        if(pos == -1) {
            if( VerboseLevel > 0 )
                err << "Parms::Parms:Error on line " << line << ", missing :" << endl;
            continue;
        }
        // get key and value and remove leading/trailing blanks
        QString key = buff.mid(0, pos).trimmed();
        QString value = buff.mid(pos + 1).trimmed();
        // error if no key
        if(key.length() == 0) {
            if( VerboseLevel > 0 )
                err << "Parms::Parms:Error on line " << line << ", missing key" << endl;
            continue;
        }
        // error if no value
        if(value.length() == 0) {
            if( VerboseLevel > 2 )
                err << "Parms::Parms:Warning on line "<< line << ", missing value" << endl;
        }

        // Now we have key and value, add to map
        Parameters[section][key] << value_t(value, current_comment);
        current_comment.clear();
    }
#ifdef __unix__
    // At last, release the lock
    flock(fd, LOCK_UN);
#endif
    return true;
}

bool Parms::write(QString parmFile) const
{
    QFile file(parmFile);
    if(!file.open(QIODevice::WriteOnly))
    {
        if(VerboseLevel > 0)
            err << "Parms::write:Error, cannot open file '" << parmFile << "' for writing" << endl;
        return false;
    }
    QTextStream ts(&file);
    QStringList section_list = Parameters.keys();
    section_list.sort();
    foreach(QString section, section_list)
    {
        const section_t& section_content = Parameters.value(section);
        foreach(const QString& l, section_content.comment)
            ts << ";" << l << endl;
        ts << QString("[%1]").arg(section) << endl;
        QStringList key_list = section_content.keys();
        key_list.sort();
        foreach(QString key, key_list)
        {
            const values_t& values = section_content.value(key);
            foreach(const value_t& val, values)
            {
                foreach(const QString& l, val.comment)
                    ts << ";" << l << endl;
                ts << QString("%1 = %2").arg(key).arg(val.value) << endl;
            }
        }
        ts << endl;
    }
    file.close();
    return true;
}

Parms::~Parms()
{
}

/*
   void removeWhitespace(string &s)
   {
   size_t pos = s.find_first_not_of(" \t\r\n");
   if(pos == string::npos)
   s = "";
   else {
   s = s.substr(pos, string::npos);
   pos = s.find_last_not_of(" \t\r\n");
   if(pos != string::npos)
   s = s.substr(0, pos + 1);
   }
   }
   */

bool Parms::operator()( QString section, QString key, bool& value ) const
{
    return operator()<bool>( section, key, value );
}

bool Parms::operator()( QString section, QString key, int& value ) const
{
    return operator()<int>( section, key, value );
}

bool Parms::operator()( QString section, QString key, float& value ) const
{
    return operator()<float>( section, key, value );
}

bool Parms::operator()( QString section, QString key, double& value ) const
{
    return operator()<double>( section, key, value );
}

bool Parms::operator()( QString section, QString key, std::string& value ) const
{
    return operator()<std::string>( section, key, value );
}

bool Parms::operator()( QString section, QString key, QString & value ) const
{
    return operator()<QString>( section, key, value );
}

bool Parms::set(QString section, QString key, const bool& value, const QStringList& comment)
{
    return set<bool>(section, key, value, comment);
}
bool Parms::set(QString section, QString key, const int& value, const QStringList& comment)
{
    return set<int>(section, key, value, comment);
}
bool Parms::set(QString section, QString key, const float& value, const QStringList& comment)
{
    return set<float>(section, key, value, comment);
}
bool Parms::set(QString section, QString key, const double& value, const QStringList& comment)
{
    return set<double>(section, key, value, comment);
}
bool Parms::set(QString section, QString key, const std::string& value, const QStringList& comment)
{
    return set<std::string>(section, key, value, comment);
}
bool Parms::set(QString section, QString key, QString value, const QStringList& comment)
{
    return set<QString>(section, key, value, comment);
}

bool Parms::add(QString section, QString key, const bool& value, const QStringList& comment)
{
    return add<bool>(section, key, value, comment);
}
bool Parms::add(QString section, QString key, const int& value, const QStringList& comment)
{
    return add<int>(section, key, value, comment);
}
bool Parms::add(QString section, QString key, const float& value, const QStringList& comment)
{
    return add<float>(section, key, value, comment);
}
bool Parms::add(QString section, QString key, const double& value, const QStringList& comment)
{
    return add<double>(section, key, value, comment);
}
bool Parms::add(QString section, QString key, const std::string& value, const QStringList& comment)
{
    return add<std::string>(section, key, value, comment);
}
bool Parms::add(QString section, QString key, QString value, const QStringList& comment)
{
    return add<QString>(section, key, value, comment);
}

bool Parms::setAll( QString section, QString key, const std::vector<bool>& value, const QList<QStringList>& comments )
{
    return setAll<std::vector<bool> >( section, key, value, comments );
}
bool Parms::setAll( QString section, QString key, const std::vector<int>& value, const QList<QStringList>& comments )
{
    return setAll<std::vector<int> >( section, key, value, comments );
}
bool Parms::setAll( QString section, QString key, const std::vector<float>& value, const QList<QStringList>& comments )
{
    return setAll<std::vector<float> >( section, key, value, comments );
}
bool Parms::setAll( QString section, QString key, const std::vector<double>& value, const QList<QStringList>& comments )
{
    return setAll<std::vector<double> >( section, key, value, comments );
}
bool Parms::setAll( QString section, QString key, const std::vector<QString>& value, const QList<QStringList>& comments )
{
    return setAll<std::vector<QString> >( section, key, value, comments );
}
bool Parms::setAll( QString section, QString key, const QStringList& value, const QList<QStringList>& comments )
{
    return setAll<QStringList>( section, key, value, comments );
}

bool Parms::all( QString section, QString key, std::vector<bool>& value ) const
{
    return all<std::vector<bool> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<int>& value ) const
{
    return all<std::vector<int> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<float>& value ) const
{
    return all<std::vector<float> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<double>& value ) const
{
    return all<std::vector<double> >( section, key, value );
}
bool Parms::all( QString section, QString key, std::vector<QString>& value ) const
{
    return all<std::vector<QString> >( section, key, value );
}
bool Parms::all( QString section, QString key, QStringList& value ) const
{
    return all<QStringList>( section, key, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<bool> >& value ) const
{
    return all<bool>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<int> >& value ) const
{
    return all<int>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<float> >& value ) const
{
    return all<float>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<double> >& value ) const
{
    return all<double>( section, value );
}

bool Parms::all( QString section, QHash<QString, std::vector<QString> >& value ) const
{
    return all<QString>( section, value );
}

bool Parms::all( QString section, QHash<QString, QStringList>& value ) const
{
    return all<QString>( section, value );
}

bool Parms::readValue( QString raw_value, bool& variable ) const
{
    QString value = raw_value.toLower();
    if( value == "true" )
    {
        variable = true;
        return true;
    }
    else if( value == "false" )
    {
        variable = false;
        return true;
    }
    return false;
}

bool Parms::readValue( QString value, QString& variable ) const
{
    variable = value;
    return true;
}

bool Parms::readValue( QString value, std::string& variable ) const
{
    variable = value.toStdString();
    return true;
}

bool Parms::writeValue(QString& raw, const bool& variable)
{
    raw = (variable ? "true" : "false");
    return true;
}

bool Parms::writeValue( QString& value, QString variable )
{
    value = variable;
    return true;
}

bool Parms::writeValue( QString& value, const std::string& variable )
{
    value = QString::fromStdString(variable);
    return true;
}

bool Parms::extractValues( QString section, QString key, QStringList& values, bool checkExist ) const
{
    parameters_t::const_iterator found_section = Parameters.find(section);
    if(found_section != Parameters.end())
    {
        section_t::const_iterator found = found_section->find(key);
        if(found != found_section->end())
        {
            values.clear();
            foreach(const value_t& v, *found)
                values << v.value;

            if( VerboseLevel > 3 )
            {
                err << "Parms::extractValues:Debug strings for key [" << section << "]" << key << ": -"
                    << values.join("-") << endl;
            }
            return true;
        }
    }

    if( checkExist && ( VerboseLevel > 0 ) )
        err << "Parms::operator():Error key not found [" << section << "]"
            << key << endl;
    return false;
}

bool Parms::setValues( QString section, QString key, const values_t& values )
{
    Parameters[section][key] = values;
    return true;
}

bool Parms::addValue( QString section, QString key, const value_t& value )
{
    Parameters[section][key] << value;
    return true;
}

bool Parms::setComment(QString section, const QString &comment)
{
    return setComment(section, comment.split("\n"));
}

bool Parms::setComment(QString section, const QStringList &comment)
{
    parameters_t::iterator found = Parameters.find(section);
    if(found == Parameters.end())
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << QString("Error, trying to set the comments on section that doesn't exist: [%1].").arg(section) << endl;
        return false;
    }
    found.value().comment = comment;
    return true;
}

bool Parms::setComment(QString section, QString key, const QString &comment)
{
    return setComment(section, key, comment.split("\n"));
}

bool Parms::setComment(QString section, QString key, const QStringList &comment)
{
    parameters_t::iterator found = Parameters.find(section);
    if(found == Parameters.end())
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << QString("Error, trying to set the comments on a key that doesn't exist: [%1]%2.").arg(section).arg(key) << endl;
        return false;
    }
    section_t::iterator found2 = found.value().find(key);
    if(found2 == found.value().end())
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << QString("Error, trying to set the comments on a key that doesn't exist: [%1]%2.").arg(section).arg(key) << endl;
        return false;
    }
    values_t& vals = found2.value();
    vals.back().comment = comment;
    return true;
}

QStringList Parms::listSections() const
{
    return Parameters.keys();
}

bool Parms::setComments(QString section, QString key, const QList<QStringList> &comments)
{
    parameters_t::iterator found = Parameters.find(section);
    if(found == Parameters.end())
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << QString("Error, trying to set the comments on a key that doesn't exist: [%1]%2.").arg(section).arg(key) << endl;
        return false;
    }
    section_t::iterator found2 = found.value().find(key);
    if(found2 == found.value().end())
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << QString("Error, trying to set the comments on a key that doesn't exist: [%1]%2.").arg(section).arg(key) << endl;
        return false;
    }
    values_t& vals = found2.value();
    if(vals.size() != comments.size())
    {
        if(VerboseLevel >= ERRORS_ONLY)
            err << QString("Error, key [%1]%2 has %3 values, but %4 comments were provided.").arg(section).arg(key).arg(vals.size()).arg(comments.size()) << endl;
        return false;
    }
    for(int i = 0 ; i < vals.size() ; ++i)
        vals[i].comment = comments[i];
    return true;
}

QTextStream& operator>>(QTextStream& ss, bool& b)
{
    QString val;
    ss >> val;
    val.toLower();
    b = (val == "true");
    return ss;
}

QTextStream& operator<<(QTextStream& ss, bool b)
{
    ss << (b ? "true" : "false");
    return ss;
}
