#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <QHash>
#include <QString>
#include <memory>
//#include "Context.hpp"
#include <stdlib.h>
//#include "MeasurementState.hpp"
#include "parms.hpp"

class Experiment;

/**
 * \class Protocol Factory.hpp "Factory.hpp"
 *
 * Abstract base class of the protocoles.
 */
struct Protocol
{
  /**
   * Constructor, with the shared experiment
   */
  Protocol(Experiment& experiment, QString sectionName);
  /**
   * Virtual destructor
   *
   * This is mandatory but should be left empty, and will probably not be overriden.
   */
  virtual ~Protocol() { };
  /**
   * Run method
   *
   * This is the main entry point of a protocol, in which the magic should happen.
   * You have to override this method to be able to create a protocol.
   */
  virtual bool run() = 0;
  //virtual void run(Context& context) = 0;
  
  /**
   * Function that will read the parameters from the given object and will create/get its subprotocols
   */
  virtual bool Init(const Parms& parms) = 0;
  
  /**
   * Function that will write extra parameters to the object
   */
  virtual bool writeParameters(Parms& parms) const = 0;
  
  Experiment& experiment() const
  {
      return _experiment;
  };
  
  QString sectionName()
  {
      return _sectionName;
  };
  
protected:
    Experiment& _experiment;
    QString _sectionName;///refers to section name in the param file
};

typedef std::shared_ptr<Protocol> ProtocolPtr;

/**
 * \class ProtocolMaker Factory.hpp "Factory.hpp"
 *
 * Abstract base class for protocol creators. The sub-classes should know how to
 * create a specific kind of protocol.
 */
struct ProtocolMaker
{
  virtual ~ProtocolMaker() { }
  virtual std::shared_ptr<Protocol> create(Experiment& experiment, QString sectionName) = 0;
};

/**
 * \class Factory Factory.hpp "Factory.hpp"
 *
 * Main factory class. This is the interface through which all the protocoles
 * should be created.
 */
class Factory
{
  /**
   * Creation of the factory is a private matter
   */
  Factory();

  /**
   * Forbid copy of the factory
   */
  Factory(const Factory& copy);

  /**
   * Dictionnary of all the known protocoles
   */
  QHash<QString, std::shared_ptr<ProtocolMaker> > protocoles;
  QHash<QString, std::shared_ptr<Protocol> > protocolInstances;
public:
  /**
   * Get/create the singleton instance of the factory
   */
  static Factory& instance();

  /**
   * Register a new protocol
   * \param name Name of the protocol
   * \param proc Protocol maker of the specific protocol type
   * \returns False if the protocol name is already in use
   */
  bool register_protocol(QString name, std::shared_ptr<ProtocolMaker> proc);
  /**
   * Un-register a protocol
   * \param name Name of the protocol
   * \param proc Instance of the protocol maker to remove
   * \return False if the name doesn't exist or if \c proc doesn't correspond to this name
   */
  bool unregister_protocol(QString name, std::shared_ptr<ProtocolMaker> proc);

  /**
   * Get a protocol
   * \param experiment Experiment running the protocol
   * \param sectionName section name
   * \returns A shared pointer to the protocol (a newly created one, or taken from the available pool)
   */
  std::shared_ptr<Protocol> protocol(Experiment& experiment, QString sectionName);//reduce the number of parameters, this calles create if necessary
  
  /**
   * Create a protocol
   * \param sectionName The section name of parms file describing the protocol
   * \param experiment Experiment running the protocol
   * \returns A shared pointer to the newly created protocol
   */   
  std::shared_ptr<Protocol> create(QString sectionName, Experiment& experiment);
  
  
};

/**
 * Short-cut to access the factory
 */
inline Factory& factory() { return Factory::instance(); }

/**
 * \class TypedProtocolMaker Factory.hpp "Factory.hpp"
 * Class template for a protocol maker to make a protocol of type \c P
 */
template <typename P>
struct TypedProtocolMaker : public ProtocolMaker
{
  /**
   * Overriden method to create a protocol of type \c P
   */
  virtual std::shared_ptr<Protocol> create(Experiment& experiment, QString sectionName) { return std::make_shared<P>(experiment, sectionName); }
};

/**
 * \class ProtocolRegistration Factory.cpp "Factory.hpp"
 * Class used to register a protocol at load time, and then un-register it at unload time.
 * This class is meant to be used through the \c REGISTER_PROTOCOL macro.
 */
template <typename P>
struct ProtocolRegistration
{
  /**
   * Type of the protocol created
   */
  typedef P ProtocolType;
  /**
   * Type of the protocol make
   */
  typedef TypedProtocolMaker<P> Maker;
  /**
   * Name of the protocol created
   */
  QString name;
  /**
   * Instance of the protocol maker
   */
  std::shared_ptr<Maker> maker;

  /**
   * Register the protocol at instanciation time.
   *
   * If the registration fails, aborts the application.
   */
  ProtocolRegistration(QString n)
    : name(n)
    , maker(std::make_shared<Maker>())
  {
    if(!factory().register_protocol(name, maker))
      ::abort();
  }

  /**
   * Unregister the protocol at desctruction time.
   *
   * If the un-registration fails, aborts the application.
   */
  ~ProtocolRegistration()
  {
    if(!factory().unregister_protocol(name, maker))
      ::abort();
  }

};

/**
 * \def REGISTER_PROTOCOL(name, ProtocolClass)
 * Register the class \c ProtocolClass under the name \c name
 */
#define REGISTER_PROTOCOL(name, ProtocolClass) \
  ProtocolRegistration<ProtocolClass> ProtocolClass##_registration(name)

#endif // FACTORY_HPP

