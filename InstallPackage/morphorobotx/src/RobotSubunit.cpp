/* 
 * File:   RobotSubunit.cpp
 * Author: cfmadmin
 * 
 * Created on March 1, 2013, 7:34 PM
 */

#include "RobotSubunit.hpp"
#include "HardwareController.hpp"
#include "StateManager.hpp"

RobotSubunit::RobotSubunit(HardwareController* hardwareController):_stateManager(hardwareController->stateManager()){
    _itsHardwareController = hardwareController;
}

RobotSubunit::~RobotSubunit() {
}

