/* 
 * File:   SensorFactory.cpp
 * Author: cfmadmin
 * 
 * Created on March 1, 2013, 10:34 PM
 */

#include "SensorFactory.hpp"
#include "parms.hpp"
#include "ForceSensor.hpp"

SensorFactory::SensorFactory() {
}

SensorFactory::~SensorFactory() {
}

Sensor* SensorFactory::sensor(const Parms& parms, QString sensorName, HardwareController* hardwareController)
{
    printf("instide SensorFactory::sensor\n");
    printf("trying to build sensor named: %s\n",  sensorName.toLocal8Bit().data());
    
    Sensor* sensorPtr;
    QString sensorTypeName;
    QString sensorSectionName;
    bool isOK = true;
    
    isOK &= parms(sensorName, "SensorSectionName", sensorSectionName);
    isOK &= parms(sensorSectionName, "Type", sensorTypeName);        
    if (!isOK)
    {
        printf("Could not parse RobotRecipe -> (...) -> %s -> Type \n", sensorName.toLocal8Bit().data());
        return 0; //null pointer
    }     
    printf("sensorName = %s\n", sensorName.toLocal8Bit().data());
    if (sensorTypeName.toLower() == "forcesensor")
    {       
        sensorPtr = new ForceSensor(parms, sensorName, hardwareController);
    } 
    else
    {        
       //support more types here, if needed
       printf("Error: sensor type %s unsupported\n", sensorTypeName.toLocal8Bit().data());
       sensorPtr = 0;
    }        
    return sensorPtr;            
}
