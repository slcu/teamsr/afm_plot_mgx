/* 
 * File:   SmarActActuatorAxis.cpp
 * Author: cfmadmin
 * 
 * Created on February 4, 2013, 9:41 PM
 */

#include "SmarActActuatorAxis.hpp"
#include "SmarActAxisStatus.hpp"




SmarActActuatorAxis::SmarActActuatorAxis(ActuatorSystem* actuatorSystem, StateManager& stateManager, unsigned int channelType, unsigned int sensorType, unsigned int localID):  
        ActuatorAxis(actuatorSystem, stateManager, localID), _channelType(channelType), _sensorType(sensorType)
{
    _isSensorMissing = true;
    holdMs = 100; //default 100 ms hold time for closed-loop commands, can be changed by calling SetHoldMs(unsigned int holdMsValue)
    _marginMicrons = 0.012; //12 nm default, you can change it by calling SetMarginMicrons(double marginMicrons)
}

/*SmarActActuatorAxis::SmarActActuatorAxis(const SmarActActuatorAxis& orig) {
}*/

SmarActActuatorAxis::~SmarActActuatorAxis() {
}

bool SmarActActuatorAxis::GotoAsync(double positionMicrons)
{
    SA_STATUS status = SA_OK;    
    Command_SA_GotoPositionAbsolute_S cmd;
    //printf("called GotoAsync...\n");
    cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), positionMicrons*1000, holdMs);
    if (cmd.Failed())    
    {   
        if (cmd.RetryOnceAllowed())
        {// printf("retry send...\n");
            cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), positionMicrons*1000, holdMs);
        }//TODO: rubbish comes out of it...
        if (cmd.Failed())
        {
            printf("ERROR: %s\n", cmd.GetStatusDescription().toLocal8Bit().data());        
            return false;
        }
    }    
    return true;
}

bool SmarActActuatorAxis::GetPositionSync(double& positionMicrons) //TODO: rubbish comes out of it...
{
    Command_SA_GetPosition_S cmd_getpos;
    bool repeat = true;
        
    signed int actualPositionNanometers;
    
    cmd_getpos.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), &actualPositionNanometers);
    
    if (cmd_getpos.Failed())
    {
        if (cmd_getpos.RetryOnceAllowed())
        {
            cmd_getpos.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), &actualPositionNanometers);
            if (cmd_getpos.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    //printf("got signed int actualPositionNanometers: %d\n", actualPositionNanometers);
    
    positionMicrons = actualPositionNanometers/1000.0; 
    
    //printf("after conversion to double microns: %f\n", positionMicrons);
    return true;
}

bool SmarActActuatorAxis::GetStatusSync(unsigned int& status) //TODO: consider if SmarActAxisStatus fits better than unsigned int
{
    //TODO: implement    
    unsigned int axisStatus;        
    SmarActAxisStatus s;
    Command_SA_GetStatus_S cmd;    
    cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), &axisStatus);
    if (cmd.Failed())
    {
        if (cmd.RetryOnceAllowed())
        {
            cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), &axisStatus);
            if (cmd.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }                   
    s.SetStatus(axisStatus);
    status = axisStatus; //TODO: consider returning s as output
    //printf("checking axis status: %s\n", s.GetName().toLocal8Bit().data()); //TODO: ugly, this        
    return true;        
}

bool SmarActActuatorAxis::GotoSync(double positionMicrons)
{
    bool result = GotoAsync(positionMicrons);
    Timer timer;    
    if (result)
    {   
        unsigned int axisStatus;        
        SmarActAxisStatus s;
        Command_SA_GetStatus_S cmd;
        timer.Reset();
        do
        {
            if (!GetStatusSync(axisStatus))
            {
                return false;
            }
            s.SetStatus(axisStatus);
            //printf("checking axis status: %s\n", s.GetName().toLocal8Bit().data()); //TODO: ugly, this will output the whole story
            if (timer.TimeElapsedMs() > 60000)
            {
                printf("Timer elapsed, give up waiting...\n");
                //TODO: call SA_Stop_S to make sure it doesn't try to move
                break;                
            }
        }
        while( (axisStatus != SA_STOPPED_STATUS)&&(axisStatus != SA_HOLDING_STATUS) );        //TODO: ugly, difficult to read        
        //TODO: add timeout        
        //TODO: wait until position achieved
        //signed int actualPositionNanometers;
        double actualPositionMicrons;
        Command_SA_GetPosition_S cmd_getpos;
        bool repeat = true;
        timer.Reset();
        do
        {
            if (!GetPositionSync(actualPositionMicrons))
            {
                return false;
            }
            //printf("checking position: %f\n", actualPositionMicrons);
            if (fabs(positionMicrons - actualPositionMicrons) < _marginMicrons) //TODO: check if abs is the right function to call (double vs. int)
            {
                repeat = false;
            }
            if (timer.TimeElapsedMs() > 100)
            {
                printf("Timer elapsed, give up waiting...\n");
                repeat = false;
            }
        }while(repeat); //TODO: add timeout
        StopSync(); //to make sure it's not moving, holding or whatever
    }
    else
    {
        return false;
    }
    return true;
}
/*
bool SmarActActuatorAxis::SetOption(AxisOption option, double value)
{
    switch(option)
    {
        case (AxisOption::ACCELERATION_LIMIT_MICRONS_PER_SECOND_SQUARE):
            return false;
            break;
            
        case (AxisOption::GOTO_HOLD_MS):
            SetHoldMs(value);            
            break;
            
        case (AxisOption::GOTO_PRECISION_MARGIN_MICRONS):
            SetMarginMicrons(value);
            break;
            
        case (AxisOption::GOTO_TIMEOUT_MS):
            return false;
            break;
            
        case (AxisOption::SPEED_LIMIT_MICRONS_PER_SECOND):
            return SetClosedLoopMoveSpeed(value);
            break;
            
        case (AxisOption::FREQUENCY_LIMIT_HZ):
            return SetClosedLoopMaxFrequency(value);
            break;
            
        default:            
            return false;
    }
    return true;
}
*/
bool SmarActActuatorAxis::StopSync()
{
    Command_SA_Stop_S cmd_stop;
    Timer timer;
    cmd_stop.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID());
    if (cmd_stop.Failed())
    {
        if (cmd_stop.RetryOnceAllowed())
        {
            cmd_stop.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID());
            if (cmd_stop.Failed())
            {
                return false;
            }
        }
    }
    unsigned int axisStatus;        
    Command_SA_GetStatus_S cmd;
    SmarActAxisStatus s;
    timer.Reset();
    do
    {
        if (!GetStatusSync(axisStatus))
        {
            return false;
        } 
        s.SetStatus(axisStatus);
        //printf("checking axis status: %s\n", s.GetName().toLocal8Bit().data()); //TODO: ugly, this will output the whole story
        if (timer.TimeElapsedMs() > 1000)
        {
            printf("Timer elapsed, give up waiting...\n");
            //TODO: call SA_Stop_S to make sure it doesn't try to move
            return false; 
            break;                            
        }
    }
    while( (axisStatus != SA_STOPPED_STATUS) );    
    return true;
}

void SmarActActuatorAxis::SetHoldMs(unsigned int holdMsValue)
{
    this->holdMs = holdMsValue;            
}
void SmarActActuatorAxis::SetMarginMicrons(double marginMicrons) //defines precision margin within which the Goto must arrive
{
    _marginMicrons = marginMicrons;
}
bool SmarActActuatorAxis::SetClosedLoopMaxFrequency(double frequencyHz)
{   
    Command_SA_SetClosedLoopMaxFrequency_S cmd;    
    cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), frequencyHz);
    if (cmd.Failed())
    {
        if (cmd.RetryOnceAllowed())
        {
            cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), frequencyHz);
            if (cmd.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }                   
    return true;
}

bool SmarActActuatorAxis::SetClosedLoopMoveSpeed(double speedMicronsPerSecond)
{
    Command_SA_SetClosedLoopMoveSpeed_S cmd;    
    cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), speedMicronsPerSecond*1000);
    if (cmd.Failed())
    {
        if (cmd.RetryOnceAllowed())
        {
            cmd.Send(itsActuatorSystem()->LocalSystemID(), this->LocalID(), speedMicronsPerSecond*1000);
            if (cmd.Failed())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }                   
    return true;
}

bool SmarActActuatorAxis::RegisterFields()
{
    bool isOK = true;
    printf("called register fields on SmarActActuatorAxis named %s\n", this->_name.toLocal8Bit().data());
    QString newName = _name+QString("_position");
    printf("trying to register name %s\n", newName.toLocal8Bit().data());
    _fieldIDPositionMicrons = _stateManager.RegisterFieldExc(newName, QString("(um)"), false);//last param is volatile yes/no
    if (_fieldIDPositionMicrons < 0)
    {
        printf("ERROR registering field\n");
        printf("name was %s\n", _name.toLocal8Bit().data());
        isOK = false;
        return false;
    }     
    printf("field ID is %d\n", _fieldIDPositionMicrons);
    return isOK;
}

bool SmarActActuatorAxis::UpdateState()
{
    bool isOK = true;
    double positionMicrons;
    isOK &= GetPositionSync(positionMicrons);
    if (!isOK)
    {
        return false;
    }
    isOK &= _stateManager.state().Update(_fieldIDPositionMicrons, positionMicrons);
    return isOK;
}