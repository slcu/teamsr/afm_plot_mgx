/* 
 * File:   Experiment.cpp
 * Author: cfmadmin
 * 
 * Created on December 11, 2012, 6:01 PM
 */

#include "Experiment.hpp"

Experiment::Experiment(): _hardwareManager(*this), _robot(Robot(*this)) {
    _timer.Reset();
}

Experiment::~Experiment() {
}

FileManager& Experiment::fileManager()
{
    return _fileManager;
}

StateManager& Experiment::stateManager()
{
    return _stateManager;
}

Robot& Experiment::robot()
{
    return _robot;
}

bool Experiment::Initialize()
{    
    int verboseLevel = 3;
    //Parms par("parameters.ini", verboseLevel);
    _parms.read("parameters.ini");
    Parms& par = _parms; //TODO: refactor the whole method (rename par to _parms)
    
    if (!par.isLoaded())
    {
        printf("ERROR: Parameters not loaded\n");
        return false;
    }
    
          
    //TODO: read and parse robot recipe, and then acquire hardware accordingly
    //TODO: include hardware descriptor to know which sort of device it is (and which library to call)
    Parms robotRecipePar("robotRecipe.ini", verboseLevel);
    if (!robotRecipePar.isLoaded())
    {
        printf("ERROR: Robot recipe parameters not loaded loaded\n");
        return false;
    }
    /*RobotRecipe robotRecipe;    
    if (!_robot.ParseRecipeFromIni(robotRecipePar, robotRecipe)) //TODO: move that method to robotRecipe
    {
        printf("ERROR: Robot::ParseRecipeFromIni failed. Could not parse robot recipe\n");
        return false;
    } */   
    
    if (!_hardwareManager.BuildRobot(robotRecipePar, _robot))
    {
        printf("ERROR: HardwareManager::BuildRobot failed.\n");
        return false;
    }
        
    /*    
    //printf("recipe parsed, there are %d entries\n", robotRecipe.entries.size()); //TODO: remove it            
    if (!_hardwareManager.AcquireHardware()) //TODO: acquire only what is needed, distinguish between smaract and other devices
    {
        printf("ERROR: hardwareManager.AcquireHardware() failed\n");
        return false;
    }             
    //TODO: build the robot from recipe        
    //TOOD: recipe holds the default values
    if (!_robot.BuildFromRecipe(robotRecipe))//TODO: hardware manager should build the robot based on the recipe
    {
        printf("ERROR: Robot::BuildFromRecipe failed\n");
        return false;
    }   
      */  
    printf("reading robot parameters...\n");
    //After the robot is built, parameters can be parsed. This happens after the robot is built in case there might be parameters referring to the parts being built.
    if(!_robot.readParameters(par)) //Robot subsystems, e.g. forceMeasurement can read their parameters here
    {
        printf("Error, cannot read robot parameters.\n");
        return false;
    }

    if (!_robot.Initialize())
    {
        printf("ERROR: robot.Initialize() failed\n");
        return false;
    }

    
    
    const QString protocolSectionName = "Protocol";
        
    QString protocolName;
    if(!par(protocolSectionName, "Name", protocolName))
    {
        printf("ERROR: no protocol name specified\n");
        return false;
    }
    
    QString protocolType;
    if(!par(protocolName, "Type", protocolType))
    {
        printf("ERROR: no protocol type specified in section '%s'\n", protocolName.toLocal8Bit().data());
        return false;
    }
            
    //ProtocolPtr proc = factory().protocol(protocolName, state);
    protocol = factory().protocol(*this, protocolName); //looks ugly, I take the value of this and pass it as a reference
    if(!protocol)
    {
        printf("Error, no protocol named '%s'.\n", protocolType.toLocal8Bit().data());
        return false;
    }    
    
    //if(!protocol->readParameters(par)) //if(!protocol->readParameters(par, protocolName))
    if(!protocol->Init(par))
    {
        printf("Error, cannot read protocol parameters. Init(parms) failed.\n");
        return false;
    }
    //TODO: here, let the fileManager work
    
    if(!_fileManager.readParameters(par))
    {
        printf("ERROR: FileManager.readParameters failed\n");
        return false;
    }
    
    //fileManager.getFirstAvailableExperimentPath(); // TODO: wrap it in prepare paths or something like that    
    if (!_fileManager.prepareNewDirectory())       
    {
        printf("ERROR: fileManager.prepareNewDirectory() failed\n");
        return false;
    }
    _stateManager.logger().SetFilename(_fileManager.getOutputDataPath()); //TODO: that's complicated. How to always remember that logger needs SetFilename ?
    
    printf("Checking what fileManager found:\n");
    printf("fileManager.getExperimentDirAbsolute() = %s\n", _fileManager.getExperimentDirAbsolute().toLocal8Bit().data());
    printf("fileManager.getCurrentSessionPath() = %s\n", _fileManager.getCurrentSessionPath().toLocal8Bit().data());
    printf("fileManager.getOutParamsPath() = %s\n", _fileManager.getOutParamsPath().toLocal8Bit().data());                   
  
    // **PBdR**
    // We need to think if the parameters may be influenced by running the process. If not, this is the best position.
    // If so, the writeParameters need to be placed after the proc->run
    if(!protocol->writeParameters(par)) 
    {
        printf("Error, cannot write protocol parameters.\n");
        return false;
    }
    

    // **PBdR** and here, we are writing the parameters to a file
    printf("_fileManager.getOutParamsPath() = %s\n", _fileManager.getOutParamsPath().toLocal8Bit().data());
    par.write(_fileManager.getOutParamsPath()); // TODO: check result
    robotRecipePar.write(_fileManager.getOutRobotRecipePath()); // TODO: check result
    
    
    return true;
}

bool Experiment::Run()
{    
    int retval = 0;    
    bool ok = protocol->run();   
    return ok;
}
