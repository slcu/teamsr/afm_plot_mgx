/* 
 * File:   ADCard.cpp
 * Author: huflejt
 * 
 * Created on November 6, 2012, 6:38 PM
 */

#include "ADCard.hpp"


ADCard::ADCard() {
    forceAcquisitionMode = FORCE_ACQUISITION_MODE_NONE;
}

/*ADCard::ADCard(const ADCard& orig) {
}*/

ADCard::~ADCard() {
}

void ADCard::init_parsed_options(parsed_options *options)
{
	memset(options, 0, sizeof(parsed_options));
	//options->filename = default_filename;
        options->filename = "";
	options->aref = AREF_GROUND;
	options->n_chan = 4;
	options->n_scan = 1000;
	options->freq = 10000.0;
	options->physical = 0;
	options->value = 0.;
};

char* ADCard::cmd_src(int src,char *buf)
{
	buf[0]=0;

	if(src&TRIG_NONE)strcat(buf,"none|");
	if(src&TRIG_NOW)strcat(buf,"now|");
	if(src&TRIG_FOLLOW)strcat(buf, "follow|");
	if(src&TRIG_TIME)strcat(buf, "time|");
	if(src&TRIG_TIMER)strcat(buf, "timer|");
	if(src&TRIG_COUNT)strcat(buf, "count|");
	if(src&TRIG_EXT)strcat(buf, "ext|");
	if(src&TRIG_INT)strcat(buf, "int|");
#ifdef TRIG_OTHER
	if(src&TRIG_OTHER)strcat(buf, "other|");
#endif

	if(strlen(buf)==0){
		sprintf(buf,"unknown(0x%08x)",src);
	}else{
		buf[strlen(buf)-1]=0;
	}

	return buf;
};

void ADCard::dump_cmd(FILE *out,comedi_cmd *cmd)
{
	char buf[100];

	fprintf(out,"subdevice:      %d\n",
		cmd->subdev);

	fprintf(out,"start:      %-8s %d\n",
		cmd_src(cmd->start_src,buf),
		cmd->start_arg);

	fprintf(out,"scan_begin: %-8s %d\n",
		cmd_src(cmd->scan_begin_src,buf),
		cmd->scan_begin_arg);

	fprintf(out,"convert:    %-8s %d\n",
		cmd_src(cmd->convert_src,buf),
		cmd->convert_arg);

	fprintf(out,"scan_end:   %-8s %d\n",
		cmd_src(cmd->scan_end_src,buf),
		cmd->scan_end_arg);

	fprintf(out,"stop:       %-8s %d\n",
		cmd_src(cmd->stop_src,buf),
		cmd->stop_arg);
};


    /*
 * This prepares a command in a pretty generic way.  We ask the
 * library to create a stock command that supports periodic
 * sampling of data, then modify the parts we want. */
bool ADCard::PrepareCmdLib(comedi_t* dev, int subdevice, int n_scan, unsigned scan_period_nanosec, comedi_cmd* cmd, FORCE_ACQUISITION_MODE mode)
{
        int ret;

	memset(cmd,0,sizeof(*cmd));

        
        printf("ADCard:PrepareCmdLib\n");
        printf("int subdevice %d\n", subdevice);
        printf("n_chan %d\n", this->n_chan);
        printf("scan_period_nanosec %d\n", scan_period_nanosec);
	/* This comedilib function will get us a generic timed
	 * command for a particular board.  If it returns -1,
	 * that's bad. */
	ret = comedi_get_cmd_generic_timed(dev, subdevice, cmd, this->n_chan, scan_period_nanosec);
	if(ret<0){
		printf("comedi_get_cmd_generic_timed failed\n");
		return false;
	}
        if (mode == FORCE_ACQUISITION_MODE_BURST)
        {
            printf("ADCard::PrepareCmdLib: mode == FORCE_ACQUISITION_MODE_BURST\n");
            cmd->stop_src = TRIG_COUNT;            
            cmd->stop_arg = n_scan;
            printf("n_scan == %d\n", n_scan);
        }
        if (mode == FORCE_ACQUISITION_MODE_CONTINUOUS)  //cancel later using comedi_cancel()
        {
            cmd->stop_src = TRIG_NONE;
            cmd->stop_arg = 0;
        }
        if (mode == FORCE_ACQUISITION_MODE_NONE)
        {
            printf("ERROR: mode == FORCE_ACQUISITION_MODE_NONE\n");
            return false;
        }
    
	/* Modify parts of the command */
	cmd->chanlist = this->chanlist;
	cmd->chanlist_len = this->n_chan;
	if(cmd->stop_src == TRIG_COUNT) cmd->stop_arg = n_scan;
        
	return true;
}

bool ADCard::Init(const QString devicePath, FORCE_ACQUISITION_MODE mode, int integrationTimeMs, int samplingFrequency)//maybe with param: FORCE_ACQUISITION_MODE mode
{
    bool retval = false;

    char *cmdtest_messages[]={
        "success",
        "invalid source",
        "source conflict",
        "invalid argument",
        "argument conflict",
        "invalid chanlist",
    };
    
    this->forceAcquisitionMode = mode;
    
    //this->previousForce = 0.0;
    //this->previousForceVoltage = 0.0;
    //this->previousPosition = 0;
    //this->previousStiffness = 0.0;
    //this->stiffness = 0.0;
    
    //int retval = 0;
    int ret = 0;
    int subdev = 0;         /* change this to your input subdevice */
    int chan = 15;           /* change this to your channel */
    int range = 1;          /* more on this later */
    int aref = AREF_GROUND; /* more on this later */
    int i = 0;
    //this->sensorGainReverse = 0;

   //this->threadData.forceMeasurement = this;
   //this->threadData.thread_id = 0;
    
    this->cmd=&(this->command); //what is that... ?!
    
    //comedi_t *it;
    //comedi_t *dev;
    
    //#define N_CHANS 256
    //static unsigned int chanlist[N_CHANS];
    //static comedi_range * range_info[N_CHANS];
    //static lsampl_t maxdata[N_CHANS];
    
    //comedi_cmd c, *cmd=&c;
    //struct timeval start,end;
    //int subdev_flags;
    lsampl_t raw;
   	//struct parsed_options options;
    
	init_parsed_options(&(this->options));
        
        //if (integrationTimeMs*10 >= BUFSZ)
        if (integrationTimeMs*samplingFrequency/1000 >= BUFSZ)
        {
            printf("Error: ADCard Init failed, too long integration time\n");
            exit(1);
            retval = false;
            return retval;
        }
    
    //this->options.filename = "/dev/comedi0";
      this->options.filename = devicePath.toLocal8Bit().data();
	this->options.subdevice = 0;
	this->options.channel = 15; ///TODO: this is going to be configurable from the robotRecipe.ini
	this->options.range = 1; //all chans: [-10,10] [-5,5] [-1,1] [-0.2,0.2]
	this->options.aref = AREF_GROUND;
	this->options.n_chan = 1; ///TODO: see how multichannel acquisition could be performed, because robotRecipe.ini would allow more than one sensor
	//this->options.n_scan = integrationTimeMs*10;
        this->options.n_scan = integrationTimeMs*samplingFrequency/1000;
	//this->options.freq = 10000.0;
        this->options.freq = samplingFrequency;

    this->dev = comedi_open(this->options.filename);
	if(!this->dev){
		comedi_perror(this->options.filename);
		//exit(1);
                return false;
	}

	// Print numbers for clipped inputs
	comedi_set_global_oor_behavior(COMEDI_OOR_NUMBER);

	/* Set up channel list */
	for(i = 0; i < 16; i++){
		this->chanlist[i] = CR_PACK(this->options.channel + i, this->options.range, this->options.aref);
		this->range_info[i] = comedi_get_range(this->dev, this->options.subdevice, this->options.channel, this->options.range);
		this->maxdata[i] = comedi_get_maxdata(this->dev, this->options.subdevice, this->options.channel);
	}
    
    //printf("Channel 15 ranges:\n");
    //printf("range = %d\n", this->range_info[15]);
    //printf("maxdata = %d\n", this->maxdata[15]);
        
    //comedi_range rng;
    //rng = comedi_
    //printf("");

    /* prepare_cmd_lib() uses a Comedilib routine to find a
     * good command for the device.  prepare_cmd() explicitly
     * creates a command, which may not work for your device. */
    //printf("preparing cmd...\n");
    //PrepareCmdLib(this->dev, this->options.subdevice, this->options.n_scan, this->options.n_chan, 1e9 / this->options.freq, this->cmd, this->forceAcquisitionMode);
    PrepareCmdLib(this->dev, this->options.subdevice, this->options.n_scan, 1e9 / this->options.freq, this->cmd, this->forceAcquisitionMode);

    //prepare_cmd(dev, options.subdevice, options.n_scan, options.n_chan, 1e9 / options.freq, cmd);

    //fprintf(stderr, "command before testing:\n");
    //dump_cmd(stderr, this->cmd);

    /* comedi_command_test() tests a command to see if the
     * trigger sources and arguments are valid for the subdevice.
     * If a trigger source is invalid, it will be logically ANDed
     * with valid values (trigger sources are actually bitmasks),
     * which may or may not result in a valid trigger source.
     * If an argument is invalid, it will be adjusted to the
     * nearest valid value.  In this way, for many commands, you
     * can test it multiple times until it passes.  Typically,
     * if you can't get a valid command in two tests, the original
     * command wasn't specified very well. */
    ret = comedi_command_test(this->dev, this->cmd);
    if(ret < 0){
            comedi_perror("comedi_command_test");
            if(errno == EIO){
                    fprintf(stderr,"Ummm... this subdevice doesn't support commands\n");
            }
            //exit(1);
            return false;
    }    
    
    //TODO: remove, this is just a debug msg:
    fprintf(stderr,"first test returned %d (%s)\n", ret,
    		cmdtest_messages[ret]);
    dump_cmd(stderr, this->cmd);  
    
    ret = comedi_command_test(this->dev, this->cmd);
    if(ret < 0){
            comedi_perror("comedi_command_test");
            //exit(1);
            return false;
    }
    
    //TODO: remove, this is just a debug msg:
    fprintf(stderr,"second test returned %d (%s)\n", ret,
    			cmdtest_messages[ret]);
            
    if(ret!=0){
            dump_cmd(stderr, this->cmd);
            fprintf(stderr, "Error preparing command\n");
            //exit(1);
            return false;
    }
    //      printf("applying calibration...\n");

    comedi_calibration_t* cal = comedi_parse_calibration_file("cal.dat");
    if (cal == 0) //if no calibration available
    {
        printf("cal.dat doesn't exist or is damaged. Trying to recreate...\n");
        system("comedi_soft_calibrate -S cal.dat");
        cal = comedi_parse_calibration_file("cal.dat");//try again
        if (cal == 0)
        {
            printf("cal.dat doesn't exist or is damaged\n");
            printf("try the following command as root: comedi_soft_calibrate -S cal.dat\n");
            return false;
        }
    }        

    int res = comedi_apply_parsed_calibration(this->dev, this->options.subdevice, 15, this->options.range, this->options.aref, cal);
    if (res)
    {
        printf("comedi_apply_parsed_calibration returned ERROR\n");
        //exit(1);
        return false;
    }
    else
    {
    //          printf("comedi_apply_parsed_calibration returned OK\n");
    }

    res = comedi_get_softcal_converter(this->options.subdevice, 15, this->options.range, COMEDI_TO_PHYSICAL, cal, &(this->polynomial_converter));
    if (res)
    {
        printf("get_softcal_converter returned ERROR\n");
        //exit(1);
        return false;
    }
    else
    {
    //        printf("get_softcal_converter returned OK\n");
        retval = true;
    }
        
    //printf("########## COMEDI ##################\n");
//
//    dev=comedi_open("/dev/comedi0");
//    if (!dev)
//    {
//        printf("error: comedi device not open\n");
//        return 1;
//    }
//    comedi_set_global_oor_behavior(COMEDI_OOR_NUMBER);
    
//    comedi_data_read(it,subdev,chan,range,aref, & data);

//    printf("%d\n",data);
    
//    printf("####################################\n");
   
    return retval;
}

bool ADCard::InitMultichannel(const QString devicePath, FORCE_ACQUISITION_MODE mode, int integrationTimeMs, unsigned int samplingFrequency, int* chanlist, int nchan)//maybe with param: FORCE_ACQUISITION_MODE mode
{
    printf("ADCard::InitMultichannel\n");
    bool retval = false;

    char *cmdtest_messages[]={
        "success",
        "invalid source",
        "source conflict",
        "invalid argument",
        "argument conflict",
        "invalid chanlist",
    };
    
    this->forceAcquisitionMode = mode;
    //###################
    // nchan has to be overridden to 16, otherwise the interlaced channels have weird numbering
    // nchan = 16;
    //###############
    int i = 0;
    for (i=0; i<16; i++)
    {
        this->chanlist[i] = chanlist[i];
        printf("this->chanlist[%d] = %d\n", i, this->chanlist[i]);
    }
    
    this->n_chan = nchan;
    printf("n_chan = %d\n", this->n_chan);
    
    //int retval = 0;
    int ret = 0;
    int subdev = 0;         /* change this to your input subdevice */
    //int chan = 15;           /* change this to your channel */
    int range = 1;          /* more on this later */
    int aref = AREF_GROUND; /* more on this later */        
    //this->sensorGainReverse = 0;
   //this->threadData.forceMeasurement = this;
   //this->threadData.thread_id = 0;    
    this->cmd=&(this->command); //what is that... ?!    
    //comedi_t *it;
    //comedi_t *dev;    
    //#define N_CHANS 256
    //static unsigned int chanlist[N_CHANS];
    //static comedi_range * range_info[N_CHANS];
    //static lsampl_t maxdata[N_CHANS];    
    //comedi_cmd c, *cmd=&c;
    //struct timeval start,end;
    //int subdev_flags;
    lsampl_t raw;
    //struct parsed_options options;
    //init_parsed_options(&(this->options)); //OK, I don't like it, but for now I don't have a replacement
    
    
    
    memset(&options, 0, sizeof(parsed_options));
    //options->filename = default_filename;
    options.filename = "";
    options.aref = AREF_GROUND;
    options.n_chan = 4;
    options.n_scan = 1000;
    options.freq = 10000.0;
    options.physical = 0;
    options.value = 0.;
    
    
    //if (integrationTimeMs*10 >= BUFSZ)
    if (integrationTimeMs*samplingFrequency/1000 >= BUFSZ)
    {
        printf("Error: ADCard Init failed, too long integration time\n");
        //exit(1); //no need to crash the program
        retval = false;
        return retval;
    }
    
    //this->options.filename = "/dev/comedi0";
    this->options.filename = devicePath.toLocal8Bit().data();
    this->options.subdevice = 0;
    //this->options.channel = 15; ///TODO: this is going to be configurable from the robotRecipe.ini
    this->options.range = 1; //all chans: [-10,10] [-5,5] [-1,1] [-0.2,0.2]
    this->options.aref = AREF_GROUND;
    //this->options.n_chan = this->n_chan; ///TODO: see how multichannel acquisition could be performed, because robotRecipe.ini would allow more than one sensor
    //this->options.n_scan = integrationTimeMs*10;
    this->options.n_scan = integrationTimeMs*samplingFrequency/1000;
    //this->options.freq = 10000.0;
    this->options.freq = samplingFrequency;

    this->dev = comedi_open(devicePath.toLocal8Bit().data());
    if(!this->dev){
            comedi_perror(devicePath.toLocal8Bit().data());
            //exit(1); //no need to crash the program
            return false;
    }

    // Print numbers for clipped inputs
    comedi_set_global_oor_behavior(COMEDI_OOR_NUMBER);
    
    
    /* Set up channel list */
    /*for(i = 0; i < 16; i++){
        //TODO: be careful, this overwrites the original array of integer channels, now the numbers no longer represent the original content
        this->chanlist[i] = CR_PACK(this->options.channel + i, this->options.range, this->options.aref); 
        this->range_info[i] = comedi_get_range(this->dev, this->options.subdevice, this->options.channel, this->options.range);
        this->maxdata[i] = comedi_get_maxdata(this->dev, this->options.subdevice, this->options.channel);
    }*/
    
    for(i = 0; i < nchan; i++){
        //TODO: be careful, this overwrites the original array of integer channels, now the numbers no longer represent the original content
        int ch = this->chanlist[i];
        this->chanlist[i] = CR_PACK(ch, this->options.range, this->options.aref); 
        this->range_info[i] = comedi_get_range(this->dev, this->options.subdevice, ch, this->options.range);
        this->maxdata[i] = comedi_get_maxdata(this->dev, this->options.subdevice, ch);
    }
    
    //printf("Channel 15 ranges:\n");
    //printf("range = %d\n", this->range_info[15]);
    //printf("maxdata = %d\n", this->maxdata[15]);
        
    //comedi_range rng;
    //rng = comedi_
    //printf("");

    /* prepare_cmd_lib() uses a Comedilib routine to find a
     * good command for the device.  prepare_cmd() explicitly
     * creates a command, which may not work for your device. */
    //printf("preparing cmd...\n");
    //PrepareCmdLib(this->dev, this->options.subdevice, this->options.n_scan, this->options.n_chan, 1e9 / this->options.freq, this->cmd, this->forceAcquisitionMode);
    printf("ADCard::InitMultichannel: this->options.freq = %f \n", this->options.freq);
    
    
    PrepareCmdLib(this->dev, this->options.subdevice, this->options.n_scan, 1e9 / this->options.freq, this->cmd, this->forceAcquisitionMode);
    //prepare_cmd(dev, options.subdevice, options.n_scan, options.n_chan, 1e9 / options.freq, cmd);

    //fprintf(stderr, "command before testing:\n");
    //dump_cmd(stderr, this->cmd);

    /* comedi_command_test() tests a command to see if the
     * trigger sources and arguments are valid for the subdevice.
     * If a trigger source is invalid, it will be logically ANDed
     * with valid values (trigger sources are actually bitmasks),
     * which may or may not result in a valid trigger source.
     * If an argument is invalid, it will be adjusted to the
     * nearest valid value.  In this way, for many commands, you
     * can test it multiple times until it passes.  Typically,
     * if you can't get a valid command in two tests, the original
     * command wasn't specified very well. */
    ret = comedi_command_test(this->dev, this->cmd);
    if(ret < 0){
            comedi_perror("comedi_command_test");
            if(errno == EIO){
                    fprintf(stderr,"Ummm... this subdevice doesn't support commands\n");
            }
            //exit(1);
            return false;
    }    
    
    //TODO: remove, this is just a debug msg:
    fprintf(stderr,"first test returned %d (%s)\n", ret,
    		cmdtest_messages[ret]);
    dump_cmd(stderr, this->cmd);  
    
    ret = comedi_command_test(this->dev, this->cmd);
    if(ret < 0){
            comedi_perror("comedi_command_test");
            //exit(1);
            return false;
    }
    
    //TODO: remove, this is just a debug msg:
    fprintf(stderr,"second test returned %d (%s)\n", ret,
    			cmdtest_messages[ret]);
            
    if(ret!=0){
            dump_cmd(stderr, this->cmd);
            fprintf(stderr, "Error preparing command\n");
            //exit(1);
            return false;
    }
    //      printf("applying calibration...\n");

    comedi_calibration_t* cal = comedi_parse_calibration_file("cal.dat");
    if (cal == 0) //if no calibration available
    {
        printf("cal.dat doesn't exist or is damaged. Trying to recreate...\n");
        system("comedi_soft_calibrate -S cal.dat");
        cal = comedi_parse_calibration_file("cal.dat");//try again
        if (cal == 0)
        {
            printf("cal.dat doesn't exist or is damaged\n");
            printf("try the following command as root: comedi_soft_calibrate -S cal.dat\n");
            return false;
        }
    }        

    
    //TODO: each channel has its own converter, not only channel 15
    int res = comedi_apply_parsed_calibration(this->dev, this->options.subdevice, 15, this->options.range, this->options.aref, cal);
    if (res)
    {
        printf("comedi_apply_parsed_calibration returned ERROR\n");
        //exit(1);
        return false;
    }
    else
    {
    //          printf("comedi_apply_parsed_calibration returned OK\n");
    }

    //TODO: each channel has its own converter, not only channel 15
    res = comedi_get_softcal_converter(this->options.subdevice, 15, this->options.range, COMEDI_TO_PHYSICAL, cal, &(this->polynomial_converter));
    if (res)
    {
        printf("get_softcal_converter returned ERROR\n");
        //exit(1);
        return false;
    }
    else
    {
    //        printf("get_softcal_converter returned OK\n");
        retval = true;
    }
        
    //printf("########## COMEDI ##################\n");
//
//    dev=comedi_open("/dev/comedi0");
//    if (!dev)
//    {
//        printf("error: comedi device not open\n");
//        return 1;
//    }
//    comedi_set_global_oor_behavior(COMEDI_OOR_NUMBER);
    
//    comedi_data_read(it,subdev,chan,range,aref, & data);

//    printf("%d\n",data);
    
//    printf("####################################\n");
   
    return retval;
}


bool ADCard::GetVoltage(double& voltage)
{
    //printf("called ADCard::GetVoltage...\n");
    bool retval = true;    
    lsampl_t raw=0;
    int subdev_flags;
    int i=0;
    int ret = 0;        
    /*printf("DEBUG:\n");
    ret = comedi_command_test(this->dev, this->cmd);
    if(ret < 0){
            comedi_perror("comedi_command_test");
            exit(1);
    }
    printf("comedi_command_test was successful\n" );
    */    
    /* start the command */
    ret = comedi_command(this->dev, this->cmd);
    if(ret < 0){
            retval = false;
            comedi_perror("comedi_command");
  //          printf("this->dev %d\n", this->dev);
  //          printf("this->cmd %d\n", this->cmd);
            exit(1); // sure ?
    }
    subdev_flags = comedi_get_subdevice_flags(this->dev, this->options.subdevice);
    
    double averageReading = 0.0;
    int total = 0;
    while(1){
        ret = read(comedi_fileno(this->dev),this->buf,BUFSZ);
        if(ret < 0){
                /* some error occurred */
                perror("read");
                retval = false;
                break;
        }else if(ret == 0){
                /* reached stop condition */
                break;
        }else{
                static int col = 0;
                int bytes_per_sample;
                total += ret;
//			printf("read %d samples\n", ret);
//			if(options.verbose)fprintf(stderr, "read %d %d\n", ret, total);
                if(subdev_flags & SDF_LSAMPL)
                        bytes_per_sample = sizeof(lsampl_t);
                else
                        bytes_per_sample = sizeof(sampl_t);
                for(i = 0; i < ret / bytes_per_sample; i++){
                        if(subdev_flags & SDF_LSAMPL) {
                                raw = ((lsampl_t *)this->buf)[i];
                        } else {
                                raw = ((sampl_t *)this->buf)[i];
                        }
        //averageReading += raw;
        averageReading += comedi_to_physical(raw, &(this->polynomial_converter)); 
        //averageReading += comedi_to_phys(raw, this->range_info[this->options.channel], this->maxdata[this->options.channel]);
                        //print_datum(raw, col, options.physical);
                        //col++;
                        //if(col == options.n_chan){
                        //	printf("\n");
                        //	col=0;
                        //}
                }
        }
    }
    
    averageReading = 2*averageReading / total;//total is a number of bytes, not samples
    //raw = averageReading;
  //  printf("ReadForceVoltage: raw=%d, averageReading=%f\n",raw, averageReading);
//    if ((this->sensorGainReverse) == 1)
//    {
//        *voltage = 5.0 - averageReading;
//    }
//    else
//    {
  //      *voltage = averageReading;
     voltage = averageReading;
     //printf(" - ADCard::GetVoltage acquired %f\n", voltage);
//    }
    
    //*voltage = comedi_to_phys(raw, this->range_info[this->options.channel], this->maxdata[this->options.channel]);
    //printf("voltage = %f\n", *voltage);
    
    return retval;
}


bool ADCard::GetVoltageMultichannel(std::vector<double>& voltages)//TODO: return array of voltages, no a single reference
{
    //printf("called ADCard::GetVoltage...\n");
    bool retval = true;    
    lsampl_t raw=0;
    int subdev_flags;
    int i=0;
    int ret = 0;        
    std::vector<double> avgVoltages;
    std::vector<int> totalReads;
    for (int i=0; i<n_chan; i++)
    {
        avgVoltages.push_back(0.0);
        totalReads.push_back(0);
    }
    
    
    /*printf("DEBUG:\n");
    ret = comedi_command_test(this->dev, this->cmd);
    if(ret < 0){
            comedi_perror("comedi_command_test");
            exit(1);
    }
    printf("comedi_command_test was successful\n" );
    */    
    /* start the command */
    ret = comedi_command(this->dev, this->cmd);
    if(ret < 0)
    {
        retval = false;
        comedi_perror("comedi_command");
        //          printf("this->dev %d\n", this->dev);
        //          printf("this->cmd %d\n", this->cmd);
        exit(1); // sure ?
    }
    subdev_flags = comedi_get_subdevice_flags(this->dev, this->options.subdevice);    
    //double averageReading = 0.0;
    

    int total = 0;
    int chid = 0;
    while(1)
    {
        ret = read(comedi_fileno(this->dev),this->buf,BUFSZ);
        if(ret < 0)
        {
            /* some error occurred */
            perror("read");
            retval = false;
            break;
        }
        else 
        {
            if(ret == 0)
            {   /* reached stop condition */
                break;
            }
            else
            {
                static int chid = 0;
                int bytes_per_sample;
                total += ret;  //			printf("read %d samples\n", ret);
//			if(options.verbose)fprintf(stderr, "read %d %d\n", ret, total);
                if(subdev_flags & SDF_LSAMPL)
                {
                    bytes_per_sample = sizeof(lsampl_t);
                }
                else
                {
                    bytes_per_sample = sizeof(sampl_t);
                }                
                for(i = 0; i < ret / bytes_per_sample; i++)
                {
                    if(subdev_flags & SDF_LSAMPL) 
                    {
                        raw = ((lsampl_t *)this->buf)[i];
                    } 
                    else 
                    {
                        raw = ((sampl_t *)this->buf)[i];
                    }
                    //averageReading += raw;
                    avgVoltages[chid] += comedi_to_physical(raw, &(this->polynomial_converter)); 
                    totalReads[chid] += 1;
                    //averageReading += comedi_to_physical(raw, &(this->polynomial_converter)); 
                    //averageReading += comedi_to_phys(raw, this->range_info[this->options.channel], this->maxdata[this->options.channel]);
                                    //print_datum(raw, col, options.physical);
                                    //col++;
                                    //if(col == options.n_chan){
                                    //	printf("\n");
                                    //	col=0;
                                    //}              
                    chid++;
                    if (chid >= this->n_chan)
                    {
                        chid = 0;
                    }
                }
            }     
        }
    }
    //printf("chid at the end of a scan = %d\n", chid);
    //printf("###################################################\n");
    for (int i=0; i<n_chan; i++)
    {   
        if (totalReads[i]>0)
        {
            avgVoltages[i] = avgVoltages[i] / totalReads[i];//totalReads is a number of samples
        }
        else //else, this would be a division by zero or a negative number of reads
        {
            avgVoltages[i] = double_nan;
        }
        //printf("v[%d] = %f \t", i, avgVoltages[i]);
        //printf("totalReads[%d] = %d \t", i, totalReads[i]);
        voltages[i] = avgVoltages[i];
    }
    
    //printf("\n");
    //averageReading = 2*averageReading / total;//total is a number of bytes, not samples
    //raw = averageReading;
  //  printf("ReadForceVoltage: raw=%d, averageReading=%f\n",raw, averageReading);
//    if ((this->sensorGainReverse) == 1)
//    {
//        *voltage = 5.0 - averageReading;
//    }
//    else
//    {
  //      *voltage = averageReading;
     //voltage = averageReading;
     //voltages[0] = voltage;
    
     //printf(" - ADCard::GetVoltage acquired %f\n", voltage);
//    }
    
    //*voltage = comedi_to_phys(raw, this->range_info[this->options.channel], this->maxdata[this->options.channel]);
    //printf("voltage = %f\n", *voltage);
    
    return retval;
}
