/* 
 * File:   SmarActCommands.hpp
 * Author: cfmadmin
 *
 * Created on February 20, 2013, 9:38 AM
 */

#ifndef SMARACTCOMMANDS_HPP
#define	SMARACTCOMMANDS_HPP

#include "SmarActCommand.hpp"

class Command_SA_GetPosition_S: public SmarActCommand {
public:
    Command_SA_GetPosition_S();
    //Command_SA_GetPosition_S(const Command_SA_GetPosition_S& orig);
    virtual ~Command_SA_GetPosition_S();
    bool Send(SA_INDEX systemID, SA_INDEX axisID, signed int* position);
private:

};

class Command_SA_GetStatus_S: public SmarActCommand {
public:
    Command_SA_GetStatus_S();
    //Command_SA_GetStatus_S(const Command_SA_GetStatus_S& orig);
    virtual ~Command_SA_GetStatus_S();
    bool Send(SA_INDEX systemID, SA_INDEX axisID, unsigned int* axisStatus);
private:

};

class Command_SA_GotoPositionAbsolute_S : public SmarActCommand  {
public:
    Command_SA_GotoPositionAbsolute_S();
    //Command_SA_GotoPositionAbsolute_S(const Command_SA_GotoPositionAbsolute_S& orig);
    virtual ~Command_SA_GotoPositionAbsolute_S();
    bool Send(SA_INDEX systemID, SA_INDEX axisID, signed int position, unsigned int holdMs);
   
protected:
  

};

class Command_SA_SetClosedLoopMaxFrequency_S : public SmarActCommand{
public:
    Command_SA_SetClosedLoopMaxFrequency_S();
    //Command_SA_SetClosedLoopMaxFrequency_S(const Command_SA_SetClosedLoopMaxFrequency_S& orig);
    virtual ~Command_SA_SetClosedLoopMaxFrequency_S();
    bool Send(SA_INDEX systemID, SA_INDEX axisID, unsigned int frequencyHz);//valid range for SmarAct: 50-18500Hz
private:

};

class Command_SA_SetClosedLoopMoveSpeed_S : public SmarActCommand {
public:
    Command_SA_SetClosedLoopMoveSpeed_S();
    //Command_SA_SetClosedLoopMoveSpeed_S(const Command_SA_SetClosedLoopMoveSpeed_S& orig);
    virtual ~Command_SA_SetClosedLoopMoveSpeed_S();
    bool Send(SA_INDEX systemID, SA_INDEX axisID, unsigned int speedNmPerSecond);//valid range for SmarAct: 0-100000000. Value 0 disables limit.
private:

};

class Command_SA_Stop_S : public SmarActCommand {
public:
    Command_SA_Stop_S();
    //Command_SA_Stop_S(const Command_SA_Stop_S& orig);
    virtual ~Command_SA_Stop_S();
    bool Send(SA_INDEX systemID, SA_INDEX axisID);
private:

};

class Command_SA_InitSystems : public SmarActCommand {
public:
    Command_SA_InitSystems();
    //Command_SA_Stop_S(const Command_SA_Stop_S& orig);
    virtual ~Command_SA_InitSystems();
    bool Send(unsigned int configuration);
private:

};

class Command_SA_SetZeroPosition : public SmarActCommand {
public:
    Command_SA_SetZeroPosition();
    //Command_SA_Stop_S(const Command_SA_Stop_S& orig);
    virtual ~Command_SA_SetZeroPosition();
    bool Send(SA_INDEX systemID, SA_INDEX axisID);
private:

};

#endif	/* SMARACTCOMMANDS_HPP */

