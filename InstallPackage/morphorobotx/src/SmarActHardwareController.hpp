/* 
 * File:   SmarActController.hpp
 * Author: cfmadmin
 *
 * Created on February 25, 2013, 5:39 PM
 */

#ifndef SMARACTHARDWARECONTROLLER_HPP
#define	SMARACTHARDWARECONTROLLER_HPP

#include "HardwareController.hpp"
#include <vector>


class SmarActHardwareController : public HardwareController{
public:
    SmarActHardwareController(const Parms& parms, QString name, StateManager& stateManager);
    virtual ~SmarActHardwareController();
    //ActuatorAxis* GetAxis(QString name, const RobotRecipe& recipe);
    bool Get(int option, double& value); ///TODO: require specific type for the option 
    bool Set(int option, double value); ///TODO: require specific type for the option 
    //bool BuildFromRecipe(const Parms& parms, QString section);    
    Axis* BuildAxis(const Parms& parms, QString name);
    bool Initialize();
    void RegisterHardwareID();
    ///returns the id needed by most low-level smaract commands (first argument)
    unsigned int systemID() 
    {
        return _systemID;
    };
    
protected:
    ///serial number of the controller box
    unsigned int _hardwareID; 
    ///small int assigned to the system, needed to send commands
    unsigned int _systemID;

    
};

class SmarActTopLevelController
{
public:
    static SmarActTopLevelController& getInstance()
    {
        static SmarActTopLevelController instance;        
        return instance;
    }
    void RegisterHardwareID(unsigned int hardwareID)
    {
        _hardwareIDs.push_back(hardwareID);
    }
    std::vector<unsigned int> GetRegisteredHardwareIDs()
    {
        return _hardwareIDs;
    }
    bool Initialize();
    bool ReleaseSystems();
    bool isInitialized()
    {
        return _isInitialized;
    }    
    
private:
    SmarActTopLevelController()
    {
        
    }    
    std::vector<unsigned int> _hardwareIDs;
    bool _isInitialized = false;
};

#endif	/* SMARACTHARDWARECONTROLLER_HPP */

