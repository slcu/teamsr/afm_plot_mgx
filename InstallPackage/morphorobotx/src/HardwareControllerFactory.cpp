/* 
 * File:   HardwareSystemFactory.cpp
 * Author: cfmadmin
 * 
 * Created on February 27, 2013, 6:51 PM
 */

#include "HardwareControllerFactory.hpp"
#include "HardwareController.hpp"
#include "SmarActHardwareController.hpp"
#include "ComediHardwareController.hpp"
#include "ZaberHardwareController.hpp"
#include "parms.hpp"

HardwareControllerFactory::HardwareControllerFactory() {
}

HardwareControllerFactory::~HardwareControllerFactory() {
}

HardwareController* HardwareControllerFactory::hardwareController(const Parms& parms, const QString hardwareControllerName, StateManager& stateManager)
{
    HardwareController* hwControllerPtr;
    QString controllerTypeName;
    bool isOK = true;
    isOK &= parms(hardwareControllerName, "Type", controllerTypeName);        
    if (!isOK)
    {
        printf("Could not parse RobotRecipe -> (...) -> %s -> Type \n", hardwareControllerName.toLocal8Bit().data());
        return 0; //null pointer
    } 
    printf("Inside HardwareControllerFactory::hardwareController\n");
    printf("hardwareControllerName = %s\n", hardwareControllerName.toLocal8Bit().data());
    if (controllerTypeName.toLower() == "smaract")
    {       
        hwControllerPtr = new SmarActHardwareController(parms, hardwareControllerName, stateManager);
    } 
    else
    {
        if (controllerTypeName.toLower() == "comedi")
        {
                hwControllerPtr = new ComediHardwareController(parms, hardwareControllerName, stateManager);
        }
        else
        { 
            if (controllerTypeName.toLower() == "zaber")
            {
                hwControllerPtr = new ZaberHardwareController(parms, hardwareControllerName, stateManager);
            }
            else
            { // support more types here, if needed
                printf("Error: HardwareController type %s unsupported\n", controllerTypeName.toLocal8Bit().data());
                hwControllerPtr = 0;
            }
        }
    }
    
    return hwControllerPtr;
}