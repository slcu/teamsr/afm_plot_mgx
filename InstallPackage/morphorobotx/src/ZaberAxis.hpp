/* 
 * File:   ZaberAxis.hpp
 * Author: cfmadmin
 *
 * Created on March 5, 2013, 11:55 AM
 */

#ifndef ZABERAXIS_HPP
#define	ZABERAXIS_HPP

#include "Axis.hpp"
class ZaberHardwareController;

class ZaberAxis : public Axis {
public:
    ZaberAxis(const Parms& parms, QString axisName, ZaberHardwareController* hardwareController);    
    virtual ~ZaberAxis();
        
    bool Set(unsigned int option, double value);
    bool Get(unsigned int option, double& value);
    bool Initialize();
    
    bool GotoSync(double positionMicrons);    
    bool GetPositionSync(double& positionMicrons);
    
protected:

};

#endif	/* ZABERAXIS_HPP */

