/* 
 * File:   ForceSensor.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 9:07 PM
 */

#ifndef FORCESENSOR_HPP
#define	FORCESENSOR_HPP

#include "Sensor.hpp"
#include "StateManager.hpp"
class HardwareController;
class Parms;

class ForceSensor : public Sensor{
    
public:
    ForceSensor(const Parms& parms, QString sensorName, HardwareController* hardwareController);
    //ForceSensor(const ForceSensor& orig);
    virtual ~ForceSensor();
    void SetGain(double newGain)
    {
        _Gain = newGain;
    }
    void SetMaxForce(double maxForce)
    {
        this->_MaxForce = maxForce;
    }
    void SetOffsetVoltage(double offsetVoltage)
    {
        this->offsetVoltage = offsetVoltage;
        
        stateManager().state().Update(_fieldIDOffsetVoltage, offsetVoltage);        
        stateManager().state().Update(_fieldIDOffsetForce, GetOffsetForce());                
    }
    double GetOffsetVoltage()
    {
        return this->offsetVoltage;
    }
    
    void TareSensor()
    {        
        double voltage = GetVoltage();
        SetOffsetVoltage(voltage);                        
    }
    
    double GetOffsetForce()
    {
        return _Gain*offsetVoltage;
    }

    double GetMaxForce()
    {
        return _MaxForce;
    }
    //double GetForce(double voltage); //in newtons ?
    double GetVoltage();
    double GetForce();
    
    bool Get(unsigned int option, double &value);
    bool Set(unsigned int option, double value);
    bool Initialize();
    bool RegisterSensorToHardwareController();
    bool isPushingPositive(); /// pushing positive means increase in axis coordinate increases pushing
    QString AxisName()
    {
        return _AxisName;
    }
    
    
protected:
    bool PowerUp();
    bool PowerDown();    
    //double maxForce;
    double offsetVoltage;    
    QString _AxisName;
    bool _AxisPositivePushing; //TODO: ugly name, I meant that increase in axis coordinate produces more indentation
    bool _InvertedGain;
    int _fieldIDForce;
    int _fieldIDOffsetVoltage;
    int _fieldIDOffsetForce; //offset force results from offset voltage
    int _fieldIDVoltage;
    
};

#endif	/* FORCESENSOR_HPP */

