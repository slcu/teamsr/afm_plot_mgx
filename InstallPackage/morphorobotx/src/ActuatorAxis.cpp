/* 
 * File:   ActuatorAxis.cpp
 * Author: cfmadmin
 * 
 * Created on February 1, 2013, 9:09 PM
 */

#include "ActuatorAxis.hpp"

ActuatorAxis::ActuatorAxis(ActuatorSystem* actuatorSystem, StateManager& stateManager, unsigned int localID): _itsActuatorSystem(actuatorSystem), _localID(localID), _stateManager(stateManager) {   
    _flip = 1;
}

//ActuatorAxis::ActuatorAxis(const ActuatorAxis& orig) {
// }

ActuatorAxis::~ActuatorAxis() {
}
/*
bool ActuatorAxis::Goto(double positionMicrons) ///TODO: it needs to be moved to the dervied classes, because the general actuator system does not know the details/properties needed to call the hardware
{
    //TODO: implement
    printf("within ActuatorAxis->Goto\n");
    printf("now calling itsActuatorSystem()->Goto\n");
    
    itsActuatorSystem()->Goto(LocalID(), positionMicrons);
    return false;
}
 */