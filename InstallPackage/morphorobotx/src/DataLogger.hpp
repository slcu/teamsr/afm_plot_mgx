/* 
 * File:   DataLogger.hpp
 * Author: huflejt
 *
 * Created on November 7, 2012, 11:27 AM
 */

#ifndef DATALOGGER_HPP
#define	DATALOGGER_HPP

#include <QString>
#include <QFile>
#include <QTextStream>
//#include "MeasurementState.hpp"
//#include "StateManager.hpp"
#include <sstream>
#include <iostream>
#include "Timer.hpp"


class StateManager;

/**
 * @class DataLogger DataLogger.hpp "DataLogger.hpp"
 * 
 */
class DataLogger {
public:
    explicit DataLogger(StateManager& stateManager);
    /*DataLogger(const DataLogger& orig);*/
    virtual ~DataLogger();
    
    bool WriteCurrentState();
    /**
     * 
     * @param filename fghgh
     * @return true if ...
     */
    void SetFilename(QString filename);
    
    bool valid() const { return !filename.isEmpty(); } //isEmpty means invalid
    
protected:
    QString filename;
    //MeasurementState* state;
    StateManager& _stateManager;
    QFile file;
    QTextStream stream;    
    bool isHeaderPrepared;
    /**
     *
     */
    bool PrepareFileHeader();//call that from WriteCurrentState() if not yet called
    int _fieldIDTime;
    Timer timer;
    
};

#endif	/* DATALOGGER_HPP */

