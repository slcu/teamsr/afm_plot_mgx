/* 
 * File:   SmarActAxisStatus.hpp
 * Author: cfmadmin
 *
 * Created on February 13, 2013, 3:15 PM
 */

#ifndef SMARACTAXISSTATUS_HPP
#define	SMARACTAXISSTATUS_HPP

#include "MCSControl.h"
#include "QString"
#include "QMap"

class SmarActAxisStatus {
public:
    SmarActAxisStatus();
    //SmarActAxisStatus(const SmarActAxisStatus& orig);
    virtual ~SmarActAxisStatus();
    unsigned int Status();
    void SetStatus(unsigned int axisStatus)
    {
        _status = axisStatus;
    }
    QString GetDescription();
    QString GetName();
    static QMap<unsigned int, QString> StatusMapDescription();
    static QMap<unsigned int, QString> StatusMapName();    
protected:
    unsigned int _status; //unfortunately the smaract library does not offer a type for this

};

#endif	/* SMARACTAXISSTATUS_HPP */

