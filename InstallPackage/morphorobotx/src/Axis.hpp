/* 
 * File:   Axis.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 4:09 PM
 */

#ifndef AXIS_HPP
#define	AXIS_HPP

#include "RobotSubunit.hpp"

class Parms;
class HardwareController;
class HardwareManager;

///TODO: consider if it needs to go into RobotSubunit.hpp
typedef enum AxisOption: unsigned int
{
    GOTO_TIMEOUT_MS=0,
    GOTO_HOLD_MS=1,
    GOTO_PRECISION_MARGIN_MICRONS=2,
    SPEED_LIMIT_MICRONS_PER_SECOND=3,
    FREQUENCY_LIMIT_HZ=4,
    ACCELERATION_LIMIT_MICRONS_PER_SECOND_SQUARE=5,
    SET_ZERO_POSITION=100,
    STOP_NOW=101
            
} AxisOption;

class Axis : public RobotSubunit {
public:
    Axis(HardwareController* hardwareController); 
    virtual ~Axis();
    
    virtual bool GotoSync(double positionMicrons)=0;    
    virtual bool GetPositionSync(double& positionMicrons)=0;    
    
protected:


};

#endif	/* AXIS_HPP */

