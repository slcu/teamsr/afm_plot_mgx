/* 
 * File:   CFMFileManager.cpp
 * Author: huflejt
 * 
 * Created on October 25, 2012, 7:30 PM
 */

#include "FileManager.hpp"

FileManager::FileManager() {
    isInitialized = false;
}

//FileManager::FileManager(const FileManager& orig) {
//}

FileManager::~FileManager() {
}

bool FileManager::isValid()
{
    return isInitialized;
}
