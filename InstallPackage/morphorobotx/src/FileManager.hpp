/* 
 * File:   CFMFileManager.h
 * Author: huflejt
 *
 * Created on October 25, 2012, 7:30 PM
 */

#ifndef CFMFILEMANAGER_H
#define	CFMFILEMANAGER_H

#include "parms.hpp"
#include <QString>
#include <QFile>
#include <QDir>


class FileManager {
    
public:
    
    FileManager();
    //FileManager(const FileManager& orig);
    virtual ~FileManager();
    //bool saveParams();    
        
    void setInParamsPath(QString path)
    {
        inParamsPath = path;
    }
    QString getInParamsPath()
    {       
        return inParamsPath;
    }    
    
    void setCurrentSessionPath(QString path)
    {
        currentSessionPath = path;
        isInitialized = false;
    }
    
    QString getCurrentSessionPath() const
    {
        return currentSessionPath;
    }    
    
    void setExperimentPrefix(QString prefix) 
    {
        experimentPrefix = prefix;
        isInitialized = false;
    }
    
    QString getOutputDataPath() const
    {
        QString path = currentExperimentDirPathAbsolute + QDir::separator() + QString("data.csv");
        return path;
    }
    
    QString getOutParamsPath() const
    {
        QString path = currentExperimentDirPathAbsolute + QDir::separator() + QString("parameters.ini");
        return path;
    }
    
    QString getOutRobotRecipePath() const
    {
        QString path = currentExperimentDirPathAbsolute + QDir::separator() + QString("robotRecipe.ini");
        return path;
    }
    
    QString getExperimentDirAbsolute() const
    {        
        return currentExperimentDirPathAbsolute;
    }

    bool readParameters(Parms par)
    {
        bool isOK = true;
        
        //QString sessionPath = QString("");
        isOK &= par("StorageSettings", "SessionPath", currentSessionPath);
        if (isOK)
        {
            isInitialized = false;
        }
        //experimentPrefix = QString("");
        isOK &= par("StorageSettings", "ExperimentPrefix", experimentPrefix);                
        if (isOK)
        {
            isInitialized = false;
        }        
        return isOK;
    }
    
    bool prepareNewDirectory()
    {
        bool isOK = true;
        getFirstAvailableExperimentPath();
        isOK &= makeDir(this->currentExperimentDirPathAbsolute);
        isInitialized = isOK;
        return isOK;
    }
    
    bool isValid();    
    //Parms getInParms() {return itsInParms;}
    
protected:    
    bool makeDir(QString path)
    {
        QDir dir;
        int dirCreated = dir.mkpath(path);
        if (dirCreated)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    QString getFirstAvailableExperimentPath()
    {
        QString path;
        QString proposedPath;
        int suffixNumber = 0;
        
        do
        {
            QString num = QString("");
            num = num.setNum(suffixNumber,10);
            while (num.length() < 5) // quite cosmetic, makes names sorted alphabetically
            {
                num = QString("0") + num;
            }
            proposedPath = currentSessionPath + experimentPrefix + num;
            suffixNumber++;
            
        } while(QFile::exists(proposedPath));
        /*
        * If you trace through the QFile::exists code, you see that it uses the
        standard C library 'access' function to do the actual work.  Here's
        what the access man page says:

       access  checks  whether  the  protocol  would be allowed to
       read, write or test for existence of the  file  (or  other
       file  system  object) whose name is pathname.  If pathname
       is a symbolic link permissions of the file referred to  by
       this symbolic link are tested.
                 */
        currentExperimentDirPathAbsolute = proposedPath;
        return proposedPath;
    }    
    
private:
    //Parms itsInParms;
    //Parms itsOutParms;
    QString inParamsPath;
    QString currentSessionPath;
    QString experimentPrefix;
    QString currentExperimentDirPathAbsolute;
    //QString outputDataPath;
    //QString outParamsPath;
    bool isInitialized;
};

#endif	/* CFMFILEMANAGER_H */