/* 
 * File:   SmarActActuatorAxis.hpp
 * Author: cfmadmin
 *
 * Created on February 4, 2013, 9:41 PM
 */

#ifndef SMARACTACTUATORAXIS_HPP
#define	SMARACTACTUATORAXIS_HPP

#include "ActuatorAxis.hpp"
//#include "ActuatorSystem.hpp"
#include "SmarActActuatorSystem.hpp"
#include "SmarActResult.hpp"
#include "SmarActCommands.hpp"
#include "Timer.hpp"

class ActuatorSystem;
//class ActuatorAxis;


class SmarActActuatorAxis : public ActuatorAxis{
public:
    SmarActActuatorAxis(ActuatorSystem* actuatorSystem, StateManager& stateManager, unsigned int channelType, unsigned int sensorType, unsigned int localID);
    //SmarActActuatorAxis(const SmarActActuatorAxis& orig);
    virtual ~SmarActActuatorAxis();    
    bool isSensorMissing()
    {
        return _isSensorMissing;
    }
    bool GotoAsync(double positionMicrons);
    bool GotoSync(double positionMicrons);
    bool GetPositionSync(double& positionMicrons);
//    bool SetOption(AxisOption option, double value);
    bool GetStatusSync(unsigned int& status);
    bool StopSync();
    void SetHoldMs(unsigned int holdMs);
    void SetMarginMicrons(double marginMicrons); ///use larger values to achieve premature stopping for coarse approach
    bool SetClosedLoopMaxFrequency(double frequencyHz);
    bool SetClosedLoopMoveSpeed(double speedMicronsPerSecond);
    bool RegisterFields();
    bool UpdateState();
protected:
    unsigned int _sensorType;
    unsigned int _channelType;
    bool _isSensorMissing;
    unsigned int holdMs;
    double _marginMicrons;
    double _timeoutMs;
    int _fieldIDPositionMicrons;
};

#endif	/* SMARACTACTUATORAXIS_HPP */

