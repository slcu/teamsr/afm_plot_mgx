/* 
 * File:   StateManager.cpp
 * Author: cfmadmin
 * 
 * Created on December 11, 2012, 6:02 PM
 */

#include "StateManager.hpp"
#include "Experiment.hpp"

StateManager::StateManager(): _logger(*this){
    
}

/*StateManager::StateManager(const StateManager& orig) {
}*/

StateManager::~StateManager() {
}

int StateManager::indexOfField(QString name)
{
    int key = -1;    
    key = state().IndexOfField(name);
    return key;
}
