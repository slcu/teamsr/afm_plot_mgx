/* 
 * File:   HardwareController.hpp
 * Author: cfmadmin
 *
 * Created on February 25, 2013, 5:33 PM
 */

#ifndef HARDWARECONTROLLER_HPP
#define	HARDWARECONTROLLER_HPP

#include "QString"
#include <vector>
class Parms;
class Axis;
class Sensor;
class StateManager;

class HardwareController {
public:
    HardwareController(QString name, StateManager& stateManager);//(QString name, const RobotRecipe& recipe); -> for subclasses
    //virtual ActuatorAxis* GetAxis(QString name, const RobotRecipe& recipe)=0; ///returns NULL if cannot create
    virtual ~HardwareController() { };
    
    virtual bool Get(int option, double& value)=0; ///TODO: require specific type for the option 
    virtual bool Set(int option, double value)=0; ///TODO: require specific type for the option 
   // virtual bool BuildFromRecipe(const Parms& parms, QString section)=0;    
    
    QString Name()
    {
        return _name;
    }
    virtual Axis* BuildAxis(const Parms& parms, QString name);
    virtual Sensor* BuildSensor(const Parms& parms, QString name);
    virtual bool Initialize()=0;
    bool isInitialized()
    {
        return _initialized;
    }
    bool isInstanceValid()
    {
        return _isInstanceValid;
    }
    StateManager& stateManager();
    
    
protected:
    QString _name;
    std::vector<Axis*> _axes;
    std::vector<Sensor*> _sensors;
    bool _initialized;
    bool _isInstanceValid = false;
    StateManager& _stateManager;

};

#endif	/* HARDWARECONTROLLER_HPP */

