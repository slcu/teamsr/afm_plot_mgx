/* 
 * File:   ForceMeasurement.cpp
 * Author: huflejt
 * 
 * Created on November 6, 2012, 2:47 PM
 */

#include "ForceMeasurement.hpp"
#include "Robot.hpp"
#include "StateManager.hpp"
#include "parms.hpp"
#include "HardwareController.hpp"

ForceMeasurement::ForceMeasurement(Robot& robot, HardwareController* hardwareController): _robot(robot)//, sensor(hardwareController)
{    
    //this->stateManager = stateManager;
    //this->state = state;
    forceUnit = UNIT_NEWTON;    
    //_experiment.stateManager().RegisterField()
    //forceId = registerFixedField(_experiment.stateManager(), "Force", "(N)"); //helper funciton that calls _experiment.stateManager().RegisterField() and throws an exception on error
    //_fieldIdForce = _experiment.stateManager().RegisterFieldExc("Force", "(N)", true);
    _fieldIdForce = _robot.stateManager().RegisterFieldExc("Force", "(N)", true);
    _fieldIdVoltage = _robot.stateManager().RegisterFieldExc("Voltage", "(V)", true);
    _fieldIdAssumedForceOffset = _robot.stateManager().RegisterFieldExc("AssumedForceOffset", "(N)");    
}

ForceMeasurement::~ForceMeasurement() {
}

bool ForceMeasurement::Init(int integrationTimeMs)
{
    bool retval = false;
    //TODO: it's disabled, the method card.Init has changed
    //retval = card.Init(FORCE_ACQUISITION_MODE_BURST, integrationTimeMs);    
    if (retval==0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ForceMeasurement::GetForceAndVoltage(ForceMeasurement::ForceUnit unit, double& force, double& voltage)
{    
    //printf("InGetForceAndVoltage\n");
    bool isOK = card.GetVoltage(voltage);
    //printf("     voltage: %f\n", voltage);
//    force = sensor.GetForce(voltage);
    //printf("       force: %f\n", voltage);       
    //printf("       _fieldIdForce: %d\n", _fieldIdForce);
    _robot.stateManager().state().Update(_fieldIdForce, force);    
    
//    _robot.stateManager().state().Update(_fieldIdAssumedForceOffset, sensor.GetOffsetForce());
    //printf("       _fieldIdAssumedForceOffset: %d\n", _fieldIdAssumedForceOffset);
    _robot.stateManager().state().Update(_fieldIdVoltage, voltage);                
    //printf("       _fieldIdVoltage: %d\n", _fieldIdVoltage);            
    //_experiment.stateManager().state().UpdateTimeEnd(); //TODO: move it to the protocol
    return isOK;
}

void ForceMeasurement::SetSensorOffsetForce(double offsetForce)
{        
//    sensor.SetOffsetForce(offsetForce);
    //state->Invalidate(MeasurementState::Force);        
    _robot.stateManager().state().Invalidate(_fieldIdForce);
};

bool ForceMeasurement::UpdateForce()
{
    bool isOK = true;
    double force = 0.0;
    double voltage = 0.0;
    isOK &= GetForceAndVoltage(forceUnit, force, voltage);    
    return isOK;  
}
/* //at the moment, Robot does that
bool ForceMeasurement::readParameters(const Parms& parms)
{
    bool isOK = true;    
    //TODO: parse the parameters
    double sensorMaxForce = 0.0;
    isOK &= parms("ForceSensorSettings", "SensorMaxForce", sensorMaxForce);                    
    if (!isOK)
    {        
        return false;
    }
    return isOK;
}
*/