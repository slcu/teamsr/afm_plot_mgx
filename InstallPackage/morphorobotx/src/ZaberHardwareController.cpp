/* 
 * File:   ZaberHardwareController.cpp
 * Author: cfmadmin
 * 
 * Created on February 27, 2013, 3:05 PM
 */

#include "ZaberHardwareController.hpp"
#include "ZaberAxis.hpp"

ZaberHardwareController::ZaberHardwareController(const Parms& parms, QString name, StateManager& stateManager) : HardwareController(name, stateManager) {
    
    _isInstanceValid = false;
    //TODO: parse relevant parameters and update _isObjectValid accordingly
    
}

ZaberHardwareController::~ZaberHardwareController() {
}

bool ZaberHardwareController::Set(int option, double value)
{
    //TODO: implement
    return false;
}

bool ZaberHardwareController::Get(int option, double& value)
{
    //TODO: implement
    return false;
}
/*
bool ZaberHardwareSystem::BuildFromRecipe(const Parms& parms, QString section)
{
    printf("within ZaberHardwareSystem::BuildFromRecipe(parms, QString section== %s\n", section.toLocal8Bit().data());
    return false;
}*/

Axis* ZaberHardwareController::BuildAxis(const Parms& parms, QString axisName)
{
    //TODO: implement
        
    printf("within ZaberHardwareController::BuildAxis, axis name: %s\n", axisName.toLocal8Bit().data());
    ZaberAxis* axis = new ZaberAxis(parms, axisName, this);   
    printf("axis pointer is %d\n", axis);
    
    if (axis)
    {
        _axes.push_back(axis);
    }        
    
    return axis;    
}

bool ZaberHardwareController::Initialize()
{
    //TODO: implement
    return false;
}