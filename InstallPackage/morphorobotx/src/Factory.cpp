#include "Factory.hpp"
#include "Experiment.hpp"
#include <iostream>
#include <QTextStream>
#include <stdio.h>

Protocol::Protocol(Experiment& e, QString sectionName)
  : _experiment(e), _sectionName(sectionName)
{        
}

Factory::Factory()
{
}

Factory& Factory::instance()
{
  static std::unique_ptr<Factory> _instance = std::unique_ptr<Factory>(nullptr);
  if(!_instance)
    _instance.reset(new Factory());
  return *_instance;
}

bool Factory::register_protocol(QString name, std::shared_ptr<ProtocolMaker> proc)
{
  if(protocoles.contains(name))
  {
    QTextStream err(stderr);
    err << "Error, there is already a protocol named '" << name << "'";
    return false;
  }
  protocoles[name] = proc;
  return true;
}

bool Factory::unregister_protocol(QString name, std::shared_ptr<ProtocolMaker> proc)
{
  auto found = protocoles.find(name);
  if(found != protocoles.end())
  {
    if(found.value() == proc)
    {
      protocoles.erase(found);
      return true;
    }
    else
    {
      QTextStream err(stderr);
      err << "Error, the protocol named '" << name << "' doesn't correspond to this protocol maker" << endl;
    }
  }
  else
  {
    QTextStream err(stderr);
    err << "Error, there are no protocol named '" << name << "'";
  }
  return false;
}

std::shared_ptr<Protocol> Factory::protocol(Experiment& experiment, QString sectionName)
{
    
    auto found = protocolInstances.find(sectionName);
    if (found != protocolInstances.end())
    {
        QTextStream err(stderr);
        err << "found already existing protocol instance, sectionName:" << sectionName << endl;
        return found.value();
    }
    else
    {
        QTextStream err(stderr);
        err << "instance of sectionName:" << sectionName << " not found, trying to create" << endl;
        std::shared_ptr<Protocol> prot = create(sectionName, experiment);
        if (prot != nullptr)
        {
            protocolInstances.insert(sectionName, prot);            
        }
        return prot;        
    }
}

std::shared_ptr<Protocol> Factory::create(QString sectionName, Experiment& experiment)
{
    QTextStream err(stderr);
    err << "create() tries to make an instance of sectionName:" << sectionName << endl;
    const Parms& parms = experiment.parms();
    QString protocolType;
    bool isOK = true;
    isOK = parms(sectionName, "Type", protocolType);
    if (isOK)
    {
        QTextStream err(stderr);
        err << "found protocol type:" << protocolType << endl;
        auto found = protocoles.find(protocolType);
        if(found != protocoles.end())
            return found.value()->create(experiment, sectionName);
    }
    return std::shared_ptr<Protocol>(nullptr);
}