#include "SmarActCommands.hpp"

Command_SA_GetPosition_S::Command_SA_GetPosition_S(): SmarActCommand() {
}

Command_SA_GetPosition_S::~Command_SA_GetPosition_S() {
}

bool Command_SA_GetPosition_S::Send(SA_INDEX systemID, SA_INDEX axisID, signed int* position)
{
    SA_STATUS status = SA_GetPosition_S(systemID, axisID, position);    
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        //printf("just got position: %d\n", *position);
        return true;
    }
    else
    {
        //printf("just got ERROR position: %d\n", *position);
        return false;
    }
}

Command_SA_GetStatus_S::Command_SA_GetStatus_S(): SmarActCommand() {
}


Command_SA_GetStatus_S::~Command_SA_GetStatus_S() {
}

bool Command_SA_GetStatus_S::Send(SA_INDEX systemID, SA_INDEX axisID, unsigned int* axisStatus)
{
    SA_STATUS status = SA_GetStatus_S(systemID, axisID, axisStatus);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Command_SA_GotoPositionAbsolute_S::Command_SA_GotoPositionAbsolute_S() : SmarActCommand() {
}

Command_SA_GotoPositionAbsolute_S::~Command_SA_GotoPositionAbsolute_S() {
}

bool Command_SA_GotoPositionAbsolute_S::Send(SA_INDEX systemID, SA_INDEX axisID, signed int position, unsigned int holdMs)
{
    SA_STATUS status = SA_GotoPositionAbsolute_S(systemID, axisID, position, holdMs);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Command_SA_SetClosedLoopMaxFrequency_S::Command_SA_SetClosedLoopMaxFrequency_S() {
}

Command_SA_SetClosedLoopMaxFrequency_S::~Command_SA_SetClosedLoopMaxFrequency_S() {
}

bool Command_SA_SetClosedLoopMaxFrequency_S::Send(SA_INDEX systemID, SA_INDEX axisID, unsigned int frequencyHz)
{
    //valid range for SmarAct: 50-18500Hz
    SA_STATUS status = SA_SetClosedLoopMaxFrequency_S(systemID, axisID, frequencyHz);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }        
}

Command_SA_SetClosedLoopMoveSpeed_S::Command_SA_SetClosedLoopMoveSpeed_S() {
}

Command_SA_SetClosedLoopMoveSpeed_S::~Command_SA_SetClosedLoopMoveSpeed_S() {
}

bool Command_SA_SetClosedLoopMoveSpeed_S::Send(SA_INDEX systemID, SA_INDEX axisID, unsigned int speedNmPerSecond)
{
        //valid range for SmarAct: 0-100000000. Value 0 disables limit.
    SA_STATUS status = SA_SetClosedLoopMoveSpeed_S(systemID, axisID, speedNmPerSecond);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }       
}

Command_SA_Stop_S::Command_SA_Stop_S() {
}

Command_SA_Stop_S::~Command_SA_Stop_S() {
}

bool Command_SA_Stop_S::Send(SA_INDEX systemID, SA_INDEX axisID)
{
    SA_STATUS status = SA_Stop_S(systemID, axisID);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
    return true;
}

Command_SA_InitSystems::Command_SA_InitSystems() {
}

Command_SA_InitSystems::~Command_SA_InitSystems() {
}

bool Command_SA_InitSystems::Send(unsigned int configuration)
{
    SA_STATUS status = SA_InitSystems(configuration);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
    return true;
}


Command_SA_SetZeroPosition::Command_SA_SetZeroPosition() {
}

Command_SA_SetZeroPosition::~Command_SA_SetZeroPosition() {
}

bool Command_SA_SetZeroPosition::Send(SA_INDEX systemID, SA_INDEX axisID)
{
    printf("sending command SA_SetZeroPosition...\n");
    printf("systemID: %d\n", systemID);    
    printf("axisID: %d\n", axisID);    
    SA_STATUS status = SA_SetZeroPosition_S(systemID, axisID);
    printf("returned status: %d\n", status);
    _result.SetStatus(status);    
    if (status == SA_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
    return true;
}