/* 
 * File:   HardwareController.cpp
 * Author: cfmadmin
 * 
 * Created on February 25, 2013, 5:33 PM
 */

#include "HardwareController.hpp"
#include "parms.hpp"
#include "Axis.hpp"
#include "Sensor.hpp"
#include "StateManager.hpp"

HardwareController::HardwareController(QString name, StateManager& stateManager): _stateManager(stateManager) {
    _name = name;
    _initialized = false;    
}
Axis* HardwareController::BuildAxis(const Parms& parms, QString name)
{
    return 0; //you can implement something meaningful in derived classes
}

Sensor* HardwareController::BuildSensor(const Parms& parms, QString name)
{
    return 0; //you can implement something meaningful in derived classes
}

StateManager& HardwareController::stateManager()
{
    return this->_stateManager;
}