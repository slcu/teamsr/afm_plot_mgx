/* 
 * File:   Robot.cpp
 * Author: cfmadmin
 * 
 * Created on February 7, 2013, 9:42 PM
 */

#include "Robot.hpp"
#include "Experiment.hpp"
#include "ForceMeasurement.hpp"
#include "Axis.hpp"
#include "Sensor.hpp"

Robot::Robot(Experiment& experiment): _hardwareManager(experiment.hardwareManager()), _stateManager(experiment.stateManager())
{
}

Robot::~Robot() {
}

bool Robot::readParameters(const Parms& parms)
{
    bool isOK = true;    
    //TODO: parse the parameters
 /*   double sensorMaxForce = 0.0;
    isOK &= parms("ForceSensorSettings", "SensorMaxForce", sensorMaxForce);                    
    if (!isOK)
    {        
        return false;
    }    
    double sensorGain = 100000.0; //too high gain is safe, too low is not
    isOK &= parms("ForceSensorSettings", "Gain", sensorGain);                    
    if (!isOK)
    {        
        return false;
    }            */
//    forceMeasurement().SetSensorMaxForce(sensorMaxForce);
//    forceMeasurement().SetSensorGain(sensorGain);    
    //TODO: set units ? forceMeasurement.SetForceUnit    
    return isOK;
}
/*
bool Robot::ParseRecipeFromIni(const Parms& robotRecipePar, RobotRecipe& robotRecipe)
{   //TODO: implement
    QString axisEntryStr = "";
    bool isOK = true;
    bool keepReading = true;
    RobotRecipe robotRecipeTmp;
    robotRecipeTmp.entries.clear();
    int entryNum=0;
    do
    {
        isOK &= robotRecipePar("RobotRecipe", "AxisEntry"+QString("%1").arg(entryNum), axisEntryStr);
        if (!isOK)
        {
            break;
        }
        QStringList tokens = axisEntryStr.split(" ");//TODO: use regexp to define whitespace
        if (tokens.size() != 5)
        {
            break;
        }
        RobotRecipeEntry entry;        
        bool ok; //TODO: there is one boolean variable in use already
        entry.actuatorSystemID = tokens.at(0).toUInt(&ok, 10);
        if (!ok)
        {
            break;
        }
        entry.localAxisID = tokens.at(1).toUInt(&ok, 10);
        if (!ok)
        {
            break;
        }            
        entry.robotAxisID = tokens.at(2).toUInt(&ok, 10);
        if (!ok)
        {
            break;            
        }         
        entry.name = tokens.at(3);
        entry.flip = tokens.at(4).toInt(&ok, 10);
        if (!ok)
        {
            break;            
        }         
        robotRecipeTmp.entries.push_back(entry);
        entryNum++;
    }while (keepReading);    
    robotRecipe=robotRecipeTmp;   
    return true;
}
*/

///TODO: implement
//bool Robot::AttachAxis(ActuatorAxis* axis)
//{
    //attach
    //register
//    return false;
//}

//bool Robot::BuildFromRecipe(RobotRecipe recipe) // TODO: is this the right place to register axes in the state manager ?
//{   //TODO: implement
    /*
    printf("Building robot from recipe...\n");
    for (int i=0; i<recipe.entries.size(); i++)
    {
        printf("-Recipe entry %d: robotAxisID: %d, systemID: %d, localAxisID: %d, name: %s, flip %d\n", 
                i, recipe.entries.at(i).robotAxisID,
                   recipe.entries.at(i).actuatorSystemID,
                   recipe.entries.at(i).localAxisID,
                   recipe.entries.at(i).name.toLocal8Bit().data(),
                   recipe.entries.at(i).flip                   );         
        //TODO: check that robotAxisID is unique within the recipe !
        QString controller_name;
        recipe(PartName, "Controller", controller_name);
        Controller* controller = _hardwareManager.GetController(recipe, controller_name);
        ActuatorAxis* axis= controller->CreateAxis(recipe, PartName);
        axis = _hardwareManager.GetAxisPointer(                               
                                recipe.entries.at(i).actuatorSystemID,
                                recipe.entries.at(i).localAxisID  );
        if (axis != 0)
        {
            controller->createAxis(PartName);
            bool result = recipe(PartName, "Name", axis->name());
            result &= recipe(PartName, "Flip", axis->flip());
            // ...
            if(!result)
                return false;
                    
            axis->SetName(recipe.entries.at(i).name);
            axis->SetFlip(recipe.entries.at(i).flip);
            printf("-adding axis to the robot\n");
            _axes.push_back(axis); //TODO: consider this: robot may have the physical axis and the logical axis, logical axis has a meaningful name            
            //_fieldIdPositionMicrons = _stateManager.RegisterFieldExc("Position", "(um)", true);        
            //TODO: register field for the axes            
            //TODO: use -D_GLIBCXX_DEBUG
        }
        else
        {
            printf("error adding axis %d, specified axis not found in the hardware resources\n", i);            
            //TODO: abort the whole building process 
            return false;
        }
    }       
    RegisterFields();//TODO: handle result
    return true;
     * */
    
//    return false;
//}
/*
bool Robot::Goto(int axis, double positionMicrons)
{   //printf("called Robot::Goto\n");
    //printf("_axes.size(): %d\n", _axes.size());
    if (axis < _actuatorAxes.size()) //otherwise you cannot call .at(i)
    {
        if (_actuatorAxes.at(axis) != 0)
        {   
            //printf("calling goto on axis named %s\n", _axes.at(axis)->Name().toLocal8Bit().data());
            return _actuatorAxes.at(axis)->GotoSync(positionMicrons);         //there are two goto methods: sync and async
        }
    }    
    printf("ERROR: axis does not exist\n");
    return false;    
}
*/
/*
bool Robot::GetPosition(int axis, double& positionMicrons)
{   //printf("called Robot::GetPosition\n");
    if (axis < _actuatorAxes.size()) //otherwise you cannot call .at(i)
    {
        if (_actuatorAxes.at(axis) != 0)
        {   
            return _actuatorAxes.at(axis)->GetPositionSync(positionMicrons);
        }
    }
    printf("ERROR: axis does not exist\n");
    return false;
}
 * */
///Sets axis option, returns false if option is unsupported or if axis is missing
/*
 bool Robot::SetAxisOption(int axis, AxisOption option, double value)
 {  //printf("called Robot::SetAxisOption\n");
    if (axis < _actuatorAxes.size()) //otherwise you cannot call .at(i)
    {
        if (_actuatorAxes.at(axis) != 0)
        {   
            return _actuatorAxes.at(axis)->SetOption(option, value);
        }
    }
    printf("ERROR: axis does not exist\n");
    return false;
 }
 */
/*
///Call it if you want to update state of an axis selectively without updating other axes
bool Robot::UpdateAxisState(int axis) 
{
    bool isOK = true;
    //double positionMicrons;
    //isOK &= GetPosition(axis, positionMicrons);    //TODO: instead, call the axis directly, it knows better what to update
    if (axis < _actuatorAxes.size()) //otherwise you cannot call .at(i)
    {
        if (_actuatorAxes.at(axis) != 0)
        {   
            isOK &= _actuatorAxes.at(axis)->UpdateState(); //TODO: actually, within one axis there might be several attributes and you might wish to update them selectively, so additional parameter would be needed... but for now it's enough, just everything will be updated at once
        }
        else
        {
            isOK = false;
        }        
    }    
    else
    {
        isOK = false; //TODO: looks very ugly !
    }
    return isOK;
} 
*/
/*
///Call it to update state for all axes
bool Robot::UpdateRobotState() 
{
    //_stateManager.
    bool isOK = true;
//    int positionMicrons;    
    for (int i=0; i<this->_actuatorAxes.size(); i++)
    {    
        isOK &= UpdateAxisState(i);
        if (isOK)
        {
            //_stateManager.state().Update(_fieldId, positionMicrons);           //TODO: how to store axis state in state manager ? 
        } 
        else
        {   //TODO:handle error       
            throw RobotException();
        }
    }
    return isOK;
}
 * */

///Registers all fields to the StateManager
//bool Robot::RegisterFields()
//{
//    bool isOK = true;
    //TODO: register fields for the robot and all axes    
    //TODO: actuator axes is outdated, use _axes and _sensors
//    int i=0;
    /*
    for (i=0; i<this->_actuatorAxes.size(); i++)
    {    
        isOK &= _actuatorAxes.at(i)->RegisterFields();//TODO: handle result/error
        if (!isOK)
        {
            printf("Error registering fields for the robot, error occured for axis %d\n", i);
            return false;
        }                
    }
    */    
/*    for (i=0; i<_axes.size(); i++)
    {
        isOK &= _axes[i].RegisterFields();
        printf("Error registering fields for the robot, error occured for axis %d\n", i);
        return false;
    }    
    for (i=0; i<_sensors.size(); i++)
    {
        isOK &=  _sensors[i].RegisterFields();
        printf("Error registering fields for the robot, error occured for sensor %d\n", i);
        return false;        
    }        */
//    return isOK;    
//}

void Robot::AddAxis(Axis* axis)
{
    printf("inside Robot:AddAxis\n");
    if (axis!=0)
    {
        _axes.push_back(axis);
    }
}

void Robot::AddSensor(Sensor* sensor) ///TODO: handle error, registering may fail
{
    printf("inside Robot:AddSensor\n");
    if (sensor)
    {
        _sensors.push_back(sensor);
        bool result = false;
        result = sensor->RegisterSensorToHardwareController();
       // sensor->hardwareController().RegisterSensor(sensor);
    }
}

bool Robot::Initialize()
{    
    // browse through internal structures (controllers, axes, sensors and initialize)
    printf("Inside Robot::Initialize()\n");
    bool isOK = true;
    int i=0;    
    for (i=0; i<_axes.size(); i++)
    {
        isOK &= _axes[i]->Initialize();      
        if (!isOK)
        {
            printf("ERROR: failed to initialize axis %s\n", _axes[i]->Name().toLocal8Bit().data());
            return false;
        }
    }
    for (i=0; i<_sensors.size(); i++)
    {
        isOK &= _sensors[i]->Initialize();
        if (!isOK)
        {
            printf("ERROR: failed to initialize sensor %s\n", _sensors[i]->Name().toLocal8Bit().data());
            return false;
        }
    }    
    return isOK;
}

///returns sensor pointer by name or null if not found
Sensor* Robot::sensor(QString sensorName)
{
    for (int i=0; i<_sensors.size(); i++)
    {
        if (_sensors[i]->Name() == sensorName)
        {
            return _sensors[i];
        }
    }
    return 0;
}

///returns axis pointer by name or null if not found
Axis* Robot::axis(QString axisName)
{
    for (int i=0; i<_axes.size(); i++)
    {
        if (_axes[i]->Name() == axisName)
        {
            return _axes[i];
        }
    }        
    return 0;
}

Axis* Robot::indentationAxis()
{
    Sensor* s = indentationSensor();
    if (s != 0)
    {        
        return axis(((ForceSensor*)s)->AxisName());
    }
    printf("ERROR: indentationSensor does not exist\n");
    if (_sensors.size() > 0)
    {
        for (int i=0; i<_sensors.size(); i++)
        {
           printf("sensor: %s\n", _sensors.at(i)->Name().toLocal8Bit().data());
        }        
    }
    else
    {
        printf("no available sensors\n");
    }
    
    return 0;
}

Sensor* Robot::indentationSensor()
{    
    return sensor("IndentationForceSensor");
}

/*
QString Robot::GetAxisName(int axis_id)
{
    return _axes[axis_id]->Name();
}
 * */