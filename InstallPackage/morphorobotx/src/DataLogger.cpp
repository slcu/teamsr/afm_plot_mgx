/* 
 * File:   DataLogger.cpp
 * Author: huflejt
 * 
 * Created on November 7, 2012, 11:27 AM
 */

#include "DataLogger.hpp"
#include "StateManager.hpp"

DataLogger::DataLogger(StateManager& stateManager): _stateManager(stateManager)
{
    //this->state = state;
    //this->filename = filename;
    //this->file.setFileName(filename);        
    isHeaderPrepared = false;
    _fieldIDTime   = _stateManager.RegisterFieldExc(QString("LoggerTime"), QString("(ms)"), true);  
    timer.Reset();
}

/*DataLogger::DataLogger(const DataLogger& orig) {
}*/

DataLogger::~DataLogger() {
}

void DataLogger::SetFilename(QString filename)
{
    this->filename = filename;
    this->file.setFileName(filename);      
    printf("setting filename to %s\n", filename.toLocal8Bit().data());
}

bool DataLogger::PrepareFileHeader()
{
    bool isOK = true;
    
    QStringList headers = _stateManager.state().GetHeaders();    
        
    if(!file.open(QIODevice::WriteOnly))
    {    
        printf("DataLogger::PrepareFileHeader() failed, cannot open file %s for writing \n", filename.toLocal8Bit().data());
        //err << "DataLogger::PrepareFile:Error, cannot open file '" << filename << "' for writing" << endl;
        return false;
    }
    //QTextStream textStream(&file);
    stream.setDevice(&file);
    int i=0;
    for (i=0; i<headers.count(); i++)
    {
        stream << headers.at(i) << ",";
    }
    stream << endl;
    isHeaderPrepared = isOK;
    return isOK;
}

bool DataLogger::WriteCurrentState()
{
    bool isOK = true;
    
    _stateManager.state().Update(_fieldIDTime, timer.TimeElapsedMs()); //TODO: consider absolute time
    
    isOK &= this->valid();
    if(!isOK)
    {
        printf("ERROR: DataLogger::WriteCurrentState() failed, because this->valid() is false \n");  
        printf("this->filename is %s\n", this->filename.toLocal8Bit().data());
        return false;
    
    }
    if (!isHeaderPrepared)
    {
        isOK &= PrepareFileHeader();
        if(!isOK)
        {
            printf("ERROR: DataLogger::WriteCurrentState().PrepareFileHeader() failed\n");  
            return false;    
        }
    }
    
    QStringList currentValues;
    
    isOK &= _stateManager.state().DumpCurrentValues(currentValues);
    
    if(!isOK)
    {        
        printf("ERROR: stateManager.state.GetCurrentValues(currentValues) failed\n");
        return false;
    }
    
    int i=0;
    for (i=0; i<currentValues.count(); i++)
    {        
        stream << currentValues.at(i) << ",";
    }
    stream << endl;
    
        
    return isOK;
}

