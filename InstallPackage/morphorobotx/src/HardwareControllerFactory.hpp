/* 
 * File:   HardwareControllerFactory.hpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 6:51 PM
 */

#ifndef HARDWARECONTROLLERFACTORY_HPP
#define	HARDWARECONTROLLERFACTORY_HPP


#include <memory>
#include "stdlib.h"
#include "QString"

class Parms;
class HardwareController;
class StateManager;

class HardwareControllerFactory {
public:
    HardwareControllerFactory();
    HardwareControllerFactory(const HardwareControllerFactory& orig);
    HardwareController* hardwareController(const Parms& parms, QString hardwareControllerName, StateManager& stateManager);
    virtual ~HardwareControllerFactory();
protected:

};

#endif	/* HARDWARECONTROLLERFACTORY_HPP */

