/* 
 * File:   SensorFactory.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 10:34 PM
 */

#ifndef SENSORFACTORY_HPP
#define	SENSORFACTORY_HPP

#include "Sensor.hpp"

class Parms;

class SensorFactory {
public:
    SensorFactory();
    Sensor* sensor(const Parms& parms, QString sensorName, HardwareController* hardwareController);
    virtual ~SensorFactory();
private:

};

#endif	/* SENSORFACTORY_HPP */

