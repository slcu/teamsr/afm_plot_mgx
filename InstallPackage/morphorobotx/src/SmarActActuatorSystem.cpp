/* 
 * File:   SmarActRobot.cpp
 * Author: huflejt
 * 
 * Created on November 5, 2012, 6:11 PM
 */

#include "SmarActActuatorSystem.hpp"
//#include "ZaberCommander.h"

SmarActActuatorSystem::SmarActActuatorSystem(unsigned int systemID, unsigned int localSystemID): ActuatorSystem(systemID, localSystemID) 
{        
//    commander.itsDevice = 0;
//    commander.verbose = 0;    
    //InitializeStatusMap(); // clears and writes tuples to StausMap;    
    //TODO: register fields here
    //TODO: wrong, you don't know what axes there are, let the axes handle this
    /*_fieldIdPositionMicrons0 = _stateManager.RegisterFieldExc("Position0", "(microns)", true);
    _fieldIdPositionMicrons1 = _stateManager.RegisterFieldExc("Position1", "(microns)", true);
    _fieldIdPositionMicrons2 = _stateManager.RegisterFieldExc("Position2", "(microns)", true);
    _fieldIdCrystalState0    = _stateManager.RegisterFieldExc("Crystal0", "(unitless)", true);
    _fieldIdCrystalState1    = _stateManager.RegisterFieldExc("Crystal1", "(unitless)", true);
    _fieldIdCrystalState2    = _stateManager.RegisterFieldExc("Crystal2", "(unitless)", true);*/
}
/*
bool SmarActActuatorSystem::InitSystem() //TODO: remove, the system has been initialised during acquisiton of the Hardware
{
	//TODO: implement it
        //	commander.InitSystem();
	return false;
}
 * */
/*
bool SmarActActuatorSystem::GotoPositionAbsolute(int axis, int position)
{	
	int retval1 = commander.GotoPositionAbsolute(axis, position);
	int retval2 = SA_OK;
	if (retval1 != SA_OK)
	{
		retval2 = commander.GotoPositionAbsolute(axis, position);
	}
	if (retval2 !=SA_OK)
	{
		return false;
	}
	return true;
}
*/
SmarActActuatorSystem::~SmarActActuatorSystem() {
}
/*
bool SmarActActuatorSystem::Init(int acceleration) //TODO: remove, it's wrong
{
    //ZaberCommander_Init(&commander, acceleration);
//    commander.InitSystem();
    return true; // TODO: parse error response from ZaberCommander
}
*/
/*
bool SmarActActuatorSystem::GetPosition(int axis, int& position) //TODO: remove, it's wrong
{        
    bool isOK = true;
//  isOK &= commander.GetPosition(axis, position);
    //TODO: if not ok, try again, make it robust
//  isOK &= !ZaberCommander_ReturnCurrentPosition(&commander, &position);    //this function expects pointers and returns zero on success        
    return isOK;    
}
 * */
/*
bool SmarActActuatorSystem::UpdateRobotState(int axis) //TODO: remove, it's wrong
{
    //_stateManager.
    bool isOK = true;
    int position;
//TODO: ask the robot at least twice, make it robust

    isOK &= GetPosition(axis, position);    
    if (isOK)
    {
	switch(axis)
	{
            case 0:
                    _stateManager.state().Update(_fieldIdPositionMicrons0, position/1000.0);
                    break;
            case 1: 
                    _stateManager.state().Update(_fieldIdPositionMicrons1, position/1000.0);
                    break;
            case 2: 
                    _stateManager.state().Update(_fieldIdPositionMicrons2, position/1000.0);
                    break;
	}	
    } 
    else
    {
        //TODO:handle error       
        throw RobotException();
    }
    return isOK;
}
 * */


/*
bool SmarActActuatorSystem::GotoPosition(int axis, int targetPosition)
{    
    //ZaberCommander_GotoAbsolute(&commander, targetPosition);   
    commander.GotoPositionAbsolute(axis, targetPosition);
    //time begin is updated in state().Renew()
    //_stateManager.state().UpdateTimeBegin();    
    return true; // TODO: parse error response from ZaberCommander
}
*/
/*
 bool SmarActActuatorSystem::Goto(int axisID, double positionMicrons)//TODO: remove, it's wrong
 {
     //TODO: implement
     printf("inside SmarActActuatorSystem::Goto\n");
     printf("LocalSystemID() : %d\n", LocalSystemID());
     printf("axisID : %d\n", axisID);
     printf("positionMicrons*1000 : %d\n", positionMicrons*1000);
     SA_STATUS status = SA_GotoPositionAbsolute_S(LocalSystemID(), axisID, positionMicrons*1000, 100);//TODO: very ugly ! move it down to the competent layer !
    
     return true;
 }
 * */
/*
bool SmarActRobot::isHomePositionKnown()
{
    int setting;
    int retval = ZaberCommander_ReturnSetting(&commander, &setting);
    
    //printf("isHomeKnown() returned %d\n", setting);
    setting &= 128; // this bit indicates if the robot knows its home position
    //printf("isHomeKnown() &= 128 is %d\n", setting);
    if (retval)
    {
        return false; //something went wrong
    }
    if (setting == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
    
}
*/
/*
bool SmarActRobot::GoHome()
{
    int retval1 = ZaberCommander_FlushInputBuffer(&commander, 500);
    int retval2 = ZaberCommander_GoHome(&commander);
    int retval3 = ZaberCommander_FlushInputBuffer(&commander, 500);        
    
    if (retval2)
    {
        return false; //something went wrong
    }
    else        
    {
        //TODO: update time begin
        //_stateManager.state().UpdateTimeBegin();
        //state->UpdateTimeBegin();
        return true;
    }
}
*/