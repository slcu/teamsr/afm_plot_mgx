/* 
 * File:   SmarActHardwareController.cpp
 * Author: cfmadmin
 * 
 * Created on February 25, 2013, 5:39 PM
 */

#include <MCSControl.h>

#include "SmarActHardwareController.hpp"
#include "Robot.hpp"
#include "SmarActAxis.hpp"
#include "SmarActCommand.hpp"
#include "SmarActCommands.hpp"
#include "parms.hpp"

SmarActHardwareController::SmarActHardwareController(const Parms& parms, QString name, StateManager& stateManager) : HardwareController(name, stateManager) 
{
    bool isOK = true;
    unsigned int hardwareID=0;
    isOK &= parms(name, "HardwareID", hardwareID);        
    if (!isOK)
    {
        printf("Could not parse RobotRecipe -> (...) -> %s -> HardwareID \n", name.toLocal8Bit().data());        
    } 
    else
    {
        _hardwareID = hardwareID;    
         SmarActTopLevelController::getInstance().RegisterHardwareID(_hardwareID); //this ID is added to a list of systems for later initialization
    }    
    _isInstanceValid = isOK; //this marks if the instace is valid or not  
}

SmarActHardwareController::~SmarActHardwareController() {
}

///register to the top level object, later this hardwareID will be initialized
void SmarActHardwareController::RegisterHardwareID()
{    
    SmarActTopLevelController::getInstance().RegisterHardwareID(_hardwareID);
}    

bool SmarActHardwareController::Set(int option, double value)
{
    //TODO: implement
    return false;
}

bool SmarActHardwareController::Get(int option, double& value)
{
    //TODO: implement
    return false;
}

Axis* SmarActHardwareController::BuildAxis(const Parms& parms, QString axisName)
{
    printf("within SmarActHardwareController::BuildAxis, axis name: %s\n", axisName.toLocal8Bit().data());
    SmarActAxis* axis = new SmarActAxis(parms, axisName, this);
    printf("axis pointer is %d\n", axis);    
    if (axis)
    {
        _axes.push_back(axis);
    }    
    return axis; 
}
//TODO: first, implement and use commands in SmarActCommands.hpp, don't use raw
bool SmarActHardwareController::Initialize()
{    
    bool isOK = true;
    printf("inside SmarActHardwareController::Initialize()\n");       
    printf(" - controller name: %s\n",this->Name().toLocal8Bit().data());
    if (isInitialized())
    {
        printf("is already initialized - doing nothing\n");
        return true;
    }
    /*printf(" - axes to initialize:\n");
    int i=0;
    for (i=0; i<_axes.size(); i++)
    {
        printf("axis %d, name: %s\n", i, this->_axes[i]->Name().toLocal8Bit().data());
    }*/
    /*for (i=0; i<_sensors.size(); i++)
    {
        printf("sensor %d, name: %s\n", i, this->_sensors[i]->Name().toLocal8Bit().data());
    }*/    
    
    
    if ( !SmarActTopLevelController::getInstance().isInitialized() )
    {
        //I cannot reach HardwareManager from here to initialize selected controllers of type smaract at once    
        isOK &= SmarActTopLevelController::getInstance().Initialize();
        if (!isOK)
        {
            printf("ERROR: SmarActTopLevelController::getInstance().Initialize() failed\n");
            return false;
        }
    }
    //TODO: here, get your own systemID based on this->_hardwareID    
    unsigned int numberOfInitializedSystems;
    SA_STATUS status = SA_GetNumberOfSystems(&numberOfInitializedSystems);
    if (status != SA_OK)
    {
        status = SA_GetNumberOfSystems(&numberOfInitializedSystems);
        if (status != SA_OK)
        {
            printf("ERROR: SmarActHardwareController::Initialize() failed: SA_GetNumberOfSystems failed\n");
            return false;
        }
    }            
    unsigned int hardwareID=0;
    bool foundMyself = false;
    unsigned int i=0;
    for (i=0; i<numberOfInitializedSystems; i++)
    {
        status = SA_GetSystemID(i, &hardwareID);
        if (status != SA_OK)
        {
            status = SA_GetSystemID(i, &hardwareID);
            if (status != SA_OK)
            {
                return false;
            }
        }
        if (hardwareID == _hardwareID)
        {
            foundMyself = true; 
            _systemID = i; // your i, thats your key
            _initialized = true;
        }              
    }
    if (!foundMyself)
    {
        printf("ERROR: SmarActHardwareController::Initialize() failed: could not find hardwareID %d in the list of successfully initialized systems\n", _hardwareID);
        return false;
    }              
    //TODO: now, according to the manual, query if the system have really been acquired    
    
    return isOK;
}



bool SmarActTopLevelController::Initialize()
{
    printf("SmarActTopLevelController::Initialize()\n");
    SA_STATUS status = SA_ClearInitSystemsList();
    if (status != SA_OK )
    {
        status = SA_ClearInitSystemsList();
        if (status != SA_OK)
        {
            return false;
        }
    }
    
    int i=0;
    printf("_hardwareIDs.size() = %d\n", _hardwareIDs.size());
    for (i=0; i<_hardwareIDs.size(); i++)
    {        
        printf("adding _hardwareIDs[%d] (%u) to init list\n", i, _hardwareIDs[i]);
        
        status = SA_AddSystemToInitSystemsList(_hardwareIDs[i]);
        if (status!=SA_OK)
        {
            printf("ERROR: SmarActTopLevelController::Initialize() failed: unable to add SmarAct system %d to init list\n", _hardwareIDs[i]);
            return false;
        }
    }       
    
    Command_SA_InitSystems cmd;
    bool result = cmd.Send(SA_SYNCHRONOUS_COMMUNICATION);
    if (!result)
    {
        if (cmd.RetryOnceAllowed())
        {
            result = cmd.Send(SA_SYNCHRONOUS_COMMUNICATION);
            if (!result)
            {
                printf("ERROR: tried twice, but could not send command to initialize smaract controllers\n");
                return false;
            }
        }
    }
    //now, check if the number of systems initialized equals the requested numbers.
    //exact checking will be done in each controller individually        
    unsigned int numberOfInitializedSystems = 0;
    status = SA_GetNumberOfSystems(&numberOfInitializedSystems);
    if (status != SA_OK)
    {
        status = SA_GetNumberOfSystems(&numberOfInitializedSystems);
        if (status != SA_OK)
        {
            printf("ERROR: SmarActTopLevelController::Initialize() failed: command SA_GetNumberOfSystems failed\n");
            return false;
        }    
    }
    if (numberOfInitializedSystems != _hardwareIDs.size())
    {
        printf("ERROR: SmarActTopLevelController::Initialize() failed: number of initialized systems different from requested\n");
        printf(" - number of initialized systems: %d, number of requested systems: %d\n", numberOfInitializedSystems, _hardwareIDs.size());
        return false;
    }        
    _isInitialized = true;
    return true;        
}

///release initialized systems and make them available to other processes
bool SmarActTopLevelController::ReleaseSystems()
{
    _isInitialized = false;
    SA_STATUS status = SA_ReleaseSystems();
    if (status != SA_OK )
    {
        status = SA_ReleaseSystems();
        if (status != SA_OK )
        {
            return false;
        }
    }
    return true;
}
/*
bool SmarActHardwareController::BuildFromRecipe(const Parms& parms, QString section)
{   
    printf("within SmarActHardwareController::BuildFromRecipe(parms, QString section== %s\n", section.toLocal8Bit().data());
    //TODOs:
    //parse and build
    //for each subsystem, call build
    return false; ///TODO: implement
}
*/
/*ActuatorAxis* SmarActHardwareSystem::GetAxis(QString name, const RobotRecipe& recipe)
{   
    
    ActuatorAxis * axisp = new SmarActActuatorAxis(_actuatorSystems.at(i), stateManager(), channelType, sensorType, j);
    return axisp;        
}*/
/*

bool isOK = true;
    SA_STATUS status = SA_OK;
    unsigned int DLLVersion = 0;
    status = SA_GetDLLVersion(&DLLVersion); //can more than one library be used on the same machine ? which one is going to be called here ?
    if (status != SA_OK)
    {
        //unable to query the smaract library, very serious error
        isOK = false;
        return isOK;
    }
    //TODO: check if the version is OK. a list of supported versions ?
    printf("SmarActLibraryVersion: %d\n", DLLVersion);
                
    //TODO: is this a task for the smaract commander ? (lower level class handling the commands directly ?)
    unsigned int idList[64];
    unsigned int idListSize = 64;
    //TODO: make sure the pointer correct
    status = SA_GetAvailableSystems(idList, &idListSize);
    if (status != SA_OK)
    {
        //unable to get available systems, perhaps too many entries or something like that
        printf("SA_GetAvailableSystems(idList, &idListSize); failed\n");
        printf("status: %d\n", status);
        isOK = false;
        return isOK;
    }
    printf("found %d Smaract systems\n", idListSize);
    
    //TODO: initialize specific systems only !
    //use SA_ClearInitSystemsList(); 
    //and            
    //SA_AddSystemToInitSystemsList();       
    
    status = SA_InitSystems(SA_SYNCHRONOUS_COMMUNICATION); //TODO: initialize only those that are required, but for now initialize all
    if (status != SA_OK)
    {
        //could not initialize systems, a serious error
        isOK = false;
    }
    //now, get the number of successfully initialized systems
    unsigned int numberOfSystems = 0;
    status = SA_GetNumberOfSystems(&numberOfSystems);
    if (status !=SA_OK)
    {
        //could not ask how many systems are initialized, a serious error
        isOK = false;
    }
    printf("Successfully initialized Smaract systems: %d\n", numberOfSystems);
    
    //now, for each system get their system ID ad add to the pool of available systems
    //with these serial numbers (IDs) the user can compose their robot. Numbering of actuators is unambigious, by ID and axis number.
    
    unsigned int systemID = 0; //tmp variable
    unsigned int nchannels = 0; //tmp variable
    unsigned int systemIdArray[16];
    for (int i=0; i<numberOfSystems; i++)
    {
        status = SA_GetSystemID(i, &systemID); //serial number of smaract controller (green box)
        systemIdArray[i] = systemID; //TODO: implement
        printf("-- creating SmarAct system of ID: %d\n", systemID);
               
        _actuatorSystems.push_back(new SmarActActuatorSystem(systemID, i));//i is the localSystemID
        status = SA_GetNumberOfChannels(i, &nchannels); //nchannels tells how many axes could be connected to the controller box. It does not say how many of the are physically available/connected/usable.
        _actuatorSystems.at(i)->nchannels = nchannels;
        printf("--- number of available channels: %d\n", nchannels);
        for (int j=0; j<nchannels; j++)
        {
            unsigned int channelType = -1; //tmp variable, possible values SA_POSITIONER_CHANNEL_TYPE, SA_END_EFFECTOR_CHANNEL_TYPE
            status = SA_GetChannelType(i, j, &channelType);            
            unsigned int sensorType = -1;
            status = SA_GetSensorType_S(i, j, &sensorType); // here, it's possible to see if a device is unconnected: (sensorType == SA_NO_SENSOR_TYPE)            
            //TODO: implement axes
            printf("---- channel %d: channeltype: %d, sensorType %d\n", j, channelType, sensorType);
            _actuatorSystems.at(i)->AddAxis( new SmarActActuatorAxis(_actuatorSystems.at(i), stateManager(), channelType, sensorType, j) ); //.j is the localID of the axis
            //note: the axes are created here, but not named. The naming depends on the user, who acquires them later or maybe not at all.
        }               
    }                
    
    return true; 
*/