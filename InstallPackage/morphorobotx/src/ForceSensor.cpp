/* 
 * File:   ForceSensor.cpp
 * Author: huflejt
 * 
 * Created on November 6, 2012, 9:07 PM
 */

#include "ForceSensor.hpp"
#include "MeasurementState.hpp"
#include "HardwareController.hpp"
#include "parms.hpp"
#include "ComediHardwareController.hpp"
#include "StateManager.hpp"

ForceSensor::ForceSensor(const Parms& parms, QString sensorName, HardwareController* hardwareController): Sensor(hardwareController) {
    _isInstanceValid = false;
    offsetVoltage = 0.0;

    bool isOK = true;
    _name = sensorName;//TODO: consider if this should be moved to base class constructor
    
    QString sensorSectionName;
    
    isOK &= parms(sensorName, "SensorSectionName", sensorSectionName);
    
    //TODO: parse parms, use sensorName etc.
    isOK &= parms(sensorSectionName, "ADChannel", _ADChannel);
    isOK &= parms(sensorSectionName, "DAChannel", _DAChannel);
    isOK &= parms(sensorSectionName, "DAVoltage", _DAVoltage);
    isOK &= parms(sensorSectionName, "IntegrationTime", _IntegrationTime);
    printf("inside ForceSensor::ForceSensor(...)\n");
    printf("_IntegrationTime = %f\n", _IntegrationTime);
    isOK &= parms(sensorSectionName, "MaxForce", _MaxForce);
    isOK &= parms(sensorSectionName, "Gain", _Gain);
    isOK &= parms(sensorSectionName, "InvertedGain", _InvertedGain);
    isOK &= parms(sensorSectionName, "Axis", _AxisName);
    isOK &= parms(sensorSectionName, "AxisPositivePushing", _AxisPositivePushing);

    //TODO: contact the state manager and register sensor fields
    _fieldIDForce = this->hardwareController()->stateManager().RegisterFieldExc(sensorName+"_Force", "(uN)", true);    
    _fieldIDOffsetVoltage = this->hardwareController()->stateManager().RegisterFieldExc(sensorName+"_OffsetVoltage", "(V)", false);    
    _fieldIDOffsetForce = this->hardwareController()->stateManager().RegisterFieldExc(sensorName+"_OffsetForce", "(uN)", false);    
    _fieldIDVoltage = this->hardwareController()->stateManager().RegisterFieldExc(sensorName+"_Voltage", "(V)", true);    
    //TODO: _fieldIDOffset missing
    
    _isInstanceValid = isOK; 
}

//ForceSensor::ForceSensor(const ForceSensor& orig) {
//}

ForceSensor::~ForceSensor() {
}
/*
double ForceSensor::GetForce(double voltage)
{
    double force = 0.0;
    
    force = voltage*gain - offsetForce;
    
    return force;
}*/

double ForceSensor::GetForce()
{    //TODO: get the force from stateMangaer, take timestamps into accout etc...
    double force = 0.0;
    force = (GetVoltage() - offsetVoltage)*this->_Gain;
    this->hardwareController()->stateManager().state().Update(_fieldIDForce, force);
    //printf("Force %f (uN), offsetVoltage = %f, gain = %f \n", force, offsetVoltage, this->_Gain);
    return force;
}

///TODO: dangerous, no error handling
///TODO: add state manager in between
///TODO: consider: shall every sensor update its voltage in state manager, or the comedi controller should do that for each sensor it samples ?
double ForceSensor::GetVoltage()
{
    double voltage = 0.0;
    //printf("calling ForceSensor::GetVoltage()\n");
    //((ComediHardwareController*)this->_itsHardwareController)->GetVoltage(voltage);
    ((ComediHardwareController*)this->_itsHardwareController)->GetVoltage(this->ADChannel(), voltage);    
    this->hardwareController()->stateManager().state().Update(_fieldIDVoltage, voltage);
    //printf("ForceSensor::GetVoltage() returns %f\n", voltage);
    return voltage;
}

bool ForceSensor::Get(unsigned int option, double& value)
{
    return false; //nothing supported
}

bool ForceSensor::Set(unsigned int option, double value)
{
    return false; //nothing supported
}

bool ForceSensor::Initialize()
{    
    printf("Inside ForceSensor::Initialize()\n");    
    if( !_isInstanceValid)
    {
        printf("ERROR: cannot initialize: instance not valid\n");
        return false;
    }
    
    bool isOK = true;
    //initialize its controller if needed. The acqusition card should already have the list of connected sensors to know how to access them (Register).
    
    if (!this->_itsHardwareController->isInitialized())
    {  
        isOK &= _itsHardwareController->Initialize();
        if (!isOK)
        {
            printf("ERROR: failed to initialize hardware controller\n");
            return false;
        }        
    }    
    
    //power the sensor up (should be possible individually for each output channel)
    PowerUp();        
    
    return isOK;
}

bool ForceSensor::PowerUp()
{
    //TODO: implement
    //use:
    //this->_DAChannel
    //this->_DAVoltage
    //ask comedi to set the voltage out       
    
    //check the voltage and maybe wait until stable (if possible)
    //alternatively, make external waiting for stable reading, and no waiting here
    
    return false;
}

bool ForceSensor::PowerDown()
{
    //TODO: the same as power up, but no need to wait until its ready
    return false;
}

bool ForceSensor::RegisterSensorToHardwareController()
{
    printf("inside of ForceSensor::RegisterSensorToHardwareController\n"); //TODO: remove
    return ((ComediHardwareController*)this->hardwareController())->RegisterSensorForSampling(this);    
}

bool ForceSensor::isPushingPositive()
{
    return _AxisPositivePushing; //TODO: ugly name, rename
}