/* 
 * File:   ZaberHardwareController.hpp
 * Author: cfmadmin
 *
 * Created on February 27, 2013, 3:05 PM
 */

#ifndef ZABERHARDWARECONTROLLER_HPP
#define	ZABERHARDWARECONTROLLER_HPP

#include "HardwareController.hpp"

class ZaberHardwareController : public HardwareController{
public:
    ZaberHardwareController(const Parms& parms, QString name, StateManager& stateManager);
    
    virtual ~ZaberHardwareController();
    virtual bool Get(int option, double& value); ///TODO: require specific type for the option 
    virtual bool Set(int option, double value); ///TODO: require specific type for the option 
    //bool BuildFromRecipe(const Parms& parms, QString section); //TODO: what section...? change parameter name to more informative
    Axis* BuildAxis(const Parms& parms, QString name); //TODO: why pass its own pointer.. ? nonsense !
    bool Initialize();
protected:

};

#endif	/* ZABERHARDWARECONTROLLER_HPP */

