/* 
 * File:   MeasurementState.cpp
 * Author: huflejt
 * 
 * Created on November 6, 2012, 3:54 PM
 */

#include "MeasurementState.hpp"
#include <vector>
#include <limits>
#include "Timer.hpp"



MeasurementState::MeasurementState() {
    //Init();
}

//MeasurementState::MeasurementState(const MeasurementState& orig) {
//}

MeasurementState::~MeasurementState() {
}

int MeasurementState::RegisterField(QString name, QString unit, bool v)
{   
    bool nameAlreadyExists = false;
    
    for (int i=0; i<names.size(); i++)
    {
        if (names.at(i).compare(name) == 0)
        {
            nameAlreadyExists = true;
            break;
        }
    }
    
    if (nameAlreadyExists)
    {
        //TODO: what to do here? Update fields or report error ?
        return -1;        
    }
    else
    {
        names.push_back(name);
        units.push_back(unit);
        vol.push_back(v);
        values.push_back(double_nan);        
        return names.size()-1; //returns the index of registered field
    }
}

QStringList MeasurementState::GetHeaders() const
{
    QStringList list;
    
    int i=0;
    for (i=0; i<names.size(); i++)
    {
        list.push_back(names.at(i) + units.at(i));        
        //list << names.at(i) + units.at(i);
    }
    
    return list;
}

void MeasurementState::Renew()
{
    int i=0;
    for (i=0; i<names.size(); i++)
    {
        if(Volatile(i)) Invalidate(i);
    }
}

static QString valueToString(double d)
{
    if(isnan(d)) return QString();
    return QString::number(d, 'g', 15);
}

bool MeasurementState::DumpCurrentValues(QStringList& stringList)
{
    bool retval = true;
    int i=0;
    /*
    for (i=0; i<names.size(); i++)
    {
        if (!valid.at(i))
        {
            retval = false; ///value not valid
            printf("MeasurementState.GetCurrentValues: value at %d not valid\n", i);
            printf("Value is %s\n", QString::number(values.at(i), '.', 8).toLocal8Bit().data());
            break;
        }
    }
    */
    if (retval) /// if all fields valid
    { 
        stringList.clear();        
        for (i=0; i<names.size(); i++)
        {
            stringList.push_back(valueToString(values.at(i)));
            if(Volatile(i)) Invalidate(i);
        }
    }            
    return retval;
}

bool MeasurementState::Update(const int key, double value)
{
    bool isOK = true;
    
    values.at(key) = value;
    
    //printf("Just updated item with key %d, value %f, valid flag %d\n", key, value, valid.at(key));
 /*   int i=0;
    for (i=0; i<values.size(); i++)
    {
        //printf("names.at(%d):%s, values.at(%d):%f, valid.at(%d):%d \n", i, names.at(i).toLocal8Bit().data(), i, values.at(i),i, valid.at(i) );
    }*/
    return isOK;
}

int MeasurementState::IndexOfField(QString name)
{
    int key = -1;
    for (int i=0; i<names.size(); i++)
    {
        if (names.at(i).compare(name) == 0)
        {
            key = i; //name found at i
            break; // don't look for further occurences
        }
    }
    return key;
}