/* 
 * File:   SmarActResult.hpp
 * Author: cfmadmin
 *
 * Created on February 12, 2013, 6:09 PM
 */

#ifndef SMARACTRESULT_HPP
#define	SMARACTRESULT_HPP

#include "QString"
#include "MCSControl.h"
#include "QMap"

class SmarActResult {
public:
    SmarActResult();
    //SmarActResult(const SmarActResult& orig);
    virtual QString GetDescription();
    virtual ~SmarActResult();
    SA_STATUS Status();
    QString GetStatusDescription(SA_STATUS status);
    static QMap<SA_STATUS, QString> StatusMap();        
    void SetStatus(SA_STATUS status)
    {
        _status = status;
    }
    void SetValue(double val)
    {
        _value = val;
    }
    double Value()
    {
        return _value;
    }
    
protected:
    SA_STATUS _status;
    double _value;

};

#endif	/* SMARACTRESULT_HPP */

