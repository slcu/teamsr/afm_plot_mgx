/* 
 * File:   ForceMeasurement.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 2:47 PM
 */

#ifndef FORCEMEASUREMENT_HPP
#define	FORCEMEASUREMENT_HPP

#include "ADCard.hpp"
#include "ForceSensor.hpp"
//#include "MeasurementState.hpp"
//#include "StateManager.hpp"
//#include "Experiment.hpp"
//#include "Robot.hpp"

class Robot;
class Parms;

class ForceMeasurement {
    
public:
    explicit ForceMeasurement(Robot& robot, HardwareController* hardwareController);
    
    virtual ~ForceMeasurement();
    
    enum ForceUnit {UNIT_NEWTON, UNIT_MICRONEWTON};
    
    bool Init(int integrationTimeMs);
    
    void SetSensorGain(double gain)
    {
//        sensor.SetGain(gain);
    };
    void SetSensorMaxForce(double maxForce) 
    {
//        sensor.SetMaxForce(maxForce);   
    };
    void SetSensorOffsetForce(double offsetForce);    
    
    void SetForceUnit(ForceUnit unit) //TODO: not so good, beacuse update of units should update the column name in the log
    {
        forceUnit = unit;
    }
    
    bool TareSensor()
    {
        bool isOK = true;        
        double force = 0.0;
        double voltage = 0.0;
        isOK &= GetForceAndVoltage(ForceMeasurement::UNIT_NEWTON, force, voltage);
        if (isOK)
        {                       
            SetSensorOffsetForce(force); //TODO: this invalidates the value of force
        }        
        return isOK;
    }
    /**
     * 
     * @return 
     */
    bool UpdateForce();        
    
   // bool readParameters(const Parms& parms);    
    
        
protected:
    ADCard card;
//    ForceSensor sensor;    
    //Experiment& _experiment;    
    Robot& _robot;
    ForceUnit forceUnit;   
    
    bool GetForceAndVoltage(ForceMeasurement::ForceUnit unit, double& force, double& voltage);
    
    int _fieldIdForce;
    int _fieldIdVoltage;
    int _fieldIdAssumedForceOffset;
};

#endif	/* FORCEMEASUREMENT_HPP */

