/* 
 * File:   Robot.hpp
 * Author: cfmadmin
 *
 * Created on February 7, 2013, 9:42 PM
 */

#ifndef ROBOT_HPP
#define	ROBOT_HPP

#include "QString"
#include "ForceMeasurement.hpp"
//#include "ActuatorAxis.hpp"
#include "Axis.hpp"


//NOTE: this is added only to support doxygen in recognizing vectors as links between classes
/*namespace std {
    template<class T> class vector { public : T element; }; 
};*/

class ActuatorAxis;
//enum AxisOption;
class HardwareManager;
class StateManager;
class Experiment;
class Parms;
class Axis;
class Sensor;
enum AxisOption: unsigned int; ///this is legal only in C++0x, forward declaration of enum

typedef struct {
    unsigned int robotAxisID;
    unsigned int actuatorSystemID;
    unsigned int localAxisID;
    QString name;
    signed int flip; ///+1 or -1, a multiplier for the coordinates ///TODO: change into true false
} RobotRecipeEntry;

///TODO: replace that with simply parameter file (robot recipe file)
typedef struct
{
    std::vector<RobotRecipeEntry> entries;
} RobotRecipe;   

class Robot {
public:
    Robot(Experiment& experiment);    
    virtual ~Robot();
    ///TODO: change RobotRecipe into a reference to par object
    //bool BuildFromRecipe(RobotRecipe recipe); ///TODO: not needed, remove 
    ///this time, int axis means the i-th axis in the robot collection, no relation to hardware id's
    //bool Goto(int axis, double positionMicrons); 
    //bool GetPosition(int axis, double& positionMicrons);
    //bool SetAxisOption(int axis, AxisOption option, double value);
    //bool UpdateAxisState(int axis);
    //bool UpdateRobotState();
    //bool RegisterFields();//TODO: probably not needed ! OK, maybe for additional fields defined above the axes and sensors
    //bool ParseRecipeFromIni(const Parms& robotRecipePar, RobotRecipe& robotRecipe);    
    bool readParameters(const Parms& parms);
    bool Initialize();
/*    ForceMeasurement& forceMeasurement()
    {
        return _forceMeasurement;
    }
 * */
    StateManager& stateManager()
    {
        return _stateManager;
    }
    void AddAxis(Axis* axis); //TODO: bool ?
    void AddSensor(Sensor* sensor); //TODO: bool ?
    /*
    QString GetAxisName(int axis_id); //TODO: remove it !
    */
    Sensor* sensor(QString sensorName);
    Axis* axis(QString axisName);
    Axis* indentationAxis();
    Sensor* indentationSensor();

    
protected:
    HardwareManager& _hardwareManager;
    std::vector<ActuatorAxis*> _actuatorAxes;//the old way of doing it
    std::vector<Axis*> _axes; //the new way, some things are missing
    std::vector<Sensor*> _sensors;
    StateManager& _stateManager;
    //ForceMeasurement _forceMeasurement;
};

#endif	/* ROBOT_HPP */

