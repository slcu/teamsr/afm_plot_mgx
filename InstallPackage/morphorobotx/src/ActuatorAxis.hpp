/* 
 * File:   ActuatorAxis.hpp
 * Author: cfmadmin
 *
 * Created on February 1, 2013, 9:09 PM
 */

#ifndef ACTUATORAXIS_HPP
#define	ACTUATORAXIS_HPP

//#include "ActuatorSystem.hpp"
#include "QString"
//#include "StateManager.hpp"

class ActuatorSystem;
class StateManager;

/*
typedef enum AxisOption
{
    GOTO_TIMEOUT_MS=0,
    GOTO_HOLD_MS=1,
    GOTO_PRECISION_MARGIN_MICRONS=2,
    SPEED_LIMIT_MICRONS_PER_SECOND=3,
    FREQUENCY_LIMIT_HZ=4,
    ACCELERATION_LIMIT_MICRONS_PER_SECOND_SQUARE=5    
} AxisOption;
*/
class ActuatorAxis {
public:
    ActuatorAxis(ActuatorSystem* actuatorSystem, StateManager& stateManager, unsigned int localID);
    //ActuatorAxis(const ActuatorAxis& orig);
    virtual ~ActuatorAxis();
    ActuatorSystem* itsActuatorSystem()
    {
        return _itsActuatorSystem;
    }
    virtual bool GotoAsync(double positionMicrons)=0;
    virtual bool GotoSync(double positionMicrons)=0;
    virtual bool GetPositionSync(double& positionMicrons)=0;
//    virtual bool SetOption(AxisOption option, double value)=0;
    //virtual bool RegisterFields()=0;
    unsigned int LocalID()
    {
        return _localID;
    }
    QString Name()
    {
        return _name;
    }
    void SetName(QString name)
    {
        _name = name;
    }
    void SetFlip(signed int flip)
    {
        _flip = flip;
    }
    virtual bool RegisterFields()=0;
    virtual bool UpdateState()=0;    ///writes actual values to the state
    
protected:
    ActuatorSystem* _itsActuatorSystem;
    StateManager& _stateManager;
    unsigned int _localID;
    int holdMs;
    QString _name;
    int _flip;
};

class RobotException: public std::exception
{    
    virtual const char* what() const throw()
    {        
        return "Robot Exception";
    }
};
#endif	/* ACTUATORAXIS_HPP */

