/* 
 * File:   RobotSubunit.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 7:34 PM
 */

#ifndef ROBOTSUBUNIT_HPP
#define	ROBOTSUBUNIT_HPP

#include "QString"
#include "HardwareController.hpp"
class HardwareController;
class StateManager;
class RobotSubunit {
public:
    ///TODO: should _name be initialized via parameter of the constructor ?
    RobotSubunit(HardwareController* hardwareController);
        
    virtual bool Set(unsigned int option, double value)=0;
    virtual bool Get(unsigned int option, double& value)=0;   
    //virtual bool RegisterFields()=0;
    
    virtual ~RobotSubunit();
    
    QString Name()
    {
        return _name;
    }
    QString ControllerName()
    {
        return _controllerName;
    }
    HardwareController* hardwareController()
    {
        return _itsHardwareController;
    }
    virtual bool Initialize()=0;
    
    bool isInstanceValid()
    {
        return _isInstanceValid;
    }    
    StateManager& stateManager()
    {
        return _itsHardwareController->stateManager();
    }
    
protected:
    QString _name;
    QString _controllerName;
    HardwareController* _itsHardwareController;    
    bool _isInstanceValid=false;
    StateManager& _stateManager;
    
};

#endif	/* ROBOTSUBUNIT_HPP */

