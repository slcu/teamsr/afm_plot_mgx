/* 
 * File:   Sensor.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 4:09 PM
 */

#ifndef SENSOR_HPP
#define	SENSOR_HPP

#include "RobotSubunit.hpp"

class Sensor : public RobotSubunit{
public:
    Sensor(HardwareController* hardwareController);  
    virtual bool RegisterSensorToHardwareController()=0;
    virtual ~Sensor();
    unsigned int ADChannel()
    {
        return _ADChannel;
    }
    double IntegrationTimeMs()
    {
        return _IntegrationTime;
    }
protected:
    unsigned int _ADChannel;
    unsigned int _DAChannel;
    double _DAVoltage;
    double _IntegrationTime;
    double _MaxForce; ///TODO: units ! MIN FORCE too !
    ///TODO: describe force horizon as limits where there is surely some reaction to changes
    ///it is dangerous to use sensor in its saturation zone. _MaxForce specified above saturation leads to a disaster.
    ///The limits of saturation should be expressed in voltage, to avoid confusion with gain and offset, especially temporarily adjusted offset    
    double _Gain; //TODO: units !        
};

#endif	/* SENSOR_HPP */

