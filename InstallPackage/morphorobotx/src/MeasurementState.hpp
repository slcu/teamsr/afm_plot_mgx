/* 
 * File:   MeasurementState.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 3:54 PM
 */

#ifndef MEASUREMENTSTATE_HPP
#define	MEASUREMENTSTATE_HPP

#include <QString>
#include <QStringList>
#include "Timer.hpp"

class MeasurementState {
   
/*    const double& operator[](int key)
    {
        return Get(key);
    }*/
    
    
    
    ///TODO: consider, maybe I need copy constructor to create past states
    //MeasurementState(const MeasurementState& orig);
   
    
    public:        
        ///TODO: remove domain knowledge from the state, put in in the protocol itself. The protocol is going to register fields through state manager or something like that.
        const double double_nan = std::numeric_limits<double>::quiet_NaN();   
        double operator[](const int key) const
        {
            return Get(key);
        }
        
        explicit MeasurementState();        
        virtual ~MeasurementState();
        //bool Init();
        QStringList GetHeaders() const; ///returns names and units
        bool DumpCurrentValues(QStringList& stringList); ///gets a list of values converted to text
        double Get(int key) const
        {
            return values.at(key);
        }
        //double GetForce();
        //int GetRobotPositionMicrosteps();
        //bool UpdatePosition(int position, double timeBeginMs, double timeEndMs);
        bool Update(int key, double value);
        void Invalidate(const int key)
        {
            values.at(key) = double_nan;
        }
        bool Volatile(int key) const { return vol.at(key); }
        //void UpdateTimeEnd();
        //void UpdateTimeBegin();
        int RegisterField(QString name, QString unit, bool vol = false);
        int IndexOfField(QString name);
        
        //bool InvalidateAll();
        //bool UpdateAll();
        void Renew(); // to remove
        
    protected:
       
        bool Update(QString name, double value);
        std::vector<double> values;
        std::vector<QString> names;
        std::vector<QString> units;
        std::vector<bool> vol;

};

#endif	/* MEASUREMENTSTATE_HPP */

