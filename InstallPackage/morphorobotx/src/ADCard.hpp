/* 
 * File:   ADCard.hpp
 * Author: huflejt
 *
 * Created on November 6, 2012, 6:38 PM
 */

#ifndef ADCARD_HPP
#define	ADCARD_HPP

#include <comedilib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>
#include "QString"

typedef struct
{
    char *filename;
    double value;
    int subdevice;
    int channel;
    int aref;
    int range;
    int physical;
    int verbose;
    int n_chan;
    int n_scan;
    double freq;
} parsed_options; //TODO: try to get rid of this, as far I see, it's only used in the examples for parsing options, I feel it's not requried by the library

typedef enum
{
    FORCE_ACQUISITION_MODE_NONE,       //nothing valid, initial state
    FORCE_ACQUISITION_MODE_BURST,      //finite number of samples at specified frequency         
    FORCE_ACQUISITION_MODE_CONTINUOUS  //infinite number of samples at specified frequency, cancel later using comedi_cancel()      
} FORCE_ACQUISITION_MODE; // ugly name !

//static char * const default_filename = "/dev/comedi0"; ///TODO: this is going to be configurable from the robotRecipe.ini

#define     N_CHANS 16
#define     BUFSZ 100000

class ADCard {
    
public:
    
    ADCard();   
    virtual ~ADCard();
    //bool Init(FORCE_ACQUISITION_MODE mode, int integrationTimeMs); //TODO: allow to change the default file name of the device
    bool Init(const QString devicePath, FORCE_ACQUISITION_MODE mode, int integrationTimeMs, int samplingFrequency);
    bool InitMultichannel(const QString devicePath, FORCE_ACQUISITION_MODE mode, int integrationTimeMs, unsigned int samplingFrequency, int* chanlist, int nchan);
    bool GetVoltage(double& voltage); ///TODO: add a way to get more channels at once, might become useful at some point, this would require to change the Init() as well    
    bool GetVoltageMultichannel(std::vector<double>& voltages);  ///TODO: add a way to get more channels at once, might become useful at some point, this would require to change the Init() as well    
    bool SetOutputVoltage(double& voltage); ///TODO: implement, I forgot to add it for TEF, because it was not needed, but for CFM it's necessary to power the sensor
    
protected:
     const double double_nan = std::numeric_limits<double>::quiet_NaN();   
    comedi_t *dev;
    comedi_cmd  command;
    comedi_cmd *cmd;
    parsed_options options;
    comedi_polynomial_t polynomial_converter;
    unsigned int chanlist[N_CHANS];
    int n_chan; //the examples are not consistent if it should be signed or unsigned
    comedi_range * range_info[N_CHANS];
    lsampl_t maxdata[N_CHANS];
    FORCE_ACQUISITION_MODE forceAcquisitionMode;
    char buf[BUFSZ];
    
    bool PrepareCmdLib(comedi_t *dev, int subdevice, int n_scan, unsigned scan_period_nanosec, comedi_cmd *cmd, FORCE_ACQUISITION_MODE mode);
    void init_parsed_options(parsed_options *options);
    char* cmd_src(int src, char *buf);
    void dump_cmd(FILE *out,comedi_cmd *cmd);

};

#endif	/* ADCARD_HPP */

