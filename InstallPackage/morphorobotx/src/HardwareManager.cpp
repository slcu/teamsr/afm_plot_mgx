/* 
 * File:   HardwareManager.cpp
 * Author: cfmadmin
 * 
 * Created on February 1, 2013, 7:14 PM
 */

#include "HardwareManager.hpp"
#include "Experiment.hpp"
#include "ActuatorSystem.hpp" //TODO: can this one be inherited frmo the other components
#include "SmarActActuatorSystem.hpp"
#include "SmarActHardwareController.hpp"
#include "ComediHardwareController.hpp"
#include "HardwareControllerFactory.hpp"
#include <set>
#include <algorithm>
#include "Robot.hpp"
#include "HardwareController.hpp"
#include "parms.hpp"
#include "Axis.hpp"

HardwareManager::HardwareManager(Experiment& experiment) : _stateManager(experiment.stateManager()) {
}

HardwareManager::~HardwareManager() 
{
    //TODO: go through the vectors and unallocate the elements
    //_actuatorSystems
    for (int i=0; i<_actuatorSystems.size(); i++)
    {
        delete _actuatorSystems.at(i);
    }
}

bool HardwareManager::AcquireHardware()
{
    //TODO: query what hardware is available
    bool result = AcquireSmaractDevices();    
    //result = AcquireZaberDevices();
    return result; //TODO: implement for other devices
}
/*
ActuatorAxis* HardwareManager::GetAxisPointer(unsigned int systemID, unsigned int localAxisID)
{    
    ActuatorSystem* asys=0;
    for (int i=0; i<_actuatorSystems.size(); i++) //find first occurence of matching systemID
    {
        printf("checking.. at %d the SystemID() returns %d\n", i, _actuatorSystems.at(i)->SystemID());
        if (_actuatorSystems.at(i)->SystemID() == systemID)
        {
            asys = _actuatorSystems.at(i);
            printf("found system %d at position %d\n", systemID, i);
        }
    }
    if (asys != 0) // if system found then get the axis pointer
    {   
        printf("calling axis->GetAxisPointer(localAxisID)...\n");
        return asys->GetAxisPointer(localAxisID);
    }
    else
    {
        return 0;
    }
}
*/
bool HardwareManager::AcquireZaberDevices()
{
    //TODO: implement
    return false;
}

bool HardwareManager::AcquireSmaractDevices() //TODO: add parameter holding a list of required systems (first, get them from robot recipe)
{   
    //TODO: remove this method
    return false;
}

bool HardwareManager::BuildRobot(const Parms& parms, Robot& robot)
{   
    HardwareControllerFactory factory;
    bool isOK = true;
    int i=0;
    
    //snippet how to get all keys from a section
    //isOK &= parms.all( QString section, QHash<QString, Container>& values);
    QHash<QString, QStringList> robotAxesHash;
    QHash<QString, QStringList> robotSensorsHash;
    
    isOK &= parms.all("RobotRecipe", robotAxesHash);
    if (!isOK)
    {
        printf("Could not parse RobotRecipe section, something wrong about the axes\n");
        return false;
    }        
    //printf("%s\n", robotSubunitHash.keys().at(i).toLocal8Bit().data()    
    for (i=0; i<robotAxesHash["Axis"].size(); i++)
    {                
        printf("robotAxesHash[Axis].at(%d)=%s\n", i, robotAxesHash["Axis"].at(i).toLocal8Bit().data());    
    }   
    
    isOK &= parms.all("RobotRecipe", robotSensorsHash);
    if (!isOK)
    {
        printf("Could not parse RobotRecipe section, something wrong about the sensors\n");
        return false;
    }        
    for (i=0; i<robotSensorsHash["Sensor"].size(); i++)
    {                
        printf("robotSensorsHash[Sensor].at(%d)=%s\n", i, robotSensorsHash["Sensor"].at(i).toLocal8Bit().data());    
    }   
    
    for (i=0; i<robotAxesHash["Axis"].size(); i++)
    {               
        //TODO: let the controller instantiate a subclass of Axis                        
        printf("getting axis %d from axesHash\n", i);
        printf("its name is: %s\n", robotAxesHash["Axis"].at(i).toLocal8Bit().data());
        QString controllerName;
        isOK &= parms(robotAxesHash["Axis"].at(i), "Controller", controllerName);
        if (!isOK)
        {
            printf("Could not parse section: %s , something wrong about the Controller entry\n", robotAxesHash["Axis"].at(i).toLocal8Bit().data());
            return false;
        }       
        HardwareController* hardwareController = 0;        
        if (!_hardwareControllersHash.contains(controllerName))        
        {
            printf("controller %s does not exist, creating...\n", controllerName.toLocal8Bit().data());            
            hardwareController = factory.hardwareController(parms, controllerName, stateManager());                     
            if (hardwareController == 0)
            {
                //TODO: handle error...
                printf("Error: could not create controller named: %s\n", controllerName.toLocal8Bit().data());
                return false;
            }
            else
            {
                _hardwareControllers.push_back(hardwareController);
                _hardwareControllersHash.insert(controllerName, hardwareController);
            }
        }
        else
        {   //get existing controller from collection
            printf("controller %s exists, getting from hash...\n", controllerName.toLocal8Bit().data());
            hardwareController = _hardwareControllersHash[controllerName];            
        }
        printf("using hardwareController pointer: %d\n", hardwareController);
        if (hardwareController == 0)
        {
            printf("Error: hardwareController is null !\n");
            return false;
        }
        Axis* axis = hardwareController->BuildAxis(parms, robotAxesHash["Axis"].at(i));
        if (axis == 0)
        {
            printf("error creating axis: %s\n", robotAxesHash["Axis"].at(i).toLocal8Bit().data());
            return false;
        }
        robot.AddAxis(axis);
    }
    
    printf("finished adding axes\n");
    printf("adding sensors...\n");
    
    for (i=0; i<robotSensorsHash["Sensor"].size(); i++)
    {                
        printf("adding sensor %s\n", robotSensorsHash["Sensor"].at(i).toLocal8Bit().data());    
        printf("getting sensor %d from sensorsHash\n", i);
        printf("its name is: %s\n", robotSensorsHash["Sensor"].at(i).toLocal8Bit().data());
        QString controllerName;
        QString sensorSectionName;
        isOK &= parms(robotSensorsHash["Sensor"].at(i), "SensorSectionName", sensorSectionName); 
        isOK &= parms(sensorSectionName, "Controller", controllerName);
        if (!isOK)
        {
            printf("Could not parse section: %s , something wrong about the Controller entry\n", sensorSectionName.toLocal8Bit().data());
            return false;
        }       
        HardwareController* hardwareController = 0;        
        if (!_hardwareControllersHash.contains(controllerName))        
        {
            printf("controller %s does not exist, creating...\n", controllerName.toLocal8Bit().data());
            hardwareController = factory.hardwareController(parms, controllerName, stateManager());                     
            if (hardwareController == 0)
            {
                //TODO: handle error...
                printf("Error: could not create controller named: %s\n", controllerName.toLocal8Bit().data());
                return false;
            }
            else
            {
                _hardwareControllers.push_back(hardwareController);
                _hardwareControllersHash.insert(controllerName, hardwareController);
            }
        }
        else
        {
            //get existing controller from collection
            printf("controller %s exists, getting from hash...\n", controllerName.toLocal8Bit().data());
            hardwareController = _hardwareControllersHash[controllerName];
            
        }
        printf("using hardwareController pointer: %d\n", hardwareController);
        if (hardwareController == 0)
        {
            printf("Error: hardwareController is null !\n");
            return false;
        }
        
        Sensor* sensor = hardwareController->BuildSensor(parms, robotSensorsHash["Sensor"].at(i));
        if (sensor == 0)
        {
            printf("error creating sensor: %s\n", robotSensorsHash["Sensor"].at(i).toLocal8Bit().data());
            return false;
        }
        if (!sensor->isInstanceValid())
        {
            printf("error: sensor: %s created, but instance is not valid\n", robotSensorsHash["Sensor"].at(i).toLocal8Bit().data());
            return false;
        }
        robot.AddSensor(sensor);
    }   
    
    /*
    
    for (i=0; i<forceSensorNames.size(); i++) //browse axes to find controllers, add controllers to collection
    {
        QString forceSensorName = forceSensorNames.at(i); 
        QString controllerName;
        isOK &= parms(forceSensorName, "Controller", controllerName);        
        if (!isOK)
        {
            printf("Could not parse RobotRecipe -> ForceSensor -> Controller\n");
            return false;
        }       
        controllerNames.push_back(controllerName); //add to list together with other controllers
    }    
    controllerNames.removeDuplicates(); //remove repeated references to controller sections   
     * */
    /*for (i=0; i<controllerNames.size(); i++) //extract controller types, needed for creation of controller objects
    {
        QString controllerTypeName;
        isOK &= parms(controllerNames[i], "Type", controllerTypeName);        
        if (!isOK)
        {
            printf("Could not parse RobotRecipe -> (...) -> %s -> Type \n", controllerNames[i].toLocal8Bit().data());
            return false;
        }       
        controllerTypeNames.push_back(controllerTypeName);
    } //now, needed controller types are known (controllerTypeNames) and can be created by name in the factory
     */ 
    //controllerTypeNames is not needed by the factory, it can parse the type out of the section name
   // _hardwareControllers.clear();
    /*for (i=0; i<controllerNames.size(); i++)
    {
        HardwareController* hardwareController = factory.hardwareController(parms, controllerNames[i]);
        if (hardwareController == 0)
        {
            printf("could not create hardwareController: %s \n", controllerNames[i].toLocal8Bit().data());
            return false;
        }                    
        _hardwareControllers.push_back(hardwareController);                
    } */          
   /* for (i=0; i<axesNames.size(); i++)
    {   
        QString controllerName;
        /*isOK &= parms(axesNames[i], "Controller", controllerName);
        if (!isOK)
        {
            printf("could not parse axis %s -> Controller\n", axesNames[i].toLocal8Bit().data());
            return false;
        }*/
 //       Axis axis(const Parms& parms, axesNames[i]);
 //   }
    
   // for (i=0; i<_hardwareControllers.size(); i++)
    //{
        //_hardwareSystems[i].Name();
   // }
       
    //printf("HardwareManager::BuildRobot returns false, because it's not fully implemented\n");
    return isOK; //TODO: it returns false until fully implemented, ok?
}
