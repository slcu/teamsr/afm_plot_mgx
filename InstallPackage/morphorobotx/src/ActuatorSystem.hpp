/* 
 * File:   ActuatorSystem.hpp
 * Author: cfmadmin
 *
 * Created on January 31, 2013, 9:16 PM
 */

#ifndef ACTUATORSYSTEM_HPP
#define	ACTUATORSYSTEM_HPP

#include "MCSControl.h"
#include "vector"
#include "stdio.h"
//#include "StateManager.hpp"

class ActuatorAxis;

class ActuatorSystem {
public:
    ActuatorSystem(unsigned int systemID, unsigned int localSystemID);   
    virtual ~ActuatorSystem();
    //virtual bool Goto(int axisID, double positionMicrons) = 0;
    unsigned int nchannels; //TODO: looks ugly
    bool AddAxis(ActuatorAxis* axisptr)
    {
        actuatorAxes.push_back(axisptr);
        return false;
    }
    unsigned int SystemID() ///in case of smaract: serial number of smaract controller (green box)
    {
        return _systemID;
    }
    unsigned int LocalSystemID() ///system id numbering devices of the same kind within the driver, in case of smaract, 0,1,2... 
    {
        return _localSystemID;
    }
    //TODO: should there be SystemType too ? what if two different robots have the same serial number, or have no serial ?
    ActuatorAxis* GetAxisPointer(unsigned int localAxisID);
 /*   StateManager& stateManager()
    {
        return _stateManager;
    }*/
protected:
    //TODO: implement axes
    std::vector <ActuatorAxis*> actuatorAxes;
    //SA_INDEX _systemIndex; //type too specific, meaning maybe ok
    //String systemID;
    unsigned int _systemID;
    unsigned int _localSystemID;
    //StateManager& _stateManager;
    

};

#endif	/* ACTUATORSYSTEM_HPP */

