/* 
 * File:   StateManager.hpp
 * Author: cfmadmin
 *
 * Created on December 11, 2012, 6:02 PM
 */

#ifndef STATEMANAGER_HPP
#define	STATEMANAGER_HPP

#include "MeasurementState.hpp"
#include "DataLogger.hpp"
#include <QString>
//#include "Experiment.hpp"

//class Experiment; //forward declaration to avoid a loop

class StateManager {
public:
    StateManager();
    /*StateManager(const StateManager& orig);*/
    virtual ~StateManager();
    
    MeasurementState& state()
    {
        return _state;
    }
    
    DataLogger& logger()
    {
        return _logger;
    }
        
    void Renew()
    {
        state().Renew();
    }    
 
    ///TODO: call RegisterField and throw an exception on error
    int RegisterFieldExc(QString name, QString unit, bool vol = false)
    {
        //TODO: if the header of the log has been already written, it should be forbidden to register further fields
        int retval = RegisterField(name, unit, vol);
        if (retval < 0)
        {
            /// TODO: throw an exception here
        	return -1;
        }
        else
        {
            return retval;
        }
    }
         
    int indexOfField(QString name);    
    
protected:
    
    ///TODO: return the key with -1 if error
    int RegisterField(QString name, QString unit, bool vol)
    {
        //for now, the registration happens inside of the state
        return state().RegisterField(name, unit, vol);
    }    
    MeasurementState _state; 
    DataLogger _logger;     
    //Experiment& _experiment;    
    //QMap<QString, int> _map;    

};

#endif	/* STATEMANAGER_HPP */

