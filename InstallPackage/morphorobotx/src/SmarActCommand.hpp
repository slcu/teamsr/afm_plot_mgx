/* 
 * File:   SmarActCommand.hpp
 * Author: cfmadmin
 *
 * Created on February 12, 2013, 6:07 PM
 */

#ifndef SMARACTCOMMAND_HPP
#define	SMARACTCOMMAND_HPP

#include "MCSControl.h"
#include "QString"
#include "SmarActResult.hpp"

class SmarActCommand {
public:
    
    SmarActCommand();
    
    virtual ~SmarActCommand();
    SA_STATUS Status() 
    {
        return _result.Status();
    }
    QString GetStatusDescription() 
    {                
        return _result.GetDescription(); //equivalent to  //return _result.GetStatusDescription(_result.Status());
    }
    double ResultValue() //TODO: maybe call it MeasuredValue() ?
    {
        return _result.Value();
    }
    bool Failed()
    {
        return (_result.Status() != SA_OK);
    }
    bool RetryOnceAllowed()
    {
        return true; //TODO: actually, it should depend on command and error code
    }
    
protected:
    SmarActResult _result; 
};

#endif	/* SMARACTCOMMAND_HPP */

