/* 
 * File:   ZaberAxis.cpp
 * Author: cfmadmin
 * 
 * Created on March 5, 2013, 11:55 AM
 */

#include "ZaberAxis.hpp"
#include "ZaberHardwareController.hpp"

ZaberAxis::ZaberAxis(const Parms& parms, QString axisName, ZaberHardwareController* hardwareController): Axis(hardwareController)
{
    //TODO: what if it fails? The same problem as with constructor of SmarActAxis
      printf("called contructor ZaberAxis::ZaberAxis\n");
    printf(" - axisName: %s\n", axisName.toLocal8Bit().data());
    printf(" - hardwareControllerName: %s\n", hardwareController->Name().toLocal8Bit().data());
    _name = axisName;
    _controllerName = hardwareController->Name();    
    //_itsController = hardwareController;
//    bool isOK = parms(axisName, "LocalAxisID", _LocalAxisID); ///TODO: what if there is error ? Make a zombie object with invalid flag ?
  //  printf(" - LocalAxisID: %d\n", _LocalAxisID );

}


ZaberAxis::~ZaberAxis() {
}

bool ZaberAxis::Set(unsigned int option, double value)
{
    return false;
}

bool ZaberAxis::Get(unsigned int option, double& value)
{
    return false;
}

bool ZaberAxis::Initialize()
{
    printf("Inside ZaberAxis::Initialize()\n");
    //TODO: implement
    return false;
}

bool ZaberAxis::GotoSync(double positionMicrons)
{
    //TODO: implement
    return false;
}
bool ZaberAxis::GetPositionSync(double& positionMicrons)
{
    //TODO: implement
    return false;
}