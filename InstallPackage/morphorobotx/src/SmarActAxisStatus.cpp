/* 
 * File:   SmarActAxisStatus.cpp
 * Author: cfmadmin
 * 
 * Created on February 13, 2013, 3:15 PM
 */

#include "SmarActAxisStatus.hpp"

SmarActAxisStatus::SmarActAxisStatus() {
}

/*SmarActAxisStatus::SmarActAxisStatus(const SmarActAxisStatus& orig) {
}*/

SmarActAxisStatus::~SmarActAxisStatus() {
}

QMap<unsigned int, QString> SmarActAxisStatus::StatusMapDescription()
{
    QMap<SA_STATUS, QString> StatusMap;
    StatusMap.clear();
    StatusMap.insert(SA_STOPPED_STATUS, "The positioner or end effector is currently not performing active movement.");
    StatusMap.insert(SA_STEPPING_STATUS, "The positioner is currently performing a stepping movement.");    
    StatusMap.insert(SA_SCANNING_STATUS, "The positioner is currently performing a scanning movement.");
    StatusMap.insert(SA_HOLDING_STATUS, "The positioner or end effector is holding its current target (see closed-loop commands, e.g. SA_GotoPositionAbsolute_S, SA_GotoAngleAbsolute_S, SA_GotoGripperForceAbsolute_S) or is holding the reference mark (see SA_FindReferenceMark_S).");
    StatusMap.insert(SA_TARGET_STATUS, "The positioner or end effector is currently performing a closed-loop movement.");
    StatusMap.insert(SA_MOVE_DELAY_STATUS, "The positioner is currently waiting for the sensors to power up before executing the movement command. This status may be returned if the the sensors are operated in power save mode.");
    StatusMap.insert(SA_CALIBRATING_STATUS, "The positioner or end effector is busy calibrating its sensor.");
    StatusMap.insert(SA_FINDING_REF_STATUS, "The positioner is moving to find the reference mark.");
    StatusMap.insert(SA_OPENING_STATUS, "The end effector (gripper) is closing or opening its jaws.");
    return StatusMap;
}

QMap<unsigned int, QString> SmarActAxisStatus::StatusMapName()
{
    QMap<SA_STATUS, QString> StatusMap;
    StatusMap.clear();
    StatusMap.insert(SA_STOPPED_STATUS, "SA_STOPPED_STATUS");
    StatusMap.insert(SA_STEPPING_STATUS, "SA_STEPPING_STATUS");    
    StatusMap.insert(SA_SCANNING_STATUS, "SA_SCANNING_STATUS");
    StatusMap.insert(SA_HOLDING_STATUS, "SA_HOLDING_STATUS");
    StatusMap.insert(SA_TARGET_STATUS, "SA_TARGET_STATUS");
    StatusMap.insert(SA_MOVE_DELAY_STATUS, "SA_MOVE_DELAY_STATUS");
    StatusMap.insert(SA_CALIBRATING_STATUS, "SA_CALIBRATING_STATUS");
    StatusMap.insert(SA_FINDING_REF_STATUS, "SA_FINDING_REF_STATUS");
    StatusMap.insert(SA_OPENING_STATUS, "SA_OPENING_STATUS");
    return StatusMap;
}

unsigned int SmarActAxisStatus::Status()
{
    return _status;
}

QString SmarActAxisStatus::GetDescription()
{
    return StatusMapDescription().value(_status);
}

QString SmarActAxisStatus::GetName()
{
    return StatusMapName().value(_status);
}
