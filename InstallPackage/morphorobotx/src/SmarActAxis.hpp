/* 
 * File:   SmarActAxis.hpp
 * Author: cfmadmin
 *
 * Created on March 1, 2013, 7:44 PM
 */

#ifndef SMARACTAXIS_HPP
#define	SMARACTAXIS_HPP

#include "Axis.hpp"

class SmarActHardwareController;
class Parms;

class SmarActAxis :public Axis{
public:
    SmarActAxis(const Parms& parms, QString axisName, SmarActHardwareController* hardwareController); ///TODO: is parms needed ?
    virtual ~SmarActAxis();    
    bool Set(unsigned int option, double value);
    bool Get(unsigned int option, double& value);
    bool Initialize();    
    bool GotoAsync(double positionMicrons);
    bool GotoSync(double positionMicrons);
    bool GetPositionSync(double& positionMicrons);
    bool GetStatusSync(unsigned int& status);
    bool StopSync();
    ///TODO: bool RegisterFields(); ///overrides bool RobotSubunit::RegisterFields()
    bool isSensorMissing()
    {
        //TODO: actually it should evaluate fresh _sensorType and _channelType values to say if it's ok
        return _isSensorMissing;
    }
protected:
    bool SetOption(AxisOption option, double value);
    void SetHoldMs(unsigned int holdMsValue);
    void SetMarginMicrons(double marginMicrons);
    bool SetClosedLoopMaxFrequency(double frequencyHz);
    bool SetClosedLoopMoveSpeed(double speedMicronsPerSecond);   
    bool SetZeroPosition();
    //TODO: move the ID to the base class 
    unsigned int _LocalAxisID;///id within the controller, used for addressing comnmands
    unsigned int _SystemID; ///id of the controller within the library, only valid after successful initialization
    double _marginMicrons; //TODO: set a default value in the constructor
    unsigned int _holdMs;        
    int _fieldIdPositionMicrons;    
    unsigned int _sensorType;
    unsigned int _channelType;
    bool _isSensorMissing;
};

#endif	/* SMARACTAXIS_HPP */

