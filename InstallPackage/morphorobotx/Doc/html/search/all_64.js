var searchData=
[
  ['datalogger',['DataLogger',['../classDataLogger.html',1,'DataLogger'],['../classDataLogger.html#a5e834d2c5a1a2db5c7764ce1c3742367',1,'DataLogger::DataLogger()']]],
  ['datalogger_2ecpp',['DataLogger.cpp',['../DataLogger_8cpp.html',1,'']]],
  ['datalogger_2ehpp',['DataLogger.hpp',['../DataLogger_8hpp.html',1,'']]],
  ['debug_5finformation',['DEBUG_INFORMATION',['../classParms.html#a80a81d2533d9b325b34ebf603acb6f50a7b04eefc246a0dd00cc201737d6cadb6',1,'Parms']]],
  ['default_5ffilename',['default_filename',['../ADCard_8hpp.html#af3cceebb86449115403b00b0cd987b26',1,'ADCard.hpp']]],
  ['dev',['dev',['../classADCard.html#a96d7db57d73224bc58d70c5452498629',1,'ADCard']]],
  ['double_5fnan',['double_nan',['../classMeasurementState.html#af58e992abd4c0ee86d5869007825c336',1,'MeasurementState']]],
  ['dump_5fcmd',['dump_cmd',['../classADCard.html#ae42b941cff6846a66cbdcdf0c1d5d499',1,'ADCard']]],
  ['dumpcurrentvalues',['DumpCurrentValues',['../classMeasurementState.html#ae8c2f2a07574b75755c7eccb8667b6fb',1,'MeasurementState']]]
];
